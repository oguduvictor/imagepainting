﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Web.Helpers
{
    public interface IViewHelper
    {
        IEnumerable<SelectListItem> GetBuilderOptions(List<int> selectedBuilderIds, bool includeBlankAndNewOption = false);

        IEnumerable<SelectListItem> GetCommunityOptions(int selectedCommunityId, bool includeNewOption = false, long? builderId = null, bool includeBlankOption = true);

        IEnumerable<SelectListItem> GetCostItemOptions(int builderId, int selectedCostId, bool includeBlankAndNewOption = false);

        IEnumerable<SelectListItem> GetCustomerRepOptions();

        IEnumerable<SelectListItem> GetElevationOptions(int selectedElevation, bool includeBlankAndNewOption = false, int? planId = null);

        IEnumerable<SelectListItem> GetExteriorColorScheme(int id, string colorCodePrefix, bool includeNewOption = false, bool includeBlankOption = true);
        
        IEnumerable<SelectListItem> GetInteriorColorScheme(int id, string colorCodePrefix, bool includeNewOption = false, bool includeBlankOption = true);
        
        IEnumerable<SelectListItem> GetLotBlockOptions(int? communityId = null);

        IEnumerable<SelectListItem> GetPlansOptions(int selectedPlan, bool includeNewOption = false, int? builderId = null, bool includeBlankOption = false);

        IEnumerable<SelectListItem> GetSingleBuilderOptions(int selectedBuilder = 0);

        IEnumerable<SelectListItem> GetStateOptions(string selectedStateCode = "FL");

        IEnumerable<SelectListItem> GetStoreLocationsOption(long? selected = null, long? storeId = null, bool includeDeleted = false);

        IEnumerable<SelectListItem> GetStoreOptions(long? selected = null, bool includeDeleted = false);

        IEnumerable<SelectListItem> GetSuperintendentOptions(bool excludeNoSuperintendent = false);

        IEnumerable<Supply> GetSuppliesByBuilder(int builderId, long storeId);

        IEnumerable<SelectListItem> GetSupplyTypeOptions(SupplyCategory category = SupplyCategory.All, int? selected = null);

        IEnumerable<SelectListItem> GetSupplyTypeOptionsAsText(TaskType taskType);

        IEnumerable<SupplyTypeMap> GetSupplyTypes(TaskType taskType, long workOrderId, int builderId, long storeId);
        
        IEnumerable<SelectListItem> GetTaskSchemasOptions(int? selected, bool includeBlankOption = true, bool includeAllTasksOption = false, bool excludeMainTask = true, bool includeMainTaskOption = false);

        string GetUserFirstName();

        IEnumerable<SelectListItem> GetUsersOptions(string selectedUser = "", bool includeInactive = false, bool includeDeleted = false, bool includeBlankOption = true, bool includeUnassigned = false);

        IEnumerable<SelectListItem> GetWorkOrderColorsAsText(long workOrderId, TaskType taskType);
        
        IEnumerable<SelectListItem> ScheduleTaskListToTaskOptions(IEnumerable<int> schedulesToExclude);
        
    }
}