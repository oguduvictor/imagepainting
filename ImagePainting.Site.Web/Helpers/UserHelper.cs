﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Web.App_Start;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImagePainting.Site.Web.Helpers
{
    public class UserHelper : IUserHelper
    {
        private readonly IUserContext _userContext;

        public UserHelper(IUserContext userContext)
        {
            _userContext = userContext;
        }

        public bool IsUserAdmin()
        {
            if (!IsAuthenticated)
            {
                return false;
            }

            var userId = _userContext.UserId;

            return _userContext.IsInRole(userId, UserRole.Administrator);
        }

        public bool IsAdminOrEmployee()
        {
            if (!IsAuthenticated)
            {
                return false;
            }

            var userId = _userContext.UserId;
            var roles = _userContext.GetRoles(userId);

            return roles.Any(role => role.IsAnyOf(UserRole.Administrator, UserRole.Employee));
        }

        public bool IsStaff()
        {
            if (!IsAuthenticated)
            {
                return false;
            }

            var userId = _userContext.UserId;
            var roles = _userContext.GetRoles(userId);
            return roles.Any(x => x.IsAnyOf(UserRole.Accountant, 
                                            UserRole.Administrator, 
                                            UserRole.SubContractor, 
                                            UserRole.Employee));
        }

        public static bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        public IEnumerable<UserRole> UserRoles
        {
            get
            {
                var userId = _userContext.UserId;
                var roles = _userContext.GetRoles(userId);

                return roles;
            }
        }

        public string UserFirstName => _userContext.FirstName;
    }
}