﻿using System.Collections.Generic;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Web.Helpers
{
    public interface IUserHelper
    {
        IEnumerable<UserRole> UserRoles { get; }

        bool IsAdminOrEmployee();

        bool IsStaff();

        bool IsUserAdmin();
    }
}