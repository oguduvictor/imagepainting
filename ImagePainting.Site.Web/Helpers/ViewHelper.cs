﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Web.App_Start;
using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Helpers
{
    public class ViewHelper: IViewHelper
    {
        private readonly IServiceFactory _serviceFactory;

        public ViewHelper(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
        }

        public  IEnumerable<SelectListItem> GetStateOptions(string selectedStateCode = "FL")
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetStates();

            task.Wait();

            return task.Result.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Code,
                Selected = x.Code == selectedStateCode
            });
        }

        public static  string GetTime(DateTime dateTime)
        {
            var hour = dateTime.Hour.ToString().PadLeft(2, '0');
            var mins = dateTime.Minute.ToString().PadLeft(2, '0');
            var secs = dateTime.Second.ToString().PadLeft(2, '0');

            return $"{hour}:{mins}:{secs}";
        }

        public  IEnumerable<SelectListItem> GetCommunityOptions(int selectedCommunityId,
            bool includeNewOption = false,
            long? builderId = null,
            bool includeBlankOption = true
            )
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetCommunitiesSummary(builderId);
            var communitiesSelect = new List<SelectListItem>();

            if (includeBlankOption)
            {
                communitiesSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            if (includeNewOption)
            {
                communitiesSelect.Add(new SelectListItem { Text = "Create New Community", Value = "0" });
            }

            task.Wait();

            communitiesSelect.AddRange(
                task.Result.Select(x => new SelectListItem
                {
                    Text = $"{x.Name} ({x.Abbreviation})",
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedCommunityId
                }).OrderBy(x => x.Text)
             );

            return communitiesSelect;
        }

        public  IEnumerable<SelectListItem> GetSuperintendentOptions(bool excludeNoSuperintendent = false)
        {
            var workOrderService = GetWorkOrderComposerService();

            var superintendentSelect = new List<SelectListItem> { new SelectListItem { Text = string.Empty, Value = string.Empty } };

            var task = workOrderService.GetSuperintendents();
            task.Wait();

            if (excludeNoSuperintendent)
            {
                task.Result.Remove(SiteContants.NO_SUPERINTENDENT);
            }

            task.Result.ForEach(x =>
            {
                superintendentSelect.Add(new SelectListItem { Text = x, Value = x });
            });

            return superintendentSelect;
        }

        public  IEnumerable<SelectListItem> GetCustomerRepOptions()
        {
            var workOrderService = GetWorkOrderComposerService();

            var customerRepSelect = new List<SelectListItem> { new SelectListItem { Text = string.Empty, Value = string.Empty } };

            var task = workOrderService.GetCustomerReps();
            task.Wait();

            task.Result.ForEach(x =>
            {
                customerRepSelect.Add(new SelectListItem { Text = x, Value = x });
            });

            return customerRepSelect;
        }

        public  IEnumerable<SelectListItem> GetBuilderOptions(List<int> selectedBuilderIds, bool includeBlankAndNewOption = false)
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetBuildersSummary();
            var buildersSelect = new List<SelectListItem>();

            if (includeBlankAndNewOption)
            {
                buildersSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
                buildersSelect.Add(new SelectListItem { Text = "Create New Builder", Value = "0" });
            }

            task.Wait();

            buildersSelect.AddRange(
                task.Result.Select(x => new SelectListItem
                {
                    Text = $"{x.Name} ({x.Abbreviation})",
                    Value = x.Id.ToString(),
                    Selected = selectedBuilderIds.Contains(x.Id)
                }).OrderBy(x => x.Text)
            );

            return buildersSelect;
        }

        public  IEnumerable<SelectListItem> GetCostItemOptions(int builderId, int selectedCostId, bool includeBlankAndNewOption = false)
        {
            var composerService = GetCommunityComposerService();
            var costItemsTask = composerService.GetCostItemsNotInBuilder(builderId);
            var costItemsSelect = new List<SelectListItem>();

            if (includeBlankAndNewOption)
            {
                costItemsSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
                costItemsSelect.Add(new SelectListItem { Text = "Create New Cost Item", Value = "0" });
            }

            costItemsTask.Wait();

            costItemsSelect.AddRange(
                costItemsTask.Result.Select(x => new SelectListItem
                {
                    Text = x.Name.ToString(),
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedCostId
                }));

            return costItemsSelect;
        }

        public  IEnumerable<SelectListItem> GetSingleBuilderOptions(int selectedBuilder = 0)
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetBuildersSummary();
            var builderSelect = new List<SelectListItem>();

            task.Wait();

            builderSelect.Add(new SelectListItem { Text = string.Empty, Value = null });
            builderSelect.AddRange(
                task.Result.Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedBuilder
                }));

            return builderSelect;
        }

        public  IEnumerable<SelectListItem> GetPlansOptions(int selectedPlan, bool includeNewOption = false, int? builderId = null, bool includeBlankOption = false)
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetPlansSummary(builderId);

            var plansSelect = new List<SelectListItem>();

            if (includeBlankOption)
            {
                plansSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            task.Wait();

            if (includeNewOption)
            {
                plansSelect.Add(new SelectListItem { Text = "Create New Plan", Value = "0" });
            }

            plansSelect.AddRange(
                task.Result.OrderBy(p => p.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedPlan
                }));

            return plansSelect;
        }

        public  IEnumerable<SelectListItem> GetElevationOptions(int selectedElevation, bool includeBlankAndNewOption = false, int? planId = null)
        {
            var composerService = GetCommunityComposerService();
            var task = composerService.GetElevationsSummary(planId);
            var elevationsSelect = new List<SelectListItem>();

            task.Wait();

            if (includeBlankAndNewOption)
            {
                elevationsSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
                elevationsSelect.Add(new SelectListItem { Text = "Create New Elevation", Value = "0" });
            }

            elevationsSelect.AddRange(
                task.Result.OrderBy(e => e.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedElevation
                }));

            return elevationsSelect;
        }

        public static IEnumerable<SelectListItem> GetItemDataType(TaskDataType item)
        {
            var datatypeSelect = new List<SelectListItem>
            {
                new SelectListItem { Text = "Yes/No", Value = "YesNo", Selected = item.ToString() == "YesNo" },
                new SelectListItem { Text = "Number", Value = "Number", Selected = item.ToString() == "Number" },
                new SelectListItem { Text = "Text", Value = "Text", Selected = item.ToString() == "Text" },
                new SelectListItem { Text = "Note", Value = "Note", Selected = item.ToString() == "Note" }
            };

            return datatypeSelect;
        }

        public  IEnumerable<SelectListItem> GetUsersOptions(
            string selectedUser = "",
            bool includeInactive = false,
            bool includeDeleted = false,
            bool includeBlankOption = true,
            bool includeUnassigned = false)
        {
            var service = GetUserInfoService();
            var task = service.GetUsersSummary(includeDeleted: includeDeleted, includeInActive: includeInactive);

            task.Wait();

            var usersSelect = new List<SelectListItem>();

            if (includeBlankOption)
            {
                usersSelect.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            if (includeUnassigned)
            {
                usersSelect.Add(new SelectListItem { Text = "Unassigned", Value = "0" });
            }

            usersSelect.AddRange(
                task.Result.Select(x => new SelectListItem
                {
                    Text = x.FullName,
                    Value = x.Id.ToString(),
                    Selected = x.Id == selectedUser
                })
            );

            return usersSelect;
        }

        public static  IEnumerable<SelectListItem> GetYesNoOptions(bool selected)
        {
            return new List<SelectListItem>
                {
                    new SelectListItem {Text = "Yes", Value = "true", Selected = selected },
                    new SelectListItem {Text = "No", Value = "false", Selected = !selected }
                };
        }

        public  IEnumerable<SelectListItem> GetExteriorColorScheme(int id, string colorCodePrefix, bool includeNewOption = false, bool includeBlankOption = true)
        {
            var service = GetCommunityComposerService();
            var exteriorColorSchemesTask = service.GetExeteriorColorSchemeSummary(colorCodePrefix);
            exteriorColorSchemesTask.Wait();

            var exteriorColorSchemesOptions = new List<SelectListItem>();

            if (includeBlankOption)
            {
                exteriorColorSchemesOptions.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            if (includeNewOption)
            {
                exteriorColorSchemesOptions.Add(new SelectListItem { Text = "Create New Color Scheme", Value = "new" });
            }

            exteriorColorSchemesOptions.AddRange(
                exteriorColorSchemesTask.Result.Select(x => new SelectListItem
                {

                    Text = x.ColorCode,
                    Value = x.Id.ToString(),
                    Selected = x.Id == id
                }));

            return exteriorColorSchemesOptions;
        }

        public  IEnumerable<SelectListItem> GetInteriorColorScheme(int id, string colorCodePrefix, bool includeNewOption = false, bool includeBlankOption = true)
        {
            var service = GetCommunityComposerService();
            var interiorColorSchemesTask = service.GetInteriorColorSchemeSummary(colorCodePrefix);
            interiorColorSchemesTask.Wait();
            var interiorColorSchemesOptions = new List<SelectListItem>();

            if (includeBlankOption)
            {
                interiorColorSchemesOptions.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            if (includeNewOption)
            {
                interiorColorSchemesOptions.Add(new SelectListItem { Text = "Create New Color Scheme", Value = "new" });
            }

            interiorColorSchemesOptions
                .AddRange(interiorColorSchemesTask
                .Result.Select(x => new SelectListItem
                {
                    Text = x.ColorCode,
                    Value = x.Id.ToString(),
                    Selected = x.Id == id
                }));

            return interiorColorSchemesOptions;
        }

        public  IEnumerable<SelectListItem> GetTaskSchemasOptions(
            int? selected,
            bool includeBlankOption = true,
            bool includeAllTasksOption = false,
            bool excludeMainTask = true,
            bool includeMainTaskOption = false)
        {
            var service = GetTaskService();
            var schemasTask = service.GetTaskSchemas(true);
            var options = new List<SelectListItem>();

            if (includeBlankOption)
            {
                options.Add(new SelectListItem { Text = string.Empty, Value = string.Empty });
            }

            if (includeAllTasksOption)
            {
                options.Add(new SelectListItem { Text = "All Tasks", Value = string.Empty, Selected = true });
            }

            schemasTask.Wait();


            if (includeMainTaskOption)
            {
                var mainTaskIds = schemasTask.Result.Where(x => x.IsMainTask).Select(x => x.Id).ToJson();
                options.Add(new SelectListItem { Text = "All Main Tasks", Value = mainTaskIds });
            }

            foreach (var schema in schemasTask.Result)
            {
                if (excludeMainTask)
                {
                    if (schema.IsMainTask) continue;
                }

                options.Add(new SelectListItem
                {
                    Text = schema.Name,
                    Value = schema.Id.ToString(),
                    Selected = selected == schema.Id
                });
            }

            return options;
        }

        public  IEnumerable<SelectListItem> GetStoreOptions(long? selected = null, bool includeDeleted = false)
        {
            var service = GetStoreService();
            var storesTask = service.GetStoreSummary(includeDeleted);
            var options = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty }
            };

            storesTask.Wait();

            storesTask.Result.ForEach(store =>
            {
                options.Add(new SelectListItem
                {
                    Text = store.StoreName,
                    Value = store.Id.ToString(),
                    Selected = store.Id == selected
                });
            });

            return options;
        }

        public  IEnumerable<SelectListItem> GetStoreLocationsOption(long? selected = null, long? storeId = null, bool includeDeleted = false)
        {
            var service = GetStoreService();
            var storeLocationsTask = service.GetStoreLocations(storeId, includeDeleted);
            var options = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty }
            };

            storeLocationsTask.Wait();

            storeLocationsTask.Result.ForEach(location =>
            {
                var address = $"{location.StoreAddress}, {location.StoreCity}";
                var text = location.LocationId.IsNullOrEmpty() ? address : location.LocationId;

                options.Add(new SelectListItem
                {
                    Text = text,
                    Value = location.Id.ToString(),
                    Selected = storeLocationsTask.Result.Count() == 1 ? true : (location.Id == selected)
                });
            });

            return options;
        }

        public static IEnumerable<SelectListItem> GetTaskTypeOptions(TaskType? selected = null)
        {
            var options = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = string.Empty } };
            var taskTypes = Enum.GetNames(typeof(TaskType));

            for (var i = 1; i < taskTypes.Length; i++)
            {
                var taskType = taskTypes[i];

                options.Add(new SelectListItem
                {
                    Value = $"{(int)Enum.GetValues(typeof(TaskType)).GetValue(i)}",
                    Text = taskType.ToString().BreakupText(),
                    Selected = selected.HasValue && taskType == selected.ToString()
                });
            };

            return options;
        }

        public  IEnumerable<Supply> GetSuppliesByBuilder(int builderId, long storeId)
        {
            if (builderId == default(int) || storeId == default(long))
            {
                return new List<Supply>();
            }

            var service = GetCommunityComposerService();
            var task = service.GetBuilderSupplies(builderId, storeId);
            task.Wait();

            var supplyList = task.Result;

            return supplyList;
        }

        public  IEnumerable<SelectListItem> GetSupplyTypeOptionsAsText(TaskType taskType)
        {
            SupplyCategory category = (taskType == TaskType.Interior || taskType == TaskType.Exterior) ?
                (SupplyCategory)taskType : SupplyCategory.All;

            var service = GetCommunityComposerService();
            var task = service.GetSupplyTypes(category);

            var options = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = string.Empty } };

            task.Wait();

            task.Result.ForEach(supplyType =>
            {
                var pretext = supplyType.SupplyCategory == SupplyCategory.Exterior ? "Ext." : "Int.";
                options.Add(new SelectListItem
                {
                    Text = $"{pretext} {supplyType.Name}",
                    Value = $"{pretext} {supplyType.Name}",
                    Selected = false
                });
            });
            return options;
        }

        public  IEnumerable<SupplyTypeMap> GetSupplyTypes(TaskType taskType, long workOrderId, int builderId, long storeId)
        {
            SupplyCategory category = (taskType == TaskType.Interior || taskType == TaskType.Exterior) ?
                (SupplyCategory)taskType : SupplyCategory.All;

            var service = GetCommunityComposerService();
            var woService = GetWorkOrderComposerService();

            var task = service.GetBuilderSupplies(builderId, storeId);
            task.Wait();

            var task3 = service.GetSupplyTypes(category);
            task3.Wait();

            var task2 = woService.GetJobInformation(workOrderId);
            task2.Wait();

            var jobInfo = task2.Result;
            var supplyTypes = task3.Result;
            var builderSupplies = task.Result;

            var merged = MapSupplyTypeToColors(jobInfo, builderSupplies, supplyTypes, category);

            return merged;
        }

        public  IEnumerable<SelectListItem> GetWorkOrderColorsAsText(long workOrderId,
            TaskType taskType)
        {
            SupplyCategory category = (taskType == TaskType.Interior || taskType == TaskType.Exterior) ?
                (SupplyCategory)taskType : SupplyCategory.All;

            var service = GetWorkOrderComposerService();
            var colors = service.GetColorSchemes(workOrderId, category).Result;

            var options = new List<SelectListItem> { new SelectListItem { Value = string.Empty, Text = string.Empty } };

            //task.Wait();

            colors.ForEach(color =>
            {
                options.Add(new SelectListItem
                {
                    Text = color,
                    Value = color,
                    Selected = false
                });
            });

            return options;
        }

        public  IEnumerable<SelectListItem> GetSupplyTypeOptions(SupplyCategory category = SupplyCategory.All, int? selected = null)
        {
            var service = GetCommunityComposerService();
            var task = service.GetSupplyTypes(category);

            var options = new List<SelectListItem> { new SelectListItem { Value = "0", Text = string.Empty } };

            task.Wait();

            task.Result.ForEach(supplyType =>
            {
                options.Add(new SelectListItem
                {
                    Text = supplyType.Name,
                    Value = supplyType.Id.ToString(),
                    Selected = supplyType.Id == selected
                });
            });

            return options;
        }

        public static IEnumerable<SelectListItem> GetMonthsOptions()
        {
            var currentMonth = DateTime.UtcNow.Month;
            var options = new List<SelectListItem>
            {
                new SelectListItem { Text = "January", Value = "1", Selected = currentMonth == 1 },
                new SelectListItem { Text = "February", Value = "2", Selected = currentMonth == 2 },
                new SelectListItem { Text = "March", Value = "3", Selected = currentMonth == 3 },
                new SelectListItem { Text = "April", Value = "4", Selected = currentMonth == 4 },
                new SelectListItem { Text = "May", Value = "5", Selected = currentMonth == 5 },
                new SelectListItem { Text = "June", Value = "6", Selected = currentMonth == 6 },
                new SelectListItem { Text = "July", Value = "7", Selected = currentMonth == 7 },
                new SelectListItem { Text = "August", Value = "8", Selected = currentMonth == 8 },
                new SelectListItem { Text = "September", Value = "9", Selected = currentMonth == 9 },
                new SelectListItem { Text = "October", Value = "10", Selected = currentMonth == 10 },
                new SelectListItem { Text = "November", Value = "11", Selected = currentMonth == 11 },
                new SelectListItem { Text = "December", Value = "12", Selected = currentMonth == 12 }
            };

            return options;
        }

        public static IEnumerable<SelectListItem> GetYearsOptions(int toYear, int fromYear = 2010)
        {
            var currentYear = DateTime.UtcNow.Year;
            var options = new List<SelectListItem>();

            for (int i = fromYear; i <= toYear; i++)
            {
                var selectListItem = new SelectListItem
                {
                    Text = $"{i}",
                    Value = $"{i}",
                    Selected = currentYear == i
                };
                options.Add(selectListItem);
            }

            return options;
        }

        public  IEnumerable<SelectListItem> GetLotBlockOptions(int? communityId = null)
        {
            var service = GetReportRepository();
            var options = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty }
            };

            var lotBlockTask = service.GetLotBlockOptions(communityId);

            lotBlockTask.Wait();

            options.AddRange(lotBlockTask.Result
                .Select(x =>
                new SelectListItem { Text = $"Lot #{x.Key.ToString()} Block {x.Value}", Value = $"{x.Key.ToString()} {x.Value}" })
                );

            return options;
        }

        public static  IEnumerable<SelectListItem> GetDeliveryInstructions(string selected)
        {
            var option1 = "Painter will pick up";
            var option2 = "Please deliver";

            return new List<SelectListItem>
            {
                //new SelectListItem { Text = string.Empty, Value = string.Empty },
                new SelectListItem { Text = option1, Value = option1, Selected = selected == option1 },
                new SelectListItem { Text = option2, Value = option2, Selected = selected == option2 }
            };
        }

        public static IEnumerable<SelectListItem> GetGroupingOptions()
        {
            var options = new List<SelectListItem>
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty },
                new SelectListItem { Text = "Monthly", Value =  "Monthly" },
                new SelectListItem { Text = "Weekly", Value =  "Weekly" },
                new SelectListItem { Text = "Daily", Value =  "Daily" }
            };

            return options;
        }

        public  IEnumerable<SelectListItem> ScheduleTaskListToTaskOptions(IEnumerable<int> schedulesToExclude)
        {
            var service = GetTaskService();
            var schemas = service.GetTaskSchemas(true, true).GetAwaiter().GetResult();

            var scheduleOptions = new List<SelectListItem>(schemas.Count() + 1)
            {
                new SelectListItem { Text = string.Empty, Value = string.Empty }
            };

            scheduleOptions.AddRange(schemas.Select(schedule => new SelectListItem
            {
                Text = schedule.Name,
                Value = schedule.Id.ToString(),
                Disabled = schedulesToExclude.Contains(schedule.Id)
            }));

            return scheduleOptions;
        }

        public static IDictionary<string, object> GetHtmlAttributes(
            bool formControl = true,
            bool disabled = false,
            bool @readonly = false,
            bool @checked = false,
            string type = null,
            string @step = null,
            string @class = null,
            bool multiple = false,
            int? rows = null,
            string id = null,
            int? maxLength = null,
            string placeholder = null,
            string dataMask = null,
            string min = null,
            string max = null,
            string dataValue = null)
        {
            var attributes = new Dictionary<string, object>();
            var className = formControl ? "form-control" : string.Empty;

            if (disabled)
            {
                attributes.Add("disabled", "disabled");
            }

            if (!@step.IsNullOrEmpty())
            {
                attributes.Add("step", @step);
            }

            if (multiple)
            {
                attributes.Add("multiple", true);
            }

            if (@checked)
            {
                attributes.Add("checked", "checked");
            }

            if (@readonly)
            {
                attributes.Add("readonly", "readonly");
            }

            if (!type.IsNullOrEmpty())
            {
                attributes.Add("type", type);
            }

            if (!id.IsNullOrEmpty())
            {
                attributes.Add("id", id);
            }

            if (!@class.IsNullOrEmpty())
            {
                className += $" {@class}";
            }

            if (rows.HasValue)
            {
                attributes.Add("rows", rows);
            }

            if (maxLength.HasValue)
            {
                attributes.Add("maxlength", maxLength);
            }

            if (!placeholder.IsNullOrEmpty())
            {
                attributes.Add("placeholder", placeholder);
            }

            if (!dataMask.IsNull())
            {
                attributes.Add("data-mask", dataMask);
            }

            if (!min.IsNullOrEmpty())
            {
                attributes.Add("min", min);
            }

            if (!max.IsNullOrEmpty())
            {
                attributes.Add("max", max);
            }

            if (!dataValue.IsNullOrEmpty())
            {
                attributes.Add($"data-value", dataValue);
            }

            attributes.Add("class", className);

            return attributes;
        }

        public  string GetUserFirstName()
        {
            try
            {
                var userContext = GetUserContext();
                return userContext.FirstName;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetTaskColorSurfix(WorkOrderTask task)
        {
            var panelColorClass = "danger";

            if (task.Completed.HasValue)
            {
                panelColorClass = "success";
            }
            else if (task.Deferred)
            {
                panelColorClass = "default";
            }
            else if (task.Permission.CanSave)
            {
                panelColorClass = "warning";
            }

            return panelColorClass;
        }

        public static string ToNoteDisplay(string noteJsonString)
        {
            if (noteJsonString.IsNullOrEmpty())
                return string.Empty;

            var notesList = new List<string>();
            var notes = noteJsonString.FromJson<List<Note>>();
            var nl = Environment.NewLine;

            if (notes.IsNullOrEmpty())
                return string.Empty;

            foreach (var note in notes)
            {
                notesList.Add(string.Format("[{0} - {1}]{3}{2}",
                    note.Author,
                    note.Created.ToLocalShortDateTime(),
                    note.Text,
                    nl));
            }

            return string.Join($"{nl}{nl}", notesList);
        }

        public static IEnumerable<SelectListItem> GetWorkOrderSearchOptions()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = "Work Order Name", Value = WorkOrderSearchOption.WorkOrderName.ToString() },
                new SelectListItem { Text = "Builder", Value = WorkOrderSearchOption.Builder.ToString() },
                new SelectListItem { Text = "Community", Value = WorkOrderSearchOption.Community.ToString() },
                new SelectListItem { Text = "Address", Value = WorkOrderSearchOption.Address.ToString() },
                new SelectListItem { Text = "Lot And Block", Value = WorkOrderSearchOption.LotAndBlock.ToString(), Selected = true },
                new SelectListItem { Text = "Exterior Base Color", Value = WorkOrderSearchOption.ExteriorBaseColor.ToString() },
                new SelectListItem { Text = "Interior Base Color", Value = WorkOrderSearchOption.InteriorBaseColor.ToString() }
            };
        }

        private  List<SupplyTypeMap> MapSupplyTypeToColors(JobInfo jobInfo,
            List<Supply> supplies, List<SupplyType> supplyTypes, SupplyCategory category)
        {
            var interiorColors = jobInfo.InteriorColors;
            var exteriorColors = jobInfo.ExteriorColors;
            var supplyTypeMaps = new List<SupplyTypeMap>();

            var interiorSupplies = supplies.Where(x => x.SupplyType.SupplyCategory == SupplyCategory.Interior);
            var exteriorSupplies = supplies.Where(x => x.SupplyType.SupplyCategory == SupplyCategory.Exterior);
            var interiorSupplyTypeNames = interiorSupplies.Select(x => x.SupplyType.Name);
            var exteriorSupplyTypeNames = exteriorSupplies.Select(x => x.SupplyType.Name);

            var interiorSupplyTypes = supplyTypes.Where(x => x.SupplyCategory == SupplyCategory.Interior && interiorSupplyTypeNames.Contains(x.Name));
            var exteriorSupplyTypes = supplyTypes.Where(x => x.SupplyCategory == SupplyCategory.Exterior && exteriorSupplyTypeNames.Contains(x.Name));

            if (category == SupplyCategory.Interior || category == SupplyCategory.All)
            {
                interiorSupplyTypes.ForEach(x =>
                {
                    var supplyTypeMap = new SupplyTypeMap();

                    if (category == SupplyCategory.Interior || category == SupplyCategory.All)
                    {
                        if (x.Name == "Base")
                        {
                            supplyTypeMap.Color = interiorColors?.InteriorBase;
                            supplyTypeMap.Quantity = (int)Math.Ceiling((double)jobInfo.TotalSize / 125) + 5;
                        }
                        if (x.Name == "Trim")
                        {
                            supplyTypeMap.Color = interiorColors?.InteriorWoodTrimBaseCasing;
                            supplyTypeMap.Quantity = (int)Math.Ceiling((double)jobInfo.TotalSize / 1000);
                        }
                        if (x.Name == "Ceiling")
                        {
                            supplyTypeMap.Color = interiorColors?.InteriorCeiling;
                        }
                        if (x.Name == "Accent Room")
                        {
                            supplyTypeMap.Color = interiorColors?.AccentRoom1;
                        }
                        if (x.Name == "Accent Wall")
                        {
                            supplyTypeMap.Color = interiorColors?.AccentWall1;
                        }

                        if (x.Name == "Beams")
                        {
                            supplyTypeMap.Color = interiorColors?.Beams;
                        }
                        if (x.Name == "Crown")
                        {
                            supplyTypeMap.Color = interiorColors?.Crown;
                        }
                        if (x.Name == "Front Door")
                        {
                            supplyTypeMap.Color = interiorColors?.FrontDoor;
                        }
                        if (x.Name == "Treads")
                        {
                            supplyTypeMap.Color = interiorColors?.Treads;
                        }
                        if (x.Name == "Newel")
                        {
                            supplyTypeMap.Color = interiorColors?.NewelPosts;
                        }
                        if (x.Name == "Rail")
                        {
                            supplyTypeMap.Color = interiorColors?.Rail;
                        }
                        if (x.Name == "Risers")
                        {
                            supplyTypeMap.Color = interiorColors?.Risers;
                        }
                        if (x.Name.Contains("Stain"))
                        {
                            supplyTypeMap.Color = interiorColors?.Stain;
                        }
                    }

                    if (supplyTypeMap != default(SupplyTypeMap))
                    {
                        supplyTypeMap.SupplyType = $"Int. {x.Name}";
                        supplyTypeMap.Name = interiorSupplies.FirstOrDefault(y => y.SupplyType.Name == x.Name)?.Name;
                        supplyTypeMap.SupplyCategory = x.SupplyCategory;
                        supplyTypeMap.Cost = interiorSupplies.FirstOrDefault(y => y.SupplyType.Name == x.Name)?.Cost ?? 0;
                        supplyTypeMaps.Add(supplyTypeMap);
                    }

                });
            }

            if (category == SupplyCategory.Exterior || category == SupplyCategory.All)
            {
                exteriorSupplyTypes.ForEach(x =>
                {
                    var supplyTypeMap = new SupplyTypeMap();

                    if (category == SupplyCategory.Exterior || category == SupplyCategory.All)
                    {
                        if (x.Name == "Base")
                        {
                            supplyTypeMap.Color = exteriorColors?.ExteriorBase;
                            supplyTypeMap.Quantity = (int)Math.Ceiling(jobInfo.TotalSize / 125);
                        }
                        if (x.Name == "Trim")
                        {
                            supplyTypeMap.Color = exteriorColors?.ExteriorTrim;
                            supplyTypeMap.Quantity = (int)Math.Ceiling((double)jobInfo.TotalSize / 1000);
                        }
                        if (x.Name == "Primer")
                        {
                            supplyTypeMap.Quantity = (int)Math.Ceiling(jobInfo.TotalSize / 125);
                        }
                        if (x.Name == "Shakes")
                        {
                            supplyTypeMap.Color = exteriorColors?.ShakeSiding;
                        }
                        if (x.Name == "Board and Batton")
                        {
                            supplyTypeMap.Color = exteriorColors?.BoardAndBatton;
                        }
                        if (x.Name == "Garage Door")
                        {
                            supplyTypeMap.Color = exteriorColors?.GarageDoor;
                        }
                        if (x.Name == "Front Door")
                        {
                            supplyTypeMap.Color = exteriorColors?.FrontDoor;
                        }
                        if (x.Name == "Soffit")
                        {
                            supplyTypeMap.Color = exteriorColors?.Soffit;
                        }
                        if (x.Name == "Shutters")
                        {
                            supplyTypeMap.Color = exteriorColors?.Shutters;
                        }
                        if (x.Name == "Hardi Siding")
                        {
                            supplyTypeMap.Color = exteriorColors?.HardiSiding;
                        }
                    }

                    if (supplyTypeMap != default(SupplyTypeMap))
                    {
                        supplyTypeMap.SupplyType = $"Ext. {x.Name}";
                        supplyTypeMap.Name = exteriorSupplies.FirstOrDefault(y => y.SupplyType.Name == x.Name)?.Name;
                        supplyTypeMap.SupplyCategory = x.SupplyCategory;
                        supplyTypeMap.Cost = exteriorSupplies.FirstOrDefault(y => y.SupplyType.Name == x.Name)?.Cost ?? 0;
                        supplyTypeMaps.Add(supplyTypeMap);
                    }

                });
            }

            if (category == SupplyCategory.All)
            {
                supplyTypeMaps.ForEach(x => x.Quantity = 1);
            }
            return supplyTypeMaps;
        }

        private ICommunityComposerService GetCommunityComposerService()
        {
            var service = _serviceFactory.GetInstance(typeof(ICommunityComposerService));

            return service as ICommunityComposerService;
        }

        private IUserInfoService GetUserInfoService()
        {
            var service = _serviceFactory.GetInstance(typeof(IUserInfoService));

            return service as IUserInfoService;
        }

        private ITaskComposerService GetTaskService()
        {
            return _serviceFactory.GetInstance(typeof(ITaskComposerService)) as ITaskComposerService;
        }

        private IPaintStoreService GetStoreService()
        {
            return _serviceFactory.GetInstance(typeof(IPaintStoreService)) as IPaintStoreService;
        }

        private IReportRepository GetReportRepository()
        {
            return _serviceFactory.GetInstance(typeof(IReportRepository)) as IReportRepository;
        }

        private IWorkOrderComposerService GetWorkOrderComposerService()
        {
            return _serviceFactory.GetInstance(typeof(IWorkOrderComposerService)) as IWorkOrderComposerService;
        }

        private IUserContext GetUserContext()
        {
            return _serviceFactory.GetInstance(typeof(IUserContext)) as IUserContext;
        }
    }
}