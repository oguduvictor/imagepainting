﻿namespace ImagePainting.Site.Web.Helpers
{
    public static class RolesConstant
    {
        public const string AdminAndEmployee = "Administrator, Employee";

        public const string Admin = "Administrator";

        public const string SubContractor = "SubContractor";

        public const string User = "User";

        public const string Staff = "Administrator, SubContractor, Accountant, Employee";
    }
}