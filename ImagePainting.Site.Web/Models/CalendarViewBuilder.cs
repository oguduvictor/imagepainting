﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using ImagePainting.Site.Common.Models.Entities;

namespace ImagePainting.Site.Web.Models
{
    [Obsolete("Use CalendarViewBuilder2 instead")]
    public static class CalendarViewBuilder
    {
        private static IUserHelper _userHelper;

        public static List<CalendarDayModel> BuildView(
            IDictionary<DateTime, IEnumerable<UserInfoSummary>> unAssignedCrews,
            TaskSearchCriteria searchCriteria, 
            IEnumerable<WorkOrderTask> searchResult,
            IList<UnAssignedCrewSetting> unAssignedCrewSettings,
            IEnumerable<PurchaseOrderSummary> purchaseOrderSummaries, IUserHelper userHelper)
        {
            var calendarDayModels = new List<CalendarDayModel>();
            _userHelper = userHelper;

            foreach (var date in unAssignedCrews.Keys)
            {
                var tasksForDate = searchResult.Where(x => x.ScheduleDateRange.BeginDate.Date == date).ToList();
                var unassignedCrews = unAssignedCrews[date].Where(x => !tasksForDate.Any(y => y.CrewId == x.Id)).ToList();
                var isAdmin = _userHelper.IsUserAdmin();

                if (!isAdmin && tasksForDate.IsNullOrEmpty())
                {
                    continue;
                }

                var unassignedCrewSettingsForScheduledDate = unAssignedCrewSettings.Where(x => x.ScheduledDate == date);
                var model = ConvertToCalendarItemModel(date, tasksForDate, unassignedCrews, searchCriteria, unassignedCrewSettingsForScheduledDate, isAdmin, purchaseOrderSummaries);

                calendarDayModels.Add(model);
            }

            if (searchCriteria.OrderByAscending)
            {
                calendarDayModels.Sort((x, y) => x.Date.CompareTo(y.Date));
            }
            else
            {
                calendarDayModels.Sort((x, y) => y.Date.CompareTo(x.Date));
            }

            return calendarDayModels;
        }

        private static CalendarDayModel ConvertToCalendarItemModel(
            DateTime date,
            IList<WorkOrderTask> tasks,
            IList<UserInfoSummary> unAssignedCrew,
            TaskSearchCriteria criteria,
            IEnumerable<UnAssignedCrewSetting> unAssignedCrewSettingsForScheduledDate,
            bool isAdmin,
            IEnumerable<PurchaseOrderSummary> tasksPurchaseOrderSummaries)
        {
            var today = DateHelper.GetLocalDateToday();
            var dayModel = new CalendarDayModel
            {
                Date = date,
                CalendarItems = new List<CalenderItemModel>(),
                IsEmpty = tasks.IsNullOrEmpty(),
                IsAdmin = isAdmin 
            };

            var items = new List<CalenderItemModel>();
            
            foreach(var task in tasks)
            {
                if (!task.Deferred)
                {
                    var taskPOs = tasksPurchaseOrderSummaries.FirstOrDefault(x => x.TaskId == task.Id);

                    var item = new CalenderItemModel
                    {
                        CrewName = task.Crew.IsNull() ? "Unassigned" : task.Crew.FullName,
                        CrewId = task.CrewId,
                        WorkOrderName = task.WorkOrder.Name,
                        WorkOrderAddress = task.WorkOrder.Address,
                        WorkOrderId = task.WorkOrderId,
                        TaskName = task.Name,
                        TaskId = task.Id,
                        IsTaskReady = task.Permission.IsTaskReady && !task.Completed.HasValue,
                        TextColor = $"text-{ViewHelper.GetTaskColorSurfix(task)}",
                        EmailSent = task.EmailSent.HasValue,
                        OrderBy = task.Crew.IsNull() ? "ZZZZZ" : task.Crew.FullName,
                        ScheduleDate = new ScheduleDateRange(task.ScheduleDateRange.BeginDate, task.ScheduleDateRange.EndDate),
                        OrderPlaced = !task.OrderPlaced.IsNull() && task.OrderPlaced.Value,
                        CanChangeSchedule = isAdmin && task.Completed.IsNull() && task.Permission.CanChangeSchedule
                    };

                    if (!taskPOs.IsNull())
                    {
                        item.WillPainterPickUp = taskPOs.DeliveryInstructions == "Painter will pick up";

                        item.LatestPOId = taskPOs.Id;
                    }

                    AddIcons(item);

                    items.Add(item);
                }

            }

            if (isAdmin && !unAssignedCrew.IsNullOrEmpty())
            {
                foreach (var crew in unAssignedCrew)
                {
                    var crewSetting = unAssignedCrewSettingsForScheduledDate.FirstOrDefault(x => x.UserId == crew.Id);
                    var itemModel = new CalenderItemModel
                    {
                        CrewName = crew.FullName,
                        CrewId = crew.Id,
                        OrderBy = crewSetting?.Busy == true ? $"ZZZAA{crew.FullName}" : $"ZZZAB{crew.FullName}",
                        IsFreeCrew = "free-crew",
                        EmailSent = crewSetting?.Emailed ?? false,
                        IsBusy = crewSetting?.Busy ?? false,
                    };

                    AddIcons(itemModel);

                    items.Add(itemModel);
                }
            }

            dayModel.CanSendEmail = !disableSendingMail(criteria)
                                    && date >= today
                                    && (!unAssignedCrew.IsNullOrEmpty() 
                                        || tasks.Any(x => x.Completed.IsNull() && x.ScheduleDateRange.BeginDate >= today));

            dayModel.CalendarItems = items
                                        .OrderBy(x => x.OrderBy)
                                        .ThenBy(x => x.TextColor)
                                        .ThenBy(x => x.WorkOrderName)
                                        .ThenBy(x => x.TaskName)
                                        .ToList();
            return dayModel;
        }

        private static void AddIcons(CalenderItemModel item)
        {
            var icons = new List<IconModel>();

            if (item.IsTaskReady)
            {
                icons.Add( new IconModel
                {
                    Icon = "text-success glyphicon glyphicon-ok",
                    ToolTip = "This task is ready"
                });
            }

            if (item.OrderPlaced)
            {
                icons.Add(new IconModel
                {
                    Icon = item.WillPainterPickUp? "text-danger fa fa-shopping-cart" : "fa fa-shopping-cart",
                    ToolTip = $"Order has been placed for {item.TaskName}",
                    LatestPOId = item.LatestPOId, 
                    IsLink= true
                });
            }

            if (item.EmailSent)
            {
                icons.Add(new IconModel
                {
                    Icon = "fa fa-envelope",
                    ToolTip = "Task has been emailed"
                });
            }

            if (item.IsBusy)
            {
                icons.Add((new IconModel
                {
                    Icon = "text-success fa fa-thumbs-up",
                    ToolTip = "Crew is busy"
                }));
            }

            item.Icons.AddRange(icons);
        }

        private static bool disableSendingMail (TaskSearchCriteria criteria) => criteria.IsOverdued ?? criteria.IsCompleted ?? false;
    }
}