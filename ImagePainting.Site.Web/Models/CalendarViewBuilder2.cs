﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Web.Models
{
    public static class CalendarViewBuilder2
    {
        private static IViewHelper _viewHelper;

        public static List<CalendarDayModel> BuildView(
            IList<TaskView> searchResult,
            TaskSearchCriteria searchCriteria,
            IDictionary<DateTime, IEnumerable<UserInfoSummary>> unAssignedCrewsMap,
            IList<UnAssignedCrewSetting> unAssignedCrewSettings,
            IViewHelper viewHelper,
            IUserHelper userHelper)
        {
            var calendarDayModels = new List<CalendarDayModel>();
            var dateRange = searchCriteria.ToDateRange();
            var isAdmin = userHelper.IsUserAdmin();
            var daysCount = (dateRange.EndDateTime - dateRange.BeginDateTime).Days;
            var searchResultList = searchResult as List<TaskView>;
            var unAssignedCrewSettingsList = unAssignedCrewSettings as List<UnAssignedCrewSetting>;
            _viewHelper = viewHelper;

            for (var i = 0; i < daysCount + 1; i++)
            {
                var date = dateRange.BeginDateTime.AddDays(i);
                var tasksForDate = searchResultList.FindAll(x => x.ScheduleDate.Date == date);

                if (!isAdmin && tasksForDate.IsNullOrEmpty())
                {
                    continue;
                }

                var unAssignedCrews = unAssignedCrewsMap.ContainsKey(date) ? unAssignedCrewsMap[date].Where(x => !tasksForDate.Any(y => y.CrewId == x.Id)) : new List<UserInfoSummary>();
                var unassignedCrewSettingsForScheduledDate = unAssignedCrewSettingsList.FindAll(x => x.ScheduledDate == date);
                var model = ConvertToCalendarItemModel(date, tasksForDate, unAssignedCrews.ToList(), searchCriteria, unassignedCrewSettingsForScheduledDate, isAdmin);

                calendarDayModels.Add(model);
            }

            if (searchCriteria.OrderByAscending)
            {
                calendarDayModels.Sort((x, y) => x.Date.CompareTo(y.Date));
            }
            else
            {
                calendarDayModels.Sort((x, y) => y.Date.CompareTo(x.Date));
            }

            return calendarDayModels;
        }

        private static CalendarDayModel ConvertToCalendarItemModel(
            DateTime date,
            List<TaskView> tasks,
            List<UserInfoSummary> unAssignedCrews,
            TaskSearchCriteria criteria,
            List<UnAssignedCrewSetting> unAssignedCrewSettingsForScheduledDate,
            bool isAdmin)
        {
            var today = DateHelper.GetLocalDateToday();
            var dayModel = new CalendarDayModel
            {
                Date = date,
                CalendarItems = new List<CalenderItemModel>(),
                IsEmpty = tasks.IsNullOrEmpty(),
                IsAdmin = isAdmin 
            };

            var items = new List<CalenderItemModel>();
            
            foreach(var task in tasks)
            {
                var item = new CalenderItemModel
                {
                    CrewName = task.CrewId.IsNull() ? "Unassigned" : task.Crew,
                    CrewId = task.CrewId,
                    WorkOrderName = task.WorkOrderName,
                    WorkOrderAddress = task.WorkOrderAddress,
                    WorkOrderId = task.WorkOrderId,
                    TaskName = task.Name,
                    TaskId = task.Id,
                    IsTaskReady = task.Permission.IsTaskReady && !task.Completed.HasValue,
                    TextColor = $"text-{GetTaskColorSurfix(task)}",
                    EmailSent = task.EmailSent.HasValue,
                    OrderBy = task.CrewId.IsNull() ? "ZZZZZ" : task.Crew,
                    ScheduleDate = new ScheduleDateRange(task.ScheduleDate, task.ScheduleDateEnd),
                    OrderPlaced = task.PurchaseOrderId.HasValue,
                    CanChangeSchedule = isAdmin && task.Completed.IsNull() && task.Permission.CanChangeSchedule,
                    LatestPOId = task.PurchaseOrderId ?? 0,
                    WillPainterPickUp = task.DeliveryInstructions == "Painter will pick up",
                };

                AddIcons(item);

                items.Add(item);
            }

            if (isAdmin && !unAssignedCrews.IsNullOrEmpty())
            {
                foreach (var crew in unAssignedCrews)
                {
                    var crewSetting = unAssignedCrewSettingsForScheduledDate.FirstOrDefault(x => x.UserId == crew.Id);
                    var itemModel = new CalenderItemModel
                    {
                        CrewName = crew.FullName,
                        CrewId = crew.Id,
                        OrderBy = crewSetting?.Busy == true ? $"ZZZAA{crew.FullName}" : $"ZZZAB{crew.FullName}",
                        IsFreeCrew = "free-crew",
                        EmailSent = crewSetting?.Emailed ?? false,
                        IsBusy = crewSetting?.Busy ?? false
                    };

                    AddIcons(itemModel);

                    items.Add(itemModel);
                }
            }

            dayModel.CanSendEmail = !EmailDisabled(criteria)
                                    && date >= today
                                    && (!unAssignedCrews.IsNullOrEmpty() 
                                        || tasks.Any(x => x.Completed.IsNull() && x.ScheduleDate.Date >= today));

            dayModel.CalendarItems = items
                                        .OrderBy(x => x.OrderBy)
                                        .ThenBy(x => x.TextColor)
                                        .ThenBy(x => x.WorkOrderName)
                                        .ThenBy(x => x.TaskName)
                                        .ToList();
            return dayModel;
        }

        private static void AddIcons(CalenderItemModel item)
        {
            var icons = new List<IconModel>();

            if (item.IsTaskReady)
            {
                icons.Add( new IconModel
                {
                    Icon = "text-success glyphicon glyphicon-ok",
                    ToolTip = "This task is ready"
                });
            }

            if (item.OrderPlaced)
            {
                icons.Add(new IconModel
                {
                    Icon = item.WillPainterPickUp? "text-danger fa fa-shopping-cart" : "fa fa-shopping-cart",
                    ToolTip = $"Order has been placed for {item.TaskName}",
                    LatestPOId = item.LatestPOId, 
                    IsLink= true
                });
            }

            if (item.EmailSent)
            {
                icons.Add(new IconModel
                {
                    Icon = "fa fa-envelope",
                    ToolTip = "Task has been emailed"
                });
            }

            if (item.IsBusy)
            {
                icons.Add((new IconModel
                {
                    Icon = "text-success fa fa-thumbs-up",
                    ToolTip = "Crew is busy"
                }));
            }

            item.Icons.AddRange(icons);
        }

        private static bool EmailDisabled (TaskSearchCriteria criteria) => criteria.IsOverdued ?? criteria.IsCompleted ?? false;

        private static string GetTaskColorSurfix(TaskView task)
        {
            var panelColorClass = "danger";

            if (task.Completed.HasValue)
            {
                panelColorClass = "success";
            }
            else if (task.Permission.CanSave && task.Permission.IsTaskReady)
            {
                panelColorClass = "warning";
            }

            return panelColorClass;
        }
    }
}