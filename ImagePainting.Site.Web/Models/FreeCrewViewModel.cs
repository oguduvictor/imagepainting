﻿using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Web.Models
{
    public class FreeCrewViewModel
    {
        public IEnumerable<string> FreeCrewIds { get; set; }

        public DateTime TaskDate { get; set; }

        public string Note { get; set; }
    }
}