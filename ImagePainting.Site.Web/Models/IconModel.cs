﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImagePainting.Site.Web.Models
{
    public class IconModel
    {
        public string ToolTip { get; set; }

        public string Icon { get; set; }

        public long LatestPOId { get; set; }

        public bool IsLink { get; set; }
        
    }
}