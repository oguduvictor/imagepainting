﻿using ImagePainting.Site.Web.Helpers;

namespace ImagePainting.Site.Web.Models
{
    public class SiteViewModel<T>
    {
        public SiteViewModel(T model, IViewHelper viewHelper, IUserHelper userHelper)
        {
            ViewModel = model;
            ViewHelper = viewHelper;
            UserHelper = userHelper;
        }

        public T ViewModel { get; private set; }

        public IViewHelper ViewHelper { get; private set; }

        public IUserHelper UserHelper { get; private set; }
    }
}