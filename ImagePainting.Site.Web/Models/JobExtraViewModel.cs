﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;

namespace ImagePainting.Site.Web.Models
{
    public class JobExtraViewModel
    {
        public long Id { get; set; }

        public string Note { get; set; }

        public long WorkOrderId { get; set; }

        public bool WorkOrderCompleted { get; set; }

        public bool IsAdmin { get; set; }

        public IEnumerable<ExtraItem> ExtraItems { get; set; }

        public Dictionary<string, IList<ExtraItem>> GroupedExtraItems { get; set; }
    }
}