﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using ImagePainting.Site.Common.Models.Enums;
using System;

namespace ImagePainting.Site.Web.Models
{
    public class SystemAlertViewModel
    {
        public SystemAlertViewModel(SystemAlert systemAlert)
        {
            Id = systemAlert.Id;
            AlertType = systemAlert.AlertType;
            JobExtra = systemAlert.JobExtra;
            Note = systemAlert.Note;
            ParentId = systemAlert.ParentId;
            Resolved = systemAlert.Resolved?.ToLocalShortDateTime();
            ResolvedBy = systemAlert.ResolvedBy?.FullName;
        }

        public Guid Id { get; set; }

        public AlertType AlertType { get; set; }

        public JobExtraAlert JobExtra { get; set; }

        public NoteAlert Note { get; set; }

        public long ParentId { get; set; }
        
        public string Resolved { get; set; }

        public string ResolvedBy { get; set; }
    }
}