﻿using ImagePainting.Site.Common.Models;
using System.Linq;
using System.Collections.Generic;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.ViewModels;

namespace ImagePainting.Site.Web.Models
{
    public class ExtraJobInfoViewBuilder
    {
        public static JobExtraViewModel BuildView(long workOrderId, WorkOrderItemViewModel<ExtraJobInfo> workOrderItemViewModel)
        {
            var groupedExtraItems = new Dictionary<string, IList<ExtraItem>>();

            foreach (var item in workOrderItemViewModel.Item.ExtraItems)
            {
                var keys = GetKeys(item, workOrderItemViewModel.TaskSummaries);

                foreach (var key in keys)
                {
                    if (groupedExtraItems.ContainsKey(key))
                    {
                        groupedExtraItems[key].Add(item);
                    }
                    else
                    {
                        groupedExtraItems[key] = new List<ExtraItem> { item };
                    }
                }
            }

            return new JobExtraViewModel
            {
                Id = workOrderItemViewModel.Item.Id,
                Note = workOrderItemViewModel.Item.Note,
                WorkOrderId = workOrderId,
                WorkOrderCompleted = workOrderItemViewModel.WorkOrderCompleted,
                GroupedExtraItems = groupedExtraItems
            };
        }

        private static IEnumerable<string> GetKeys(ExtraItem extraItem, IEnumerable<TaskSummary> taskSummaries)
        {
            if (extraItem.TaskIds.IsNullOrEmpty())
            {
                return new List<string> { extraItem.TaskType.ToString().BreakupText() };
            }

            var tasks = taskSummaries?.Where(x => x.TaskType == extraItem.TaskType) ?? new List<TaskSummary>();

            if (tasks.IsNullOrEmpty())
            {
                return new List<string> { extraItem.TaskType.ToString().BreakupText() };
            }

            return taskSummaries.Where(x => extraItem.TaskIds.Contains(x.Id)).Select(x => x.Name);
        }
    }
}