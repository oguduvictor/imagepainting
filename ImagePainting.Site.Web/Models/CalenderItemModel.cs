﻿using System;
using System.Collections.Generic;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Web.Helpers;

namespace ImagePainting.Site.Web.Models
{
    public class CalenderItemModel
    {
        
        public CalenderItemModel()
        {
            Icons = new List<IconModel>();
        }

        public string OrderBy { get; set; }

        public string CrewName { get; set; }

        public string CrewId { get; set; }

        public string WorkOrderName { get; set; } = string.Empty;

        public string WorkOrderAddress { get; set; } = string.Empty;

        public long WorkOrderId { get; set; }

        public string TaskName { get; set; } = string.Empty;

        public long TaskId { get; set; }

        public bool CanChangeSchedule { get; set; }

        public string TextColor { get; set; } = string.Empty;

        public bool IsTaskReady { get; set; }

        public bool EmailSent { get; set; }

        public ScheduleDateRange ScheduleDate { get; set; }

        public string TaskTimeRange 
        {
            get
            {
                var endDate = !string.IsNullOrWhiteSpace(FormattedEndTime) ? $" - {FormattedEndTime}" : string.Empty;
                return $"{FormatedStartTime}{endDate}";
            }
        }
        
        public bool OrderPlaced { get; set; }

        public List<IconModel> Icons { get; set; }

        public string IsFreeCrew { get; set; }

        public bool IsBusy { get; set; }

        public bool WillPainterPickUp { get; set; }

        public long LatestPOId { get; set; }


        private static bool HasValidBeginDateTime(ScheduleDateRange scheduleDate)
        {
            return !scheduleDate.IsNull() && !scheduleDate.BeginDate.IsDefaultTime();
        }

        private static bool HasValidEndDateTime(ScheduleDateRange scheduleDate)
        {
            return !scheduleDate.IsNull() && scheduleDate.EndDate.HasValue && !scheduleDate.EndDate.Value.IsDefaultTime();
        }

        private string FormatedStartTime => HasValidBeginDateTime(ScheduleDate)
            ? ScheduleDate.BeginDate.ToShortTimeString()
            : string.Empty;

        private string FormattedEndTime => HasValidEndDateTime(ScheduleDate)
            ? ScheduleDate.EndDate.Value.ToShortTimeString()
            : string.Empty;

        public string StartTimeString => HasValidBeginDateTime(ScheduleDate)
            ? ViewHelper.GetTime(ScheduleDate.BeginDate)
            : string.Empty;

        public string EndTimeString => HasValidEndDateTime(ScheduleDate)
            ? ViewHelper.GetTime(ScheduleDate.EndDate.Value)
            : string.Empty;
    }
}