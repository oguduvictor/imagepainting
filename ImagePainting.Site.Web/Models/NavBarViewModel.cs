﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImagePainting.Site.Web.Models
{
    public class NavBarViewModel
    {
        public bool IsAdmin { get; set; }

        public bool IsStaff { get; set; }

        public bool IsAdminOrEmployee { get; set; }

        public string UserFirstName { get; set; }
    }
}