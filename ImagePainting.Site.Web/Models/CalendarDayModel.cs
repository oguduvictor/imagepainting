﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Web.Helpers;

namespace ImagePainting.Site.Web.Models
{
    public class CalendarDayModel
    {
        public DateTime Date { get; set; }

        public List<CalenderItemModel> CalendarItems { get; set; }
        
        public bool CanSendEmail { get; set; }

        public string PanelClass => CanSendEmail ? "upcoming-task" : string.Empty;

        public string PanelBodyClass => CanSendEmail ? "isUpcoming" : string.Empty;

        public bool IsEmpty { get; set; }

        public bool IsAdmin { private get; set; }

        public bool CanShowEmailButton => IsAdmin && CanSendEmail;
    }
}