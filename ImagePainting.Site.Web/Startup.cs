﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImagePainting.Site.Web.Startup))]
namespace ImagePainting.Site.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
