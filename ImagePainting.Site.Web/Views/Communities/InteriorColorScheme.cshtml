﻿@model SiteViewModel<ImagePainting.Site.Common.Models.InteriorColorScheme>
@{
    var model = Model.ViewModel;
    ViewBag.Title = "InteriorColorScheme";
    ViewBag.CommunitiesActive = "active";
    var isAdmin = Model.UserHelper.IsUserAdmin();
}

@if (!ViewBag.IsPartial)
{
    <div class="jumbotron child">
        <h1>@(model.Id == 0 ? "New Interior Color Scheme" : $"Interior Color Scheme - {model.InteriorBase}")</h1>
    </div>
}

<section class="container body-content">
    @if (ViewBag.IsPartial)
    {
        <div class="form-group">
            <h2>@(model.Id == 0 ? "New Interior Color Scheme" : $"Interior Color Scheme - {model.InteriorBase}")</h2>
        </div>
    }
    @using (Html.BeginForm("InteriorColorScheme", "Communities", FormMethod.Post))
    {
        @Html.AntiForgeryToken()

        @(Html.For(model).HiddenFor(x => x.Id))

        <div class="row">
            <div class="form-group col-md-12">
                <label class="required-field" for="colorCode">Color Code</label>
                @(Html.For(model).TextBoxFor(x => x.ColorCode,ViewHelper.GetHtmlAttributes(disabled: !isAdmin)))
                @(Html.For(model).ValidationMessageFor(x => x.ColorCode, null, new { @class = "text-danger" }))
            </div>
        </div>

        <div class="row">

            <div class="form-group col-md-4">
                <label class="required-field" for="colorCode">Interior Base</label>
                @(Html.For(model).TextBoxFor(x => x.InteriorBase,ViewHelper.GetHtmlAttributes(disabled: !isAdmin)))
                @(Html.For(model).ValidationMessageFor(x => x.InteriorBase, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="InteriorWoodTrimBaseCasing">Interior Wood Trim Base/Casing</label>
                @(Html.For(model).TextBoxFor(x => x.InteriorWoodTrimBaseCasing,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.InteriorWoodTrimBaseCasing, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="frontDoor">Front Door</label>
                @(Html.For(model).TextBoxFor(x => x.FrontDoor,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.FrontDoor, null, new { @class = "text-danger" }))
            </div>


        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <label for="beams">Beams</label>
                @(Html.For(model).TextBoxFor(x => x.Beams, new { @class = "form-control" }))
                @(Html.For(model).ValidationMessageFor(x => x.Beams, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="interiorDoors">Interior Doors</label>
                @(Html.For(model).TextBoxFor(x => x.InteriorDoors,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.InteriorDoors, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="interiorCeiling">Interior Ceiling</label>
                @(Html.For(model).TextBoxFor(x => x.InteriorCeiling,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.InteriorCeiling, null, new { @class = "text-danger" }))
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <label for="accentroom1">Accent Room 1</label>
                @(Html.For(model).TextBoxFor(x => x.AccentRoom1,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentRoom1, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="accentroom">Accent Room 2</label>
                @(Html.For(model).TextBoxFor(x => x.AccentRoom2,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentRoom2, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="accentroom3">Accent Room 3</label>
                @(Html.For(model).TextBoxFor(x => x.AccentRoom3,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentRoom3, null, new { @class = "text-danger" }))
            </div>


        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <label for="accentwall1">Accent Wall 1</label>
                @(Html.For(model).TextBoxFor(x => x.AccentWall1,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentWall1, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="accentwall2">Accent Wall 2</label>
                @(Html.For(model).TextBoxFor(x => x.AccentWall2,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentWall2, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="accentwall3">Accent Wall 3</label>
                @(Html.For(model).TextBoxFor(x => x.AccentWall3,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.AccentWall3, null, new { @class = "text-danger" }))
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-4">
                <label for="rail">Rail</label>
                @(Html.For(model).TextBoxFor(x => x.Rail,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.Rail, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="nowelposts">Newel Posts</label>
                @(Html.For(model).TextBoxFor(x => x.NewelPosts,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.NewelPosts, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="treads">Treads</label>
                @(Html.For(model).TextBoxFor(x => x.Treads,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.Treads, null, new { @class = "text-danger" }))
            </div>


        </div>
        <div class="row">

            <div class="form-group col-md-4">
                <label for="crown">Crown</label>
                @(Html.For(model).TextBoxFor(x => x.Crown,ViewHelper.GetHtmlAttributes(disabled: !isAdmin)))
                @(Html.For(model).ValidationMessageFor(x => x.Crown, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="risers">Risers</label>
                @(Html.For(model).TextBoxFor(x => x.Risers,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.Risers, null, new { @class = "text-danger" }))
            </div>

            <div class="form-group col-md-4">
                <label for="stain">Stain</label>
                @(Html.For(model).TextBoxFor(x => x.Stain,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, maxLength: 50)))
                @(Html.For(model).ValidationMessageFor(x => x.Stain, null, new { @class = "text-danger" }))
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="Note">Note</label>
                @(Html.For(model).TextAreaFor(x => x.Note,ViewHelper.GetHtmlAttributes(disabled: !isAdmin, rows: 4)))
                @(Html.For(model).ValidationMessageFor(x => x.Note, null, new { @class = "text-danger" }))
            </div>

        </div>

        <div>
            <div class="form-group">
                @PageHelpers.CancelButton("InteriorColorSchemes", "Communities")
                @if (model.Id > 0 && isAdmin)
                {
                    <button class="btn btn-danger"
                            data-execute="true"
                            data-url="@Url.Action("DeleteInteriorColorScheme", "Communities")"
                            data-data="@((new {id = model.Id}).ToJson())"
                            data-confirmmessage="Are you sure you want to permanently delete @Model.ViewModel.InteriorBase?"
                            data-confirmtitle="Delete Color Scheme?"
                            data-redirect="@Url.Action("InteriorColorSchemes", "Communities")">
                        Delete
                    </button>
                }

                @if (isAdmin)
                {
                    <input value="Save" type="submit" class="btn btn-primary" />

                }
            </div>
        </div>
    }
</section>


