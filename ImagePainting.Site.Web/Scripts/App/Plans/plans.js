﻿var app = app || {};

app.plans = app.plans || {}; 

(function (plans, common) {
    'use strict';

    var builderId;
    var communities;

    plans.initPlans = function () {
        $('a#add-community').click(handleCommunityAdd);
        $('div.communities').on('change', 'select.communities-dropdown', handleCommunityChange);
        $('div.communities').on('click', 'button#community-delete', handleCommunityDelete)
        builderId = $('#BuilderId').val() || 0;
        getBuilderCommunities(builderId);
        $('input[type="submit"]').click(SavePlan);
    }

    function handleCommunityAdd() {
        var existingDropDown = $('select.communities-dropdown');

        if (existingDropDown.length > 0) {
            return;
        }

        var currentBuilderId = $('#BuilderId').val();

        if (currentBuilderId != builderId) {
            getBuilderCommunities(currentBuilderId, true);
            builderId = currentBuilderId;
        } else {
            if (!generateDropDown(communities)) {
                common.okDialog.show('No more communities available to be added for this this plan.', 'No more communities.');
            }
        }
    }

    function getBuilderCommunities(id, add) {
        $.get('/communities/BuilderCommunities/' + id, function (data) {
            communities = data;
            if (add) {
                if (!generateDropDown(communities)) {
                    common.okDialog.show('No more communities available to be added for this this plan.', 'No more communities.');
                }
            }
        });
    }

    function SavePlan(e) {
        e.preventDefault();
        var $form = $(e.currentTarget.closest('form'));
        renameAddedCommunities($form)

        $form.submit();
    }

    function handleCommunityDelete(e) {
        e.preventDefault();
        var $parentDiv = $(this).closest('div#community');
        $parentDiv.remove();
    }

    function handleCommunityChange(e) {
        var communityId = e.target.value;
        $(this).remove();
        var communityTag = getCommunityTag(communityId);
        $('div.communities').append(communityTag);
    }

    function getCommunityTag(communityId) {

        var communityCount = $('p#community').length;

        var community = communities.find(function (d) {
            return d.Id == communityId;
        });

        var communityTag = '<div id="community"><p class="hideable-container" id="community">' +
            '<span class="community" id="' + community.Id + '">' + community.Name + '</span>' +
            '<span class="hideable">' +
            '<button class="btn btn-danger btn-xs" id="community-delete">' +
            '<span class="glyphicon glyphicon-remove"></span></button></span></p>' +
            '<input class="hidden communityId" name="Communities[' + communityCount + '].Id" value="' + community.Id + '" />' +
            '<input class="hidden communityName" name="Communities[' + communityCount + '].Name" value="' + community.Name + '" />' +
            '<input class="hidden communityAbbreviation" name="Communities[' + communityCount + '].Abbreviation" value="' + community.Abbreviation + '" />' +
            '</div>';

        return communityTag;
    }

    function generateDropDown(data) {
        var distintCommunities = getDistintCommunities(data);

        if (distintCommunities.length < 1) {
            return false;
        }

        var options = '<option></option>';

        distintCommunities.forEach(function (d) {
            options += '<option value=' + d.Id + '> ' + d.Name + ' </option > ';
        });

        var select = '<select class="form-control communities-dropdown">' + options + '</select>';

        $('div.communities').append(select);

        $('select.communities-dropdown').blur(function () {
            $(this).remove();
        });

        return true;
    }

    function getDistintCommunities(data) {
        var existingCommunities = $('span.community');
        var existingCommunitiesIds = [];
        if (existingCommunities.length < 1) {
            return data;
        }

        existingCommunities.each(function (index, element) {
            existingCommunitiesIds.push(Number(element.id));
        });

        return data.filter(function (d) {
            return !(existingCommunitiesIds.indexOf(d.Id) > -1)
        });
    }

    function renameAddedCommunities($form) {

        $form.find('input.communityId').each(function (index, item) {
            item.name = 'Communities[' + index + '].Id';
        });

        $form.find('input.communityName').each(function (index, item) {
            item.name = 'Communities[' + index + '].Name';
        });

        $form.find('input.communityAbbreviation').each(function (index, item) {
            item.name = 'Communities[' + index + '].Abbreviation';
        });

    }


})(app.plans, app.common);