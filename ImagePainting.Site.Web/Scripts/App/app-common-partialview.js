﻿var app = app || {};

app.common = app.common || {};

(function (vm) {
    'use strict';

    var $mainContent = $('.main-content');
    var $partialViewContainer = $('.partial-content');

    vm.initPartialView = function () {
        processHash();

        $(window).on('hashchange', processHash);

        $partialViewContainer.on('click', '[data-dismiss=partialView]', vm.closePartialView);
    };

    vm.closePartialView = function () {
        $partialViewContainer.html('').hide(500);
        $mainContent.show(500);
        location.hash = '';
    };

    function processHash() {
        var hash = (location.hash || '#').substr(1);

        if (!hash || hash === 'no-navigate') {
            return;
        }

        var urlParts = hash.split('|');
        var url = urlParts[0];
        var loadPartialView;

        if (urlParts.length > 1 && urlParts[1]) {
            var module = urlParts[1];

            loadPartialView = app[module].enablePartialView;
        }

        (loadPartialView || enablePartialView)(url);
    }

    function enablePartialView(url) {
        $partialViewContainer.load(url, function () {
            $mainContent.hide(500);
            $partialViewContainer.show(500);
        });
    }
})(app.common);

