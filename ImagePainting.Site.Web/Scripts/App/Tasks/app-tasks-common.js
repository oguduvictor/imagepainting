﻿var app = app || {};

app.tasksCommon = app.tasksCommon || {};

(function (vm, common) {
    'use strict';
    vm.beginTimeString = 'beginTime';
    vm.endTimeString = 'endtime';

    vm.confirmCrewNotBusy = function (crewId, date) {
        var deferred = $.Deferred();

        $.get('/tasks/GetBusyUser', { crewId: crewId, date: date }).always(function (response) {
            // Note: user object is returned when a user is found.
            // However, if no user is found it returns a response object
            var isUserObject = response.hasOwnProperty('FirstName');

            if (!isUserObject && (response.status > 300 || !response.responseText)) {
                deferred.resolve(true);
                return;
            }

            var user = isUserObject ? response : JSON.parse(response.responseText);
            var dateArray = date.split('-');
            var dateStr = (new Date(dateArray[0], Number(dateArray[1]) - 1, dateArray[2])).toDateString();
            var message = '"' + user.FullName + '" is currently marked busy for "' + dateStr + '", do you want to continue?';

            common.confirmDialog.show(message, 'Confirm Assignment of Busy Crew')
                .done(function (ok) {
                    deferred.resolve(true);
                })
                .fail(function (cancel) {
                    deferred.reject(false);
                });
        });

        return deferred.promise();
    };

    vm.ValidateTaskTimeRange = function (startTime, endTime, buttonToDisable = null) {
      var timeIsValid; // returns if the supplied start and endtime is valid;
      isTimeRangeValid(startTime,
        endTime,
        function(isValid, inValidInput) {
          if (!isValid) {
            if (buttonToDisable) {
              buttonToDisable.prop('disabled', true);
            }
          }
          if (isValid) {

            startTime.removeClass('border-danger');
            endTime.removeClass('border-danger');
            if (buttonToDisable) {
              buttonToDisable.removeProp('disabled');
            }

          } else if (!isValid && inValidInput == vm.endTimeString) {
            endTime.addClass('border-danger');

          } else {
            startTime.addClass('border-danger');
          }
          timeIsValid = isValid;
        });
      
      function isTimeRangeValid(startTime, endTime, callBack) {
        var startHour = startTime.val().split(':')[0];
        var endHour = endTime.val().split(':')[0];

        if (!isNaN(startHour)) {
          startHour = parseInt(startHour);
        }

        if (!isNaN(endHour)) {
          endHour = parseInt(endHour);
        }

        if (startHour && !endHour) {
          callBack(true, null);
          return;
        }

        if (!startHour && endHour) {
          callBack(false, vm.beginTimeString);
          return;
        }

        if (endHour >= startHour) {
          callBack(true, null);

        } else if (endHour < startHour) {
            callBack(false, vm.endTimeString);

        } else {
          callBack(false, vm.beginTimeString);
        }
      }

      return timeIsValid;
    }

})(app.tasksCommon, app.common);
