﻿var app = app || {};

app.task = app.task || {};

(function (vm, viewTaskCommon) {
    'use strict';

    vm.initWorkOrderTask = function () {
        viewTaskCommon.init(reloadPage);

        // method for performing reload when task is saved, completed or unlocked
        function reloadPage(d) {
            if (d) {
                toastr.success('Task - ' + d.Name + ' was updated successfully');
            }

            location.reload();
        }
    };

})(app.task, app.viewTaskCommon);
