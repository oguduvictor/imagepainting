﻿var app = app || {};

app.task = app.task || {};

(function (task, common) {
    'use strict';

    var $taskItemContainer = $('div#task-item');
    var $newTaskItemButton = $('button#new-taskItem');

    var newButtonsState;

    task.initTask = function () {
        $('button#new-taskItem').click(addNewItem);
        $('button#delete-task').click(removeItem);
        $('#submitBtn').click(reAssignNames);

        common.initDraggables();
    }

    task.initDataTable = function () {
        var table= $('table#schemaTable').DataTable({
            rowGroup: {
                dataSrc: '1'
            },
            columnDefs: [
                { "visible": false, "targets": 1 }
            ],
            "pageLength": 50
        });

        $('table#schemaTable').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });

        //trigger grouping
        $('table#schemaTable').find('tr:eq(1)').trigger('click');
    }

    function reAssignNames() {
        $('.task-name').each(function (index, item) {
            item.name = 'TaskItems[' + index + '].Name';

            var $row = $(item).parents('div.row');
            $row.attr({ id: 'taskItem_' + index });
        });

        $('.task-type').each(function (index, item) {
            item.name = 'TaskItems[' + index + '].DataType'
        });
    }

    function addNewItem(e) {
        e.preventDefault();

        processNewItem(e.target, $taskItemContainer);
    }

    function processNewItem(target, container) {
        var itemHtml = getForm();
        container.append(itemHtml);

        validateNewInput(target);
    }

    function validateNewInput(target) {
        var $lastTypeItem = $('input.task-name').last();
        var $typeDeleteButton = $('button.delete-task').last();
        var $Savebutton = $('#submitBtn');

        target.disabled = true;
        newButtonsState = $newTaskItemButton.prop("disabled");
        $Savebutton.attr('disabled', newButtonsState);

        $lastTypeItem.on('keyup', function () {
            if (Boolean($lastTypeItem.val())) {
                target.disabled = false;
                newButtonsState = $newTaskItemButton.prop("disabled");
                $Savebutton.attr('disabled', newButtonsState);
            }
            else {
                target.disabled = true;
                newButtonsState = $newTaskItemButton.prop("disabled");
                $Savebutton.attr('disabled', newButtonsState);
            }
        });

        $typeDeleteButton.on('click', function (e) {
            target.disabled = false;
            newButtonsState = $newTaskItemButton.prop("disabled");
            $Savebutton.attr('disabled', newButtonsState);
            removeItem(e);
        });
    }

    function removeItem(e) {
        e.preventDefault();

        $(e.currentTarget).parents('div.row').remove();
        reAssignNames();
    }

    function getForm() {
        var index = $taskItemContainer.children().length;

        return getTemplate(index);
    }

    function getTemplate(index) {
        var name = 'TaskItems[' + index + ']';

        var options = '<option value ="YesNo">Yes/No</option>';

        options += '<option value ="Number">Number</option>';
        options += '<option value ="Text">Text</option>';
        options += '<option value ="Note">Note</option>';

        var nameLabel = '<div class="col-md-3"><label for="name">Item Name</label></div>';
        var dataTypeLabel = '<div class="col-md-3"><label for="name">Data Type</label></div>';
        var input = '<div class="col-md-9"><input type="text" name=' + name + '.Name class="form-control task-name" /></div>';
        var dataTypeSelect = '<div class="col-md-9"><select name=' + name + '.DataType class="form-control task-type">' + options + '</select></div>';
        var deletebutton = '<button class="btn btn-danger delete-task" id=delete-task><i class="glyphicon glyphicon-remove"></i></button>';

        var textField = '<div class="form-group col-md-5">' + nameLabel + input + '</div>';
        var dropdownfield = '<div class="form-group col-md-5">' + dataTypeLabel + dataTypeSelect + '</div>';
        var deleteField = '<div class="form-group col-md-2">' + deletebutton + '</div>';
        var newItemField = '<div class="row draggable" draggable="true" id="taskItem_' + index + '">' + textField + dropdownfield + deleteField + '</div>';

        return newItemField;
    }

})(app.task, app.common)
