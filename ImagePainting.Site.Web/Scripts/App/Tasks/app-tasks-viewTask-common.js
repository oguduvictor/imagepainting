﻿var app = app || {};

app.viewTaskCommon = app.viewTaskCommon || {};

(function (vm, common, tasksCommon, modalDialog) {
    'use strict';

    var taskId;
  

    vm.init = function (reloadPage) {
        vm.initWorkOrderTask(reloadPage);
        initWorkItems();
        common.initDraggables();
    };

    vm.initWorkOrderTask = function (reloadPage) {
        $('button.btn-save').click(saveTask);
        $('button.btn-complete').click(completeTask);
        $('button.btn-unlock').click(unlockTask);
        $('button.btn-add-new').click(addNewTaskItem);
        $('button.btn-sign-document').click(displayDocumentModal);
        $('.item-checkbox').change(toggleCompleteBtn);
        $('span.btn-edit-note').click(showNoteTextArea);
        $('div.form').on('click', 'i.btn.btn-danger', removeTaskItem);
        $('div.form').on('keyup', '.new-input', toggleNewItemBtn);
        $('input#BeginDate').change(handleTimeChange);
        $('input#EndDate').change(handleTimeChange);
        $('a.download-document').click(function(e) {
          $(this).attr('disabled', true);
        });
        $('input#deferred-checkbox').click(handleDeferredCheckBoxClicked)


        function handleDeferredCheckBoxClicked(e) {
            var target = e.currentTarget;
            var id = $('label.deferred-label').data('taskid');
            var taskName = $('a[href="#collapseTwo"]').text().trim();
            var deferredSymbolElement = $('#deferred-symbol' + id);
            var heading = "";
            var message = "";

            if (target.checked) {
                heading = "Confirm Defer of " + taskName;
                message = 'If you check the "Defer" checkbox no work can be performed on this task until the box is unchecked. Click OK to continue';
            } else {
                heading = "Confirm Undefer of " + taskName;
                message = 'Are you sure you want to undefer this task?';
            }

            common.confirmDialog.show(message, heading)
                .done(function (ok) {
                    $.post('/tasks/deferTask/' + id).done(function () {

                        if (target.checked) {
                            deferredSymbolElement.html('D');
                        } else {
                            deferredSymbolElement.empty();
                        }

                        reloadPage()
                    });
                })
                .fail(function (cancel) {
                    target.checked = !target.checked;
                });
        }

        
        function handleTimeChange(e) {
            var $form = getParentForm(e);
            var startDate = $form.find('input#BeginDate');
            var endDate = $form.find('input#EndDate');
            var $savebutton = $(e.currentTarget).closest('.panel').find('button.btn-save');

            tasksCommon.ValidateTaskTimeRange(startDate, endDate, $savebutton);
        }
      

        function displayDocumentModal() {
          var modal = new modalDialog();
          var taskId = $('#mainTask').val();
          
          var buttons = [{
            label: "Cancel", btnClass: "btn btn-default", action: function () {
              
              modal.hide();
            }
          },
            {
              label: "Ok", btnClass: "btn create-pdf btn-primary", action: function () {
                convertTaskPageToPdf(taskId);
                modal.hide();
              }
            }];
          $.get('/tasks/GetTaskPdfTemplate/?taskid=' + taskId).done(function (content) {
            modal.show(content, "Sign Document", buttons, 'lg');
            triggerSignatureContainer();
          });
          
        }

        function triggerSignatureContainer() {
          var $signDiv = $('div.signature-container');

          var $ok = $('.modal-footer').find('button.create-pdf');
          $ok.prop('disabled', true);

          if ($signDiv.length > 0) {
            $signDiv.jSignature({ 'decor-color': 'transparent', 'height': 200 });
          }

          $signDiv.bind('change',
            function(e) {
              if ($signDiv.jSignature('getData', 'native').length > 0) {
                $ok.removeProp('disabled');
              }
            });
        }

        function getSignature() {
          var $signatureDiv = $('div.signature-container');
          
          var signature = $signatureDiv.jSignature("getData", "svgbase64");
          var encodedimage = "data:" + signature[0] + "," + signature[1]; // create the base64 encoded svg image;
          
          return encodedimage;
        }

        function convertTaskPageToPdf(taskId) {
          var encodedimage = getSignature();
          if (encodedimage) {
            $.post('/tasks/CreateTaskPagePdf/?taskid=' + taskId, {encodedimage : encodedimage}).done(function () {
              reloadPage();
            });
          }
        }

        function saveTask(evt) {
            postForm(evt, '/tasks/saveWorkOrderTask');
        }

        function showNoteTextArea() {
            var $noteItem = $(this).parents('div.note-container');

            $noteItem.find('textarea[name^=TaskItems]').removeClass('hidden');
            $noteItem.find('textarea[name^=taskNote]').hide();
        }

        function toggleCompleteBtn(evt) {
            var $panel = $(evt.currentTarget).closest('.panel');
            var $form = $panel.closest('div.form');
            var $completeBtn = $panel.find('button.btn-complete');
            var unCheckedBoxes = $panel.find('input.item-checkbox:not(:checked)').length;

            $completeBtn.prop('disabled', unCheckedBoxes > 0);
        }

        function completeTask(evt) {
            evt.preventDefault();

            var isMainTask = $('#is-main-task').val() === 'True';
            var taskId = $('#Id').val();
            var completionUrl = '/tasks/completeTask';

            if (!isMainTask) {
                postForm(evt, completionUrl);
                return;
            }

            $.get('/purchaseorders/purchaseorders/?taskId=' + taskId + '&emailed=false')
                .done(function (pos) {
                    if (!pos || !pos.length) {
                        postForm(evt, completionUrl);
                    } else {
                        var message = unsentEmailMessage(pos);

                        common.okDialog.show(message, 'Task Completion Failed')
                        .done(function () {
                            //do nothing.
                        });
                    }
                })
                .error(function (err) {
                    toastr.error('Error occurred while trying to complete your task. Please try again later.');
                    console.error(err);
                });
        }

        function unsentEmailMessage(data) {
            var pos = [];

            for (var i = 0; i < data.length; i++) {
                pos.push('"PO ' + data[i].PoNumber + '"');
            }

            return 'This task cannot be set to complete because the following purchase order(s) have not been emailed: ' + pos.join(', ');
        }

        function unlockTask(evt) {
            evt.preventDefault();

            var $form = getParentForm(evt);
            var id = $form.children('#Id').val();

            $.post('/tasks/unlockTask/' + id).done(reloadPage);
        }

        function addNewTaskItem(evt) {
            evt.preventDefault();

            var $form = getParentForm(evt);
            var taskItemLength = $form.find('.task-item').length;

            var options = '<option value ="YesNo">Yes/No</option>';
            options += '<option value ="Number">Number</option>';
            options += '<option value ="Text">Text</option>';
            options += '<option value ="Note">Note</option>';

            var inputField = '<div class="col-sm-7"><input type="text" name="TaskItems[' + taskItemLength + '].Name"' +
            ' class="form-control new-input task-item" placeholder="Enter Task Name" /></div>';

            var selectList = '<div class="col-sm-4"><select name="TaskItems[' + taskItemLength + '].DataType" class="form-control task-dataType">'
                + options + '</select></div>';
            var hiddenInput = '<input type="hidden" name="TaskItems[' + taskItemLength + '].Value" class="task-value" />';
            var removeBtn = '<div class="col-sm-1"><i class="btn btn-danger glyphicon glyphicon-remove" id="removeItem"></i></div>';
            var html = '<div class="row form-group item-container" id="newItemRow">' + inputField + hiddenInput + selectList + removeBtn + '</div>';

            $(evt.currentTarget).prop('disabled', true);
            $form.find('div.new-items').append(html);
        }

        function removeTaskItem(evt) {
            var $panel = $(evt.currentTarget).closest('div.panel');

            $(evt.currentTarget).closest('div.form-group').remove();
            toggleNewItemBtn(evt, $panel);
        }

        function toggleNewItemBtn(evt, $panel) {
            $panel = $panel || $(evt.currentTarget).closest('div.panel');

            if ($panel.find('div.new-items .new-input').last().val() === '') {
                $panel.find('button.btn-add-new').prop('disabled', true);
            }
            else {
                $panel.find('button.btn-add-new').removeProp('disabled');
            }
        }

        /* Helper Methods */
        function postForm(evt, url) {
            evt.preventDefault();

            var $form = getParentForm(evt);
            var crewId = $form.find('#CrewId').val();
            var date = $form.find('#ScheduleDate').val();

            tasksCommon.confirmCrewNotBusy(crewId, date).done(function (ok) {
              renameFormData($form);

              var formData = common.serializeForm($form);
              formData += getBeginAndEndTime(date, $form);

                $.post(url, formData).done(reloadPage);
            });
        }

        function getBeginAndEndTime(date, $form) {
          var startdate = $form.find("input#BeginDate").val();
          var enddate = $form.find("input#EndDate").val();

          var startDateTime = date + ' ' + startdate;
          var endDateTime = date + ' ' + enddate;

          return '&ScheduleDateRange.EndDate=' + endDateTime + '&ScheduleDateRange.BeginDate=' + startDateTime;
        }
      
        function renameFormData($form) {
            $form.find('.task-value').each(function (index, item) {
                if ($(item).hasClass('checkbox')) {
                    item.value = item.checked;
                    $(item).next().val(item.value);
                }
                item.name = 'TaskItems[' + index + '].Value';
            });

            $form.find('.task-item').each(function (index, item) {
                item.name = 'TaskItems[' + index + '].Name';
            });

            $form.find('.task-dataType').each(function (index, item) {
                item.name = 'TaskItems[' + index + '].DataType';
            });
        }

        function getParentForm(evt) {
            return $(evt.currentTarget)
                    .closest('div.panel')
                    .find('div.form');
        }
    };

    function initWorkItems() {
        taskId = $('#workItems').data('taskid');

        $('#workItems #SelectAll').change(handleSelectAllChange);
        $('#workItems table tbody input[type="checkbox"]').change(handleCheckBoxChange);
        $('#workItems table tbody input[type="checkbox"]').change(toggleSelectAll);

        function handleSelectAllChange(e) {
            var target = e.currentTarget;
            var workItems = [];

            $('#workItems table tbody input[type="checkbox"]').each(function (index, item) {
                item.checked = target.checked;
                workItems.push({
                    id: item.id,
                    selected: target.checked
                });
            });

            $.post('workorders/saveworkitems', { taskId: taskId, workitems: workItems }).done(refreshLaborPricing);
        }

        function handleCheckBoxChange(e) {
            var target = e.currentTarget;
            var workitem = {
                id: target.id,
                selected: target.checked
            };

            $.post('workorders/saveworkitems', { taskId: taskId, workitems: [workitem] }).done(refreshLaborPricing);
        }

        function toggleSelectAll(e) {
            $('#workItems #SelectAll')[0].checked = $('#workItems tbody tr').length === $('#workItems tbody input[type="checkbox"]:checked').length;
        }

        function refreshLaborPricing() {
            $('div.pricing-container').load('/WorkOrders/TaskLaborPricing/', $.param({ taskId: taskId }));
        }
    }

})(app.viewTaskCommon, app.common, app.tasksCommon, app.ModalDialog);
