﻿var app = app || {};

app.task = app.task || {};
app.tasksCommon = app.tasksCommon || {};

(function (vm, common, ModalDialog, tasksCommon) {
    'use strict';
    var users;
    var calenderPageFilter;

    vm.initTaskButtons = function () {
        getUserOptions();
        $('div#result').on('click', 'div.upcoming-task div.assignedTask', toggleSelectedTask);
        $('div#result').on('click', 'div.panel.upcoming-task', toggleMailerButton);
        $('div#result').on('click', 'button.mailer', sendMailToCrew);
        $('div#result').on('change', 'input.check-all', checkAllTasks);
        $('.schedule-popover-button').click(triggerSchedulePopover);
        $('.busy-popover-button').click(popoverForUnassignedCrewStatus);
    };

    vm.setDateRange = function (dateTimeRange) {
        $('#DateRange_BeginDateTime').val(dateTimeRange.BeginDateTime.split('T')[0]);
        $('#DateRange_EndDateTime').val(dateTimeRange.EndDateTime.split('T')[0]);
    };

    vm.initMultiSelect = function () {
        var savedFilterSettings = getFilterSettings();

        if (savedFilterSettings) {
            restoreFilterValues(savedFilterSettings);

            setSearchCriteria(savedFilterSettings);
            getFilteredTasks(savedFilterSettings);
        }

        $('#CrewIds').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            nonSelectedText: 'Everyone',
            allSelectedText: 'Everyone',
            selectAllText: 'Everyone',
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            buttonWidth: '100%'
        });

        $('#CommunityIds').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            nonSelectedText: 'All Communities',
            allSelectedText: 'All Communities',
            selectAllText: 'All Communities',
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            buttonWidth: '100%'
        });

        $('#TimePeriod').multiselect({
            buttonWidth: '100%',
            onChange: function (option, checked, select) {
                var checkedOption = option.val();
                
                if (checkedOption === 'Daily') {
                    $('#DateRange_EndDateTime').val(null);
                }
                else if (checkedOption) {
                    $('#DateRange_BeginDateTime, #DateRange_EndDateTime').val(null);
                }

                toggleNavigationButton();
            }
        });

        $('#TaskSchemaIds').multiselect({
            buttonWidth: '100%',
            enableFiltering: true,
            includeSelectAllOption: true,
            nonSelectedText: 'All Tasks',
            allSelectedText: 'All Tasks',
            selectAllText: 'All Tasks',
            onChange: function (option, checked, select) {
                if (option.html() === 'All Main Tasks' && !!checked) {
                    checkMainTasks(option.val(), true);
                }
                else if (option.html() === 'All Main Tasks' && !checked) {
                    checkMainTasks(option.val(), false);
                }
            }
        });

        $('.multiselect-container.dropdown-menu').css('width', '100%');

        $('#DateRange_BeginDateTime, #DateRange_EndDateTime').change(handleDateChanged);

        $('.task-navigator, button#searchBtn').click(handleSearchTasks);

        function getFilterSettings() {
            var location = window.location.pathname;

            switch (true) {
                case common.endsWith(location, '/Tasks/AllTasks'):
                    calenderPageFilter = 'AllTasksFilterSettings';
                    break;
                case common.endsWith(location, '/Tasks/UpcomingTasks'):
                    calenderPageFilter = 'UpcomingTasksFilterSettings';
                    break;
                case common.endsWith(location, '/Tasks/CompletedTasks'):
                    calenderPageFilter = 'CompletedTasksFilterSettings';
                    break;
                case common.endsWith(location, '/Tasks/OverdueTasks'):
                    calenderPageFilter = 'OverdueTasksFilterSettings';
                    break;
                default:
                    break;
            }

            return JSON.parse(localStorage.getItem(calenderPageFilter))
                ||
                {
                    Grouping: 'Weekly',
                    TaskSchemaIds: JSON.parse($('select#TaskSchemaIds option:contains("All Main Tasks")').val())
                };
        }

        function checkMainTasks(values, checkedValue) {
            var mainTaskIds = JSON.parse(values);
            if (checkedValue) {
                return $('#TaskSchemaIds').multiselect('select', mainTaskIds);
            }

            $('#TaskSchemaIds').multiselect('deselect', mainTaskIds);
        }

        function restoreFilterValues(savedFilterSettings) {
            var filterSettingOptions = {
                CommunityIds: '#CommunityIds',
                CrewIds: '#CrewIds',
                TaskSchemaIds: '#TaskSchemaIds',
                Grouping: '#TimePeriod',
                WorkOrderName: '#WorkOrder',
                'DateRange.BeginDateTime': '#DateRange_BeginDateTime',
                'DateRange.EndDateTime': '#DateRange_EndDateTime'
            };

            for (var option in filterSettingOptions) {
                if ((option === 'DateRange.BeginDateTime' || option === 'DateRange.EndDateTime') && !!option) {
                    var $selectedDate = $('input[type="date"][name="' + option + '"]');

                    $selectedDate.val(savedFilterSettings.DateRange && savedFilterSettings.DateRange[option.slice(option.indexOf('.') + 1)]);
                } else {
                    var filterSettingValue = savedFilterSettings[option];

                    if (option === 'TaskSchemaIds' && savedFilterSettings[option].length) {
                        filterSettingValue = setMainTasksCheckedValue(filterSettingValue);
                    }
                    $(filterSettingOptions[option]).val(filterSettingValue);
                }
            }
        }

        function toggleAllMainTaskCheck(allSelectedTasksIds, mainTasksIds) {
            var mainTasksChecked = arrayIsContainedIn(allSelectedTasksIds, mainTasksIds);
            var $mainTaskCheckBox = $('#tasks label.checkbox:contains("All Main Tasks")').children('input');
            var $mainTaskOption = $mainTaskCheckBox.closest('li');

            $('#TaskSchemaIds option:contains("All Main Tasks")').prop('selected', mainTasksChecked);
            
            if (mainTasksChecked) {
                $mainTaskOption.addClass('active');
            }
            else {
                $mainTaskOption.removeClass('active');
            }
            $mainTaskCheckBox.prop('checked', mainTasksChecked);
        }

        function setMainTasksCheckedValue(selectedTasksIds) {
            var taskids = Array.from(selectedTasksIds);
            var mainTasksIds = JSON.parse($('#TaskSchemaIds option:contains("All Main Tasks")').val());
            var mainTaskChecked = arrayIsContainedIn(selectedTasksIds.map(function (x) { return Number(x); }), mainTasksIds);

            if (mainTaskChecked) {
                taskids.push(JSON.stringify(mainTasksIds));
            }

            return taskids;
        }

        function arrayIsContainedIn(superSetArray, subSetArray) {
            if (subSetArray.length > superSetArray.length) {
                return false;
            }

            return subSetArray.every(function (value) {
                return superSetArray.indexOf(value) >= 0;
            });
        }

        function handleDateChanged(e) {
            var initiator = e.currentTarget;

            if (initiator.id === 'DateRange_EndDateTime' || (initiator.id === 'DateRange_BeginDateTime' && $('#TimePeriod').val() !== 'Daily')) {
                $('input[type="radio"][value=""]').trigger('click');
            }
        }

        function toggleNavigationButton() {
            if (!$('#TimePeriod').val() || ($('#DateRange_BeginDateTime').val() && $('#DateRange_EndDateTime').val())) {
                $('a.task-navigator').addClass('hidden');
            }
            else {
                $('a.task-navigator').removeClass('hidden');
            }
        }
        
        function handleNavigationWithDate(e) {
            var dateString = $('#DateRange_BeginDateTime').val();

            if (dateString.length === 0 || typeof e === 'undefined' || !$(e.target).hasClass('task-navigator')) {
                return false;
            }
            var navTimeInSeconds = ($(e.target).hasClass('navigator-right') ? 1 : -1) * 86400000;
            var dateObject = new Date(dateString);

            dateObject.setTime(dateObject.getTime() + navTimeInSeconds);

            $('#DateRange_BeginDateTime').val(dateObject.getFullYear() + '-' + ('0' + (dateObject.getMonth() + 1)).slice(-2) + '-' + ('0' + dateObject.getDate()).slice(-2));
            $('#DateRange_BeginDateTime').trigger('change');
            handleSearchTasks();

            return true;
        }

        function handleSearchTasks(e) {
            if (handleNavigationWithDate(e)) {
                return;
            }

            var beginDate = $('#DateRange_BeginDateTime').val();
            var endDate = $('#DateRange_EndDateTime').val();
            var grouping = $('#TimePeriod').val();

            if (grouping && grouping !== 'Daily') {
                $('#DateRange_EndDateTime').val('');
            }
            
            if ((!grouping && beginDate && !endDate) || (grouping === 'Daily' && !beginDate) || (grouping && grouping !== 'Daily' && beginDate && endDate)) {
                return;
            }

            var communityIds = $('#CommunityIds :selected').toArray().map(function (x) { return x.value; });
            var crewIds = $('#CrewIds :selected').toArray().map(function (x) { return x.value; });
            var taskSchemaIds = $('#TaskSchemaIds :selected').toArray().filter(function (x) { return !!Number(x.value); }).map(function (x) { return x.value; });
            var workOrderName = $('#WorkOrder').val();
            var mainTasksIds = JSON.parse($('#TaskSchemaIds option:contains("All Main Task")').val());
            var dateRange;

            toggleAllMainTaskCheck(taskSchemaIds.map(function (x) { return Number(x); }), mainTasksIds);

            var pageNumber = 0;

            if (e) {
                pageNumber = $(e.currentTarget).data('pagenumber');
            }
            else {
                pageNumber = $('#contents').data('pagenumber');
            }

            if (beginDate) {
                dateRange = {
                    BeginDateTime: beginDate,
                    EndDateTime: endDate
                };   
            }
            
            if (!pageNumber) {
                pageNumber = 0;
            }

            var requestPayload = {
                CommunityIds: communityIds,
                CrewIds: crewIds,
                TaskSchemaIds: taskSchemaIds,
                Grouping: grouping,
                PageNumber: pageNumber,
                DateRange: dateRange,
                WorkOrderName: workOrderName
            };

            localStorage.setItem(calenderPageFilter, JSON.stringify(requestPayload));

            setSearchCriteria(requestPayload);

            getFilteredTasks(requestPayload);
        }

        function getFilteredTasks(requestPayload) {
            var url = $('#result').data('url');

            $('#result').load(url, requestPayload, function () {
                toggleNavigationButton();
                $('#panelTimeSpanText').text($('#timeSpanText').val());
                $('.task-navigator').click(handleSearchTasks);
                $('.schedule-popover-button').click(triggerSchedulePopover);
                $('.busy-popover-button').click(popoverForUnassignedCrewStatus);
            });
        }

        function setSearchCriteria(requestPayLoad) {
            var location = window.location.pathname;
            switch (true) {
                case common.endsWith(location, '/Tasks/AllTasks'):
                    requestPayLoad.OrderByAscending = true;
                    break;
                case common.endsWith(location, '/Tasks/UpcomingTasks'):
                    requestPayLoad.IsUpcoming = true;
                    requestPayLoad.OrderByAscending = true;
                    break;
                case common.endsWith(location, '/Tasks/CompletedTasks'):
                    requestPayLoad.IsCompleted = true;
                    requestPayLoad.OrderByAscending = false;
                    break;
                case common.endsWith(location, '/Tasks/OverdueTasks'):
                    requestPayLoad.IsOverdued = true;
                    requestPayLoad.OrderByAscending = false;
                    break;
                default:
                    break;
            }
        }
    };

    function handleSchedulePopoverSubmit(e) {
        e.preventDefault();

        var crewId = $(this).siblings('.input-group:last').find('select').val();
        var date = $(this).siblings('.input-group:first').find('input').val();
        var data = $(this).closest('div.row').data('workorder');
        var startTime = $(this).siblings('#start-time').find('input').val();
        var endTime = $(this).siblings('#end-time').find('input').val();

        var message = 'Are you sure you want to change the schedule for "' + data.taskTitle + '" ?';

        common.confirmDialog.show(message, 'Change Task Schedule')
            .done(function (d) {
                // workaround to remove modal
                $('.modal, .modal-backdrop').remove();
                tasksCommon.confirmCrewNotBusy(crewId, date).done(function (ok) {
                    reScheduleTask(data, crewId, date, startTime, endTime);
                });
            });
    }

    function popoverForUnassignedCrewStatus() {
        $(this).popover({
            title: 'Change Busy Status',
            content: getUnassignedCrewStatusContent($(this)),
            html: true,
            trigger: 'manual',
            placement: 'bottom'
        });
        $(this).popover('toggle');
        $('p.crew-name').on('click', 'button.crew-status', updateCrewStatus);
    }

    function getUnassignedCrewStatusContent($target) {
        var btnText = $target.data('crewsetting').busy === "True" ? "Mark Available" : "Mark Busy";

        return '<button type="button" class="btn btn-primary col-xs-12 crew-status">' + btnText + '</button>';
    }

    function triggerSchedulePopover() {
        $(this).popover({
            title: 'Change Crew or Date',
            content: getSchedulePopOverContent($(this)),
            html: true,
            trigger: 'manual',
            placement: 'bottom'
        });
        $(this).popover('toggle');

        var $clickedPopOverButton = $(this);
        $('div.assignedTask').off('change', 'input.start-time, input.end-time').on('change', 'input.start-time, input.end-time',
          function() {
            validateTaskTimeRange($clickedPopOverButton);
          });
       
        $('div.assignedTask, div.unAssignedTask').on('click', 'button.popbutton', handleSchedulePopoverSubmit);
    }

    function validateTaskTimeRange(evt) {
      var startTime = evt.siblings('div.popover').find('input.start-time');
      var endTime = evt.siblings('div.popover').find('input.end-time');
      var $saveButton = evt.siblings('div.popover').find('button.popbutton');
      tasksCommon.ValidateTaskTimeRange(startTime, endTime, $saveButton);
    }

    function getSchedulePopOverContent(target) {
        var data = target.closest('div.row').data('workorder');
        var dropdown = generateUsersDropDown(data.crewId);
        var startTime = data.startTime;
        var endTime = data.endTime; 

        return '<div class="input-group">' +
                        '<div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>' +
                        '<input type="date" class="form-control date-picker" value="' + data.date + '" min="' + data.maxDate +'"/>' +
                '</div>' +
                              '<div class="input-group" id="start-time">' +
                                  '<div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>' +
                                    '<input type="time" class="form-control start-time" value="'+ startTime + '"/>' +
                                  '</div>' + 
                              '</div>' + 
                                '<div class="input-group" id="end-time">' +
                                    '<div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>' +
                                       '<input type="time" class="form-control end-time" value="'+ endTime +'"/>' +
                                    '</div>' +
                                '</div>' +
                      '<div class="input-group">' +
                        '<div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>' + dropdown +
                      '</div>' +
                      '<button class="btn btn-primary col-xs-12 popbutton"><i class="glyphicon glyphicon-floppy-disk"></i> Save </button>';
    }
    
    function getUserOptions() {
        $.get('/users/staff', function (d) { users = d; });
    }

    function updateCrewStatus(e) {
        e.preventDefault();
        var data = $(e.delegateTarget).find('.busy-popover-button').data('crewsetting');

        $.post('/tasks/UpdateUnassignedCrewStatus/', { crewId: data.crewId, scheduledDate: data.scheduledDate, status: data.busy === 'True' ? false : true })
            .done(function(d) { if (d) { location.reload(); } });
    }
    
    function generateUsersDropDown(currentCrewId) {
        var options = '<option></option>';
        users.forEach(function (user) {
            options += '<option' + (user.Id === currentCrewId ? ' selected': '') + ' value= ' + user.Id + ' > ' + user.FullName + '</option > ';
        });

        return '<select class="form-control users-select">' + options + '</select>';
    }
    
    function reScheduleTask(data, userId, date, startTime, endTime) {
        var __RequestVerificationToken = $('input[name="__RequestVerificationToken"]').val();
        var endDateTime = date + " " + endTime;  

        date += " " + startTime;
        var taskmodel = {
            scheduleDate: data.date,
            crewId: data.crewId,
            TaskId: data.taskId,
            scheduleEndDate : endDateTime
        };

        taskmodel.crewId = userId;
        
        if (date) {
            taskmodel.scheduleDate = date;
        }
        
        $.post('/tasks/UpdateTaskSchedule/',
            $.param({ task: taskmodel, crewAndDateOnly: true,
                __RequestVerificationToken: __RequestVerificationToken })).done(reloadPage);
    }

    function toggleSelectedTask(e) {
        if (e.target.classList.contains('popbutton')) return;

        var $target = $(e.currentTarget);

        $target.toggleClass('selected-task active');

        var $panel = $target.parents('.panel.upcoming-task');
        var $selectedDiv = $panel.find('div.assignedTask');
        var $selected = $.grep($selectedDiv, function (p, i) {
            return $(p).hasClass('selected-task');
        });

        var $checkbox = $target.parents('.panel.upcoming-task').find('input:checkbox');

        $checkbox.prop('checked', $selectedDiv.length === $selected.length);
    }

    function checkAllTasks() {
        var $panel = $(this).parents('.panel');
        var $selectedDiv = $panel.find('div.assignedTask');
        var noClassCounter = 0;

        $selectedDiv.each(function (index, item) {
            if (item.className.indexOf('selected-task') < 0) {
                $(item).addClass('selected-task active');
                noClassCounter++;
            }
        });

        if (noClassCounter === 0) {
            $selectedDiv.toggleClass('selected-task active');
        }

        $panel.trigger('click');
    }

    function toggleMailerButton(e) {
        if (e.target.classList.contains('popbutton')) return;
       
        var $target = $(e.currentTarget);
        var $checkBox = $target.find('.check-all');
        var $assignedTasks = $target.find('.assignedTask');
        var $selectedTasks = $target.find('div.selected-task');

        if ($selectedTasks.length === 0) {
            $target.find('button.mailer').attr('disabled', true);
        }
        else {
            $target.find('button.mailer').removeAttr('disabled');
        }

        if ($assignedTasks.length > $selectedTasks.length) {
            return $checkBox.removeProp('checked');
        }

        return $checkBox.prop('checked', 'checked');
    }

    function sendMailToCrew() {
        var selectedTaskNames = $(this).parents('.panel').find('div.selected-task p.taskName');
        var tasks = [];
        var freeCrewIds = [];

        selectedTaskNames.each(function (index, item) {
            if (item.id !== "0") {
                tasks.push({
                    'id': item.id, 'name': item.innerText, 'workOrderName': item.dataset['workordername'],
                     'taskTimeStamp': item.dataset['tasktime']});
            }
        });
        
        var freeCrewTasks = $(this).parents('.panel').find('div.selected-task.free-crew');
        var freeCrewsWorkOrder = freeCrewTasks.first().data('workorder');
        var taskDate = freeCrewsWorkOrder && freeCrewsWorkOrder.date;

        freeCrewTasks.each(function (index, item) {
            var data = JSON.parse(item.dataset['workorder']);
            freeCrewIds.push(data.crewId);
        });

        activateMailDialog(tasks, freeCrewIds, taskDate);
    }

    function activateMailDialog(tasks, freeCrewIds, taskDate) {
        var modal = new ModalDialog();
        var content = getModalContent(tasks, freeCrewIds.length > 0);
        var buttons = [{ label: "Cancel", btnClass: "btn btn-default", action: function () { modal.hide(); } },
            { label: "Email", btnClass: "btn btn-primary", action: function () { sendEmail(freeCrewIds, taskDate); modal.hide(); } }];

        modal.show(content, "Task Email", buttons, 'md');

        common.initDraggables();
    }

    function getModalContent(tasks, includeUnAssignedNoteDiv = false) {
        var listParent = '<div class="draggable-container" id="">';
        var tasksDiv = '<div><h5>Tasks</h5>';
        var taskNotesDiv = '<div><h5>Tasks Note</h5><textarea name="note" rows="5" class="form-control" id="email-note"></textarea>';
        var unAssignedNotesDiv = '<div><h5>Unassigned Crews Note</h5><textarea name="note" rows="5" class="form-control" id="unassigned-email-note"></textarea>';
        var body = "";

        if (tasks.length > 0) {

            tasks.forEach(function (task) {
                var list = '<div draggable="true" style="height: 50px;" class="draggable taskitem" Id="task' + task.id + '"><ul class="list-group"><li class="list-group-item task-item" id="' + task.id + '">' + task.name + ' (' + task.workOrderName + ')  <small class="margin-left-10">  ' + task.taskTimeStamp + '</small></li></ul></div>';
                listParent += list;
            });

            tasksDiv += '<div class="panel panel-default"><div class="panel-body" style="height:150px; overflow-y:scroll">' +
                listParent + '</div></div></div></div>' + taskNotesDiv + '</div>';

            body += tasksDiv;
        }

        if (includeUnAssignedNoteDiv) {
            body += unAssignedNotesDiv;
        }

        return body;
    }

    function sendEmail(freeCrewIds, taskDate) {
        var ids = [];
        var viewModel = {
            taskDate: taskDate,
            freeCrewIds: freeCrewIds, 
            note: $("textarea#unassigned-email-note").val()
        };
        var note = $("textarea#email-note").val();
        var url = '/tasks/emailScheduleTask/';

        $('li.task-item').each(function (index, item) { ids.push(item.id); });

        $.post(url, $.param({ ids: ids, note: note, freeCrewsModel: viewModel })).done(reloadPage);
    }

    function reloadPage(d) {
        if (d) {
            toastr.success('Task - ' + d.Name + ' was updated successfully');
        }
        location.reload();
    }

})(app.task, app.common, app.ModalDialog, app.tasksCommon);
