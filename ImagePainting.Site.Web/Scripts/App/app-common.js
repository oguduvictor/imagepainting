﻿var app = app || {};

app.common = app.common || {};

(function (vm, ModalDialog) {
    'use strict';

    var _appRoot = '';

    vm.initPage = function () {
        vm.initPartialView();
        vm.initAjaxSetup();
        vm.initConfirmButtons();
        vm.initExecuteButtons();
        vm.initMenuDropdownHover();
        vm.initFileupload();
        vm.Table();
    };

    vm.initAjaxSetup = function () {
        //init spinner for during ajax calls 
        var $loadingSpinner = $('#loading-spinner');

        $(document).ajaxStart(function () {
            $loadingSpinner.show();
        });

        $(document).ajaxStop(function () {
            $loadingSpinner.hide();
        });

        // disable caching for ajax calls
        $.ajaxSetup({
            cache: false,
            beforeSend: function (xhr, options) {
                var appRoot = vm.getAppRoot();
                if (!vm.startsWith(options.url, appRoot) && !vm.startsWith(options.url, 'http')) {
                    options.url = appRoot + options.url;
                }
            }
        });
    };

    vm.initExecuteButtons = function () {
        $('body').on('click', '[data-execute=true]', function (e) {
            e.preventDefault();
            var $element = $(this);
            var elementData = $element.data();
            var url = elementData.url;
            var redirect = elementData.redirect;
            var onExecute = elementData.onexecute;
            var requestData = elementData.data;
            var message = elementData.confirmmessage;
            var title = elementData.confirmtitle || 'Confirm';

            vm.runExecute(url, redirect, onExecute, requestData, message, title);
        });
    };

    vm.runExecute = function (url, redirect, onExecute, requestData, confirmMessage, confirmMessageTitle) {
        vm.confirmDialog.show(confirmMessage, confirmMessageTitle).done(function () {
            $.post(url, requestData)
                .done(function (result) {
                    if (redirect) {
                        location.href = redirect.replace('{result}', result);
                    }

                    if (onExecute) {
                        eval(onExecute);
                    }
                })
                .fail(function (err) {
                    toastr.error(err.statusText, 'Failed!');
                });
        });
    };

    vm.initConfirmButtons = function () {
        $('body').on('click', '[data-confirm=true]', function (e) {
            e.preventDefault();
            var $element = $(this);
            var data = $element.data();
            var message = data.confirmmessage;
            var title = data.confirmtitle || 'Confirm';

            vm.confirmDialog.show(message, title).done(function () {
                $element.parents('form').submit();
            });
        });
    };

    vm.initFileupload = function () {
        $('body').on('change', '[name=file-upload-select]', function (e) {
            var $form = $(e.target).parents('form#frm-upload');
            var $container = $form.parents('.upload-container');
            var files = e.target.files;
            var formData = new FormData();
            var verificationToken = $form.find(':hidden[name=__RequestVerificationToken]').val();
            var url = $form.attr('action');

            for (var i = 0; i < files.length; i++) {
                formData.append('file' + i, files[i], files[i].name);
            }

            formData.append('__RequestVerificationToken', verificationToken);

            $.ajax({
                type: "POST",
                url: url,
                xhr: function () {
                    return $.ajaxSettings.xhr();
                },
                success: function (data) {
                    vm.reloadFileslist($container.attr('id'));
                },
                error: function (error) {
                    toastr.error('A server error occurred while uploading your file(s)');
                    console.error(error);
                },
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                timeout: 60000
            });
        });
    };

    vm.reloadFileslist = function (containerId) {
        var $container = $('div.upload-container#' + containerId);
        var url = $container.find(':hidden[name=files-list-url]').val();

        $.get(url).done(function (d) {
            var contentSelector = 'div.panel-body';
            var newContent = $(d).find(contentSelector);

            $container.find(contentSelector).html(newContent);
        });
    };

    vm.convertToComboBoxes = function (inputs, selectDropdownSource) {
        var options = $(selectDropdownSource).find('option');
        var searchOptions = $.map(options, function (option) {
            return option.text;
        });
        var bh = new Bloodhound({
            local: searchOptions,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            datumTokenizer: function(d) {
                var test = Bloodhound.tokenizers.whitespace(d);
                var i = 0;
                $.each(test, function (k, v) {
                    i = 0;
                    while( (i+1) < v.length )
                    { 
                        test.push(v.substr(i,v.length));
                        i++; 
                    } 
                })
                return test;
            }
        });
        $(inputs).typeahead({
            hint: true,
            highlight: true,
            minLength: 0,
            limit: 5
        },
        {
            name: 'tasktypes',
            source: bh
        });

    };

    vm.convertSelectsToComboBoxes = function (selects) {
        selects.each(function (index) {
            var thisSelect = $(selects[index]);
            var input = $('<input>').addClass('form-control').addClass('combo-box').addClass('typeahead').width('auto');
            $(input).insertBefore(thisSelect);
            var options = $(thisSelect).find('option');
            var selectedText = "";
            var text = $.map(options, function (option) {
                if (thisSelect.val() === option.value) {
                    selectedText = option.text;
                }
                return option.text;
            });
            var bh = new Bloodhound({
                local: text,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.whitespace
            });
            $(input).val(selectedText);
            $(input).typeahead(
                {
                    hint: true,
                    highlight: true,
                    minLength: 0,
                    limit: 5
                },
                {
                    name: 'tasktypes',
                    source: bh
                }
            ).css('width', '100%').parent().css('width', '100%');
        });

        $('.combo-box').on('change', vm.updateSelect);
    };

    vm.updateSelect = function (event) {
        var target = $(event.target);
        var text = target.val();
        var select = target.closest('span').parent().find('select:first');
        if (select.length === 1) {
            var options = $(select).find('option');
            var matchFound = false;
            options.each(function (index) {
                if ($(options[index]).text().toLowerCase() === text.toLowerCase()) {
                    select.val(options[index].value);
                    matchFound = true;
                }
            });
            if (!matchFound) {
                select.append('<option value = ' + 0 + ' selected>' + text + '</option>');
                select.parent().find('.supply-type-name').val(text);
            }
        }
    };

    vm.roundToDecimal = function (value, decimals) {
        return parseFloat(value.toString()).toFixed(decimals);
    };

    vm.roundToDecimalWithCommas = function (value, decimals) {
        value = vm.roundToDecimal(value, decimals);
        var parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    };

    vm.confirmDialog = new SimpleDialog(true, true);

    vm.okDialog = new SimpleDialog(true, false);

    vm.initMenuDropdownHover = function () {
        $('ul.nav li.dropdown').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    };

    vm.getFormData = function (form) {
        var formData = {};
        var $form = typeof (form) === 'string' ? $(form) : form;

        $form.find('input:not([type=submit],[type=button]), select, textarea')
            .each(function (index, element) {
                if (!element.name)
                    return;

                else if (element.tagName === 'SELECT' && element.type === 'select-multiple') {
                    formData[element.name] = $('#' + element.id + ' :selected').toArray().map(function (input) {
                        return input.value;
                    });

                    return;
                }

                formData[element.name] = element.value;
            });

        return formData;
    };

    vm.serializeForm = function (form, useTraditional) {
        var formData = vm.getFormData(form);

        return $.param(formData, !!useTraditional);
    };

    vm.valid = function (target) {
        return $(target).valid();
    };

    vm.initFormValidation = function ($form) {
        $form.removeData('validator');
        $form.removeData('unobtrusiveValidation');

        $.validator.unobtrusive.parse($form);
    };

    vm.isjQueryElement = function ($element) {
        if (!$element || !$element[0]) {
            return false;
        }

        return $element[0] instanceof HTMLElement;
    };

    vm.Table = function () {
        var delayedSearch = null;
        var sortDescending = false;
        var sortSummary = {
            ByBuilder: true
        };

        initDataTables();

        function initDataTables() {
            var $table = $('.table.data-table');
            var url = $table.data("url");

            if (url) {
                loadTable(50, "", 1);
            } else {
                $table.DataTable({
                    pageLength: 50
                });
            }
        }

        function resetTable(selectedPage) {
            var $table = $('.table.data-table');

            if (!$table.hasClass('dataTable')) {
                $table.DataTable({
                    "lengthMenu": [10, 50, 100, 200]
                });
                $('select:first').val(50);
                $('select:first').trigger('change');
                event.stopPropagation();
            }

            addWorkOrderSelectOptions();

            var recordCount = parseInt($table.find('.record-holder').data("recordcount"), 10);
            createPagination(recordCount, selectedPage);

            $table.find('th').removeClass('sorting').off();
            $($table.find('th')).slice(0, 7).addClass('sorting');
            if (sortDescending) {
                $('.sorting').removeClass('sorting_desc').addClass('sorting_asc');
            } else {
                $('.sorting').removeClass('sorting_asc').addClass('sorting_desc');
            }

            sortSummary.IsAscending = !sortDescending;

            bindDataTableHandlers();
        }

        function addWorkOrderSelectOptions() {
            var $filterLabel = $('label:nth(1)');
            var $options = $('#SearchOptions').clone().removeClass('hidden')
                                              .removeAttr('id')
                                              .addClass('searchOptions')
                                              .val(sortSummary.SearchOption);
            
            $('select.searchOptions').remove();
            $filterLabel.append($options);
        }

        function bindDataTableHandlers() {
            $('input[type=search]').off(); $('select:first').off(); $('.paginate_button').off();
            $('input[type=search]').on('keyup', handleInputChanged);
            $('input[type=search]').on('keyup', handleSearch);
            $('select:first').on('change', recordsToDisplayChanged);
            $('.paginate_button').on('click', handleInputChanged);
            $('.paginate_button').on('click', paginateClicked);
            $('.sorting').on('click', handleSortClicked);
        }

        function handleSearch() {
            $($('.paginate_button')[1]).click();
        }

        function recordsToDisplayChanged(event) {
            var countValue = $(event.target).val();
            sortSummary.ResultsCount = countValue;
            $($('.paginate_button')[1]).click();
        }

        function loadTable(resultCount, searchString, selectedPage) {
            var $table = $('.table.data-table');
            var url = $table.data('url');
            sortSummary.ResultCount = resultCount;
            if (searchString !== null) {
                sortSummary.SearchString = searchString;
                sortSummary.SearchOption = $('select.searchOptions').val() || $('select#SearchOptions').val();
            }
            sortSummary.PageNumber = selectedPage - 1;
            $table.load(url, { sortSummary: sortSummary }, function () {
                resetTable(selectedPage);
            });
        }

        function createSortObject(sortType) {
            sortSummary.ByBuilder = sortType === "builder";
            sortSummary.ByCommunity = sortType === "community";
            sortSummary.ByName = sortType === "name";
            sortSummary.ByStatus = sortType === "status";
            sortSummary.ByLastModified = sortType === "lastmodifieddate";
            sortSummary.ByCreated = sortType === "created";
            sortSummary.ByAddress = sortType === "address";
        }

        function handleSortClicked(event) {
            var $target = $(event.target);

            var sortType = $target.data("sorttype");

            createSortObject(sortType);

            var isAscending = $target.hasClass('sorting_asc');

            if (isAscending) {
                $target.removeClass('sorting_desc').addClass('sorting_asc');
                sortDescending = false;
            } else {
                $target.removeClass('sorting_asc').addClass('sorting_desc');
                sortDescending = true;
            }

            sortSummary.IsAscending = isAscending;

            $('input[type=search]').trigger('keyup');

        }

        function paginateClicked(event) {

            var $currentActive = $('.paginate_button.active');

            $('.paginate_button').removeClass('active');

            var $target = $(event.currentTarget);

            if ($target.hasClass('previous')) {
                $currentActive.prev().addClass('active');
            } else if ($target.hasClass('next')) {
                $currentActive.next().addClass('active');
            } else {
                $target.addClass('active');
            }

        }

        function createPagination(recordCount, selectedPage) {
            var shownRecordCount = $('select:first').val();
            var groupingSize = 5;
            var pageCount = Math.ceil(recordCount / parseInt(shownRecordCount, 10));
            var numberOfGroupings = Math.ceil(pageCount / groupingSize);
            if (selectedPage < 1) { selectedPage = 1; }
            var shown = Math.min(shownRecordCount * selectedPage, recordCount);
            $('.dataTables_info').text("Showing " + (((selectedPage - 1) * shownRecordCount) + 1) + " to " + shown + " of " + recordCount + " entries");
            var buttons = $('.paginate_button');
            for (var btn = 2; btn < buttons.length - 1; btn++) {
                $(buttons[btn]).remove();
            }
            var firstPage = $(buttons[1]);
            var currentPage = firstPage;
            var activeClassAdded = false;
            var clone = null;
            for (var index = 0; index < numberOfGroupings; index++) {
                var startIndex = (index * groupingSize) + 1;
                var stopIndex = Math.min((index + 1) * groupingSize, pageCount);
                if (selectedPage >= startIndex && selectedPage <= stopIndex) {
                    for (var i = startIndex; i <= stopIndex; i++) {
                        clone = currentPage.clone().removeClass('active');
                        clone.find('a').text(i);
                        if (i === selectedPage) {
                            clone.addClass('active');
                            activeClassAdded = true;
                        }
                        clone.insertAfter(currentPage);
                        currentPage = clone;
                    }
                    //do nothing
                } else {
                    clone = currentPage.clone().removeClass('active');
                    clone.find('a').text(startIndex + ' - ' + stopIndex);
                    clone.insertAfter(currentPage);
                    currentPage = clone;
                }
                firstPage.remove();
            }
            var $nextButton = $('.paginate_button.next');
            var $previousButton = $('.paginate_button.previous');
            if (selectedPage < pageCount) {
                $nextButton.removeClass('disabled');
            } else {
                $nextButton.addClass('disabled');
            }
            if (selectedPage > 1) {
                $previousButton.removeClass('disabled');
            } else {
                $previousButton.addClass('disabled');
            }
            if (!activeClassAdded) {
                firstPage.addClass('active');
            }
        }
        
        function handleInputChanged(event) {
            var $searchBox = $('input[type=search]');
            var $numEntries = $('select:first');
            var $table = $('.table.data-table');

            clearTimeout(delayedSearch);

            delayedSearch = setTimeout(function () {
                var searchString = $searchBox.val();
                var resultCount = $numEntries.val();
                var currentPage = $('.paginate_button.active');
                var selectedPage = parseInt(currentPage.text(), 10);
                var url = $table.data('url');
                if (url.trim().length > 0) {
                    loadTable(resultCount, searchString, selectedPage, sortDescending);
                }
            }, 1000);
        }
    };

    vm.endsWith = function (source, text) {
        var textLen = text.length;

        if (textLen > source.length) {
            return false;
        }

        return (text === source.substr(textLen * -1));
    };

    vm.startsWith = function (source, text) {
        var textLen = text.length;

        if (textLen > source.length) {
            return false;
        }

        return (text === source.substr(0, textLen));
    }

    vm.contains = function (item, collection) {
        for (var i = 0; i < collection.length; i++) {
            if (collection[i] == item)
                return true;
        }

        return false;
    }

    function SimpleDialog(hasOkBtn, hasCancelBtn) {
        var modal = new ModalDialog();
        var buttons = [];

        if (hasCancelBtn) {
            buttons.push(modal.cancelButton());
        }

        if (hasOkBtn) {
            buttons.push(modal.okButton())
        }

        function showDialog(message, title, size) {
            var dialogSize = size || 'md';

            return modal.show(message, title, buttons, size);
        }

        return {
            show: showDialog,
            hide: modal.hide
        };
    };

    vm.getAppRoot = function () {
        return _appRoot;
    };

    vm.setAppRoot = function (appRoot) {
        _appRoot = appRoot;
    }

})(app.common, app.ModalDialog);
