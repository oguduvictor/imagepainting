﻿var app = app || {};

app.reports = app.reports || {};

(function (vm, common) {
    'use strict';

    vm.initTaskReport = function () {
        initCommonReport(false, true);
    };

    vm.initRevenueReport = function () {
        initCommonReport();
    };

    vm.initLaborReport = function () {
        initCommonReport();
    };

    vm.initPurchaseOrderReport = function () {
        initCommonReport(true, false);
        $('#communities').on('change', 'select#CommunityId', { hasLotBlock: true }, handleCommunityChange);
    }

    vm.initMultiSelect = function () {
        $('#TaskSchemaIds').multiselect({
            buttonWidth: '100%',
            enableFiltering: true,
            includeSelectAllOption: true,
            nonSelectedText: 'All Tasks',
            allSelectedText: 'All Tasks',
            selectAllText: 'All Tasks',
            checkboxName: function (input) {
                return 'TaskId';
            },
            onChange: function (option, checked, select) {
                if (option.html() === 'All Main Tasks' && !!checked) {
                    checkMainTasks(option.val(), true);
                }
                else if (option.html() === 'All Main Tasks' && !checked) {
                    checkMainTasks(option.val(), false);
                }
                toggleAllMainTasksOption();
            }
        });

        $('#CrewIds').multiselect({
            enableFiltering: true,
            includeSelectAllOption: true,
            nonSelectedText: 'Everyone',
            allSelectedText: 'Everyone',
            selectAllText: 'Everyone',
            enableCaseInsensitiveFiltering: true,
            numberDisplayed: 1,
            buttonWidth: '100%'
        });
    }

    function initCommonReport(hasLotBlock, hasTaskIds) {
        initDataTable();
        styleCommunitiesPage();
        $('button#searchBtn').click({ hasLotBlock: hasLotBlock, hasTaskIds: hasTaskIds}, search);
        $('select#BuilderId').change(handleBuilderSelectChange);
    }


    function checkMainTasks(values, isChecked) {
        var mainTaskIds = JSON.parse(values);

        if (isChecked) {
            $('#TaskSchemaIds').multiselect('select', mainTaskIds);
            return;
        }

        $('#TaskSchemaIds').multiselect('deselect', mainTaskIds);
    }

    function toggleAllMainTasksOption(e) {
        var allSelectedTasksIds = $('#TaskSchemaIds :selected').toArray()
                                    .filter(function (x) { return !!Number(x.value); })
                                    .map(function (x) {
                                        return Number(x.value);
                                    });

        var mainTasksIds = JSON.parse($('#TaskSchemaIds option:contains("All Main Task")').val());
        var mainTasksChecked = arrayIsContainedIn(allSelectedTasksIds, mainTasksIds);
        var $mainTaskCheckBox = $('#tasks label.checkbox:contains("All Main Tasks")').children('input');
        var $mainTaskOption = $mainTaskCheckBox.closest('li');

        $('#TaskSchemaIds option:contains("All Main Tasks")').prop('selected', mainTasksChecked);

        if (mainTasksChecked) {
            $mainTaskOption.addClass('active');
        }
        else {
            $mainTaskOption.removeClass('active');
        }
        $mainTaskCheckBox.prop('checked', mainTasksChecked);
    }

    function arrayIsContainedIn(superSetArray, subSetArray) {
        if (subSetArray.length > superSetArray.length) {
            return false;
        }

        return subSetArray.every(function (value) {
            return (superSetArray.indexOf(value) >= 0);
        });
    }

    function initDataTable() {
        var table = $('table').DataTable({
            searching: false,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel'
            ],
            pageLength: 50
        });
    }

    function handleBuilderSelectChange(e) {
        e.preventDefault();

        var value = e.target.value;
        
        var $plansDiv = $('form#searchCollapse').find('div#plans');

        if ($plansDiv.length > 0) {
            var planUrl = '/Workorders/Plans/' + value + '?showCreateOption=false';
            $('div#plans').load(planUrl);
        }
        var url = '/WorkOrders/Communities/' + value + '?showCreateOption=false';
        $('div#communities').load(url, function () {
            styleCommunitiesPage();

            $('select#CommunityId').trigger('change');
        });
    }

    function handleCommunityChange(e) {
        e.preventDefault();

        var hasLotBlock = e.data.hasLotBlock;
        var value = e.target.value;
        var url = '/reports/lotblock?communityId=' + value;

        if (hasLotBlock) {
            $('div#lotBlock').load(url);
        }
    }

    function search(e) {
        e.preventDefault();

        var $form = $('form#searchCollapse');

        if (!$form.valid()) {
            return;
        }

        var formData = common.serializeForm($form, true);
        if (e.data.hasTaskIds) {
            var taskIds = $('select#TaskSchemaIds').val();
            formData = formatTaskIdsData(taskIds, formData);
        }
        
        if (e.data.hasLotBlock) {
            var lotblock = $('select#LotBlock').val();

            if (lotblock) {
                lotblock = lotblock.split(' ');
                var Lot = "&Lot=" + lotblock[0];
                var Block = "&Block=" + lotblock[1];

                formData += (Lot + Block);
            }
        }

        var url = getUrl();

        $('#tableDiv').load(url, formData, function (res, status, xhr) {
            if (status === 'success') {
                initDataTable();
            }
            else {
                console.error(res);
                toastr.error('Something went wrong');
            }
        });
    }
    
    function formatTaskIdsData(taskIds, formData) {
        if (taskIds == null) {
            return formData;
        }
        else if (taskIds.length > 1 && common.startsWith(taskIds[0], '[')) {
            formData = decodeURIComponent(formData);
            
            var stringToRemove = formData.substring(formData.indexOf('&TaskSchemaIds=['), formData.indexOf(']') + 1);

            formData = formData.replace(stringToRemove, '');
        }

        return formData;
    }

    function styleCommunitiesPage() {
        var $communities = $('#communities');
        $communities.find('.container').removeClass('container');
        $communities.find('label').removeClass('required-field');
    }

    function getUrl() {
        var locationUrl = location.pathname;
        
        switch (true) {
            case common.endsWith(locationUrl, '/Reports/ProjectedRevenue'):
                return '/Reports/ProjectedRevenueList';
            case common.endsWith(locationUrl, '/Reports/Tasks'):
                return '/Reports/TasksReport';
            case common.endsWith(locationUrl, '/Reports/ProjectedLabor'):
                return '/Reports/ProjectedLaborList';
            case common.endsWith(locationUrl, '/Reports/PurchaseOrder'):
                return '/reports/purchaseorderlist/';
            default:
                throw new Error('Invalid Path');
        }
    }

})(app.reports, app.common)