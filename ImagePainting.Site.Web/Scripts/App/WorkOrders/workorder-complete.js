﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm) {

    'use strict';

    vm.completeWorkOrder = function () {
        var $checkBox = $('#complete-checkbox');
        var workOrder = vm.getWorkOrder();
        var isComplete = workOrder.Completed != null;

        if (isComplete) {
            $checkBox.prop('checked', 'checked')
        } else {
            $checkBox.removeProp('checked');
        }
        $('#Id').val(workOrder.Id);
    };

    vm.areAllTasksCompleted = function (workOrder) {
        var tasks = workOrder.Tasks || [];
        var completed = false;

        for (var i = 0; i < tasks.length; i++) {
            if (!tasks[i].Completed) {
                return false;
            }

            completed = true;
        }

        return completed;
    };

})(app.workorders);