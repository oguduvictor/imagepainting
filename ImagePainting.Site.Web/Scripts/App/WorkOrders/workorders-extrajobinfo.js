﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm, common) {
    'use strict';
    var groupedModel, taskSummaries, _allExtraItems, workOrderId, disabled, $taskTypesSelectOptions, extraJobInfoId, _currentSchema;
    var $mainContainer = $('div#wo-container');
    var taskTypes = [
        { index: 0, key: 'Unknown', text: 'Unknown' },
        { index: 1, key: 'Interior', text: 'Interior' },
        { index: 2, key: 'Exterior', text: 'Exterior' },
        { index: 3, key: 'PreCarpet', text: 'Pre Carpet' },
        { index: 4, key: 'AfterCarpet', text: 'After Carpet' },
        { index: 6, key: 'QCI', text: 'QCI' },
        { index: 7, key: 'HO', text: 'HO' },
        { index: 8, key: 'XHO', text: 'XHO' },
        { index: 9, key: 'CustomerService', text: 'Customer Service' },
        { index: 10, key: 'PaintFoam', text: 'Paint Foam' },
        { index: 11, key: 'Drylock', text: 'Drylock' },
        { index: 12, key: 'PaintMeter', text: 'Paint Meter' },
        { index: 13, key: 'Stain', text: 'Stain' },
        { index: 14, key: 'WallOut', text: 'WallOut' },
        { index: 15, key: 'PaintEnamel', text: 'Paint Enamel' },
        { index: 16, key: 'PreCarpetAfterCarpet', text: 'Pre Carpet/After Carpet' },
        { index: 17, key: 'XXHO', text: 'X X H O' },
        { index: 18, key: 'RepaintExterior', text: 'Repaint Exterior' },
        { index: 19, key: 'RepaintInterior', text: 'Repaint Interior' },
        { index: 5, key: 'Others', text: 'Others' }
    ];
    
    vm.initJobExtras = function (modelData) {
        groupedModel = [];
        _allExtraItems = null;
        _currentSchema = vm.workflowSchema['extraJob'];
        disabled = modelData.WorkOrderCompleted;
        workOrderId = modelData.WorkOrderId;
        extraJobInfoId = modelData.Id;

        $.get('/SystemAlert/GetJobExtraAlerts/', { workOrderId: workOrderId }).then(function (data) {
            data.forEach(function (alert) {
                var extra = modelData.ExtraItems.find(function (extraItem) {
                    return alert.JobExtra.ExtraItem.Id === extraItem.Id;
                });

                if (extra) {
                    extra.Alert = alert;
                }
            });
        }).done(function () {
            loadJobExtraTemplate();

            $mainContainer.off('click', 'i.remove', handleRemoveClick).on('click', 'i.remove', handleRemoveClick);
            $mainContainer.off('click', 'a.unresolved-jobextras', showResolvePopOver).on('click', 'a.unresolved-jobextras', showResolvePopOver);

            $('#extraItems').off('click', 'button.popover-button', handleTaskPopoverClick).on('click', 'button.popover-button', handleTaskPopoverClick);
            $('#newItem').click(handleNewItemClick);
            $('#select-extras').click(selectExtrasHandler);

            $('span.btn-edit-note').click(showNoteTextArea);
            $('input#btn-continue').click(saveJobExtras);
        });

        function loadJobExtraTemplate() {
            $('#extraItems').load('/Templates/_jobExtraItems.html', function () {
                groupedModel = generateGroupingModel(modelData.ExtraItems);

                rivets.binders.input = {
                    publishes: true,
                    routine: rivets.binders.value.routine,
                    bind: function (el) {
                        el.addEventListener('input', this.publish);
                    },
                    unbind: function (el) {
                        el.removeEventListener('input', this.publish);
                    }
                };

                rivets.binders.groupclass = function (el, value) {
                    el.title = 'Task Type should be added to these extra items';
                    el.className += value === 'Unknown' ? ' text-danger' : '';
                };

                rivets.binders.walkqtycolor = function (el, value) {
                    if (value.WalkQuantity > value.Quantity) {
                        el.className += " highlighted-text";
                    }
                };

                rivets.binders.disable = function (el, value) {
                    $(el).attr('disabled', value);
                };

                rivets.formatters.resolvedAlert = function (alert) {
                    return 'Resolved by ' + alert.ResolvedBy + ' on ' + alert.Resolved;
                };

                rivets.bind($('#extraItems'), { groupedModel: groupedModel, disabled: disabled, workOrderId: workOrderId });
            });
        }
    };

    vm.initJobExtraModalRivetBindings = function () {
        rivets.bind($('div#extra-items-list-container'), { allExtraItems: _allExtraItems });
    };

    function showResolvePopOver(e) {
        e.preventDefault();

        if (disabled) {
            return;
        }

        var $target = $(e.currentTarget);

        $target.popover({
            title: '<small>The Quantity is different from the Walk Quantity</small>',
            content: '<button class="btn btn-primary acknowledgeBtn" style="display: block; margin: 0 auto;">Resolve</button>',
            html: true,
            placement: 'bottom',
            trigger: 'manual'
        });

        $('div.row.items').off('click').on('click', 'button.acknowledgeBtn', { element: $target }, resolveAlert);
        $target.popover('toggle');
    }

    function resolveAlert(e, ak) {
        e.preventDefault();
        var $popoverElement = e.data.element;
        var $target = $(e.currentTarget);
        var data = $target.closest('div.popover').siblings('a.unresolved-jobextras').data();
        var extraItem = groupedModel[data.group].extraItems[data.extraitemindex];

        $.post('/SystemAlert/ResolveAlert/', { id: extraItem.Alert.Id }).then(function () {
            $.get('/SystemAlert/GetJobExtraAlerts', { workOrderId: workOrderId }).then(function (alerts) {
                var savedExtraAlert = alerts.find(function (alert) {
                    return alert.Id === extraItem.Alert.Id;
                });

                extraItem.Alert = savedExtraAlert;

                $popoverElement.popover('toggle');
            });
        });
    }

    function handleNewItemClick(e) {
        var newExtraItemGroup = groupedModel.find(function (group) {
            return group.groupName === 'Unknown';
        });
        var extraItem = {
            Name: '',
            BuilderCost: 0,
            Quantity: 0,
            WalkQuantity: 0,
            LaborCost: 0,
            TaskType: 0
        };

        if (newExtraItemGroup) {
            newExtraItemGroup.extraItems.push(extraItem);
        } else {
            groupedModel.push({
                groupName: 'Unknown',
                extraItems: [extraItem]
            });
        }
    }

    function showNoteTextArea() {
        var $noteContainer = $(this).parents('div.note-container');

        $noteContainer.find('textarea[name^=Note]').removeClass('hidden');
        $noteContainer.find('textarea[name^=ReadOnlyNote]').hide();
    }

    function handleRemoveClick(e) {
        var elementData = $(e.currentTarget).data();

        removeFromLiveObject(elementData);
    }

    function removeFromLiveObject(elementData) {
        var extraItem = groupedModel[elementData.group].extraItems[elementData.extraitemindex];

        extraItem.checked = false;
        
        for (var groupIndex = 0; groupIndex < groupedModel.length; groupIndex++) {
            var group = groupedModel[groupIndex];

            for (var extraItemIndex = 0; extraItemIndex < group.extraItems.length; extraItemIndex++) {
                var extra = group.extraItems[extraItemIndex];
                if (extra.Name === extraItem.Name && group.extraItems.length > 1) {
                    group.extraItems.splice(extraItemIndex, 1);
                    extraItemIndex = extraItemIndex - 1;
                } else if (extra.Name === extraItem.Name) {
                    groupedModel.splice(groupIndex, 1);
                    groupIndex = groupIndex - 1;
                }
            }
        }
    }

    function generateGroupingModel(extraItems) {
        var groupedExtraItems = [];
        taskSummaries = vm.getWorkOrder().Tasks || [];
        extraItems = extraItems || [];

        extraItems.forEach(function (extraItem) {
            if (extraItem.TaskIds) {
                extraItem.TaskIds = extraItem.TaskIds.filter(function (id) {
                    return id !== 0 && taskSummaries.findIndex(function(task) { return task.Id == id; }) > -1;
                });
            }

            var keys = getKeys(extraItem);

            groupExtraItemsByKeys(keys, groupedExtraItems, extraItem);
        });

        return groupedExtraItems;
    }

    function groupExtraItemsByKeys(keys, groupedExtraItems, extraItem) {
        keys.forEach(function (key) {
            var grouping = groupedExtraItems.find(function (group) {
                return group.groupName === key;
            });

            if (grouping) {
                grouping.extraItems.push(extraItem);
            } else {
                groupedExtraItems.push({
                    groupName: key,
                    extraItems: [extraItem]
                });
            }
        });
    }

    function getKeys(extraItem) {
        if (!(extraItem.TaskIds && extraItem.TaskIds.length)) return [taskTypeNumberToText(extraItem.TaskType)];

        var tasks = taskSummaries.filter(function (task) {
            return task.TaskType === extraItem.TaskType || task.TaskType === taskTypeNumberToText(extraItem.TaskType, 'key');
        });

        if (!(tasks && tasks.length)) return [taskTypeNumberToText(extraItem.TaskType)];

        return extraItem.TaskIds.map(function (taskId) {
            return taskSummaries.find(function (task) {
                return task.Id === taskId;
            }).Name;
        });
    }

    function taskTypeNumberToText(taskTypeIndex, format) {
        var tasktype = taskTypes.find(function (taskType) {
            return taskType.index === taskTypeIndex;
        });

        if (!tasktype) {
            return '';
        }
            
        if (format === 'key') {
            return tasktype.key;
        }

        return tasktype.text;
    }
    
    function handleTaskPopoverClick(e) {
        var $target = $(e.currentTarget);

        $target.popover({
            title: 'Change Task Type',
            content: getPopOverContent($(this)),
            html: true,
            trigger: 'manual',
            placement: 'bottom'
        });

        $mainContainer.on('click', 'button.popbutton', { element: $target }, handleTaskPopOverUpdate);
        $mainContainer.on('change', 'select.task-type-option', handleTaskTypeOptionChange);

        $target.popover('toggle');
    }

    function getPopOverContent($target) {
        var dropdown = generateDropDown($target);
        var mainTaskIds = $target.data('maintaskids');
        var mainTaskIdsForExtraItem = mainTaskIds && mainTaskIds.toString().split(",").map(function (item) { return Number(item); });
        var selectedTaskTypeOptionValue = Number($target.data('tasktype'));
        var selectedTaskType = (taskTypes.find(function (type) {
            return selectedTaskTypeOptionValue === type.index;
        })).key;
        var tasksForSelectedOption = getTasksForSelectedTaskType(selectedTaskType, selectedTaskTypeOptionValue);

        return dropdown + generateTaskNamesAndCheckBox(tasksForSelectedOption, mainTaskIdsForExtraItem) + '<button class="btn btn-default col-xs-12 popbutton">Update</button>';
    }

    function handleTaskPopOverUpdate(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);
        var $popoverElementTrigger = e.data.element;
        var $popoverContent = $target.closest('div.form-group');

        var itemName = $popoverContent.find('input.item-name').val();
        var taskIds = [];

        $popoverContent.find('input.task-checkbox:checked').each(function (index, item) {
            taskIds.push(Number(item.value));
        });

        var message = 'Are you sure you want to change task type of ' + itemName + '?';
        
        common.confirmDialog.show(message, 'Change Extra Item Task Type')
            .done(function () {
                var $selectedTaskOption = $target.siblings('select').find(':selected');
                var taskType = $selectedTaskOption.val();
                var elementData = $target.closest('div.popover').siblings('button').data();
                var extraItem = groupedModel[elementData.group].extraItems[elementData.extraitemindex];
                removeFromLiveObject(elementData);
                extraItem.TaskIds = taskIds;
                extraItem.TaskType = Number(taskType);

                var keys = getKeys(extraItem);
                
                groupExtraItemsByKeys(keys, groupedModel, extraItem);

                $popoverElementTrigger.popover('toggle');
            });
    }

    function handleTaskTypeOptionChange(e) {
        var $target = $(e.currentTarget);
        var selectedTaskTypeOptionValue = Number(e.currentTarget.selectedOptions[0].value);
        var taskType = (taskTypes.find(function (type) {
            return selectedTaskTypeOptionValue === type.index;
        })).key;

        var tasks = getTasksForSelectedTaskType(taskType, selectedTaskTypeOptionValue);
        var mainTaskDataIds = $target.closest('div.popover').siblings('button.popover-button').data('maintaskids');

        var taskIds = mainTaskDataIds && mainTaskDataIds.toString().split(",").map(function (item) { return Number(item); });

        $target.siblings('div.form-group, br').remove();
        $target.after(generateTaskNamesAndCheckBox(tasks, taskIds));
    }

    function getTasksForSelectedTaskType(selectedTaskType, selectedTaskTypeOptionValue) {
        var mainTasks = vm.getWorkOrder().Tasks;

        return mainTasks.filter(function (task) {
            return task.TaskType == selectedTaskType || task.TaskType == selectedTaskTypeOptionValue;
        });
    }

    function generateDropDown($target) {
        if (!($taskTypesSelectOptions && $taskTypesSelectOptions.length)) {
            $taskTypesSelectOptions = $('<select class="form-control task-type-option">');
            taskTypes.forEach(function (taskType) {
                $taskTypesSelectOptions.append('<option value=' + taskType.index + '>' + (taskType.index === 0 ? '' : taskType.text) + '</option>');
            });
        }

        $taskTypesSelectOptions.children('option').removeAttr('selected');
        $taskTypesSelectOptions.find('option[value="' + $target.data('tasktype') + '"]').attr('selected', 'selected');

        return $taskTypesSelectOptions.prop('outerHTML');
    }

    function generateTaskNamesAndCheckBox(tasks, selectedTaskIds) {
        if (!tasks.length) {
            return '';
        }
        var inputFields = '<br />';

        tasks.forEach(function (task, index) {
            var setChecked = selectedTaskIds && selectedTaskIds.indexOf(task.Id) > -1 ? 'checked' : '';

            inputFields += '<div class="form-group"><label class="check"><input class="checkbox task-checkbox" type="checkbox" value="' + task.Id + '"' +
                            setChecked + ' /> <input type="hidden" /><span class="animated-checkbox">' +
                            '<i class="animated-checkbox-icon glyphicon glyphicon-ok"></i></span>' + task.Name + '</label></div>';
        });

        return inputFields;
    }

    function selectExtrasHandler(e) {
        e.preventDefault();

        var workOrderId = vm.getWorkOrder().Id;

        if (_allExtraItems) {
            displayExtrasModal(_allExtraItems);
            return;
        }

        $.getJSON('/workorders/allextraitems/' + workOrderId)
            .done(function (data) {
                data.forEach(function (item) {
                    item.checked = false;
                    groupedModel.forEach(function (group) {
                        var extraitem = group.extraItems.find(function (extra) { return extra.Name === item.Name; });

                        if (extraitem) {
                            item.checked = true;
                            return;
                        }
                    });
                });

                _allExtraItems = data;

                displayExtrasModal(data);
            })
            .fail(function (err) {
                console.error(err);
                toastr.error('Error loading extra items. Please try again later');
            });
    }

    function displayExtrasModal(items) {
        var selectedItems = [];

        $('#extraItems input.item-name').each(function (index, element) {
            selectedItems.push(element.value);
        });

        $('div.modal-body').load('/Templates/_jobExtraItemModal.html', function (htmlContent) {
            rivets.bind($('div#extra-items-list-container'), { allExtraItems: _allExtraItems });

            common.confirmDialog
                .show(htmlContent, 'Extra Job Items', 'lg')
                .done(applyItemsSelection);

            $('#extra-items-list-container :checkbox').change(vm.setMainFormDirty);
        });
    }

    function createExtrasModalContent(selectedItems, allItems) {
        var oHtml = '';

        oHtml += '<div id="extra-items-list-container" style="max-height: 370px; overflow-y: scroll; padding: 10px;">';

        for (var i = 0; i < allItems.length; i++) {
            var selected = common.contains(allItems[i].Name, selectedItems);

            oHtml += '<div class="col-md-4 col-xs-6">';
            oHtml += '<div class="checkbox">';
            oHtml += '<label>';
            oHtml += '<input type="checkbox" ' + (selected ? 'checked="checked"' : '') + ' value="' + (selected ? 'true' : 'false') + '"/>';
            oHtml += allItems[i].Name;
            oHtml += '</label>';
            oHtml += '</div>';
            oHtml += '</div>';
        }

        oHtml += '</div>';

        return oHtml;
    }

    function applyItemsSelection() {
        var selectedExtras = _allExtraItems.filter(function (item) {
            return item.checked;
        });

        groupedModel.forEach(function (group) {
            group.extraItems.forEach(function (extra) {
                var extraItem = selectedExtras.find(function (_extraItem) {
                    return _extraItem.Name === extra.Name;
                });

                if (extraItem) {
                    extraItem.Id = extra.Id;
                    extraItem.BuilderCost = extra.BuilderCost;
                    extraItem.Quantity = extra.Quantity;
                    extraItem.WalkQuantity = extra.WalkQuantity;
                    extraItem.LaborCost = extra.LaborCost;
                    extraItem.TaskType = extra.TaskType;
                    extraItem.TaskIds = extra.TaskIds;
                    extraItem.Alert = extra.Alert;
                }

                if (!extra.Id) {
                    selectedExtras.push(extra);
                }
            });
        });

        var groupedSelected = generateGroupingModel(selectedExtras);

        groupedModel.splice(0, groupedModel.length);

        groupedSelected.forEach(function (group) {
            groupedModel.push(group);
        });
    }

    function saveJobExtras(e) {
        if (disabled) {
            return;
        }

        e.preventDefault();
        var extraItems = [];
        var noTaskTypesCounter = 0;
        
        groupedModel.forEach(function (group) {
            group.extraItems.forEach(function (extraItem) {
                var extraItemExistsInList = extraItems.findIndex(function (item) {
                    return item.Name === extraItem.Name;
                }) > -1;

                if (!extraItem.TaskType || extraItem.TaskType == 0) {
                    noTaskTypesCounter++;
                }

                if (!extraItemExistsInList) {
                    extraItems.push(extraItem);
                }
            });
        });

        if (noTaskTypesCounter > 0) {
            toastr.error(noTaskTypesCounter + ' Extra Item(s) do not currently have task type set', 'Resolve Task Type');

            return;
        }

        var jobExtraPayload = {
            Id: extraJobInfoId,
            ExtraItems: extraItems,
            Note: $('textarea[name^=Note]').val()
        };

        $.post('/WorkOrders/ExtraJob',
            {
                __RequestVerificationToken: $('input[name^=__RequestVerificationToken]').val(),
                workOrderId: workOrderId,
                extraJobInfo: jobExtraPayload
            })
        .done(function (d) {
            if (_currentSchema.onFormPost) {
                vm[_currentSchema.onFormPost](d);
            }

            vm.setMainFormPristine();
            vm.moveNext();
        })
        .fail(function (err) {
            toastr.error('An error occured on the server.');
            console.error(err);
        });
    }

}(app.workorders, app.common));