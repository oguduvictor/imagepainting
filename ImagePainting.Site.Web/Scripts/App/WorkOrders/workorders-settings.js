﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm, common) {
    'use strict';

    var $mainContainer = $('div#wo-container');
    var builderSelector = 'div#wo-builder select';
    var communitySelector = 'div#wo-community select';
    var lotSelector = 'input#Lot';
    var blockSelector = 'input#Block';
    var nameSelector = 'input#Name';
    var saveBtn = 'input#btn-continue';
    var _workOrderNameIsDirty = false;

    vm.initWorkOrderSettings = function () {
        $mainContainer.on('change', 'div#wo-builder select', handleBuilderChange);
        $mainContainer.on('change', 'div#wo-community select', handleCommunityChange);
        $('input#CustomerPhoneNumber.form-control').mask('(000) 000-0000');
        $(lotSelector + ', ' + blockSelector).keyup(setName);
        $(nameSelector).change(setNameFieldDirty);
        $(saveBtn).click(validateWorkOrder);
        $('span.btn-edit-note').click(showNoteTextArea);
        handleSuperintendentChange();
        handleCustomerRepChange();
        
    };

    function showNoteTextArea() {
        var $noteContainer = $(this).parents('div.note-container');

        $noteContainer.find('textarea[name^=Description]').removeClass('hidden');
        $noteContainer.find('textarea[name^=ReadOnlyDescription]').hide();
    }

    function handleBuilderChange(e) {
        e.preventDefault();

        var value = e.target.value;

        if (value === '0') {
            vm.enablePartialView('/communities/builder/0?partial=true', true)
            .done(function () {
                vm.processPartialPageForm().done(function (d) {
                    var $element = $(e.target);
                    if (d) {
                        var option = '<option value="' + d.Id + '">' + d.Name + ' ('+ d.Abbreviation + ')' + '</option>';

                        $element.append(option);
                        $element.val(d.Id);

                        vm.dismissPartialView();
                        $element.change();
                    } else {
                        var wo = vm.getWorkOrder();
                        $element.val(wo.BuilderId || '');
                    }
                });
            });
        } else if (value) {
            var url = '/workorders/communities/' + value;

            $mainContainer.find('div#wo-community').load(url);
            setName();
        }
    }

    function handleSuperintendentChange() {
        $('.superintendent-list').hide();

        var inputs = $('#initialSettings .item-superintendent');
        var selectDropdownSource = $('#initialSettings .superintendent-list');
        
        common.convertToComboBoxes(inputs, selectDropdownSource);
    }

    function handleCustomerRepChange() {
        $('.customer-reps-list').hide();

        var inputs = $('#initialSettings .item-customer-rep');
        var selectDropdownSource = $('#initialSettings .customer-reps-list');

        common.convertToComboBoxes(inputs, selectDropdownSource);
    }

    function handleCommunityChange(e) {
        e.preventDefault();

        var value = e.target.value;

        if (value === '0') {
            vm.enablePartialView('/communities/community/0?partial=true', true)
                .done(function () {
                    $('#BuilderIds').val($('#BuilderId').val());
                    vm.processPartialPageForm().done(function (d) {
                        var $element = $(e.target);
                        if(d){
                            var option = '<option value="' + d.Id + '">' + d.Name + ' ('+ d.Abbreviation + ')' + '</option>';

                            $element.append(option);
                            $element.val(d.Id);
                            vm.dismissPartialView();
                            $element.change();
                        } else {
                            var wo = vm.getWorkOrder();
                            $element.val(wo.CommunityId || '');
                        }
                     });
                    
                });
               
            return;
        }

        else if (value) {
            setName();
        }
    }

    function setName() {
        var selectedBuilder = $(builderSelector + ' :selected').html();
        var selectedCommunity = $(communitySelector + ' :selected').html();
        var value = getAbbreviation(selectedBuilder) + ' ' + getAbbreviation(selectedCommunity) +
            ' Lot ' + $(lotSelector).val() + ' Block ' + $(blockSelector).val();

        $(nameSelector).val(value);

        setNameFieldDirty();
    }

    function validateWorkOrder(e) {
        if (!_workOrderNameIsDirty) {
            return;
        }

        e.preventDefault();

        var workOrderId = vm.getWorkOrder().Id;
        var name = $(nameSelector).val();
        var data = $.param({ name: name });

        $.get('/workorders/WorkOrderExists/' + workOrderId, data, function (data, status, xhr) {
            if (data === true) {
            var message = 'WorkOrder Name, "' + name + '", already exists for another ' +
                            'work order, do you want to continue?';

            common.confirmDialog
                .show(message, 'Confirm Saving WorkOrder Name')
                .done(function (okClicked) {
                    if (okClicked) {
                        $(e.currentTarget).closest('form').trigger('submit');
                        setNameFieldPristine();
                    }
                });
            }
            else {
                $(e.currentTarget).closest('form').trigger('submit');
                setNameFieldPristine();
            }
        });
    }

    function getAbbreviation(name) {
        var index = name.indexOf('(');
        return name.slice(index + 1, name.length - 1);
    }

    function setNameFieldDirty() {
        var name = vm.getWorkOrder().Name;

        if (name !== $(nameSelector).val()) {
            _workOrderNameIsDirty = true;
        }
        else {
            setNameFieldPristine();
        }
    }

    function setNameFieldPristine() {
        _workOrderNameIsDirty = false;
    }

})(app.workorders, app.common);