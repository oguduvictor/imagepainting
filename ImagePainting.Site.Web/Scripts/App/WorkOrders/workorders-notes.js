﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm) {
    'use strict';

    var resolvedNotes;
    var unresolvedNotes;

    vm.initNotes = function () {
        $('div.note-block').on('click', 'a.unresolved-notes', showResolvePopOver);

        setNoteAlerts();

        function setNoteAlerts() {
            var workOrderId = vm.getWorkOrder().Id;

            $.get('/SystemAlert/GetNoteAlerts/', { workOrderId: workOrderId }).then(function (data) {
                resolvedNotes = data.filter(function (x) { return x.Resolved; });
                unresolvedNotes = data.filter(function (x) { return !x.Resolved; });

                $('a.unresolved-notes, span.resolved-notes').remove();

                showIcons();
            });
        }

        function showIcons() {
            $('blockquote').each(function (index, element) {
                var id = element.dataset.id;
                var elementResolved = resolvedNotes.find(function (x) { return x.Note.Note && x.Note.Note.Id === id; });
                var elementUnResolved = unresolvedNotes.find(function (x) { return x.Note.Note && x.Note.Note.Id === id; });

                if (elementResolved) {
                    $(element).children()
                        .append('<span data-toggle="tooltip" data-placement="bottom" class="glyphicon glyphicon-ok-sign text-success resolved-notes" title="Resolved by '
                        + elementResolved.ResolvedBy + ' on ' + elementResolved.Resolved + '"></span>');
                }

                else if (elementUnResolved) {
                    $(element).children()
                        .append('<a class="unresolved-notes" data-noteid=' + elementUnResolved.Id + '><span class="glyphicon glyphicon-question-sign"></span></a>');
                }
            });

            $('[data-toggle="tooltip"]').tooltip();
        }

        function resolveNote(e) {
            e.preventDefault();

            var $target = $(e.currentTarget);
            var alertId = $target.closest('div.popover').siblings('a.unresolved-notes').data('noteid');

            $.post('/SystemAlert/ResolveAlert/', { id: alertId }).then(function () {
                $('div.popover').remove();
                setNoteAlerts();
            });
        }

        function showResolvePopOver(e) {
            e.preventDefault();

            $(this).popover({
                title: '<small>This note is yet to<br>be acknowledged</small>',
                content: '<button class="btn btn-primary acknowledgeBtn" style="display: block; margin: 0 auto;">Acknowledge</button>',
                html: true,
                placement: 'bottom',
                trigger: 'manual'
            });

           $('div.note-block').off('click').on('click', 'button.acknowledgeBtn', resolveNote);
           $('div.note-block').on('click', 'a.unresolved-notes', showResolvePopOver);
            $(this).popover('toggle');
        }
    }

})(app.workorders);