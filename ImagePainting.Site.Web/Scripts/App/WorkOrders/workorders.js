﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm, common) {
    'use strict';

    var workOrderDetails = { Id: 0 };
    var isUserAdmin = false;
    var userRoles = [];
    var $mainContainer = $('div#wo-container');
    var $partialViewContainer = $('div#wo-partial-view');

    var _currentSchema;
    var _mainFormIsDirty = false;
    var _partialFormIsDirty = false;

    vm.initWorkOrders = function (workOrderId, roles) {
        isUserAdmin = common.contains('Administrator', roles);
        userRoles = roles;
        workOrderDetails.Id = workOrderId;

        if (workOrderId) {
            vm.loadWorkOrder().done(enableCurrentWorkflowSteps);
            return;
        }

        enableCurrentWorkflowSteps();
    };

    vm.enablePartialView = function (url, doNotReplace) {
        var confirmMessage = 'You are about to navigate away from this page, all your unsaved changes will be lost.';
        var deferred = $.Deferred();

        if (_partialFormIsDirty || _mainFormIsDirty) {
            common.confirmDialog
                .show(confirmMessage)
                .done(function (okClicked) {
                    if (okClicked) {
                        loadView(url, doNotReplace, deferred);
                    } else {
                        // change hash in order to be able to navigate to the same url again
                        // TODO: see why this is not being called.
                        location.hash = 'no-navigate';
                    }
                });
        } else {
            loadView(url, doNotReplace, deferred);
        }

        return deferred.promise();
    };

    vm.dismissPartialView = function () {
        $partialViewContainer.html('').hide(500);
        $mainContainer.show(500);
    };

    vm.moveNext = function () {
        activateAllValidLinks();

        var nextNode = getNextNode();

        if (!nextNode) {
            return;
        }

        var nodeIsObject = $.isPlainObject(nextNode);
        var node = nodeIsObject ? nextNode.node : nextNode;
        var subnode = nodeIsObject ? nextNode.subnode : '';
        var nextSchema = vm.workflowSchema[node];
        var url = activateLink(node, subnode, nextSchema, true);

        location.hash = url;
    };

    vm.processMainPageForm = function () {
        var $form = getMainPageForm();
        var pageData = vm.getPageData() || {};

        vm.setMainFormPristine();
        vm.setPartialFormPristine();

        // Sets currrent schema to match current page
        _currentSchema = vm.workflowSchema[pageData.node];

        highlightActiveLink(pageData.node, pageData.subnode);

        if (!$form) {
            $mainContainer.find('#btn-continue').click(function () {
                vm.moveNext();
            });

            return;
        }

        common.initFormValidation($form);

        enableChangeTracking($form, vm.setMainFormDirty);

        $form.submit(function (e) {
            e.preventDefault();

            var url = e.target.action;

            // if not valid, return
            if (!common.valid(e.target)) {
                return;
            }

            // if not dirty and valid, move next
            if (!_mainFormIsDirty) {
                vm.moveNext();
                return;
            }

            // if dirty and valid, submit form
            $.post(url, $form.serialize())
                .done(function (d) {
                    if (_currentSchema.onFormPost) {
                        vm[_currentSchema.onFormPost](d);
                    }

                    vm.setMainFormPristine();
                    vm.moveNext();
                })
                .fail(function (err) {
                    toastr.error('An error occured on the server.');
                    console.error(err);
                });
        });

        $form.find('#btn-cancel').click(function (e) {
            e.preventDefault();

            // TODO: go previous
        });
    };

    vm.processPartialPageForm = function (onStartFormSubmit) {
        var deferred = $.Deferred();
        var $form = $partialViewContainer.find('form:not(#frm-upload):first');

        vm.setPartialFormPristine();

        if ($form.length < 1) {
            return;
        }

        common.initFormValidation($form);

        $form.submit(function (e) {
            e.preventDefault();

            var url = e.target.action;

            if (onStartFormSubmit && $.isFunction(onStartFormSubmit)) {
                onStartFormSubmit();
            }

            // if not valid, return
            if (!common.valid(e.target)) {
                return;
            }

            $.post(url, $form.serialize())
                .done(function (d) {
                    vm.setPartialFormPristine();
                    deferred.resolve(d);
                })
                .fail(function (err) {
                    console.error(err);
                    toastr.error('Error occurred on the server.');
                    deferred.reject(err);
                });
        });

        $form.find('#btn-cancel').click(function (e) {
            e.preventDefault();
            deferred.resolve(null);
            vm.dismissPartialView();
        });

        enableChangeTracking($form, vm.setPartialFormDirty);

        return deferred.promise();
    };

    vm.loadWorkOrder = function () {
        var deferred = $.Deferred();
        var id = workOrderDetails.Id;

        $.get('/workorders/workOrderDetails/' + id)
            .done(function (d) {
                vm.setWorkOrder(JSON.parse(d)); // TODO: remove parse when result is fixed from server
                deferred.resolve(d);
            })
            .fail(function (err) {
                deferred.reject(err);
                toastr.error('Error occurred while loading your work order.');
                console.error(err);
            });

        return deferred.promise();
    };

    vm.setWorkOrder = function (wo) {
        workOrderDetails = wo;

        if (wo.Name) {
            $('#workorder-name').text(wo.Name);
        }
    };

    vm.getWorkOrder = function () {
        return workOrderDetails;
    };

    vm.setWorkOrderField = function (value) {
        if (_currentSchema.completeField) {
            workOrderDetails[_currentSchema.completeField] = value;
        }
    };

    vm.getPageData = function () {
        return $('.wo-step-screen:first').data();
    };

    vm.isAdmin = function () {
        return isUserAdmin;
    };

    vm.setMainFormDirty = function () {
        _mainFormIsDirty = true;
    };

    vm.setMainFormPristine = function () {
        _mainFormIsDirty = false;
    };

    vm.setPartialFormDirty = function () {
        _partialFormIsDirty = true;
    };

    vm.setPartialFormPristine = function () {
        _partialFormIsDirty = false;
    };

    function enableChangeTracking($form, action) {
        $form.on('change', 'input, textarea', action);

        $form.on('change', 'select', function (e) {
            if (e.target.value !== '0') {
                action();
            }
        });
    }

    function loadView(url, doNotReplace, deferred) {
        vm.setMainFormPristine();
        vm.setPartialFormPristine();

        if (doNotReplace) {
            $partialViewContainer.load(url,
                function () {
                    $mainContainer.hide(500);
                    $partialViewContainer.show(500);

                    $partialViewContainer.find('[data-mask]').mask('(000) 000-0000');

                    if (deferred) {
                        deferred.resolve(true);
                    }
                });
        } else {
            $mainContainer.load(url, function () {
                $partialViewContainer.hide(500);
                $mainContainer.show(500);

                if (deferred) {
                    deferred.resolve(true);
                }

                vm.processMainPageForm();
            });
        }
    };

    function getMainPageForm() {
        var $forms = $mainContainer.find('form');

        for (var i = 0; i < $forms.length; i++) {
            var $form = $($forms[i]);

            if ($form.find('#btn-continue').length == 1) {
                return $form;
            }
        }

        return null;
    }

    function getNextNode(currentSchema) {
        var nextNode;
        var schema = currentSchema || _currentSchema;

        if ($.isFunction(vm[schema.next])) {
            nextNode = vm[schema.next](workOrderDetails);
        } else {
            nextNode = schema.next;
        }

        if (typeof (nextNode) === 'string' && !hasAccessToNode(nextNode)) {
            return getNextNode(vm.workflowSchema[nextNode]);
        }

        return nextNode;
    }

    function enableCurrentWorkflowSteps() {
        var workflow = vm.workflowSchema;
        var node = workflow.start;
        var currentUrl;

        activateAllValidLinks();

        var url = activateLink(node, '', workflow[node], true);
        var hash = location.hash;

        if (!hash || !common.endsWith(hash, '|workorders')) {
            location.hash = url;
        }

        //var workflow = vm.workflowSchema
        //var node = workflow.start;

        //activateAllValidLinks();

        //do {
        //    var schema = workflow[node];
        //    var newNode = workOrderDetails[schema.completeField] ? schema.next : null;

        //    if (!newNode) {
        //        if (isWorkOrderFunction(schema.next)) {
        //            newNode = workflow.start;
        //        } else {
        //            newNode = node;
        //        }

        //        var url = activateLink(newNode, '', schema, true);
        //        location.hash = url;

        //        break;
        //    }

        //    node = newNode;

        //} while (Boolean(node));
    }

    function activateAllValidLinks() {
        for (var node in vm.workflowSchema) {
            var schema = vm.workflowSchema[node];

            if (!hasAccessToNode(node)) {
                continue;
            }

            if (schema.title) {
                if (!schema.subNodes) {
                    activateLink(node, '', schema);
                } else {
                    addDropDownLinks(node, schema);
                }
            }
        }
    }

    function activateLink(node, subnode, schema, forceLink) {
        var nodeId = getNodeId(node, subnode);
        var $progressLinks = $('ul.progress-items');
        var $linkNode = $progressLinks.find('div#' + nodeId);
        var url = getUrlForPartial(schema.url, subnode);
        var completedField = getCompleteField(schema);

        if ($linkNode.length === 0) {
            var linkHtml = '<li>';
            linkHtml += '<div id="' + nodeId + '">';
            linkHtml += '<i class="fa ' + schema.icon + '"></i>';
            linkHtml += '<span class="hidden-sm hidden-xs">' + schema.title + '</span>';
            linkHtml += '</div>';
            linkHtml += '</li>';

            $progressLinks.append(linkHtml);
            $linkNode = $progressLinks.find('div#' + nodeId);
        }

        if (!forceLink && (!completedField || ($.isArray(completedField) && completedField.length == 0))) {
            return url;
        }

        var innerContent = $linkNode.html();

        if ($linkNode.find('a').length > 0) { //if already has link
            innerContent = $linkNode.find('a').html();
        }

        var link = '<a href="#' + url + '" title="' + schema.title + '">' + innerContent + '</a>';

        $linkNode.html(link);

        return url;
    }

    function addDropDownLinks(node, schema) {
        var $parentLink = $('ul.progress-items div#' + node);
        var childLinksHtml = '';
        var subNodes = vm[schema.subNodes](workOrderDetails);

        if (!subNodes) {
            return null;
        }

        var url;

        if ($parentLink.length === 0) {
            $('ul.progress-items').append('<li><div id="' + node + '"><i class"' + schema.icon + '"></><span class="hidden-sm hidden-xs">' + schema.title + '</span></div><ul></ul></li>');
            $parentLink = $('ul.progress-items div#' + node);
        }

        for (var i = 0; i < subNodes.length; i++) {
            var subNode = subNodes[i];
            var nodeId = getNodeId(node, subNode.Id);
            var deferredSymbol = "";

            if (subNode.Deferred) {
                deferredSymbol = "D";
            }

            var deferredSymbolElement = '<sup class="deferred-symbol" id="deferred-symbol' + subNode.Id + '">' + deferredSymbol + '</sup>';

            url = vm[schema.url](workOrderDetails, subNode.Id) + '|workorders';
            childLinksHtml += '<li><div id="' + nodeId + '"><a href="#' + url + '" title="' + subNode.Name + '"><i class="fa ' + schema.icon + '">' + deferredSymbolElement + '</i><span class="hidden-sm hidden-xs">' + subNode.Name + '</span></a></div></li>';
        }
        
        $parentLink.find('span').html(schema.title);
        $parentLink.siblings('ul').html(childLinksHtml);

        return url;
    }

    function getUrlForPartial(url, subnode) {
        return getUrl(url, subnode) + '|workorders';
    }

    function getUrl(url, subnode) {
        if (!url) {
            return '';
        }

        if ($.isFunction(vm[url])) {
            return vm[url](workOrderDetails, subnode);
        }

        var variables = url.match(/(\{[a-zA-Z0-9]+\})/g);

        if (!variables || variables.length < 1) {
            return url;
        }

        var resolvedurl = url;

        for (var i = 0; i < variables.length; i++) {
            var variable = variables[i];
            var prop = variable.substr(1, variable.length - 2);

            resolvedurl = resolvedurl.replace(variable, workOrderDetails[prop]);
        }

        return resolvedurl;
    }

    function highlightActiveLink(node, subnode) {
        var id = getNodeId(node, subnode);

        $('.progress-items div').removeClass('active');
        $('.progress-items div#' + id).addClass('active');
    }

    function getNodeId(node, subnode) {
        return node + (subnode ? '_' + subnode : '');
    }

    function getCompleteField(schema) {
        if (isWorkOrderFunction(schema.completeField)) {
            return vm[schema.completeField](workOrderDetails);
        }

        return workOrderDetails[schema.completeField];
    }

    function isWorkOrderFunction(functionName) {
        return $.isFunction(vm[functionName]);
    }

    function hasAccessToNode(node) {
        var schema = vm.workflowSchema[node];
        
        if (!schema) return false;

        if (!isUserAdmin && schema.requiresAdmin) {
            return false;
        }

        if (schema.requiresRoles && $.isArray(schema.requiresRoles)) {
            for (var i = 0; i < schema.requiresRoles.length; i++) {
                if (common.contains(schema.requiresRoles[i], userRoles)) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

})(app.workorders, app.common);