﻿var app = app || {};

app.workorders = app.workorders || {};
(function (vm, common, tasksCommon) {
    'use strict';

    var completedTasks;

    vm.initScheduleTasks = function (tasks) {
        completedTasks = tasks;
        hideDisabledDropdownOptions();

        $("input[type=checkbox]:checked").click(handleCheckBoxClicked);
        $('#scheduleTaskLink').click(function (e) {
            $('#ScheduleNewTask').toggleClass('hidden');
        });
        $('#ScheduleNewTask').change(handleScheduleNewTaskDropDown);
        $('#scheduleForm').on('change', 'select.item-task-user-id, input.item-task-date', checkIfCrewIsBusy);
    };

    function checkIfCrewIsBusy(e) {
        e.preventDefault();

        var $target = $(e.currentTarget);
        var $taskRows = $target.closest('div.task-rows');
        var crewId = $taskRows.find('select.item-task-user-id').val();
        var date = $taskRows.find('input.item-task-date').val();

        tasksCommon.confirmCrewNotBusy(crewId, date)
            .done(function (ok) {
                $target.data('value', $target.val());
            })
            .fail(function (ok) {
                $target.val($target.data('value'));
            });
    }

    function hideDisabledDropdownOptions() {
        $('select#ScheduleNewTask option:disabled').removeProp('disabled').addClass('hidden');
    }

    function handleScheduleNewTaskDropDown(e) {
        var target = e.currentTarget;
        var $parentRow = $(target).closest('div.row');

        $parentRow.before(generateTaskRow(target));

        target.selectedOptions[0].className = 'hidden';
        target.value = '';
        $('#scheduleTaskLink').trigger('click');
    }

    function generateTaskRow(target) {
        var schemaId = target.value;
        var taskName = target.selectedOptions[0].innerText;
        var $clonedHTML = $('#cloneable').clone().removeClass('hidden').removeAttr('id');
        var newIndex = $('form .task-rows').length;

        $clonedHTML.find('input, select, label').each(function (index, item) {
            if (item.localName === 'label') {
                item.lastChild.textContent = generateTaskLabelName(taskName);

                $(item).append('<input type="hidden" value="' + item.lastChild.textContent + '" name="[' +
                    newIndex + '].TaskName" class="item-task-name" />');
            }
            else {
                item.name = '[' + newIndex + item.name.substr(item.name.indexOf('].'));
                if (common.endsWith(item.name, 'SchemaId')) {
                    item.value = schemaId;
                }
            }
        });

        return $clonedHTML;
    }

    function generateTaskLabelName(taskName) {
        if (completedTasks.indexOf(taskName) == -1) {
            return taskName;
        }
        var lastTaskInList = findLast(taskName);

        if (common.endsWith(lastTaskInList, ')')) {
            var number = Number(lastTaskInList.substring(Number(lastTaskInList.lastIndexOf('(')) + 1, lastTaskInList.length - 1));
            return lastTaskInList.slice(0, lastTaskInList.lastIndexOf('(') + 1) + (number + 1) + ')';
        }
        else {
            return lastTaskInList + ' (' + 1 + ')';
        }
    }

    function handleCheckBoxClicked(e) {
        var $checkbox = $(e.currentTarget);

        if ($checkbox[0].checked) {
            return;
        }

        e.preventDefault();

        var taskName = $checkbox.parent("label").text();
        var message = 'You just unchecked \'' + taskName + '\' and it will be permanently deleted from the system. Click OK to continue.';

        common.confirmDialog.show(message).done(function (d) {
            $checkbox[0].checked = false;
            vm.setMainFormDirty();
        });
    }

    function findLast(taskName) {
        for (var i = completedTasks.length - 1; i >= 0; i--) {
            var item = completedTasks[i];
            var exists = item.indexOf(taskName) != -1;

            if (exists) {
                return item;
            }
        }
    }

})(app.workorders, app.common, app.tasksCommon);
