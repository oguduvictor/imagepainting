﻿var app = app || {};

app.workorders = app.workorders || {};

app.workorders.workflowSchema = {
    start: 'settings',

    settings: {
        title: 'Initial Settings',
        url: '/workOrders/initialSettings/{Id}',
        completeField: 'Id', // field that must have a value for this step to be considered complete
        onFormPost: 'setWorkOrder', // Name of the function that runs after the form is submitted
        next: 'jobInfo', // name of the next schema to run
        previous: '', // name of the previous schema
        icon: 'fa-cogs'
    },

    jobInfo: {
        title: 'Job Information',
        url: '/workOrders/JobInfo/{Id}',
        completeField: 'JobInfoId',
        next: 'extraJob',
        onFormPost: 'setWorkOrderField',
        previous: 'settings',
        icon: 'fa-calculator'
    },

    extraJob: {
        title: 'Job Extras',
        url: '/workOrders/ExtraJob/{Id}',
        completeField: 'ExtraJobInfoId',
        next: 'scheduleTasks',
        previous: 'jobInfo',
        onFormPost: 'setWorkOrderField',
        icon: 'fa-drivers-license'
    },

    scheduleTasks: {
        title: 'Schedule Tasks',
        url: '/workOrders/ScheduleTasks/{Id}',
        completeField: 'Tasks',
        next: 'viewTasks',
        previous: '',
        onFormPost: 'setWorkOrderField',
        requiresAdmin: true,
        icon: 'fa-calendar'
    },

    viewTasks: {
        title: 'Tasks',
        url: 'getTaskUrl',
        subNodes: 'getSubNodes',
        completeField: 'getTaskStepCompleted',
        next: 'getNextTaskStep',
        previous: '',
        icon: 'fa-tasks'
    },

    workOrderNotes: {
        title: 'Notes',
        url: '/WorkOrders/Notes/{Id}',
        completeField: 'Tasks',
        next: 'fileUploads',
        previous: '',
        requiresRoles: ['Administrator', 'Employee'],
        icon: 'fa-sticky-note'
    },

    fileUploads: {
        title: 'File Uploads',
        url: '/WorkOrders/FileUploads/{Id}',
        completeField: 'Tasks',
        next: 'revenueProjection',
        previous: '',
        requiresRoles: ['Administrator'],
        icon: 'fa-file'
    },

    revenueProjection: {
        title: 'Revenue Projection',
        url: '/projection/RevenueProjection/{Id}',
        completeField: 'JobInfoId',
        next: 'laborProjection',
        previous: '',
        requiresAdmin: true,
        icon: 'fa-industry'
    },

    laborProjection: {
        title: 'Labor Projection',
        url: '/projection/LaborProjection/{Id}',
        completeField: 'Tasks',
        next: 'purchaseOrders',
        previous: '',
        requiresAdmin: true,
        icon: 'fa-line-chart'
    },

    //materialProjection: {
    //    title: 'Material Projection',
    //    url: '/projection/MaterialProjection/{Id}',
    //    completeField: 'Tasks',
    //    next: 'purchaseOrders',
    //    previous: '',
    //    requiresAdmin: true,
    //    icon: 'fa-bar-chart-o'
    //},

    purchaseOrders: {
        title: 'Purchase Orders',
        url: '/PurchaseOrders/WorkOrderPurchaseOrders/{Id}',
        completeField: 'Tasks',
        next: 'profitProjection',
        previous: '',
        requiresAdmin: true,
        icon: 'fa-credit-card'
    },

    profitProjection: {
        title: 'Profit Projection',
        url: '/projection/ProfitProjection/{Id}',
        completeField: 'Tasks',
        next: 'completion',
        previous: '',
        requiresAdmin: true,
        icon: 'fa-calculator'
    },
    
    completion: {
        title: 'Completion',
        url: '/workorders/Complete/{Id}',
        completeField: 'areAllTasksCompleted',
        next: null, // setting it as empty string causes stack overflow error on workorder.getNextNode()
        previous: '',
        requiresAdmin: true,
        onFormPost: 'loadWorkOrder',
        icon: 'fa-calendar-check-o'
    }
};