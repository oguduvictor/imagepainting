﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm, viewTaskCommon) {
    'use strict';

    var tasks;
    var currentTask;
    var stepAfterTasks = 'revenueProjection';
    var taskStep = 'viewTasks';

    vm.initTaskButtons = function () {
        viewTaskCommon.init(reloadPage);

        // method for performing reload when task is saved, completed or unlocked
        function reloadPage() {
            var taskId = $('#mainTask').val();
            var url = '/workOrders/Task/' + taskId;

            vm.enablePartialView(url, false);
        }
    };

    vm.getSubNodes = function (workOrder) {
        return workOrder.Tasks;
    };

    vm.getTaskUrl = function (workOrder, id) {
        if (!workOrder.Tasks || workOrder.Tasks.length < 1) {
            return null;
        }

        if (!id) {
            id = workOrder.Tasks[0].Id;
        }

        return '/workOrders/Task/' + id;
    };

    vm.getNextTaskStep = function (workOrder) {
        var tasks = workOrder.Tasks;
        var pageData = vm.getPageData();
        var step = { node: pageData.node, subnode: null };

        if (!pageData.subnode) {
            step.subnode = tasks[0].Id;
        } else {
            var taskId = getNextTaskId(tasks, pageData.subnode);

            if (!taskId) {
                return stepAfterTasks;
            }

            step.subnode = taskId;
        }

        return step;
    };

    vm.getTaskStepCompleted = function (workOrder) {
        return workOrder.Tasks && workOrder.Tasks.length > 0;
    };

    function getNextTaskId(tasks, currentTaskId) {
        if (!tasks || tasks.length < 1) {
            return null;
        }

        var currentIndex = null;

        for (var i = 0, len = tasks.length; i < len; i++) {
            if (tasks[i].Id === currentTaskId) {
                currentIndex = i;
                break;
            }
        }

        if (currentIndex !== null && currentIndex < tasks.length - 1) {
            return tasks[currentIndex + 1].Id;
        }

        return null;
    }
    
})(app.workorders, app.viewTaskCommon);
