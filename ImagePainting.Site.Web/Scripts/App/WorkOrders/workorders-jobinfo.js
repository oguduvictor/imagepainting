﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm) {
    'use strict';

    var $mainContainer = $('div#wo-container');
    var $partialViewContainer = $('div#wo-partial-view');

    var planSelector = 'div#wo-plans select';
    var elevationSelector = 'div#wo-elevations select';

    var exteriorColorSelector = 'select#exterior-color-select';
    var interiorColorSelector = 'select#interior-color-select';

    var firstFloor = 'input#FirstFloorSqft';
    var secondFloor = 'input#SecondFloorSqft';
    var garageSqft = 'input#GarageSqft';
    var lanaiSqft = 'input#LanaiSqft';
    var workOrder;

    vm.initWorkOrderJobInfo = function () {
        setWorkOrderId();

        $(firstFloor).keyup(calculateTotal);
        $(secondFloor).keyup(calculateTotal);
        $(garageSqft).keyup(calculateTotal);
        $(lanaiSqft).keyup(calculateTotal);
        $(planSelector).change(handlePlanChange);
        $(elevationSelector).change(handleElevationChange);
        $(exteriorColorSelector).change(handleExteriorColorChange);
        $(interiorColorSelector).change(handleInteriorColorChange);
        $('span.btn-edit-note').click(showNoteTextArea);

        calculateTotal();
    };

    vm.setJobInfo = function (jobInfo) {
        var workOrder = vm.getWorkOrder();

        workOrder.JobInfoId = jobInfo.Id;
    };

    function showNoteTextArea() {
        var $noteContainer = $(this).parents('div.note-container');

        $noteContainer.find('textarea[name^=Note]').removeClass('hidden');
        $noteContainer.find('textarea[name^=ReadOnlyNote]').hide();
    }

    function sortDropdowns($select) {
        var $options = $select.find('option:not([value=""]):not([value="0"])');
        
        $options.sort(function (item1, item2) {
            var item1text = item1.innerText.toLowerCase();
            var item2text = item2.innerText.toLowerCase();

            return item1text > item2text ? 1 : item1text < item2text ? -1 : 0;
        });

        //Remove existing options from DOM
        $options.remove();

        // Append newly sorted options
        $select.append($options);
    }

    function calculateTotal() {
        var firstVal = Number($(firstFloor).val() || 0);
        var secondVal = Number($(secondFloor).val() || 0);
        var lanaiVal = Number($(lanaiSqft).val() || 0);
        var garageVal = Number($(garageSqft).val() || 0);
        var totalLiving = firstVal + secondVal;
        var totalURSqft = totalLiving + lanaiVal + garageVal;

        $('input#totalLiving').val(totalLiving);
        $('input#totalURSqft').val(totalURSqft);
    }

    function handlePlanChange(e) {
        e.preventDefault();

        var value = e.target.value;

        if (value === '0') {
            vm.enablePartialView('/communities/plan/0?partial=true', true)
            .done(function () {
                vm.processPartialPageForm().done(function (d) {

                    var $element = $(e.target);
                    if (d) {
                        var option = '<option value="' + d.Id + '">' + d.Name + '</option>';

                        $element.append(option);
                        $element.val(d.Id);

                        sortDropdowns($element);

                        vm.dismissPartialView();
                        $element.change();
                    } else {
                        $element.val('');
                    }
                });
                var wo = vm.getWorkOrder();
                $('#BuilderId').val(wo.BuilderId);
            });
        }
        else if (Number(value)) {
            var url = '/workorders/elevations/' + value;

            $mainContainer.find(elevationSelector).prop('disabled', false).load(url);
        }
    }

    function handleExteriorColorChange(e) {
        colorChange(e, 'ExteriorColors');
    }

    function handleInteriorColorChange(e) {
        colorChange(e, 'InteriorColors');
    }

    function colorChange(e, elementNamePrefix) {
        e.preventDefault();
        var value = e.target.value;

        if (value === 'new') {
            vm.enablePartialView('/communities/' + elementNamePrefix + 'cheme/0?partial=true', true)
            .done(function () {
                vm.processPartialPageForm().done(function (d) {
                    if (d) {
                        var $element = $(e.target);
                        var option = '<option value="' + d.Id + '">' + d.ColorCode + '</option>';

                        $element.append(option);
                        $element.val(d.Id);

                        vm.dismissPartialView();
                        $element.change();

                        handleColorChange(d.Id, elementNamePrefix);
                        vm.setMainFormDirty();
                    }
                    else {
                        vm.setMainFormPristine();
                    }
                });
            });
        }
        else if (value) {
            handleColorChange(value, elementNamePrefix);
        }
    }

    function handleColorChange(value, elementNamePrefix) {
        if (!value) {
            updateColors({}, elementNamePrefix);
            return;
        }

        var url = '/communities/'+ elementNamePrefix +'cheme/' + value + '?json=true';

        $.get(url).done(function (d) {
            updateColors(d, elementNamePrefix);
        });
    }

    function updateColors(data, elementNamePrefix) {
        $('input[name^="' + elementNamePrefix + '."]').each(function (idx, element) {
            var fieldNamePaths = element.name.split('.');
            var fieldName = fieldNamePaths.length > 1 ? fieldNamePaths[1] : null;

            if (!fieldName) return;

            element.value = data[fieldName] || '';
        });
    }

    function handleElevationChange(e) {
        e.preventDefault();

        var value = e.target.value;

        if (value === '0') {
            vm.enablePartialView('/communities/elevation/0?partial=true&planId=' + $(planSelector).val(), true)
            .done(function () {
                vm.processPartialPageForm().done(function (d) {
                    var $element = $(e.target);
                    if (d) {
                        var option = '<option value="' + d.Id + '">' + d.Name + '</option>';

                        $element.append(option);
                        sortDropdowns($element);
                        $element.val(d.Id);

                        vm.dismissPartialView();
                        $element.change();
                    } else {
                        $element.val('');              
                    }
                    
                });
            });
        }
        else if (value) {

            var url = '/communities/elevation/' + value + '?json=true';
            $.get(url).done(function (d) {
                $(firstFloor).val(d.FirstFloorSqft);
                $(secondFloor).val(d.SecondFloorSqft);
                $(garageSqft).val(d.GarageSqft);
                $(lanaiSqft).val(d.LanaiSqft);
                calculateTotal();
            });
        }
    }

    function setWorkOrderId() {
        workOrder = vm.getWorkOrder();
        $('#workOrderId').val(workOrder.Id);
    }

})(app.workorders);