﻿var app = app || {};

app.workorders = app.workorders || {};

(function (vm, common) {
    'use strict';

    vm.isEmailing = false;
    vm.emailUrl = null;
    vm.previewUrl = null;
    vm.isPreviewing = false;
    vm.isPartialView = false;
    vm.poLinkRef = null;
    vm.poLinkRowId = null;
    vm.referrer = document.referrer;
    vm.parsedSuppliesJSON = null;

    vm.redirect = function () {
        if (!vm.isPartialView) {
            var taskid = $(':hidden[name=TaskId]').val();
            
            vm.referrer = common.getAppRoot() + 'Tasks/ViewTask/' + taskid;
        }

        location.href = vm.referrer;
    };

    vm.initPurchaseOrder = function () {
        $('#btn-submit').click(handleFormSubmit);
        $('select#storeId').change(handleStoreChange);
        $('#btn-email').click(emailPurchaseOrder);
        $('#btn-email-again').click(emailPurchaseOrderAgain);
        $('#btn-preview').click(previewPurchaseOrder);
        $('#btn-delete').click(deletePurchaseOrder);
        $('#colorsPreview').click(previewColors);
        $('#btn-unlock').click(unlockPurchaseOrder);

        vm.initPurchaseOrderItemRows();
        setDefaultSelection();
        setSuppliesJSON();
    };

    function setSuppliesJSON() {
        vm.parsedSuppliesJSON = JSON.parse($('#SuppliesJSON').val());
    }
    
    vm.initPurchaseOrderItemRows = function () {
        var $viewContainer = $('div#wo-partial-view');
        var $newItemBtn = $('#btn-new-item');

        if (!vm.isPartialView) {
            $viewContainer = $('form#purchaseorderform');
            $('form#purchaseorderform #btn-cancel').click(function () {
                vm.redirect();
            });
        }

        $('select#paintLocation').change(handleLocationChange);

        //in case there are other listeners
        $newItemBtn.off('click');
        $viewContainer.off('click', 'td .item-remove');
        $viewContainer.off('change', 'td .total-calculator');

        $newItemBtn.click(addItemToPurchaseOrder);
        $viewContainer.on('click', 'td .item-remove', removeItemFromPurchaseOrder);
        $viewContainer.on('change', 'td .total-calculator', calculateTotalValue);

        if ($('#purchaseOrder').hasClass('view-only')) {
            $('#purchaseOrder').find('.can-be-disabled').prop('disabled', true);
        }
    };

    vm.initPurchaseOrders = function () {
        $('div#wo-container').off('click', 'a.order', showPurchaseOrder).on('click', 'a.order', showPurchaseOrder);
        $('#newOrder').click(showPurchaseOrder);
    };

    function setDefaultSelection(){
        var $store = $('select#storeId');
        if (!($store.is(':disabled')) && $store.val() == '') {
            var firstOption = $($store.find('option')[1]);
            if(!!firstOption){
                $store.val(firstOption.val());
            }
            
            $store.trigger('change');
        }
    }

    function calculateTotalValue(event) {
        var $target = $(event.target);
        var $currentRow = $target.closest('.item-row');
        var qty = parseFloat($currentRow.find('.item-quantity').val() || '0');
        var price = parseFloat($currentRow.find('.item-unit-price').val() || '0');
        var rowTotal = qty * price;

        $currentRow.find('.total-value').text('$' + common.roundToDecimalWithCommas(rowTotal, 2));

        setGrandTotal();
    }

    function setGrandTotal() {
        var sum = 0.0;

        $.each($('.total-value'), function (index, element) {
            var amount = parseFloat($(element).text().replace(/[\$\,]/g, ''));
            
            if (amount > 0) {
                sum += amount;
            }
        });

        $('.grand-total').text('$' + common.roundToDecimalWithCommas(sum, 2));
    }

    function removeItemFromPurchaseOrder(e) {
        e.preventDefault();
        $(e.target).closest('tr.item-row').remove();
        $('.total-calculator').trigger('change');
    }

    function addItemToPurchaseOrder(e) {
        e.preventDefault();

        var newClone = $('.item-row-cloneable').clone().removeClass('item-row-cloneable').addClass("item-row");
        $(newClone.show()).insertBefore('.total-row');
        var inputBox = newClone.find('.item-supply-type');
        var selectDropdownSource = $('#purchaseOrder .supply-type-list');
        common.convertToComboBoxes(inputBox, selectDropdownSource);
        inputBox = newClone.find('.item-color');
        selectDropdownSource = $('#purchaseOrder .colors-list');
        common.convertToComboBoxes(inputBox, selectDropdownSource);
        inputBox = newClone.find('.item-name');
        selectDropdownSource = $('#purchaseOrder .supply-name-list');
        common.convertToComboBoxes(inputBox, selectDropdownSource);

        bindPurchaseOrderEventHandlers();
    }

    function normalizeForm(e) {
        $("#purchaseOrder .item-row .tt-hint").remove();
        $('tr.item-row').each(function (index, row) {
            var $row = $(row);

            $row.find('.item-id').attr('name', 'PurchaseOrderItems[' + index + '].Id');
            $row.find('.item-po-id').attr('name', 'PurchaseOrderItems[' + index + '].PurchaseOrderId');
            $row.find('.item-name').attr('name', 'PurchaseOrderItems[' + index + '].Name');
            $row.find('.item-supply-type').attr('name', 'PurchaseOrderItems[' + index + '].SupplyType');
            $row.find('.item-color').attr('name', 'PurchaseOrderItems[' + index + '].Color');
            $row.find('.item-unit-price').attr('name', 'PurchaseOrderItems[' + index + '].UnitPrice');
            $row.find('.item-quantity').attr('name', 'PurchaseOrderItems[' + index + '].Quantity');
            $row.find('.item-total').attr('name', 'PurchaseOrderItems[' + index + '].Total');
        });
    }

    function handleFormSubmit(e) {
        vm.setPartialFormPristine();
        normalizeForm(e);

        if (!vm.isPartialView) {
            return;
        }

        e.preventDefault();

        var taskId = $(e.currentTarget).data('taskid');
        var url = $(e.currentTarget).data('url') + 'taskId=' + taskId;

        var formData = $('form#purchaseorderform:first').serialize();

        $.post(url, formData).then(function (d) {
            callBackFunction(d);
        })
        .done(function () {
            reload();
        });
    }

    function showPurchaseOrder(e, rowId) {
        e.preventDefault();

        vm.isPartialView = true;

        var taskId = vm.getPageData().subnode;

        if (!taskId) {
            console.error('Cannot find the Task ID needed to display the requested page.');
            return;
        }

        var target = e.target;
        var ref = target.href ? target.href.split('?')[0] + "?partial=true&taskId=" + taskId : vm.poLinkRef;
        if (!$(target).hasClass('.new-purchase-order-btn')) {
            vm.poLinkRef = ref;
            vm.poLinkRowId = rowId;
        }
        setupPartialView(ref, rowId);
    }

    function setupPartialView(ref, rowId) {
        vm.enablePartialView(ref + '&rowid=' + (rowId || 0), true).done(function () {
            makeSelectComboBoxes();
            bindPurchaseOrderEventHandlers();
            setSuppliesJSON();
            vm.processPartialPageForm();
        });
    }

    function bindPurchaseOrderEventHandlers() {
        $('#purchaseOrder .item-row .item-supply-type').on('typeahead:close', handleSupplyTypeChange);
    }

    function getMatchingSupplyByName(supplyName) {
        var matchingSupply = null;
        for (var i = 0; i < vm.parsedSuppliesJSON.length;  i++){
            if(vm.parsedSuppliesJSON[i].Name == supplyName){
                matchingSupply = vm.parsedSuppliesJSON[i];
                break;
            }
        }
        return matchingSupply;
    }

    function getMatchingSupplyByType(supplyType) {
        var matchingSupply = null;
        for (var i = 0; i < vm.parsedSuppliesJSON.length; i++) {
            if (vm.parsedSuppliesJSON[i].SupplyType == supplyType) {
                matchingSupply = vm.parsedSuppliesJSON[i];
                break;
            }
        }
        return matchingSupply;
    }

    function handleSupplyTypeChange(event) {
        var $target = $(event.target);
        var supplyType = $target.val();
        var matchingSupply = getMatchingSupplyByType(supplyType);
        var row = $target.closest('.item-row');;
        var itemColor = row.find('.item-color');
        var itemQty = row.find('.item-quantity');
        var itemPrice = row.find('.item-unit-price');
        var itemName = row.find('.item-name');
        if (!!matchingSupply) {
            itemColor.typeahead('val', matchingSupply.Color);
            itemName.typeahead('val', matchingSupply.Name);
            itemQty.val(matchingSupply.Quantity);
            itemPrice.val(matchingSupply.Cost);
            itemQty.trigger('change');
        } else {

            itemColor.typeahead('val', '');
            itemQty.val('1');
        }
        return false;
    }

    function makeSelectComboBoxes() {
        var inputs = $('#purchaseOrder .item-row .item-supply-type');
        var selectDropdownSource = $('#purchaseOrder .supply-type-list');
        common.convertToComboBoxes(inputs, selectDropdownSource);
        inputs = $('#purchaseOrder .item-row .item-color');
        selectDropdownSource = $('#purchaseOrder .colors-list');
        common.convertToComboBoxes(inputs, selectDropdownSource);
        inputs = $('#purchaseOrder .item-row .item-name');
        selectDropdownSource = $('#purchaseOrder .supply-name-list');
        common.convertToComboBoxes(inputs, selectDropdownSource);
    }

    function handleLocationChange(e) {
        var locationId = $('#paintLocation').val();

        $.get('/PurchaseOrders/GetContactEmailForStoreLocation/?locationId=' + locationId, function (data) {
            $('#emailTextBox').val(data);
        });
    }

    function deletePurchaseOrder(e) {
        e.preventDefault();
        var url = $(e.target).data('url');
        var id = $(e.target).data('id');
        $.post(url).then(function () {
            $('#poLink' + id).closest('li').remove();
            $('form#purchaseorderform  #btn-cancel').click();
        });
    }

    function emailPurchaseOrder(e) {
        if (!$(':text[name=Email]').val()) {
            common.okDialog.show('The Email field is required.', 'Email Address Required', 'sm');
            return;
        }

        // if email hasn't been sent before, try to submit the form with a flag to also send email
        if (!$(':hidden[name=EmailSent]').val()) {
            // set sendEmail to true
            $(':hidden[name=sendEmail]').val('true');

            normalizeForm(e);
            $('#purchaseOrder').find('.can-be-disabled').prop('readonly', true);
            $('#btn-submit').trigger('click');
            return;
        }

        var url = $(e.target).data('url');
        vm.emailUrl = url;
        vm.isEmailing = true;
        var emailAddress = $('#emailTextBox').val();

        var orderId = Number($('#purchase-order-id').val());

        vm.isEmailing = false;
    }

    function emailPurchaseOrderAgain(e) {
        e.preventDefault();

        var emailAddress = $(':text[name=Email]').val();

        if (!emailAddress) {
            common.okDialog.show('The Email field is required.', 'Email Address Required', 'sm');
            return;
        }

        var orderId = Number($('#purchase-order-id').val());
        var url = $(e.currentTarget).data('url');

        $.post(url, { id: orderId, emailAddress: emailAddress });
    }

    function unlockPurchaseOrder(e) {
        e.preventDefault();
        var url = $(e.target).data('url');
        var orderId = Number($('#purchase-order-id').val());
        $.post(url + orderId).then(function () {
            reload();
        });
    }

    function previewPurchaseOrder(e) {
        e.preventDefault();
        var url = $(e.target).data('url');

        vm.previewUrl = url;
        vm.isPreviewing = true;

        if (!vm.isPartialView) {
            var formUrl = '/PurchaseOrders/PurchaseOrder';

            var previewUrl = '/PurchaseOrders/PreviewPurchaseOrder/';
            vm.isPreviewing = true;

            var taskId = $(e.currentTarget).data('taskid');
            var formData = $('form#purchaseorderform:first').serialize();

            $.post(formUrl, formData).done(function (d) {
                var newHref = '/PurchaseOrders/PurchaseOrder/' + d.Id + '?taskId=' + taskId;
                var appRoot = common.getAppRoot();

                newHref = appRoot === '/' ? newHref : appRoot + newHref;
                
                $.post(previewUrl + d.Id).then(function (response) {
                    var parsedResponse = JSON.parse(response);
                    app.common.okDialog.show('<div style="overflow-y: scroll;height: 400px;">' + parsedResponse.Body + '</div>', parsedResponse.Subject, 'lg')
                    .fail(function () {
                        location.href = newHref;
                    })
                    .done(function () {
                        location.href = newHref;
                    });
                });

                vm.isPreviewing = false;
            });

            return;
        }

        if ($('#btn-submit').length == 0 || $('#btn-submit').is(':disabled')) {
            var orderId = Number($('#purchase-order-id').val());
            $.post(url + orderId).then(function (response) {
                var parsedResponse = JSON.parse(response);
                app.common.okDialog.show('<div style="overflow-y: scroll;height: 400px;">' + parsedResponse.Body + '</div>', parsedResponse.Subject, 'lg');
            });
            vm.isPreviewing = false;
        } else {
            $('#btn-submit').click();
        }
    }

    function previewColors(e) {
        e.preventDefault();
        var workOrder = vm.getWorkOrder();
        if(workOrder.Id == 0){
            workOrder.Id = $("#work-order-id").val();
        }
        var url = '/workorders/JobInfo/' + workOrder.Id + '?colorsOnly=true';

        $.get(url).then(function (response) {
            var exteriorColors = JSON.parse(response[0].Value);
            var interiorColors = JSON.parse(response[1].Value);
            var title = 'Color selection';

            app.common.okDialog.show('<div style="overflow-y: scroll;height: 400px;">' + GetColorHTML(exteriorColors) +
                GetColorHTML(interiorColors) + '</div>', title, 'lg');
        });
    }

    function SeparateStrings(word) {
        var newSentence = "";
        for (var i = 0; i < word.length; i++)
        {
            if(word[i] == word[i].toUpperCase()){
                newSentence += " ";
            }
            newSentence += word[i];
           
        }
        return newSentence;
    }
    function GetColorHTML(color){
        var colorType = color.hasOwnProperty('InteriorBase') ? 'Interior' : 'Exterior';
        var listsHTML = '';
        var hasOneValue = false;
        var heading = '<h3>' +colorType + ' Colors - ' + (color.ColorCode || '') + '</h3>';
        for (var prop in color) {
            var value = color[prop];
            if (color.hasOwnProperty(prop) && prop !== 'Id'&& prop !== 'ColorCode' && value) {
                var li = '<li class="list-group-item"><strong>' + SeparateStrings(prop) + ':</strong> ' +value + '</li>';
                listsHTML = listsHTML + li;
                hasOneValue = true;
            }
        }
        if(hasOneValue){
            return heading + '<ul class="list-group">' + listsHTML + '</ul>';
        } else {
            return '<h3> No '+ colorType + ' colors</h3>';;
        }
    }

    function handleStoreChange(e) {
        e.preventDefault();

        var value = e.target.value || '0';
        var appRoot = common.getAppRoot();

        appRoot = appRoot == '/' ? '' : appRoot;

        var url = appRoot + '/PurchaseOrders/StoreLocation/' + value;

        $('div#location').load(url, function () {
            $('select#paintLocation').change(handleLocationChange);
            makeSelectComboBoxes();
            bindPurchaseOrderEventHandlers();
            setSuppliesJSON();
        });

        var po_url = appRoot + $('#purchaseOrderItems').find('#purchaseOrderRows').data('url') + value;
        $('#purchaseOrderItems').load(po_url, function () {
            vm.initPurchaseOrderItemRows();
            makeSelectComboBoxes();
            bindPurchaseOrderEventHandlers();
            setSuppliesJSON();
        });
    }

    function reload() {
        var url = vm.poLinkRef;
        if (!url) {
            location.reload();
            return;
        }

        vm.setPartialFormPristine();
        vm.enablePartialView(url, true)
            .done(function () {
                vm.processPartialPageForm();
            });
    }
    
    function callBackFunction(d) {
        if (vm.isEmailing && !!vm.emailUrl) {
            vm.isEmailing = false;
            vm.emailUrl = vm.emailUrl + d.Id;
            $.post(vm.emailUrl).then(function () {
                $('#purchaseOrder').find('.can-be-disabled').prop('disabled', true);
            });
            vm.emailUrl = null;
        }

        if (vm.isPreviewing && !!vm.previewUrl) {
            vm.isPreviewing = false;
            vm.previewUrl = vm.previewUrl + d.Id;
            $.post(vm.previewUrl).then(function (response) {
                var parsedResponse = JSON.parse(response);
                app.common.okDialog.show('<div style="overflow-y: scroll;height: 400px;">' + parsedResponse.Body + '</div>', parsedResponse.Subject, 'lg');
            });
            vm.previewUrl = null;
        }

        if (d && vm.isPartialView) {
            var orderId = Number($('#purchase-order-id').val());
            var $purchaseOrdersList = $('div.purchase-orders-container ul.list-group');
            var taskId = vm.getPageData().subnode;
            var newHref = '/PurchaseOrders/PurchaseOrder/' + d.Id + '?partial=true&taskId=' + taskId;
            var appRoot = common.getAppRoot();

            newHref = appRoot === '/' ? newHref : appRoot + newHref;

            var newPurchaseOrder = '<li class="col-sm-12 list-group-item" id="' + d.PoNumber + '"><a class="order" id=poLink' + d.Id + ' href="' + newHref + '">PO - ' + d.PoNumber + '</a></li>';
            $('#purchase-order-id').val(d.Id);

            if (orderId > 0) {
                $purchaseOrdersList.find('li.list-group-item#' + d.PoNumber).remove();
            }

            $purchaseOrdersList.append(newPurchaseOrder);

            vm.poLinkRef = newHref;
            vm.poLinkRowId = d.Id;

            setupPartialView(vm.poLinkRef, vm.poLinkRowId);
        }
        else {
            vm.dismissPartialView();
            vm.initPurchaseOrders();
        }
    }
})(app.workorders, app.common);