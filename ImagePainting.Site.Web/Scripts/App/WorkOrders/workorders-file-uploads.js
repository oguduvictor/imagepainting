﻿var app = app || {};
app.workorders = app.workorders || {};

(function (vm) {
    'use strict';

    vm.initFileUploads = function () {
        $('#btn-continue').click(vm.moveNext);
    };

    vm.reloadWorkOrderFileslist = function (url) {
        $('#wo-container').load(url);
    };

})(app.workorders);