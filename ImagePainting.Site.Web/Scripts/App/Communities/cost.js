﻿var app = app || {};

app.costs = app.costs || {};

(function (costs, common) {
    'use strict';

    var $costItem;
    var $isExtraCheckBox;


    costs.initCost = function () {
        $costItem = $('#CostItem_Name');
        $isExtraCheckBox = $('#isExtra');

        $('#CostItemId').on('change', toggleCostItemNameField);
        $isExtraCheckBox.change(toggleRequireTaskType);

        $isExtraCheckBox.trigger('change');

        $('.percentage-sqft').keyup(clearOtherSqft);
    }

    function clearOtherSqft(e) {
        $(e.currentTarget).parents('div.row').siblings('div.row').find('.percentage-sqft').val('');
    }

    costs.initLaborcost = function () {
        $('div.draggable').on('dragstart', drag);
        $('div.col-md-3.form-group.place-draggable').on('drop', drop);
        $('div.col-md-3.form-group.place-draggable').on('dragover', allowDrop);
        $('div.col-md-3.form-group.place-draggable').on('dragleave', dragleave);
        $('#saveBtn').on('click', reOrder);

        function allowDrop(e) {
            e.preventDefault();
            $(e.currentTarget).addClass('highlighted');
        }

        function dragleave(e) {
            e.preventDefault();
            $(e.currentTarget).removeClass('highlighted');
        }

        function drag(e) {
            var target = e.currentTarget;
            e.originalEvent.dataTransfer.setData('dragged', target.parentNode.id);
        }

        function drop(e) {
            e.preventDefault();
            var $target = $(e.currentTarget);
            var data = e.originalEvent.dataTransfer.getData('dragged');
            var $draggedDiv = $('#' + data);

            if (isAfter($target, $draggedDiv)) {
                $target.before($draggedDiv);
            }
            else {
                $target.after($draggedDiv);
            }

            $target.removeClass('highlighted');
        }

        function reOrder() {
            $('input.order').each(function (index, item) {
                item.value = index + 1;
            });
        }

        function isAfter($targetDiv, $draggedDiv) {
            return $('.place-draggable').index($targetDiv) < $('.place-draggable').index($draggedDiv);
        }
    }

    function toggleCostItemNameField(e) {
        var target = e.target.value;
        if (target === "" || Number(target) <= 0) {
            $costItem.prop("disabled", false);
            $costItem.prop("placeholder", "Enter Name of Cost Item");
            $('.name-validate').show();
        }
        else {
            $costItem.prop("disabled", true);
            $costItem.prop("placeholder", "");
            $('.name-validate').hide();
        }
    }

    function toggleRequireTaskType(e) {
        var isChecked = e.target.checked;
        var $taskType = $('#CostItem_TaskType');
        var $taskTypeLabel = $('#taskTypeLabel');

        if (isChecked) {
            $taskType.attr('required', 'required');
            $taskTypeLabel.addClass('required-field');
        }
        else {
            $taskType.removeAttr('required');
            $taskTypeLabel.removeClass('required-field');
        }
    }
})(app.costs, app.common)