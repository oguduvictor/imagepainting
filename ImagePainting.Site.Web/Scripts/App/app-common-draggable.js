﻿var app = app || {};

app.common = app.common || {};

(function (vm) {
    vm.initDraggables = function () {
        $('.draggable-container').on('dragstart', '.draggable', drag);
        $('.draggable-container').on('drop', '.draggable', drop);
        $('.draggable-container').on('dragover', '.draggable', allowDrop);
        $('.draggable-container').on('dragleave', '.draggable', dragleave);
    };

    function allowDrop(e) {
        e.preventDefault();
        $(e.currentTarget).addClass('highlighted');
    }

    function dragleave(e) {
        e.preventDefault();
        $(e.currentTarget).removeClass('highlighted');
    }

    function drag(e) {
        var target = e.currentTarget;
        e.originalEvent.dataTransfer.setData('dragged', $(target));
    }

    function drop(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);

        var data = e.originalEvent.dataTransfer.getData('dragged');

        var $draggedDiv = data;

        if (isAfter($target, $draggedDiv)) {
            $target.before($draggedDiv);
        }
        else {
            $target.after($draggedDiv);
        }

        $target.removeClass('highlighted');
    }

    function isAfter($targetDiv, $draggedDiv) {
        var $itemContainer = $targetDiv.parent('div.draggable-container');
        return $itemContainer.children().index($targetDiv) < $itemContainer.children().index($draggedDiv);
    }

})(window);
