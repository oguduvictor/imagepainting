﻿var app = app || {};

app.workordersList = app.workordersList || {};

(function (vm, common) {
    'use strict';

    $('table.workorders-list').on('click', 'td.option-icons button.clone-wo-btn', cloneWorkOrderHandler);
    $('table.workorders-list').on('click', 'td.option-icons button.delete-wo-btn', deleteWorkOrderHandler);

    function cloneWorkOrderHandler(e) {
        var workOrderName = getWorkOrderName(e);
        var url = common.getAppRoot() + 'WorkOrders/CloneWorkOrder';
        var data = { id: getWorkOrderId(e) };
        var confirmMessageTitle = 'Copy work order?';
        var confirmMessage = 'Are you sure you want to create a new work order from "' + workOrderName + '"? The new work order will still require you to fill in more details. Click OK to continue.';
        var redirect = common.getAppRoot() + 'WorkOrders/WorkOrder/{result}';

        common.runExecute(url, redirect, null, data, confirmMessage, confirmMessageTitle);
    }

    function deleteWorkOrderHandler(e) {
        var workOrderName = getWorkOrderName(e);
        var url = common.getAppRoot() + 'WorkOrders/DeleteWorkOrder';
        var data = { id: getWorkOrderId(e) };
        var confirmMessageTitle = 'Delete work order?';
        var confirmMessage = 'Are you sure you want to delete "' + workOrderName + '"?. Click OK to continue.';
        var redirect = common.getAppRoot() + 'WorkOrders/Index';

        common.runExecute(url, redirect, null, data, confirmMessage, confirmMessageTitle);
    }

    function getWorkOrderId(e) {
        return $(e.currentTarget).parents('tr').prop('id');
    }

    function getWorkOrderName(e) {
        return $(e.currentTarget).parents('tr').find('td.name').text();
    }

})(app.workordersList, app.common);
