﻿var app = app || {};

app.supplies = app.supplies || {};

(function (supplies) {
    var $submitBtn = $('#submitBtn');
    var $paintStore = $('#PaintStoreId');

    supplies.initSupplies = function () {
        checkForValidationErrors();

        $submitBtn.click(processFormData);

        $paintStore.change(toggleCanSave);
        $paintStore.change(reloadSupplies);

        supplies.bindHandlers();
      
        makeComboBoxes();
        toggleCanSave();
    };

    supplies.bindHandlers = function(){
        $('button.addSupply-btn').click(addNewSupply);

        $('tbody').on('click', 'button.btn-danger', removeSupplyRow);

        $('tbody').on('keyup', '.supply-description, .supply-cost, .supply-note', validateTableRows);

        $('tbody').on('change', '.supply-type', validateTableRows);
    };

    function checkForValidationErrors() {
        var $validationErrors = $('.field-validation-error');

        if ($validationErrors.length) {
            toastr.error('Failed to save supplies!');
            
            var $collapsibles = $validationErrors.parents('.collapse');

            $collapsibles.collapse('show');
        }

        else {
            $('#exterior').collapse('show');
        }
    }

    function reloadSupplies(event) {
        var $target = $(event.target);
        var storeId = $target.val();

        var $supplyContainer = $('#suppliesByPaintStore');
        var url = $supplyContainer.find('#accordion').data('url') + storeId;

        $supplyContainer.load(url, function () {
            supplies.bindHandlers();
            makeComboBoxes();
            $('#exterior').collapse('show');
        });
    };

    function makeComboBoxes() {
        app.common.convertSelectsToComboBoxes($('.supply-type'));
    }
    function toggleCanSave() {
        var $createdSupplies = $('table tbody tr');

        if ($createdSupplies.length) {
            var paintStoreIsSelected = $paintStore.val() != '';
            var addSupplyButtonsAreEnabled = true;

            $('.addSupply-btn').each(function (index, btn) {
                if ($(btn).is(':disabled')) {
                    addSupplyButtonsAreEnabled = false;
                    return false;
                }
            });

            if (paintStoreIsSelected && addSupplyButtonsAreEnabled) {
                $submitBtn.prop('disabled', false);
            } else {
                $submitBtn.prop('disabled', true)
            }
            
        } else {
            $submitBtn.prop('disabled', true);
        }
    }

    function validateTableRows(e) {
        var noInvalidField = true;

        var $tableBody = $(e.target).parents('tbody');

        var $formFields = $([]);

        $formFields = $formFields.add($tableBody.find('.supply-description'));

        $formFields = $formFields.add($tableBody.find('.supply-cost'));

        $formFields = $formFields.add($tableBody.find('.supply-type'));

        $formFields.each(function (index, item) {
            if (item.value == '') {
                noInvalidField = false;
                return false;
            }
        });

        var $addBtn = $tableBody.parents('.panel-body').find('button.addSupply-btn');

        if (noInvalidField){
            $addBtn.prop('disabled', false);
        } else {
            $addBtn.prop('disabled', true);
        }

        toggleCanSave();
    }

    function processFormData() {
        setPaintStoreId();

        renameFormData();
    }

    function renameFormData() {
        var $form = $('form');

        $form.find('input.supply-type-name').each(function (index, item) {
            item.name = 'Supplies[' + index + '].SupplyType.Name';
        });

        $form.find('input.supply-category').each(function (index, item) {
            item.name = 'Supplies[' + index + '].SupplyType.SupplyCategory';
        });

        $form.find('input.paintStoreId').each(function (index, item) {
            item.name = 'Supplies[' + index + '].PaintStoreId';
        });

        $form.find('input.supply-builderId').each(function (index, item) {
            item.name = 'Supplies[' + index + '].BuilderId';
        });

        $form.find('input.supply-id').each(function (index, item) {
            item.name = 'Supplies[' + index + '].Id';
        });

        $form.find('select.supply-type').each(function (index, item) {
            item.name = 'Supplies[' + index + '].SupplyTypeId';
        });

        $form.find('input.supply-description').each(function (index, item) {
            item.name = 'Supplies[' + index + '].Name';
        });

        $form.find('input.supply-cost').each(function (index, item) {
            item.name = 'Supplies[' + index + '].Cost';
        });

        $form.find('textarea.supply-note').each(function (index, item) {
            item.name = 'Supplies[' + index + '].Notes';
        });
    }

    function setPaintStoreId() {
        var paintStoreId = $('#PaintStoreId').val();
        var supplies = $('tbody input.paintStoreId');

        supplies.val(paintStoreId);
    }

    function removeSupplyRow(e) {
        e.preventDefault();

        var $currentTableRow = $(this).parents('tr');

        var $curentTableBody = $currentTableRow.parents('tbody');

        var $siblingRows = $currentTableRow.siblings('tr');

        $currentTableRow.remove();

        if ($siblingRows.length) {
            $siblingRows.first().find('select.form-control').trigger('change');
        } else {
            enableAddSupplyBtn($curentTableBody);
        }

        toggleCanSave();
    }

    function enableAddSupplyBtn($tbody) {
        $addBtn = $tbody.parents('.panel-body').find('button.addSupply-btn');
        $addBtn.prop('disabled', false);
    }

    function addNewSupply(e) {
        e.preventDefault();

        $(this).prop('disabled', true);

        var $panel = $(this).parents('.panel-body');
        var $tableBody = $panel.find('tbody');
        var builderId = $tableBody.data('builderid');
        var id = $tableBody.attr('id');

        var supplyCategory = id;

        var trIndex = $('table tbody tr').length;

        var hiddenFields = createHiddenFields(trIndex, supplyCategory, builderId);

        var newTableRow = createTableRow($panel, trIndex, hiddenFields);

        $tableBody.append(newTableRow);

        var newlyAdded = $tableBody.find("tr:last");
        app.common.convertSelectsToComboBoxes($(newlyAdded).find('.supply-type'));
        toggleCanSave();
    }

    function createHiddenFields(trIndex, supplyCategory, builderId) {
        var hiddenFields = '<input class="supply-category" name="Supplies[' + trIndex + '].SupplyType.SupplyCategory" type="hidden" value="' + supplyCategory + '">';
        hiddenFields += '<input class="paintStoreId" name="Supplies[' + trIndex + '].PaintStoreId" type="hidden">';
        hiddenFields += '<input class="supply-builderId" name="Supplies[' + trIndex + '].BuilderId" type="hidden" value="' + builderId + '">';
        hiddenFields += '<input class="supply-id" name="Supplies[' + trIndex + '].Id" type="hidden" value="0">';

        return hiddenFields;
    }

    function createTableRow($panel, trIndex, hiddenFields) {
        var $supplyType = $panel.find('select#supply-type');

        var options = $supplyType.clone().html();

        var supplyTypeName = '<td><input type="hidden" class="form-control supply-type-name" name="Supplies[' + trIndex + '].SupplyType.Name"/>';

        supplyTypeName += '<select class="form-control supply-type supply-type-list" name="Supplies[' + trIndex + '].SupplyTypeId">' + options + '</select>';
        supplyTypeName += '<span class="field-validation-valid" data-valmsg-for="Supplies[' + trIndex + '].SupplyTypeId" data-valmsg-replace="true"></span></td>';

        var supplyName = '<td><input class="form-control supply-description" name="Supplies[' + trIndex + '].Name" type="text">';
        supplyName += '<span class="field-validation-valid" data-valmsg-for="Supplies[' + trIndex + '].Name" data-valmsg-replace="true"></span></td>';

        var costInput = '<input class="form-control supply-cost" name="Supplies[' + trIndex + '].Cost" type="number">';
        var supplyCost = '<td><div class="input-group"><div class="input-group-addon">$</div>' + costInput + '</div>';
        supplyCost += '<span class="field-validation-valid" data-valmsg-for="Supplies[' + trIndex + '].Cost" data-valmsg-replace="true"></span></td>';

        var deleteBtn = '<td><button type="button" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i></button></td>';

        var tableRow = '<tr>' + hiddenFields +supplyTypeName + supplyName + supplyCost + deleteBtn + '</tr>';

        return tableRow;
    }
})(app.supplies);