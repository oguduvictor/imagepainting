﻿var app = app || {};

app.dashboard = app.dashboard || {};

(function (vm, common) {
    'use strict';

    var notesTableAPI;
    var jobExtrasTableAPI;
    var deferredTasksTableAPI;
    var deferredTaskCount = 0;

    vm.loadDeferredTasksTab = function () {
        $('#deferredTasks').load('/dashboard/deferredtasks', function () {

            deferredTaskCount = $('#deferredTasks tr.hideable-container').length;
            $('#deferredTasksTitle').html('Deferred Tasks (' + deferredTaskCount + ')');

            deferredTasksTableAPI = $('#deferredTasks table').DataTable({
                "pageLength": 50,
                "dom": '<"toolbar">frtip'
            });

            $("#deferredTasks div.toolbar").html('<button class="btn btn-primary pull-right save disabled">Save</button>');

            $('#deferredTasks input:checkbox').change(function () {
                var uncheckedInputs = $('#deferredTasks tr.hideable-container input:checkbox:not(:checked)');
                if (uncheckedInputs.length >= 1) {
                    $('div.toolbar .save').removeClass('disabled');
                } else {
                    $('div.toolbar .save').addClass('disabled');
                }
            });
        });
    }

    vm.initDataTable = function () {
        notesTableAPI = $('#notes table').DataTable({
            "pageLength": 50,
            "dom": '<"toolbar">frtip'
        });

        jobExtrasTableAPI = $('#jobExtras table').DataTable({
            "pageLength": 50,
            "dom": '<"toolbar">frtip'
        });

        vm.loadDeferredTasksTab();

        $("div.toolbar").html('<button class="btn btn-primary pull-right resolve-all">Resolve Displayed Alerts</button>');
    }

    vm.initEventBindings = function () {
        $('#jobExtras').on('click', 'button.resolve-btn', { tableAPI: jobExtrasTableAPI, parentDivId: '#jobExtras' }, handleResolveAlert);
        $('#notes').on('click', 'button.resolve-btn', { tableAPI: notesTableAPI, parentDivId: '#notes' }, handleResolveAlert);
        $('#deferredTasks').on('click', 'button.save', { tableAPI: deferredTasksTableAPI, parentDivId: '#deferredTasks' }, handleSaveDeferredTasks);

        $('#jobExtras').on('click', 'div.toolbar button.resolve-all', { tableAPI: jobExtrasTableAPI, parentDivId: '#jobExtras' }, handleResolveDisplayedAlerts);
        $('#notes').on('click', 'div.toolbar button.resolve-all', { tableAPI: notesTableAPI, parentDivId: '#notes' }, handleResolveDisplayedAlerts);

        function handleResolveAlert(e) {
            e.preventDefault();

            var $element = $(e.currentTarget);
            var id = $element.attr('id');
            var message = 'Click Ok to resolve the alert.';
            var title = $element.closest('table').data('confirmtitle');
            var $row = $element.closest('tr');

            showConfirmAlert(message, title, id, e.data, $row);
        }

        function handleResolveDisplayedAlerts(e) {
            var ids = [];
            var $rows = $(e.data.parentDivId + ' tbody tr');

            $rows.find('.hideable-td button.resolve-btn').each(function (index, item) {
                ids.push(item.id);
            });

            if (!ids.length) {
                return;
            }

            var message = 'Click Ok to resolve the displayed alerts.';
            var title = $(e.currentTarget).closest('div.toolbar').siblings('table').data('confirmtitle');

            showConfirmAlert(message, title, ids, e.data, $rows);
        }

        function handleSaveDeferredTasks(e) {
            var $uncheckedInputs = $('tr.hideable-container input:checkbox:not(:checked)');
            if ($uncheckedInputs.length !== 0) {
                var message = ($uncheckedInputs.length === 1) ? 'Are you sure you are ready to work on this task?' : 'Are you sure you are ready to work on these tasks?';
                var title = $(e.currentTarget).closest('div.toolbar').siblings('table').data('confirmtitle');

                showSaveDeferredTasksConfirmMessage(message, title, $uncheckedInputs);
            }
            return;
        }

        function showSaveDeferredTasksConfirmMessage(message, title, $uncheckedInputs) {
            var ids = [];
            $uncheckedInputs.each(function (index, item) {
                ids.push($(this).data('id'));
            });

            common.confirmDialog.show(message, title)
                .done(function () {
                    $.post('/dashboard/updatedeferredtasks', { ids: ids }).then(function () {
                        removeSavedTasks($uncheckedInputs);
                    });
                });
        }

        function showConfirmAlert(message, title, ids, elementData, $row) {
            var tableAPI = elementData.tableAPI;
            var parentDivId = elementData.parentDivId;

            common.confirmDialog.show(message, title).done(function () {
                $.post('/systemalert/resolvealert', { id: ids }).then(function () {
                    tableAPI.rows($row).remove().draw();
                    setTableCount(tableAPI.page.info().recordsTotal, parentDivId);
                });
            });
        }

        function removeSavedTasks($uncheckedInputs) {
            deferredTaskCount = deferredTaskCount - $uncheckedInputs.length;
            deferredTasksTableAPI.rows($($uncheckedInputs).parent().parent()).remove().draw();
            
            $('#deferredTasksTitle').html('Deferred Tasks (' + deferredTaskCount + ')');
            $('div.toolbar .save').addClass('disabled');
            if (deferredTaskCount < 1) {
                $('#deferredTasks').html('<h3 class="text-center">You currently have no deferred tasks</h3>');
            }
        }

        function setTableCount(count, parentDivId) {
            var $tabbedLinkTitle = $('a[href="' + parentDivId + '"]');
            var fullText = $tabbedLinkTitle.html();
            var textWithoutNumber = fullText.substring(0, fullText.indexOf('(') + 1);

            $tabbedLinkTitle.html(textWithoutNumber + count + ')');
        }
    }

})(app.dashboard, app.common)