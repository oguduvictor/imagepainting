﻿var app = app || {};

app.ModalDialog = function () {
    'use strict';

    var $modal = $('div#app-modal-dialog');

    //button's definition = {label: 'button title', btnClass: 'optional bootstrap class like btn-primary', action: function() {}}
    // size is sm, md, lg

    function showDialog(content, title, buttons, size) {
        var $title = $modal.find('.modal-title');
        var $body = $modal.find('.modal-body');
        var $footer = $modal.find('.modal-footer');
        var deferred = $.Deferred();
        var buttonsHtml = '';
        var btnActions = {};
        var modalSize = 'modal-' + (size || self.SIZE.medium);

        $title.text(title);
        $body.html(content);

        // add buttons to the modal
        for (var i = 0; i < buttons.length; i++) {
            var btnClass = buttons[i].btnClass || 'btn-default';

            buttonsHtml += '<button class="btn ' + btnClass + '"' + 'id="btn-' + buttons[i].label + '">' + buttons[i].label + '</button>';
        }

        $footer.html(buttonsHtml);

        // put all actions in a hashset
        for (var i = 0; i < buttons.length; i++) {
            btnActions[buttons[i].label] = buttons[i].action;
        }

        // add click event handler to the buttons
        $footer.find('button').click(function (e) {
            var label = $(e.target).text();
            var btnAction = btnActions[label];

            if (btnAction) {
                btnAction(self, deferred);
                return;
            }

            hideDialog();
            deferred.resolve(true);
        });

        // add click event handler to the close "x" button
        $modal.find('.x-cancel-btn').click(function () {
            hideDialog();
            deferred.reject(false);
        });

        // set modal size
        $modal.find('.modal-dialog')
            .removeClass(function (index, className) {
                return className;
            })
            .addClass(modalSize)
            .addClass('modal-dialog');

        // display modal
        $modal.modal('show');

        return deferred.promise();
    }

    function hideDialog() {
        $('div#app-modal-dialog').modal('hide');
        //$modal.modal('hide');
    }

    function getOkButton(label) {
        return {
            label: label || 'Ok',
            btnClass: 'btn-primary',
            action: function (modal, deferred) {
                deferred.resolve(true);
                modal.hide();
            }
        };
    }

    function getCancelButton(label) {
        return {
            label: label || 'Cancel',
            btnClass: 'btn-default',
            action: function (modal, deferred) {
                deferred.reject(false);
                modal.hide();
            }
        };
    }

    var self = {
        show: showDialog,
        hide: hideDialog,
        okButton: getOkButton,
        cancelButton: getCancelButton,
        SIZE: {
            small: 'sm',
            medium: 'md',
            large: 'lg'
        }
    };

    return self;
};

