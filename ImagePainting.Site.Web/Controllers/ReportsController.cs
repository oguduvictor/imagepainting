﻿using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models.Reports;
using ImagePainting.Site.Web.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize(Roles = RolesConstant.Admin)]
    public class ReportsController : BaseController
    {
        private readonly IReportRepository _reportRepository;

        public ReportsController(
            IReportRepository reportRepository,
            IViewHelper viewHelper,
            IUserHelper userHelper)
            : base(viewHelper, userHelper)
        {
            _reportRepository = reportRepository;
        }

        public ActionResult PurchaseOrder()
        {
            var model = new List<PurchaseOrderReportItem>();

            return View2("Purchases", model);
        }
        
        public async Task<ActionResult> PurchaseOrderList(PurchaseReportQuery query)
        {
            var report = await _reportRepository.GetPurchaseReport(query);

            return PartialView("_PurchaseOrdersTable", report);
        }

        public ActionResult Tasks()
        {
            var model = new TasksReport();

            return View2(model);
        }

        public async Task<PartialViewResult> TasksReport(TasksReportQuery query)
        {
            var report = await _reportRepository.GetTasksReport(query);

            return PartialView("_TasksTable", report);
        }

        public ActionResult ProjectedRevenue()
        {
            var model = new ProjectedRevenueReport();

            return View2(model);
        }
        
        public async Task<PartialViewResult> ProjectedRevenueList(ProjectedRevenueReportQuery query)
        {
            var model = await _reportRepository.GetProjectedRevenueReport(query);

            return PartialView("_ProjectedRevenueTableList", model);
        }

        public ActionResult ProjectedLabor(ProjectedLaborReportQuery query)
        {
            var model = new ProjectedLaborReport();

            return View2(model);
        }

        public async Task<PartialViewResult> ProjectedLaborList(ProjectedLaborReportQuery query)
        {
            var report = await _reportRepository.GetProjectedLaborReport(query);

            return PartialView("_ProjectedLaborTableList", report);
        }
    }
}