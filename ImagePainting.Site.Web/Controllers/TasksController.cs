﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using ImagePainting.Site.Web.Helpers;
using static ImagePainting.Site.Common.Helpers.DateHelper;
using static ImagePainting.Site.Web.Helpers.RolesConstant;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize]
    public class TasksController : BaseController
    {
        private readonly ITaskComposerService _taskService;
        private readonly IWorkOrderTaskManager _taskManager;
        private readonly ILogger _logger;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IUserCacheService _userCacheService;
        private readonly IViewHelper _viewHelper;
        private readonly IUserHelper _userHelper;

        public TasksController(
            ITaskComposerService taskService,
            IWorkOrderTaskManager taskManager,
            ILogger logger,
            IUserCacheService userCacheService,
            IPurchaseOrderRepository purchaseOrderRepository,
            IViewHelper viewHelper, IUserHelper userHelper) : base(viewHelper, userHelper)

        {
            _taskService = taskService;
            _taskManager = taskManager;
            _logger = logger;
            _purchaseOrderRepository = purchaseOrderRepository;
            _userCacheService = userCacheService;
            _viewHelper = viewHelper;
            _userHelper = userHelper;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var tasks = await _taskService.GetTaskSchemas();
            return View(tasks);
        }

        [HttpGet, Authorize(Roles = Staff)]
        public async Task<ActionResult> Task(int id = 0)
        {
            var task = await _taskService.GetWorkSchemaTask(id);
            return View2(task);
        }

        [HttpGet, Authorize(Roles = Staff)]
        public async Task<ActionResult> ViewTask(int id)
        {
            var task = await _taskManager.GetTaskWithPermissions(id, true);

            if (!task.IsMainTask)
            {
                var mainTask = await _taskManager.GetMainTask(id);
                ViewBag.MainTaskId = mainTask.Id;
            }
            else
            {
                ViewBag.MainTaskId = task.Id;
            }

            return View2(task);
        }

        [HttpPost, Authorize(Roles = Staff)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveWorkOrderTask(WorkOrderTask task)
        {
            var result = await _taskManager.SaveWorkOrderTask(task);

            if (Request.IsAjaxRequest())
            {
                result.RemovePermission();
                return Json(result);
            }

            return RedirectToAction("UpcomingTasks");
        }

        [HttpPost, Authorize(Roles = Staff)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateTaskSchedule(TaskSchedule task)
        {
            var result = await _taskManager.UpdateTaskSchedule(task);
            result.RemovePermission();
            return Json(result);
        }

        [HttpPost, Authorize(Roles = Staff)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Task(WorkOrderTaskSchema task)
        {
            if (!ModelState.IsValid)
            {
                return View2(task);
            }

            await _taskService.SaveTaskSchema(task);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = Staff)]
        public Task<ActionResult> UpcomingTasks(int pageNumber = 0)
        {
            var today = GetLocalDateToday();
            var searchCriteria = new TaskSearchCriteria
            {
                IsUpcoming = true,
                IncludeCrew = true,
                IncludeWorkOrder = true,
                OrderByAscending = true,
                PageNumber = pageNumber,
                DateRange = new DateTimeRange
                {
                    BeginDateTime = today,
                    EndDateTime = today.AddDays(30)
                }
            };

            ViewBag.minDate = today;

            return CalendarView(searchCriteria, true);
        }

        [HttpGet]
        [Authorize(Roles = Staff)]
        public Task<ActionResult> CompletedTasks(int pageNumber = 0)
        {
            var today = GetLocalDateToday();
            var searchCriteria = new TaskSearchCriteria
            {
                IsCompleted = true,
                IncludeCrew = true,
                IncludeWorkOrder = true,
                OrderByAscending = false,
                PageNumber = pageNumber,
                DateRange = new DateTimeRange
                {
                    BeginDateTime = today.AddDays(-30),
                    EndDateTime = today
                }
            };

            return CalendarView(searchCriteria, true);
        }

        [HttpGet]
        [Authorize(Roles = Staff)]
        public Task<ActionResult> OverdueTasks(int pageNumber = 0)
        {
            var today = GetLocalDateToday();
            var searchCriteria = new TaskSearchCriteria
            {
                IsOverdued = true,
                IncludeCrew = true,
                IncludeWorkOrder = true,
                OrderByAscending = false,
                PageNumber = pageNumber,
                DateRange = new DateTimeRange
                {
                    BeginDateTime = today.AddDays(-30),
                    EndDateTime = today
                }
            };

            ViewBag.maxDate = today;

            return CalendarView(searchCriteria, true);
        }

        [HttpGet]
        [Authorize(Roles = Staff)]
        public Task<ActionResult> AllTasks(int pageNumber = 0)
        {
            var today = GetLocalDateToday();
            var searchCriteria = new TaskSearchCriteria
            {
                IncludeCrew = true,
                IncludeWorkOrder = true,
                OrderByAscending = true,
                PageNumber = pageNumber,
                DateRange = new DateTimeRange
                {
                    BeginDateTime = today.AddDays(-30),
                    EndDateTime = today
                }
            };

            return CalendarView(searchCriteria, true);
        }

        [HttpPost]
        [Authorize(Roles = Staff)]
        public async Task<ActionResult> LoadTasks(TaskSearchCriteria criteria)
        {
            var dateRange = criteria.ToDateRange();

            if (!criteria.CrewIds.IsNullOrEmpty())
            {
                criteria.CrewIds = CleanUpCrewIds(criteria.CrewIds.ToArray());
            }

            ViewBag.PageNumber = criteria.PageNumber;
            ViewBag.DateTimeRange = dateRange;

            criteria.IncludeCrew = true;
            criteria.IncludeCommunity = true;

            var viewModel = await GetCalendarViewModel2(criteria, dateRange);
            
            SetNavButtonsDisplay(criteria, dateRange);

            return PartialView("WorkOrderTasks", viewModel);
        }

        [HttpPost, Authorize(Roles = Staff)]
        public async Task<ActionResult> CompleteTask(WorkOrderTask task)
        {
            var result = await _taskManager.CompleteTask(task);

            result.RemovePermission();

            return Json(result);
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task<ActionResult> UnlockTask(long id)
        {
            var result = await _taskManager.UnlockTask(id);

            result.RemovePermission();

            return Json(result);
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task<ActionResult> DeferTask(long id)
        {
            await _taskService.SetTaskDeferState(id);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task EmailScheduleTask(IEnumerable<long> ids, string note, FreeCrewViewModel freeCrewsModel)
        {
            if (!freeCrewsModel.FreeCrewIds.IsNullOrEmpty())
            {
                await _taskService.SendNoTaskEmailAsync(freeCrewsModel.FreeCrewIds, freeCrewsModel.TaskDate, freeCrewsModel.Note);
            }

            if (!ids.IsNullOrEmpty())
            {
                await _taskService.SendWorkOrderMails(ids, note);
            }
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteTaskSchema(int id)
        {
            try
            {
                await _taskService.DeleteTaskSchema(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var msg = "Deleting task schema failed!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task<JsonResult> UpdateUnassignedCrewStatus(string crewId, DateTime scheduledDate, bool status)
        {
            await _taskService.UpdateUnAssignedCrewSetting(new List<string>{crewId}, scheduledDate, busy: status);

            return Json(new {Success = true});
        }

        [HttpGet]
        public async Task<JsonResult> GetBusyUser(string crewId, DateTime date)
        {
            var user = await _taskService.GetBusyUser(crewId, date);

            return Json(user, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetTaskPdfTemplate(long taskId)
        {
            var template = await _taskService.GetTaskPdfTemplate(taskId);

            return Json(template, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<HttpStatusCodeResult> CreateTaskPagePdf(long taskId, string encodedImage)
        {
            await _taskService.CreatePdf(taskId, encodedImage);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet, Authorize(Roles = Admin)]
        public async Task<FileContentResult> DownloadTaskSignedPdf(long taskId)
        {
            var file = await _taskService.GetTaskSignedPdf(taskId);

            return File(file, "application/pdf", $"EPO_Billing_{DateTime.UtcNow.ToString("yyyyMMdd")}.pdf");
        }

        private string[] CleanUpCrewIds(string[] ids)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                if (ids[i] == "0")
                {
                    ids[i] = null;
                    break;
                }
            }
            return ids;
        }

        [Obsolete("Use GetCalendarViewModel2 instead")]
        private async Task<IEnumerable<CalendarDayModel>> GetCalendarViewModel(TaskSearchCriteria criteria, DateTimeRange dateRange)
        {
            var result = await _taskService.GetWorkOrderTasks(criteria);
            var unAssignedSubContractors = await _taskService.GetUnAssignedSubContractors(dateRange);
            var emailedUnAssignedCrewsRegistries = await _taskService.GetEmailedUnAssignedCrewsInRange(dateRange);
            var emailedTasksPurchaseOrdersSummaries =
                await _purchaseOrderRepository.GetEmailedPurchaseOrders(result.Select(x => x.Id).ToArray());
            var viewModel = CalendarViewBuilder.BuildView(
                                                unAssignedSubContractors, 
                                                criteria, 
                                                result, 
                                                emailedUnAssignedCrewsRegistries, 
                                                emailedTasksPurchaseOrdersSummaries,
                                                _userHelper);
            return viewModel;
        }

        private async Task<IEnumerable<CalendarDayModel>> GetCalendarViewModel2(TaskSearchCriteria criteria, DateTimeRange dateRange)
        {
            var result = await _taskService.SearchWorkOrderTasks(criteria);
            var unAssignedSubContractors = await _taskService.GetUnAssignedSubContractors(dateRange);
            var emailedUnAssignedCrewsRegistries = await _taskService.GetEmailedUnAssignedCrewsInRange(dateRange);
            var viewModel = CalendarViewBuilder2.BuildView(result, criteria, unAssignedSubContractors, emailedUnAssignedCrewsRegistries, _viewHelper, _userHelper);

            return viewModel;
        }

        private async Task<ActionResult> CalendarView(TaskSearchCriteria searchCriteria, bool returnEmptyResult = false)
        {
            var dateRange = searchCriteria.ToDateRange();

            ViewBag.PageNumber = searchCriteria.PageNumber;
            ViewBag.DateTimeRange = dateRange;

            SetNavButtonsDisplay(searchCriteria, dateRange);

            var viewModel = returnEmptyResult 
                            ? new List<CalendarDayModel>() 
                            : await GetCalendarViewModel2(searchCriteria, dateRange);

            return View2(viewModel.ToList());
        }

        private void SetNavButtonsDisplay(TaskSearchCriteria criteria, DateTimeRange dateRange)
        {
            var today = GetLocalDateToday();

            ViewBag.ShowPrevButton = true;
            ViewBag.ShowNextButton = true;

            if (criteria.IsUpcoming == true && dateRange.BeginDateTime <= today)
            {
                ViewBag.ShowPrevButton = false;
            }

            if (criteria.IsOverdued == true && dateRange.EndDateTime >= today.AddDays(-1))
            {
                ViewBag.ShowNextButton = false;
            }
        }
    }
}