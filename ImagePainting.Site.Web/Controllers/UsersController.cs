﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using ImagePainting.Site.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static ImagePainting.Site.Web.Helpers.RolesConstant;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly IUserInfoService _userInfoService;
        private readonly IUserContext _userContext;
        private readonly ILogger _logger;

        public UsersController
            (IUserInfoService userInfoService,
            IUserContext userContext,
            ILogger logger, 
            IViewHelper viewHelper,
            IUserHelper userHelper)
            : base(viewHelper, userHelper)
        {
            _userInfoService = userInfoService;
            _userContext = userContext;
            _logger = logger;
        }

        // GET: Users
        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> Index()
        {
            var users = await _userInfoService.GetUsers();
            return View(users);
        }

        [HttpGet]
        public async Task<ActionResult> UserProfile(string id)
        {
            var dbUser = await _userInfoService.GetUserAsync(id);
            ViewBag.UserId = _userContext.UserId;
            
            return View2(dbUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserProfile(UserInfo userInfo)
        {
            if (!ModelState.IsValid)
            {
                return View(userInfo);
            }

            await _userInfoService.SaveUserInfo(userInfo);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> UserRoles(string id)
        {
            var user = await _userInfoService.GetUserRoles(id);
            return View(user);
        }

        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<JsonResult> Staff()
        {
            var staff = await _userInfoService.GetUsersSummary();
            return Json(staff, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> UserRoles(string id, IEnumerable<RoleSelection> Roles)
        {
            await _userInfoService.AssignRolesToUser(id, Roles);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteUser(string id)
        {
            try
            {
                await _userInfoService.DeleteUser(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var msg = "User was not deleted successfully";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        } 
    }
}