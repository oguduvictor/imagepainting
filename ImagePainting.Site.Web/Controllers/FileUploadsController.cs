﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static ImagePainting.Site.Web.Helpers.RolesConstant;
using ImagePainting.Site.Web.Helpers;

namespace ImagePainting.Site.Web.Controllers
{
    public class FileUploadsController : BaseController
    {
        private readonly IFileUploadRepository _fileUploadRepository;
        private readonly IUserContext _userContext;

        public FileUploadsController(
            IFileUploadRepository fileUploadRepository, 
            IUserContext userContext,
            IViewHelper viewHelper,
            IUserHelper userHelper) : base(viewHelper, userHelper)
        {
            _fileUploadRepository = fileUploadRepository;
            _userContext = userContext;
        }

        [HttpGet]
        public async Task<FileContentResult> File(long id)
        {
            var fileUpload = await _fileUploadRepository.GetFileUpload(id);

            if (fileUpload == null)
            {
                throw new ArgumentException($"{id} is not a valid file upload ID");
            }

            SecuredFileValidation(fileUpload);

            return File(fileUpload.Content, fileUpload.ContentType);
        }

        public PartialViewResult Files(long ownerId, FileOwnerType ownerType, FileOwnerType? secondaryOwnerType = null)
        {

            var filesTask = _fileUploadRepository.GetFileUploads(ownerId, ownerType, secondaryOwnerType ?? FileOwnerType.Unknown);

            filesTask.Wait();

            ViewBag.OwnerId = ownerId;
            ViewBag.OwnerType = (int)ownerType;

            return PartialView2(filesTask.Result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Upload(long ownerId, FileOwnerType ownerType)
        {
            var fileUploads = new List<FileUpload>();
            var dateNow = DateTime.UtcNow;
            var userId = _userContext.UserId;
            
            foreach(var fileKey in Request.Files.AllKeys)
            {
                var file = Request.Files[fileKey];
                var fileUpload = new FileUpload { OwnerId = ownerId, OwnerType = ownerType };

                using (var reader = new BinaryReader(file.InputStream))
                {
                    fileUpload.Content = reader.ReadBytes(file.ContentLength);
                }

                fileUpload.ContentType = file.ContentType;
                fileUpload.Name = file.FileName;
                fileUpload.CreatedById = userId;
                fileUpload.Created = dateNow;

                fileUploads.Add(fileUpload);
            }

            if (fileUploads.Any())
            {
                await _fileUploadRepository.CreateFileUploads(fileUploads);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public async Task Delete(long id)
        {
            await _fileUploadRepository.DeleteFileUpload(id);
        }

        private void SecuredFileValidation(FileUpload fileUpload)
        {
            if (fileUpload.OwnerType == FileOwnerType.SecuredWorkOrder && !_userContext.IsAdmin())
            {
                throw new Exception($"User does not have permission to download file {fileUpload.Name}");
            }
        }
    }
}