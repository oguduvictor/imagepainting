﻿using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Web.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    public class SystemAlertController : Controller
    {
        private readonly ISystemAlertService _systemAlertService;

        public SystemAlertController(ISystemAlertService systemAlertService)
        {
            _systemAlertService = systemAlertService;
        }

        [HttpGet]
        public async Task<JsonResult> GetNoteAlerts(long workOrderId)
        {
            var alerts = await _systemAlertService.GetNoteAlerts(workOrderId)
                .ContinueWith(task => task.Result.Select(alert => new SystemAlertViewModel(alert)));

            return Json(alerts, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetJobExtraAlerts(long workOrderId)
        {
            var searchCriteria = new AlertSearchCriteria
            {
                AlertTypes = new[] { AlertType.JobExtraDifference },
                ParentIds = new[] { workOrderId },
                ParentType = AlertParentType.WorkOrder
            };

            var result = await _systemAlertService.SearchAlerts(searchCriteria)
                            .ContinueWith(task => task.Result.Select(alert => new SystemAlertViewModel(alert)));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task ResolveAlert(params Guid[] id)
        {
            await _systemAlertService.ResolveAlerts(id);
        }
    }
}