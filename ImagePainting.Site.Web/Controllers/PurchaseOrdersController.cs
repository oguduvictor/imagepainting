﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Web.Helpers;
using Newtonsoft.Json;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    public class PurchaseOrdersController : BaseController
    {
        private readonly IPurchaseOrderService _purchaseOrderService;
        private readonly IPaintStoreService _paintStoreService;
        private readonly IWorkOrderMailer _worderOrderMailer;


        public PurchaseOrdersController(IPurchaseOrderService purchaseOrderService,
            IPaintStoreService paintStoreService, IWorkOrderMailer workOrderMailer,
            IViewHelper viewHelper,
            IUserHelper userHelper)
            : base(viewHelper, userHelper)
        {
            _purchaseOrderService = purchaseOrderService;
            _paintStoreService = paintStoreService;
            _worderOrderMailer = workOrderMailer;
        }
        
        public ActionResult PurchaseOrders(WorkOrderTask task, bool? emailed = null)
        {
            var purchaseOrdersTask = _purchaseOrderService.GetPurchaseOrders(task.Id, task.WorkOrderId, true, false, emailed: emailed);

            purchaseOrdersTask.Wait();

            if (emailed.HasValue)
            {
                return Json(purchaseOrdersTask.Result, JsonRequestBehavior.AllowGet);
            }

            ViewBag.workOrderTask = task;
            ViewBag.IsPartial = false;
            if (Request.IsAjaxRequest())
            {
                ViewBag.IsPartial = true;
            }

            return PartialView(purchaseOrdersTask.Result);
        }

        public async Task<ActionResult> WorkOrderPurchaseOrders(long id)
        {
            var purchaseOrders = await _purchaseOrderService.WorkOrderPurchaseOrders(id);

            return PartialView(purchaseOrders);
        }
        
        public async Task<ActionResult> UpdateViewWithRows(int taskId,int id, long? paintStoreId)
        {
            var model = await _purchaseOrderService.GetPurchaseOrder(taskId, id, paintStoreId);

            return PartialView2("_PurchaseOrder", model);
        }

        public async Task<ActionResult> PurchaseOrder(long taskId, long id)
        {
            var model = await _purchaseOrderService.GetPurchaseOrder(taskId, id);

            return View2(model);
        }

        [HttpPost]
        public async Task Delete(long id)
        {
           await _purchaseOrderService.DeletePurchaseOrder(id);
        }

        public async Task<ActionResult> ViewPurchaseOrder(long id)
        {
            var model = await _purchaseOrderService.GetPurchaseOrder(id, true);
            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PurchaseOrder(PurchaseOrder order, bool sendEmail = false)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(ModelState));
            }

            var result = await _purchaseOrderService.SavePurchaseOrder(order, sendEmail);

            result.PurchaseOrderItems.ForEach(x => x.PurchaseOrder = null);

            if (Request.IsAjaxRequest())
            {
                return Json(result);
            }

            return RedirectToAction("PurchaseOrder", new { id = order.Id, taskId = order.TaskId });
        }

        public async Task<string> GetContactEmailForStoreLocation(long locationId)
        {
            var location = await _paintStoreService.GetStoreLocation(locationId);
            return location?.StoreContactEmail ?? string.Empty;
        }

        public ActionResult StoreLocation(long id)
        {
            var model = new PurchaseOrder
            {
                PaintStoreLocation = new StoreLocation { StoreId = id }
            };

            return PartialView(model);
        }
        
        [HttpPost]
        public async Task EmailPurchaseOrder(long id, string emailAddress)
        {
            await _worderOrderMailer.EmailPurchaseOrder(id, emailAddress);
            await _purchaseOrderService.UpdatePurchaseOrderEmailSent(id, emailAddress);
        }

        [HttpPost]
        public async Task<string> PreviewPurchaseOrder(long id)
        {
            var preview =  await _worderOrderMailer.PreviewPurchaseOrder(id).ConfigureAwait(false);
            return preview.ToJson();
        }

        [HttpPost]
        public async Task UnlockPurchaseOrder(long id)
        {
            await _purchaseOrderService.UnlockPurchaseOrder(id);
        }
    }
}
