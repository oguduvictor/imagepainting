﻿using System.Web.Mvc;
using ImagePainting.Site.Web.Helpers;
using ImagePainting.Site.Web.Models;

namespace ImagePainting.Site.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IUserHelper _userHelper;
        private readonly IViewHelper _viewHelper;

        public HomeController(IViewHelper viewHelper, 
            IUserHelper userHelper) :base(viewHelper, userHelper)
        {
            _userHelper = userHelper;
            _viewHelper = viewHelper;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult NavBar()
        {
            var model = GetNavViewModel();
            return PartialView(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Index()
        {
            return View2(_userHelper);
        }

        private NavBarViewModel GetNavViewModel()
        {
            return new NavBarViewModel
            {
                IsAdmin = _userHelper.IsUserAdmin(),
                IsStaff = _userHelper.IsStaff(),
                IsAdminOrEmployee = _userHelper.IsAdminOrEmployee(),
                UserFirstName = _viewHelper.GetUserFirstName()
            };
        }
    }
}