﻿using ImagePainting.Site.Common.Interfaces.Service;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize]
    public class ProjectionController : Controller
    {
        private readonly IProjectionComposerService _projectionService;
        private readonly ILogger _logger;

        public ProjectionController(
            IProjectionComposerService projectionService,
            ILogger logger)
        {
            _projectionService = projectionService;
            _logger = logger;
        }
       
        public async Task<PartialViewResult> RevenueProjection(long id)
        {
            try
            {
                var projection = await _projectionService.GetRevenueProjection(id);

                return PartialView(projection);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw ex;
            }
        }

        public async Task<PartialViewResult> MaterialProjection(long id)
        {
            var projection = await _projectionService.GetWorkOrder(id);

            return PartialView(projection);
        }

        public async Task<PartialViewResult> LaborProjection(long id)
        {
            var projection = await _projectionService.GetLaborProjection(id);

            return PartialView(projection);
        }

        public async Task<PartialViewResult> ProfitProjection(long id = 0)
        {
            var projection = await _projectionService.GetProfitProjection(id);

            return PartialView(projection);
        }
    }
}
