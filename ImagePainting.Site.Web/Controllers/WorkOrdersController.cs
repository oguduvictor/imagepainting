﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Common.ViewModels;
using ImagePainting.Site.Web.Helpers;
using ImagePainting.Site.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using static ImagePainting.Site.Web.Helpers.RolesConstant;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize(Roles = Staff)]
    public class WorkOrdersController : BaseController
    {
        private readonly IWorkOrderComposerService _workOrderService;
        private readonly IWorkOrderTaskManager _taskManager;
        private ILogger _logger;
        private readonly IWorkOrderMailer _workOrderMailer;
        private readonly IUserHelper _userHelper;

        public WorkOrdersController(
            IWorkOrderComposerService workOrderService,
            IWorkOrderTaskManager taskManager,
            ILogger logger,
            IWorkOrderMailer workOrderMailer,
            IViewHelper viewHelper,
            IUserHelper userHelper)
            : base(viewHelper, userHelper)
        {
            _workOrderService = workOrderService;
            _taskManager = taskManager;
            _workOrderMailer = workOrderMailer;
            _logger = logger;
            _userHelper = userHelper;
        }

        [Authorize(Roles = Admin)]
        public async Task<ActionResult> Index()
        {
            var query = new WorkOrderQuery();
            var result = await _workOrderService.SearchWorkOrders(query);

            return View2(result);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteWorkOrder(long id)
        {
            try
            {
                await _workOrderService.DeleteWorkOrder(id);
            }

            catch(Exception ex)
            {
                _logger.Error(ex);

                var msg = "Deleting work order failed!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }

        [HttpGet]
        public PartialViewResult Complete()
        {
            return PartialView();
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task Complete(long id)
        {
            await _workOrderService.CompleteWorkOrder(id);
        }

        [HttpPost]
        public async Task<PartialViewResult> WorkOrderList(WorkOrderQuery sortSummary)
        {
            var result = await _workOrderService.SearchWorkOrders(sortSummary);

            return PartialView2("_WorkOrderList", result);
        }

        public ActionResult WorkOrder(long id = 0)
        {
            return View2(id);
        }

        public async Task<string> WorkOrderDetails(long id)
        {
            var workOrder = await _workOrderService.GetWorkOrder(id, true);

            // TODO: result json result instead of string
            return workOrder.ToJson();
        }

        public async Task<PartialViewResult> InitialSettings(int id = 0)
        {
            var model = await _workOrderService.GetInitialSettingsModel(id);
            ViewBag.workOrderCompleted = model.WorkOrderCompleted;
            
            return PartialView2(model.Item);
        }

        public async Task<JsonResult> WorkOrderExists(int id, string name)
        {
            var result = await _workOrderService.WorkOrderExists(id, name);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Communities(int? id, bool showCreateOption = true, bool showBlankOption = true)
        {
            var model = new WorkflowInitalSettingsViewModel
            {
                 BuilderId = id 
            };
            ViewData["showCreateOption"] = showCreateOption;
            ViewData["showBlankOption"] = showBlankOption;
            
            return PartialView2(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Admin)]
        public async Task<JsonResult> SaveInitialSettings(WorkflowInitalSettingsViewModel model)
        {
            var workOrder = await _workOrderService.SaveInitialSettings(model);

            return Json(workOrder);
        }

        public async Task<ActionResult> JobInfo(long id = 0, bool? colorsOnly = null)
        {
            var model = await _workOrderService.GetJobInfo(id);
            ViewBag.WorkOrderCompleted = model.WorkOrderCompleted;
            ViewBag.BuilderId = model.BuilderId;
            ViewBag.ColorCodePrefix = model.BuilderAbbrevation;
            ViewBag.WorkOrderId = id;

            if (colorsOnly.HasValue && colorsOnly.Value)
            {
                var colorsList = new List<KeyValuePair<string, string>> {
                    new KeyValuePair<string, string>("Exterior", model.Item.ExteriorColors.ToJson()),
                    new KeyValuePair<string, string>("Interior", model.Item.InteriorColors.ToJson())
                };

                return Json(colorsList, JsonRequestBehavior.AllowGet);
            }
            
            return PartialView2(model.Item);
        }

        public PartialViewResult Plans(int? id, int? planId, bool showCreateOption = true)
        {
            ViewData["BuilderId"] = id;
            ViewData["showCreateOption"] = showCreateOption;

            var model = new JobInfo
            {
                 PlanId = planId 
            };

            return PartialView2(model);
        }
        
        public PartialViewResult Elevations(int id)
        {
            var model = new JobInfo
            {
                PlanId = id 
            };
            
            return PartialView2(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Admin)]
        public async Task<JsonResult> SaveJobInfo(long workOrderId, JobInfo jobInfo)
        {
            var result = await _workOrderService.SaveJobInfo(workOrderId, jobInfo);

            return Json(result.Id);
        }

        public async Task<PartialViewResult> ExtraJob(long id)
        {
            var workOrderItemVM = await _workOrderService.GetExtraJobInfo(id);
            var isAdmin = _userHelper.IsUserAdmin();
            var viewName = isAdmin ? "ExtraJob" : "ExtraJob_Disabled";
            
            var viewModel = isAdmin ? new JobExtraViewModel
            {
                Id = workOrderItemVM.Item.Id,
                Note = workOrderItemVM.Item.Note,
                ExtraItems = workOrderItemVM.Item.ExtraItems,
                WorkOrderCompleted = workOrderItemVM.WorkOrderCompleted,
                WorkOrderId = id,
                IsAdmin = isAdmin
            } : ExtraJobInfoViewBuilder.BuildView(id, workOrderItemVM);
            
            return PartialView2(viewName, viewModel);
        }

        public async Task<JsonResult> AllExtraItems(long id)
        {
            var items = await _workOrderService.GetAllJobExtras(id);

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Admin)]
        public async Task<long> ExtraJob(long workOrderId, ExtraJobInfo extraJobInfo)
        {
            var newExtraJobInfo = await _workOrderService.SaveExtraJobInfo(workOrderId, extraJobInfo);
            await _workOrderMailer.EmailProjection(workOrderId);
            
            return newExtraJobInfo.Id;
        }

        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<PartialViewResult> ScheduleTasks(long id)
        {
            var model = await _taskManager.GetTaskSchedules(id);
            
            return PartialView2(model.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Admin)]
        public async Task<JsonResult> ScheduleTasks(long id, IEnumerable<ScheduleWorkOrderTask> model)
        {
            var result = await _workOrderService.ScheduleTask(id, model);
            
            return Json(result);
        }

        /// <summary>
        /// Gets Scheduled Task
        /// </summary>
        /// <param name="id">Task Id</param>
        public async Task<PartialViewResult> Task(long id)
        {
            var model = await _taskManager.GetTaskWithPermissions(id);

            return PartialView2(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Staff)]
        public async Task Task(WorkOrderTask model)
        {
            await _workOrderService.SaveWorkOrderTask(model);
        }

        [HttpGet]
        [Authorize(Roles = Staff)]
        public ActionResult TaskLaborPricing(long taskId)
        {
            var model = _workOrderService.GetTaskPricing(taskId);
            
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<long> CloneWorkOrder(long id)
        {
            return await _workOrderService.CloneWorkOrder(id);
        }

        [HttpGet]
        [Authorize(Roles = AdminAndEmployee)]
        public async Task<PartialViewResult> Notes(long id)
        {
            var notes = await _workOrderService.GetNotes(id);

            return PartialView(notes);
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task ChangeExtraItemTaskType(long id, string extraItemName, TaskType taskType, List<long> taskIds)
        {
            await _workOrderService.ChangeExtraItemTaskType(id, extraItemName, taskType, taskIds);
        }

        [HttpGet, Authorize(Roles = Admin)]
        public ActionResult WorkItems(long taskId)
        {
            var result = _workOrderService.GetWorkItems(taskId);

            ViewBag.taskId = taskId;
            ViewBag.workOrderCompleted = result.WorkOrderCompleted;

            return PartialView2(result.Item);
        }

        [Authorize(Roles = Admin)]
        public async Task<PartialViewResult> FileUploads(long id)
        {
            var model = await _workOrderService.GetFileUploads(id);

            return PartialView(model);
        }

        [HttpPost, Authorize(Roles = Admin)]
        public async Task SaveWorkItems(long taskId, IEnumerable<WorkItem> workItems)
        {
            await _workOrderService.SaveWorkItems(taskId, workItems);
        }
    }
}