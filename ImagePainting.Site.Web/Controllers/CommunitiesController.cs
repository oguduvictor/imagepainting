﻿using ImagePainting.Site.Domain.Models;
using ImagePainting.Site.Web.Helpers;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Interfaces.Service;
using static ImagePainting.Site.Web.Helpers.RolesConstant;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize(Roles = AdminAndEmployee)]
    public class CommunitiesController : BaseController
    {
        private ICommunityComposerService _composerService;
        private ILogger _logger;

        public CommunitiesController(
            ICommunityComposerService composerService, 
            ILogger logger,
            IViewHelper viewHelper, 
            IUserHelper userHelper)
            :base(viewHelper, userHelper)
        {
            _composerService = composerService;
            _logger = logger;
        }

        public async Task<ActionResult> Index()
        {
            var communities = await _composerService.GetCommunities();

            return View2(communities.ToList());
        }

        #region Communities
        [HttpGet]
        public async Task<ActionResult> Community(int id = 0)
        {
            try
            {
                var community = await _composerService.GetCommunity(id);

                var communityViewModel = new CommunityViewModel();

                communityViewModel.MapToViewModel(community);

                return View2(communityViewModel);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Community(CommunityViewModel communityViewModel)
        {            
            if (!ModelState.IsValid)
            {
                return View2(communityViewModel);
            }

            communityViewModel.Builders = await _composerService.GetBuilders(communityViewModel.BuilderIds);

            var community = communityViewModel.MapFromViewModel();

            var newCommunity = await _composerService.SaveCommunity(community);

            if (Request.IsAjaxRequest())
            {
                newCommunity.CommunitySummary();

                return Json(newCommunity);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteCommunity(int id)
        {
            try
            {
                var deletedCommunity = await _composerService.DeleteCommunity(id);

                if (!deletedCommunity)
                {
                    var msg = "Unable to delete this community!";

                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var msg = "Unable to delete this community";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }
        #endregion

        #region Builders
        [HttpGet]
        public async Task<ActionResult> Builders()
        {
            var builders = await _composerService.GetBuilders();
           
            return View2(builders);
        }

        [HttpGet]
        public async Task<ActionResult> Builder(int id = 0)
        {
            var builder = await _composerService.GetBuilder(id, includePlans: true);

            return View2(builder);
        }

        [HttpGet]
        public async Task<JsonResult> BuilderCommunities(int id)
        {
            var builderCommunities =  await _composerService.GetCommunitiesSummary(id);

            return Json(builderCommunities, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Builder(Builder builder, int builderCopiedCostId = 0)
        {
            if (!ModelState.IsValid)
            {
                return View2(builder);
            }

            var newBuilder = await _composerService.SaveBuilder(builder, builderCopiedCostId);

            if (Request.IsAjaxRequest())
            {
                newBuilder.BuilderSummary();
                return Json(newBuilder);
            }

            return RedirectToAction("Builders");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteBuilder(int id)
        {
            var deletedBuilder = await _composerService.DeleteBuilder(id);

            if (!deletedBuilder)
            {
                var msg = "Cannot delete this builder as it is currently associated with community, but you can make it inactive";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }
        #endregion

        #region Costs
        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> Costs(int id)
        {
            var builderCosts = await _composerService.GetBuilder(id, true)
                .ContinueWith(x =>
                {
                    x.Result.Costs = x.Result.Costs.OrderBy(y => y.CostItem.Order).ToList();
                    return x.Result;
                });

            return View2(builderCosts);
        }

        [HttpGet]
        public async Task<ActionResult> LaborCosts()
        {
            var costItems = await _composerService.GetCostItems()
                .ContinueWith(x => x.Result.OrderBy(y => y.Order));

            return View2(costItems.ToList());
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> LaborCosts(IEnumerable<CostItem> costItems)
        {
            await _composerService.UpdateCostItems(costItems, true);

            return RedirectToAction("LaborCosts");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Costs(int id, Builder builder)
        {
            if (id == default(int))
            {
                var costItems = builder.Costs.Select(x => x.CostItem);

                await _composerService.UpdateCostItems(costItems);
            }
            else
            {
                builder.Id = id;
                await _composerService.UpdateBuilderCosts(builder);
            }

            return RedirectToAction("Builders");
        }

        [HttpGet]
        public async Task<ActionResult> Cost(int id = 0, int costId = 0)
        {
            var cost = await _composerService.GetCostForBuilder(id, costId);

            return View2(cost);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Cost(Cost cost)
        {
            if (cost.BuilderId == default(int))
            {
                await _composerService.SaveCostItem(cost.CostItem);
            }
            else
            {
                await _composerService.SaveCost(cost);
            }

            return RedirectToAction("Costs", new { id = cost.BuilderId });
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteCost(int id, int builderId)
        {
            try
            {
                await _composerService.DeleteCost(id);
            }
            catch(Exception ex)
            {
                _logger.Equals(ex);

                var msg = "Unable to delete cost!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return RedirectToAction("Costs", new { id = builderId });
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteCostItem(int id)
        {
            try
            {
                await _composerService.DeleteCostItem(id);
            }
            catch (Exception ex)
            {
                _logger.Equals(ex);

                var msg = "Unable to delete cost!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }
#endregion

        #region Plans

        [HttpGet]
        public async Task<ActionResult> Plans()
        {
            var allPlans = await _composerService.GetPlans();
            return View2(allPlans.ToList());
        }

        [HttpGet]
        public async Task<ActionResult> Plan(int id = 0, int builderId = 0)
        {
            var plan = await _composerService.GetPlan(id, includeElevations: true, includeCommunities:true);

            if (plan.Id == 0)
            {
                plan.BuilderId = builderId;
            }

            return View2(plan);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Plan(Plan plan)
        {
            if (!ModelState.IsValid)
            {
                return View2(plan);
            }

            var newPlan = await _composerService.SavePlan(plan);

            if (Request.IsAjaxRequest())
            {
                return Json(newPlan);
            }

            return RedirectToAction("Plans");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeletePlan(int id)
        {
            var deletedPlan = await _composerService.DeletePlan(id);

            if (!deletedPlan)
            {
                var msg = "Cannot delete this Plan as it currently has some elevations";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return RedirectToAction("Plans");
        }

        [HttpPost]
        [Authorize(Roles = Admin )]
        public async Task<ActionResult> DeletePlanCommunity(int planId, int communityId)
        {
            var deletedCommunity = await _composerService.DeletePlanCommunity(planId, communityId);
            if(!deletedCommunity)
            {
                var msg = "Community could not be deleted";
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(deletedCommunity);

        }

        #endregion

        #region Elevation

        [HttpGet]
        public async Task<ActionResult> Elevation(int id = 0, int planId = 0, bool json = false)
        {
            var elevation = await _composerService.GetElevation(id);

            if (json)
            {
                return Json(elevation, JsonRequestBehavior.AllowGet);
            }

            return View2(elevation);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Elevation(Elevation elevation)
        {
            if (!ModelState.IsValid)
            {
                return View2(elevation);
            }

            var newElevation = await _composerService.SaveElevation(elevation);

            if (Request.IsAjaxRequest())
            {
                return Json(newElevation);
            }

            return RedirectToAction("Plan", new { id = newElevation.PlanId });
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteElevation(int id)
        {
            var elevation = await _composerService.DeleteElevation(id);

            if (elevation == null)
            {
                var msg = "Elevation was not deleted successfully!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return RedirectToAction("Plan", new { id = elevation.PlanId });
        }

        #endregion

        #region InteriorColorScheme
        [HttpGet]
        public async Task<ActionResult> InteriorColorSchemes()
        {
            var colorSchemes = await _composerService.GetInteriorColorSchemes();

            return View2(colorSchemes.ToList());
        }

        [HttpGet]
        public async Task<ActionResult> InteriorColorScheme(int id = 0, bool json = false)
        {
            var colorScheme = await _composerService.GetInteriorColorScheme(id);

            if (json)
            {
                return Json(colorScheme, JsonRequestBehavior.AllowGet);
            }

            return View2(colorScheme);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InteriorColorScheme(InteriorColorScheme model)
        {
            if (!ModelState.IsValid)
            {
                return View2(model);
            }

            var result = await _composerService.SaveInteriorColorScheme(model);

            if (Request.IsAjaxRequest())
            {
                return Json(result);
            }

            return RedirectToAction("InteriorColorSchemes");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteInteriorColorScheme(int id)
        {
            var deletedColorScheme = await _composerService.DeleteInteriorColorScheme(id);

            if (!deletedColorScheme)
            {
                var msg = "Color Scheme was not deleted successfully";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }

        #endregion

        #region ExteriorColorScheme

        [HttpGet]
        public async Task<ActionResult> ExteriorColorSchemes()
        {
            var colorSchemes = await _composerService.GetExteriorColorSchemes();

            return View2(colorSchemes.ToList());
        }

        [HttpGet]
        public async Task<ActionResult> ExteriorColorScheme(int id = 0, bool json = false)
        {
            var colorScheme = await _composerService.GetExteriorColorScheme(id);

            if (json)
            {
                return Json(colorScheme, JsonRequestBehavior.AllowGet);
            }

            return View2(colorScheme);
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExteriorColorScheme(ExteriorColorScheme model)
        {
            if (!ModelState.IsValid)
            {
                return View2(model);
            }

            var result = await _composerService.SaveExteriorColorScheme(model);

            if (Request.IsAjaxRequest())
            {
                return Json(result);
            }

            return RedirectToAction("ExteriorColorSchemes");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> DeleteExteriorColorScheme(int id)
        {
            var deletedColorScheme = await _composerService.DeleteExteriorColorScheme(id);

            if (!deletedColorScheme)
            {
                var msg = "Color Scheme was not deleted successfully";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }

        #endregion

        [HttpGet]
        [Authorize(Roles = Admin)]
        public async Task<ActionResult> Supplies(int builderId, long paintStoreId = 0)
        {
            var viewModel = await _composerService.GetSupplies(builderId, paintStoreId);

            return View2(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> UpdateSuppliesPage(int builderId, long paintStoreId = -1)
        {
            var viewModel = await _composerService.GetSupplies(builderId, paintStoreId);

            return PartialView2("_Supplies", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Supplies(SuppliesViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View2(viewModel);
            }

            await _composerService.SaveSupplies(viewModel);

            return RedirectToAction("Builders");
        }

        [HttpPost]
        [Authorize(Roles = Admin)]
        public async Task<JsonResult> AddSupplyTypesByName(List<SupplyTypeSummary> supplyTypes)
        {
            return Json(await _composerService.AddSupplyTypes(supplyTypes)) ;
        }
    }
}