﻿using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.ViewModels;
using ImagePainting.Site.Web.Helpers;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize]
    public class PaintStoresController : BaseController
    {
        private readonly IPaintStoreService _paintStoreService;
        private readonly ILogger _logger;
        private readonly ICommunityComposerService _communityComposerService;
        private readonly IPurchaseOrderService _purchaseOrderService;

        public PaintStoresController(IPaintStoreService paintStoreService, ILogger logger, 
            ICommunityComposerService communityComposerService, IPurchaseOrderService purchaseOrderService, 
            IViewHelper viewHelper,
            IUserHelper userHelper) 
            : base(viewHelper, userHelper)
        {
            _paintStoreService = paintStoreService;
            _logger = logger;
            _communityComposerService = communityComposerService;
            _purchaseOrderService = purchaseOrderService;
        }

        // GET: PaintStores
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var stores = await _paintStoreService.GetStores();
            return View2(stores.ToList());
        }

        [HttpGet]
        public async Task<ActionResult> Store(long id = 0, long locationId = 0)
        {
            var store = await _paintStoreService.GetStore(id, locationId);
            return View2(store);
        }

        [HttpPost]
        [Authorize(Roles = RolesConstant.Admin)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Store(PaintStoreViewModel store)
        {
            if (!ModelState.IsValid)
            {
                return View2(store);
            }

            await _paintStoreService.SaveStore(store);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize(Roles = RolesConstant.Admin)]
        public async Task<ActionResult> DeleteLocation(long id)
        {
            try
            {
                await _paintStoreService.DeleteLocation(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var msg = "Deleting store location failed!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }
            
            return Json(new { Success = true });
        }

        [HttpPost]
        [Authorize(Roles = RolesConstant.Admin)]
        public async Task<ActionResult> DeleteStore(long id)
        {
            try
            {
                await _paintStoreService.DeleteStore(id);
                
                _purchaseOrderService.DeleteUnsentPurchaseOrdersByStoreId(id);

                _communityComposerService.DeleteSuppliesByStoreId(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                var msg = "Deleting store failed!";

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, msg);
            }

            return Json(new { Success = true });
        }
    }
}