﻿using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using System.Threading.Tasks;
using System.Web.Mvc;
using static ImagePainting.Site.Web.Helpers.RolesConstant;

namespace ImagePainting.Site.Web.Controllers
{
    [Authorize(Roles = Admin)]
    public class DashboardController : Controller
    {
        private readonly ISystemAlertService _systemAlertService;
        private readonly ITaskComposerService _taskService;

        public DashboardController(
            ISystemAlertService systemAlertService,
            ITaskComposerService taskService)
        {
            _systemAlertService = systemAlertService;
            _taskService = taskService;
        }

        public async Task<ActionResult> Index()
        {
            var searchCriteria = new AlertSearchCriteria
            {
                Resolved = false
            };

            var unresolved = await _systemAlertService.SearchAlerts(searchCriteria);

            return View(unresolved);
        }

        public async Task<ActionResult> DeferredTasks()
        {
            var model = await _taskService.GetDeferredTasks();

            return PartialView("_DeferredTasks", model);
        }

        [HttpPost]
        public async Task UpdateDeferredTasks(params long[] ids)
        {
            await _taskService.UnDeferTasks(ids);
        }
    }
}