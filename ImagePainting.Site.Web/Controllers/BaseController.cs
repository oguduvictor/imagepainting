﻿using ImagePainting.Site.Web.Helpers;
using ImagePainting.Site.Web.Models;
using System.Web.Mvc;

namespace ImagePainting.Site.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly IViewHelper _viewHelper;
        private readonly IUserHelper _userHelper;

        public BaseController(IViewHelper viewHelper, IUserHelper userHelper)
        {
            _viewHelper = viewHelper;
            _userHelper = userHelper;
        }

        protected SiteViewModel<T> GetViewModel<T>(T model)
        {
            return new SiteViewModel<T>(model, _viewHelper, _userHelper);
        }

        protected ViewResult View2<T>(T model)
        {
            return View(GetViewModel(model));
        }

        protected ViewResult View2<T>(string viewName, T model)
        {
            return View(viewName, GetViewModel(model));
        }

        protected PartialViewResult PartialView2<T>(T model)
        {
            return PartialView(GetViewModel(model));
        }

        protected PartialViewResult PartialView2<T>(string viewName , T model)
        {
            return PartialView(viewName ,GetViewModel(model));
        }
    }
}