﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Web.App_Start;
using Microsoft.ApplicationInsights.Extensibility;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ImagePainting.Site.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MapperConfig.Register();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            IoC.Register();

            // Application Insights Settings
            TelemetryConfiguration.Active.DisableTelemetry = !ConfigurationManager.AppSettings["AppInsightsEnabled"].IsTrue();
            TelemetryConfiguration.Active.InstrumentationKey = ConfigurationManager.AppSettings["AppInsightsIntrumentationKey"];
        }
    }
}