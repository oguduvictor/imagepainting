﻿using Elmah;
using ImagePainting.Site.Common.Interfaces.Service;
using System;
using System.Web;

namespace ImagePainting.Site.Web.Providers
{
    public class Logger : ILogger
    {
        public void Error(Exception ex)
        {
            var errorSignal = ErrorSignal.FromCurrentContext();

            if (HttpContext.Current == null && errorSignal != null)
            {
                errorSignal.Raise(ex);
                return;
            }

            var errorLog = ErrorLog.GetDefault(null);

            errorLog.Log(new Error(ex));
        }
    }
}
