﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ImagePainting.Site.Web.Providers
{
    public class UrlProvider : IUrlService
    {
        public string GetAppBaseUrl()
        {
            var httpContext = GetHttpContext();
            var serverVariables = httpContext.Request.ServerVariables;
            var appPath = httpContext.Request.ApplicationPath?.TrimEnd('/') ?? string.Empty;

            if (serverVariables.Count == 0)
            {
                return httpContext.Request.Url.ToString();
            }

            var protocol = serverVariables["SERVER_PROTOCOL"].Equals("HTTP/1.1", StringComparison.CurrentCultureIgnoreCase) ? "http" : "https";
            var host = serverVariables["HTTP_HOST"];

            return $"{protocol}://{host}{appPath}";
        }

        public string GenerateUrl(string actionName, string controllerName, object routeValues)
        {
            var httpContext = GetHttpContext();
            var httpContextBase = new HttpContextWrapper(httpContext);
            var requestContext = new RequestContext(httpContextBase, new RouteData());
            var urlHelper = new UrlHelper(requestContext);
            var action = urlHelper.Action(actionName, controllerName, routeValues);
            var url = $"{GetAppBaseUrl()}{action}";

            return url;
        }

        private HttpContext GetHttpContext()
        {
            var httpContext = HttpContext.Current;

            if (httpContext == null)
            {
                var request = new HttpRequest("/", "DefaultAppBaseUrl".SettingValue(), string.Empty);
                var response = new HttpResponse(new StringWriter());

                httpContext = new HttpContext(request, response);
            }

            return httpContext;
        }
    }
}