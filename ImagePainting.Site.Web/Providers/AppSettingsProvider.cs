﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using System;
using System.IO;

namespace ImagePainting.Site.Web.Providers
{
    public class AppSettingsProvider : IAppSettings
    {
        public string EmailSender => "EmailSender".SettingValue();

        public string ATaskEmailAddress => "ATaskEmailAddress".SettingValue();

        public string BTaskEmailAddress => "BTaskEmailAddress".SettingValue();

        public string CTaskEmailAddress => "CTaskEmailAddress".SettingValue();

        public string AccountingEmailAddress => "AccountingEmailAddress".SettingValue();

        public string ScheduleEmailAddress => "ScheduleEmailAddress".SettingValue();

        public string POEmailAddress => "POEmailAddress".SettingValue();

        public bool SmtpEnableSsl => "SmtpEnableSsl".SettingValue().IsTrue();

        public string SmtpHost => "SmtpHost".SettingValue();

        public string SmtpPassword => "SmtpPassword".SettingValue();

        public int SmtpPort => "SmtpPort".SettingValue().ToSafeInt(25);

        public string SmtpUsername => "SmtpUsername".SettingValue();

        public string SystemEmailSender => "SystemEmailSender".SettingValue();

        public string[] EmailTemplateLocations
        {
            get
            {
                return new[] 
                {
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Views", "Emails"),
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Views", "PdfTemplates"),
                };

            }
        }
    }
}