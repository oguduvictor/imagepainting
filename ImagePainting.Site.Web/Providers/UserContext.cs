﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace ImagePainting.Site.Web.Providers
{
    public class UserContext : IUserContext, IDisposable
    {
        private ApplicationUserManager _userManager;
        private UserInfoSummary _userInfo;
        private readonly IUserCacheService _userCacheService;

        public UserContext(IUserCacheService userCacheService)
        {
            _userCacheService = userCacheService;
        }

        public string Email
        {
            get
            {
                ValidateContext();

                return GetUserInfo().Email;
            }
        }

        public string FirstName
        {
            get
            {
                ValidateContext();

                return GetUserInfo().FirstName;
            }
        }

        public string LastName
        {
            get
            {
                ValidateContext();

                return GetUserInfo().LastName;
            }
        }

        public string UserId
        {
            get
            {
                ValidateContext();

                return GetUserInfo().Id;
            }
        }

        public bool IsAdmin()
        {
            ValidateContext();

            var user = GetUserInfo();

            return user.Roles.Contains(UserRole.Administrator);
        }

        public IList<Guid> GetMemberIdsInRole(UserRole role)
        {
            ValidateContext();

            var users = _userCacheService.GetUsersInRoles(role);

            return users.Select(u => Guid.Parse(u.Id)).ToList();
        }

        public IList<UserRole> GetRoles(string userId)
        {
            ValidateContext();

            var user = _userCacheService.GetUser(userId);

            if (user == null)
            {
                throw new KeyNotFoundException($"No user matches the Id '{userId}'");
            }

            return user.Roles.ToList();
        }

        public async Task AssignUserToRoles(string userId, IEnumerable<UserRole> roles)
        {
            ValidateContext();

            var userMgr = GetUserManager();
            var rolesString = roles.Select(x => x.ToString()).ToArray();

            await userMgr.AddToRolesAsync(userId, rolesString);
        }

        public bool IsInRole(string userId, UserRole role)
        {
            ValidateContext();

            var user = _userCacheService.GetUser(userId);

            if (user == null)
            {
                throw new KeyNotFoundException($"No user matches the Id '{userId}'");
            }

            return user.Roles.Contains(role);
        }

        public void Dispose()
        {
            if (_userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
        }

        private string GetClaimValue(string claimType)
        {
            ValidateContext();

            var claims = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var idClaim = claims.Claims.FirstOrDefault(x => x.Type == claimType);

            return idClaim.Value;
        }

        private void ValidateContext()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                throw new UnauthorizedAccessException("Cannot access user data.");
            }
        }

        private ApplicationUserManager GetUserManager()
        {
            if (_userManager == null)
            {
                _userManager = HttpContext.Current.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            return _userManager;
        }

        private UserInfoSummary GetUserInfo()
        {
            if (_userInfo == null)
            {
                var userId = GetClaimValue(ClaimTypes.NameIdentifier);

                _userInfo = _userCacheService.GetUser(userId);
            }

            return _userInfo;
        }
    }
}