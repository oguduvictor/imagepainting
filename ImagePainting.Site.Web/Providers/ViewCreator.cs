﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace ImagePainting.Site.Web.Providers
{
    public class ViewCreator : IViewCreator
    {
        private const string TemplateLayoutKey = "emailLayout";
        private static ReaderWriterLockSlim _fileLock = new ReaderWriterLockSlim();
        private readonly string[] _emailTemplateLocations;

        public ViewCreator(IAppSettings appSettings)
        {
            _emailTemplateLocations = appSettings.EmailTemplateLocations;
        }

        public string CreateView<T>(Email.TemplateType templateType, T model, IDictionary<string, object> viewBag = null)
        {
            return CreateView(ToTemplateName(templateType), model, viewBag);
        }

        public string CreateView<T>(string viewName, T model, IDictionary<string, object> viewBag = null, bool addLayout = true)
        {
            var modelType = model.GetType();
            var viewBagData = viewBag == null ? null : new DynamicViewBag(viewBag);

            if (Engine.Razor.IsTemplateCached(viewName, modelType))
            {
                return Engine.Razor.Run(viewName, modelType, model, viewBagData);
            }

            if (addLayout)
            {
                AddTemplateLayout();
            }

            var template = GetView(viewName);
            var result = Engine.Razor.RunCompile(template, viewName, null, model, viewBagData);

            return result;
        }

        private string ToTemplateName(Email.TemplateType templateType)
        {
            switch (templateType)
            {
                case Email.TemplateType.TaskSchedule:
                    return "TasksSchedule";

                case Email.TemplateType.TaskCompletion:
                    return "TaskCompletion";

                case Email.TemplateType.PurchaseOrder:
                    return "PurchaseOrder";

                case Email.TemplateType.PassswordReset:
                    return "ForgotPassword";

                case Email.TemplateType.RevenueProjection:
                    return "Projection";

                case Email.TemplateType.NoTaskAssignment:
                    return "NoTaskAssignment";

                default:
                    throw new ArgumentException($"{templateType} is not a supported {nameof(Email.TemplateType)}");
            }
        }

        private string GetView(string viewName)
        {
            var filename = $"{viewName}.cshtml";
            var fullViewPath = default(string);
            var fileContent = default(string);

            foreach (var emailTemplateLocation in _emailTemplateLocations)
            {
                var path = Path.Combine(emailTemplateLocation, filename);

                if (File.Exists(path))
                {
                    fullViewPath = path;
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(fullViewPath))
            {
                throw new FileNotFoundException($"Cannot find any view with the name {viewName}");
            }

            _fileLock.EnterReadLock();

            try
            {
                fileContent = File.ReadAllText(fullViewPath);
            }
            finally
            {
                _fileLock.ExitReadLock();
            }

            return fileContent;
        }

        private void AddTemplateLayout()
        {
            if (Engine.Razor.IsTemplateCached(TemplateLayoutKey, null))
                return;

            var template = GetView($"_{TemplateLayoutKey}");

            Engine.Razor.AddTemplate(TemplateLayoutKey, template);
            Engine.Razor.Compile(TemplateLayoutKey);
        }
    }
}