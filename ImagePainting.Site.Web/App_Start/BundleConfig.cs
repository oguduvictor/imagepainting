﻿namespace ImagePainting.Site.Web
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                            "~/Scripts/jquery-{version}.js",
                            "~/Scripts/toastr.min.js",
                            "~/Scripts/jquery.validate.min.js",
                            "~/Scripts/jquery.validate.unobtrusive.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                            "~/Scripts/bootstrap.js",
                            "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryMask").Include(
                            "~/Scripts/jquery.mask.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/rivets").Include(
                "~/Scripts/rivets.bundled.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatable")
                .Include("~/Scripts/DataTables/jquery.dataTables.js",
                "~/Scripts/DataTables/dataTables.bootstrap.js",
                "~/Scripts/DataTables/dataTables.buttons.min.js",
                "~/Scripts/DataTables/buttons.flash.min.js",
                "~/Scripts/DataTables/dataTables.jszip.min.js",
                "~/Scripts/DataTables/buttons.html5.min.js",
                "~/Scripts/DataTables/buttons.print.min.js",
                "~/Scripts/DataTables/dataTables.rowGroup.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsignature")
                .IncludeDirectory("~/Scripts/jSignature", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/typeahead")
                .Include("~/Scripts/typeahead.bundle.min.js",
                "~/Scripts/typeahead.jquery.min.js", "~/Scripts/bloodhound.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/app")
                        .Include(
                            "~/Scripts/App/app-modal-dialog.js",
                            "~/Scripts/App/app-common.js",
                            "~/Scripts/App/app-common-partialview.js",
                            "~/Scripts/App/app-common-draggable.js"));

            bundles.Add(new ScriptBundle("~/bundles/workorders")
                        .Include("~/Scripts/App/Tasks/app-tasks-common.js")
                        .Include("~/Scripts/App/Tasks/app-tasks-viewTask-common.js")
                        .IncludeDirectory("~/Scripts/App/WorkOrders", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/workorders-list")
                         .Include("~/Scripts/App/workorders-list.js"));

            bundles.Add(new ScriptBundle("~/bundles/reports")
                        .IncludeDirectory("~/Scripts/App/Reports", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/tasks")
                        .IncludeDirectory("~/Scripts/App/Tasks", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/plans")
                        .IncludeDirectory("~/Scripts/App/Plans", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapcombobox").Include(
                            "~/Scripts/bootstrap-combobox.js"));

            bundles.Add(new ScriptBundle("~/bundles/communities")
                        .IncludeDirectory("~/Scripts/App/Communities", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/supplies")
                .IncludeDirectory("~/Scripts/App/Supplies", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/reports")
                .IncludeDirectory("~/Scripts/App/Reports", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/dashboard")
                .IncludeDirectory("~/Scripts/App/Dashboard", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapmultiselect")
                .Include("~/Scripts/bootstrap-multiselect.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                            "~/Content/normalize.css",
                            "~/Content/bootstrap.css",
                            "~/Content/DataTables/css/dataTables.bootstrap.css",
                            "~/Content/DataTables/css/buttons.dataTables.min.css",
                            "~/Content/bootstrap-datetimepicker.css",
                            "~/Content/bootstrap-combobox.css",
                            "~/Content/font-awesome.min.css",
                            "~/Content/toastr.min.css",
                            "~/Content/site.min.css",
                            "~/Content/bootstrap-multiselect.css"));
        }
    }
}