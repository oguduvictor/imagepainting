﻿using AutoMapper;
using ImagePainting.Site.Common;
using ImagePainting.Site.Data;

namespace ImagePainting.Site.Web.App_Start
{
    public class MapperConfig
    {
        public static void Register()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<CoreDataMapperProfile>();
                cfg.AddProfile<SqlDataMapperProfile>();
            });
        }
    }
}