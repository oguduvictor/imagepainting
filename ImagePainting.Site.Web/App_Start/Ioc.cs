﻿using ImagePainting.ServiceBus;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Data;
using ImagePainting.Site.Domain;
using ImagePainting.Site.Web.Helpers;
using ImagePainting.Site.Web.Providers;
using LightInject;
using System.Net.Mail;

namespace ImagePainting.Site.Web.App_Start
{
    public class IoC
    {
        public static ServiceContainer Container { get; private set; }

        public static void Register()
        {
            Container = new ServiceContainer();

            Container.RegisterControllers();
            
            Container.Register<SmtpClient>(new PerScopeLifetime());
            
            Container.Register<ILogger, Logger>(new PerContainerLifetime());
            Container.Register<IViewCreator, ViewCreator>(new PerContainerLifetime());
            Container.Register<IUrlService, UrlProvider>(new PerContainerLifetime());
            Container.Register<IAppSettings, AppSettingsProvider>(new PerContainerLifetime());

            Container.Register<IViewHelper>(serviceFactory => new ViewHelper(serviceFactory), new PerScopeLifetime());
            Container.Register<IUserHelper, UserHelper>(new PerScopeLifetime());

            Container.Register<IUserContext, UserContext>(new PerScopeLifetime());

            Container.RegisterFrom<DataIoCModule>();
            Container.RegisterFrom<DomainIoCModule>();
            Container.RegisterFrom<ServiceBusIoCModule>();

            Container.EnableMvc();
        }
    }
}