﻿using ImagePainting.Site.Common.ServiceBus;
using System;

namespace ImagePainting.Site.Retrofits.Commands
{
    public class RetrofitJobExtraItemsCommand : IMessage
    {
        public RetrofitJobExtraItemsCommand(string userId)
        {
            Id = Guid.NewGuid();
            Created = DateTime.UtcNow;
            UserId = userId;
        }

        public Guid Id { get; private set; }

        public string UserId { get; private set; }

        public DateTime Created { get; private set; }
    }
}
