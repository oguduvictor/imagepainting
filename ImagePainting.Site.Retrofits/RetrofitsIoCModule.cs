﻿using LightInject;

namespace ImagePainting.Site.Retrofits
{
    public class RetrofitsIoCModule : ICompositionRoot
    {
        public void Compose(IServiceRegistry container)
        {
            container.Register<TaskWorkItemsUpdater>(new PerScopeLifetime());
            container.Register<JobInfoNoteRetrofix>(new PerScopeLifetime());
            container.Register<WorkOrderDescriptionRefrofix>(new PerScopeLifetime());
        }
    }
}
