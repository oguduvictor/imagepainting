﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Retrofits
{
    public class WorkOrderDescriptionRefrofix
    {
        private readonly IWorkOrderRepository _workOrderRepository;

        public WorkOrderDescriptionRefrofix(IWorkOrderRepository workOrderRepository)
        {
            _workOrderRepository = workOrderRepository;
        }

        public async Task FixDescriptionInWorkOrders()
        {
            var query = new WorkOrderQuery { PageNumber = 0, ResultsCount = 5, ByName = true };

            var workOrders = await _workOrderRepository.GetWorkOrders(query);

            while (workOrders.Any())
            {
                foreach (var workOrder in workOrders)
                {
                    await DoFix(workOrder);
                }

                query.PageNumber++;
                workOrders = await _workOrderRepository.GetWorkOrders(query);
            }
        }

        private async Task DoFix(WorkOrder workOrder)
        {
            if (string.IsNullOrWhiteSpace(workOrder.Description))
            {
                return;
            }

            if (workOrder.Description.IsDeserializeable<List<Note>>())
            {
                return; 
            }

            var notes = new List<Note>
            {
                new Note
                {
                    Id = Guid.NewGuid(),
                    Author = workOrder.CreatedBy?.FullName,
                    Text = workOrder.Description,
                    Created = workOrder.Created
                }
            };

            workOrder.Description = notes.ToJson();

            await _workOrderRepository.UpdateWorkOrder(workOrder);
        }

    }
}
