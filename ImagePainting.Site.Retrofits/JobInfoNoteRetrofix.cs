﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Retrofits
{
    public class JobInfoNoteRetrofix
    {
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly IUserInfoRepository _userInfoRepository;

        public JobInfoNoteRetrofix(IWorkOrderRepository workOrderRepository, IUserInfoRepository userInfoRepository)
        {
            _workOrderRepository = workOrderRepository;
            _userInfoRepository = userInfoRepository;
        }

        public async Task FixAllJobInfoNotes()
        {
            var query = new WorkOrderQuery { PageNumber = 0, ResultsCount = 5, ByName = true };
            var workOrders = await _workOrderRepository.GetWorkOrders(query);

            while (workOrders.Any())
            {
                foreach (var workOrder in workOrders)
                {
                    await DoFix(workOrder);
                }

                query.PageNumber++;
                workOrders = await _workOrderRepository.GetWorkOrders(query);
            }
        }

        private async Task DoFix(WorkOrder workOrder)
        {
            if (workOrder.JobInfoId.IsNull() || workOrder.JobInfoId == default(long))
            {
                return;
            }

            var jobInfo = workOrder.JobInfo;

            if (string.IsNullOrWhiteSpace(jobInfo?.Note) || jobInfo.Note.IsDeserializeable<List<Note>>())
            {
                return;
            }

            var user = await _userInfoRepository.GetUserAsync(workOrder.ModifiedById);
            var notes = new List<Note>
            {
                new Note
                {
                    Id = Guid.NewGuid(),
                    Author = user.FullName,
                    Text = jobInfo.Note,
                    Created = jobInfo.Created
                }
            };

            jobInfo.Note = notes.ToJson();

            await _workOrderRepository.UpdateJobInfo(jobInfo);
        }
    }
}
