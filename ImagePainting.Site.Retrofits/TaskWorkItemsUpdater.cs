﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Retrofits
{
    public class TaskWorkItemsUpdater
    {
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly ITaskRepository _taskRepository;

        public TaskWorkItemsUpdater(IWorkOrderRepository workOrderRepository, ITaskRepository taskRepository)
        {
            _workOrderRepository = workOrderRepository;
            _taskRepository = taskRepository;
        }

        public async Task SetJobExtraTaskIds()
        {
            var query = new WorkOrderQuery { PageNumber = 0, ResultsCount = 5, ByName = true };
            var workOrders = await _workOrderRepository.GetWorkOrders(query);

            while (workOrders.Any())
            {
                foreach (var workOrder in workOrders)
                {
                    var extraJobInfo = await _workOrderRepository.GetExtraJobInfoByWorkOrderId(workOrder.Id);
                    var mainTasks = await _taskRepository.GetTasksByWorkOrderId(workOrder.Id, mainTaskOnly: true);

                    await UpdateExtraJobInfo(extraJobInfo, mainTasks);

                    var unUpdatedTasks = mainTasks.Where(x => !x.IncludeBase).ToArray();

                    if (!unUpdatedTasks.Any())
                    {
                        continue;
                    }

                    foreach (var task in unUpdatedTasks)
                    {
                        task.IncludeBase = true;
                    }
                   
                    await _taskRepository.UpdateWorkOrderTasks(unUpdatedTasks);
                }

                query.PageNumber++;
                workOrders = await _workOrderRepository.GetWorkOrders(query);
            }
        }

        private async Task UpdateExtraJobInfo(ExtraJobInfo extraJobInfo, IEnumerable<WorkOrderTask> mainTaskSummaries)
        {
            if (extraJobInfo.IsNull() || mainTaskSummaries.IsNullOrEmpty())
            {
                return;
            }

            extraJobInfo.ExtraItems.ForEach(extraItem =>
            {
                if (extraItem.Id == Guid.Empty)
                {
                    extraItem.Id = Guid.NewGuid();
                }

                if (!extraItem.TaskIds.IsNullOrEmpty()) return;

                extraItem.TaskIds = mainTaskSummaries
                                        .Where(x => x.TaskType == extraItem.TaskType)
                                        .Select(x => x.Id).ToList();
            });

            extraJobInfo.ExtraItems = extraJobInfo.ExtraItems;

            await _workOrderRepository.UpdateExtraJobInfo(extraJobInfo);
        }
    }
}
