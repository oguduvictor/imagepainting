﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Interfaces
{
    public interface IUserContext
    {
        string Email { get; }

        string FirstName { get; }

        string LastName { get; }

        string UserId { get; }

        IList<Guid> GetMemberIdsInRole(UserRole role);

        IList<UserRole> GetRoles(string userId);

        bool IsInRole(string userId, UserRole role);

        bool IsAdmin();
    }
}