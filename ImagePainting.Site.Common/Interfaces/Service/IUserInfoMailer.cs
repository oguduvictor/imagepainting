﻿using ImagePainting.Site.Common.Models;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IUserInfoMailer
    {
        Task EmailForgotPassWord(UserInfo user, string url);
    }
}
