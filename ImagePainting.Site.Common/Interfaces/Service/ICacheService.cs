﻿using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ICacheService
    {
        TObject Get<TObject>(string key, bool defaultIfNotFound = false);

        void AddOrUpdate<TObject>(string key, TObject value, TimeSpan cacheDuration = default(TimeSpan));
        
        void Remove(string key);
        
        IList<TObject> GetList<TObject>(IEnumerable<string> keys, string prefix);

        void Clear(string keyPrefix);
    }
}
