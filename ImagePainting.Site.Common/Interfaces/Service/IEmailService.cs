﻿using ImagePainting.Site.Common.Models;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IEmailService
    {
        void Send(Email email);

        Task SendAsync(Email email);

        MailMessage PreviewEmail(Email email);
    }
}
