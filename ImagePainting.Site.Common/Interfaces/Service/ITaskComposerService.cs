﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ITaskComposerService
    {
        Task<WorkOrderTaskSchema> GetWorkSchemaTask(int taskId);

        Task<List<WorkOrderTaskSchema>> GetTaskSchemas(bool isSummary = false, bool mainTaskOnly = false);

        Task<WorkOrderTaskSchema> SaveTaskSchema(WorkOrderTaskSchema task);

        Task<IEnumerable<WorkOrderTask>> GetWorkOrderTasks(TaskSearchCriteria searchCriteria);

        Task<IList<TaskView>> SearchWorkOrderTasks(TaskSearchCriteria searchCriteria);

        Task SendWorkOrderMails(IEnumerable<long> ids, string note);

        Task DeleteTaskSchema(int id);

        Task SendNoTaskEmailAsync(IEnumerable<string> userIds, DateTime date, string note);

        Task<IDictionary<DateTime, IEnumerable<UserInfoSummary>>> GetUnAssignedSubContractors(DateTimeRange dateRange);

        Task<IList<UnAssignedCrewSetting>> GetEmailedUnAssignedCrewsInRange(DateTimeRange dateRange);

        Task UpdateUnAssignedCrewSetting(IEnumerable<string> userIds, DateTime date, bool? email = null, bool? busy = null);

        Task<UserInfo> GetBusyUser(string crewId, DateTime date);

        Task CreatePdf(long taskId, string encodedsignatureImage);

        Task<string> GetTaskPdfTemplate(long taskId);

        Task<byte[]> GetTaskSignedPdf(long taskId);

        Task<IEnumerable<WorkOrderTask>> GetDeferredTasks();

        Task UnDeferTasks(params long[] taskIds);

        /// <summary>
        /// Toggles the state if deferred is not stated
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="deferred"></param>
        Task SetTaskDeferState(long taskId, bool? deferred = null);
    }
}
