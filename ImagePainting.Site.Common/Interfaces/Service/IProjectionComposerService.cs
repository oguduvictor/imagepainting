﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IProjectionComposerService
    {
        Task<IEnumerable<MaterialProjection>> GetWorkOrder(long id);

        Task<RevenueProjection> GetRevenueProjection(long workOrderId);

        Task<LaborProjection> GetLaborProjection(long workOrderId);

        Task<ProfitProjection> GetProfitProjection(long workOrderId);
    }
}