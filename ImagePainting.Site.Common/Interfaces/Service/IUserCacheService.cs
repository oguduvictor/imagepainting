﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IUserCacheService
    {
        UserInfoSummary GetUser(string userId);
        Task<IList<UserInfoSummary>> GetUsersAsync(IEnumerable<string> userIds);
        IList<UserInfoSummary> GetUsers();
        IList<UserInfoSummary> GetUsersInRoles(params UserRole[] userRoles);
        void UpdateUserInCache(UserInfo user);
        void ClearAll();
        void CacheUser(UserInfoSummary userInfo);
        void Remove(string userId);
    }
}
