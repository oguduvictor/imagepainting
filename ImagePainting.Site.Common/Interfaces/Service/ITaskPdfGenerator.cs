﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ITaskPdfGenerator
    {
        string GetTaskPdfTemplate(WorkOrderTask task, string encodedSignature = null, bool completeCopy = false, IDictionary<string, decimal> billingInfo = null, DateTime? signedDate = null);

        byte[] CreateTaskPdf(WorkOrderTask task, string encodedSignature = null, bool completeCopy = false, IDictionary<string, decimal> billingInfo = null, DateTime? signedDate = null);
    }
}
