﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IUserInfoService
    {
        Task<List<UserInfo>> GetUsersSummary(bool includeDeleted = false, bool includeInActive = false);

        Task AssignRolesToUser(string userId, IEnumerable<RoleSelection> model);

        Task<UserInfo> GetUserAsync(string userId); 

        Task<UserInfo> GetUserRoles(string id);

        Task<IEnumerable<UserInfo>> GetUsers();

        Task SaveUserInfo(UserInfo userInfo);

        Task<UserInfo> GetUserByEmailAsync(string userEmail);

        Task DeleteUser(string id);

    }
}