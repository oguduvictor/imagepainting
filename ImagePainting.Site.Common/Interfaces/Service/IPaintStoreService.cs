﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IPaintStoreService
    {
        Task<IEnumerable<Store>> GetStores();

        Task<PaintStoreViewModel> GetStore(long id, long locationId);

        Task<Store> SaveStore(PaintStoreViewModel store);

        Task<List<Store>> GetStoreSummary(bool includeDeleted = false);

        Task<IEnumerable<StoreLocation>> GetStoreLocations(long? storeId = null, bool includeDeleted = false);

        Task<StoreLocation> GetStoreLocation(long locationId);

        Task DeleteLocation(long locationId);

        Task DeleteStore(long storeId);
    }
}
