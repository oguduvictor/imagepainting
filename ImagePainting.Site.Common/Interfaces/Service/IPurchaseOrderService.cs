﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IPurchaseOrderService
    {
        Task DeletePurchaseOrder(long id);

        Task<PurchaseOrder> GetPurchaseOrder(long taskId, long id, long? storeId = 0);

        Task<PurchaseOrder> GetPurchaseOrder(long id, bool complete = false);

        Task<List<PurchaseOrder>> GetPurchaseOrders(long? taskId = null, long? workOrderId = null, 
            bool includeStoreLocation = false, bool includeItems = false, bool? emailed = null);

        Task<PurchaseOrder> SavePurchaseOrder(PurchaseOrder order, bool sendEmail = false);

        Task UpdatePurchaseOrderEmailSent(long id, string emailAddress);

        void DeleteUnsentPurchaseOrdersByStoreId(long paintStoreId);

        Task<IEnumerable<PurchaseOrder>> WorkOrderPurchaseOrders(long workOrderId);

        Task UnlockPurchaseOrder(long id);
    }
}