﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IWorkOrderMailer
    {
        Task EmailProjection(long workOrderId);

        Task EmailSchedules(IEnumerable<WorkOrderTask> tasks, string note);
        
        Task EmailTaskCompletion(long workOrderTaskId, TaskCategory taskCategory);

        Task EmailPurchaseOrder(long orderId, string emailAddress);

        Task<MailMessage> PreviewPurchaseOrder(long orderId);

        Task EmailFreeCrewsSchedule(IEnumerable<UserInfo> freeCrews, DateTime taskDate, string note);
    }
}