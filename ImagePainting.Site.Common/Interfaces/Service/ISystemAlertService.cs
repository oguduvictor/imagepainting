﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ISystemAlertService
    {
        Task DeleteAlerts(params SystemAlert[] alerts);
        
        Task<IList<SystemAlert>> SearchAlerts(AlertSearchCriteria searchCriteria);

        Task<IList<SystemAlert>> GetNoteAlerts(long workOrderId);

        Task ResolveAlerts(params Guid[] ids);
        
        Task SaveJobInfoNoteAlert(WorkOrder workOrder, JobInfo originalJobInfo);

        Task SaveTaskNoteAlert(WorkOrder workOrder, WorkOrderTask task, Note note);

        Task SaveExtraItemAlerts(IEnumerable<ExtraItem> extraItems, WorkOrder workOrder);

        Task SaveJobExtraNoteAlert(WorkOrder workOrder, ExtraJobInfo originalJobExtra);

        Task SaveWorkOrderNoteAlert(WorkOrder workOrder, string originalDescription);
    }
}