﻿using System;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ILogger
    {
        void Error(Exception ex);
    }
}
