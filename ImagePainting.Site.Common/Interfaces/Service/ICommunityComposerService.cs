﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface ICommunityComposerService
    {
        Task<bool> DeleteBuilder(int builderId);
        Task<bool> DeleteCommunity(int communityId, int builderId = 0);
        Task DeleteCost(int id);
        Task DeleteCostItem(int id);
        Task<Elevation> DeleteElevation(int elevationId);
        Task<bool> DeleteExteriorColorScheme(int colorSchemeId);
        Task<bool> DeleteInteriorColorScheme(int colorSchemeId);
        Task<bool> DeletePlan(int planId);
        Task<Builder> GetBuilder(int? builderId, bool includeCost = false, bool includePlans = false);
        Task<List<Builder>> GetBuilders();
        Task<List<Builder>> GetBuilders(List<int> builderIds);
        Task<List<Builder>> GetBuildersSummary();
        Task<List<Community>> GetCommunities();
        Task<List<Community>> GetCommunitiesSummary(long? builderId = default(long?));
        Task<Community> GetCommunity(int communityId);
        Task<Cost> GetCostForBuilder(int builderId, int costId);
        Task<List<CostItem>> GetCostItems(int builderId = 0);
        Task<IEnumerable<CostItem>> GetCostItemsNotInBuilder(int builderId);
        Task<Elevation> GetElevation(int elevationId);
        Task<List<Elevation>> GetElevationsSummary(int? planId = default(int?));
        Task<IEnumerable<ExteriorColorScheme>> GetExeteriorColorSchemeSummary(string colorCodePrefix = null);
        Task<ExteriorColorScheme> GetExteriorColorScheme(int colorSchemeId);
        Task<IEnumerable<ExteriorColorScheme>> GetExteriorColorSchemes();
        Task<InteriorColorScheme> GetInteriorColorScheme(int colorSchemeId);
        Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemes();
        Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemeSummary(string colorCodePrefix = null);
        Task<Plan> GetPlan(int planId, bool includeElevations = false, bool includeCommunities = false);
        Task<IEnumerable<PlanSummary>> GetPlans();
        Task<List<PlanSummary>> GetPlansSummary(int? builderId);
        Task<List<State>> GetStates();
        Task<Builder> SaveBuilder(Builder builder, int buiderCopiedCostId);
        Task<Community> SaveCommunity(Community community);
        Task<Cost> SaveCost(Cost cost);
        Task<CostItem> SaveCostItem(CostItem costItem);
        Task<Elevation> SaveElevation(Elevation elevation);
        Task<ExteriorColorScheme> SaveExteriorColorScheme(ExteriorColorScheme colorScheme);
        Task<InteriorColorScheme> SaveInteriorColorScheme(InteriorColorScheme colorScheme);
        Task<Plan> SavePlan(Plan plan);
        Task<bool> DeletePlanCommunity(int planId, int communityId);
        Task UpdateBuilderCosts(Builder builder);
        Task UpdateCostItems(IEnumerable<CostItem> costItems, bool laborCostOnly = false);
        Task<List<SupplyType>> GetSupplyTypes(SupplyCategory category = SupplyCategory.All);
        Task<SuppliesViewModel> GetSupplies(int builderId, long paintStoreId = 0);
        Task<List<Supply>> GetBuilderSupplies(int builderId, long paintStoreId);
        Task SaveSupplies(SuppliesViewModel viewModel);
        Task<List<int>> AddSupplyTypes(List<SupplyTypeSummary> supplyTypes);
        void DeleteSuppliesByStoreId(long paintStoreId);
    }
}