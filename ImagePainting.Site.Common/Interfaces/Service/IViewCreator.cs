﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IViewCreator
    {
        string CreateView<T>(Email.TemplateType templateType, T model, IDictionary<string, object> viewBag = null);

        string CreateView<T>(string viewName, T model, IDictionary<string, object> viewBag = null, bool addLayout = true);
    }
}
