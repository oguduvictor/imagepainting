﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IWorkOrderComposerService
    {
        Task<WorkOrderItemViewModel<WorkflowInitalSettingsViewModel>> GetInitialSettingsModel(long workOrderId);

        Task<WorkOrder> SaveInitialSettings(WorkflowInitalSettingsViewModel model);

        Task<WorkOrderItemViewModel<JobInfo>> GetJobInfo(long workOrderId);

        Task<WorkOrder> GetWorkOrder(long workOrderId, bool includeTasks = false);

        Task<IEnumerable<WorkOrder>> GetWorkOrders(WorkOrderQuery query);

        Task<PagedResult<WorkOrderView>> SearchWorkOrders(WorkOrderQuery query);

        Task DeleteWorkOrder(long workOrderId);

        Task<JobInfo> SaveJobInfo(long workOrderId, JobInfo model);

        Task<long> GetTotalWorkOrderCount(string searchString = "");

        Task<WorkOrderItemViewModel<ExtraJobInfo>> GetExtraJobInfo(long workOrderId);

        Task<IEnumerable<ExtraItem>> GetAllJobExtras(long workOrderId);

        Task<ExtraJobInfo> SaveExtraJobInfo(long workOrderId, ExtraJobInfo model);

        Task<IEnumerable<TaskSummary>> ScheduleTask(long id, IEnumerable<ScheduleWorkOrderTask> model);

        Task<WorkOrderTask> GetWorkOrderTask(long taskId);

        Task SaveWorkOrderTask(WorkOrderTask model);

        Task CompleteWorkOrder(long id);

        IEnumerable<TaskPricingViewModel> GetTaskPricing(long taskId);

        Task<List<string>> GetSuperintendents();

        Task<List<string>> GetCustomerReps();

        Task<long> CloneWorkOrder(long workOrderId);

        Task<List<string>> GetColorSchemes(long workOrderId, SupplyCategory category);

        Task<IDictionary<string, IList<Note>>> GetNotes(long workorderId);

        Task<bool> WorkOrderExists(long workOrderId, string name);

        Task<JobInfo> GetJobInformation(long workOrderId);

        Task ChangeExtraItemTaskType(long workOrderId, string extraItemName, TaskType taskType, List<long> taskIds);

        WorkOrderItemViewModel<List<WorkItem>> GetWorkItems(long taskId);

        Task SaveWorkItems(long taskId, IEnumerable<WorkItem> workItems);

        Task<Dictionary<string, IEnumerable<FileUpload>>> GetFileUploads(long workOrderId);
    }
}