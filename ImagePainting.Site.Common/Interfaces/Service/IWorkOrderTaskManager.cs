﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IWorkOrderTaskManager
    {
        Task<WorkOrderTask> CompleteTask(WorkOrderTask task);

        Task<WorkOrderTask> GetTaskWithPermissions(long taskId, bool includeWorkOrder = false);

        Task<IEnumerable<ScheduleWorkOrderTask>> GetTaskSchedules(long workOrderId);

        Task<WorkOrderTask> SaveWorkOrderTask(WorkOrderTask task);

        Task<WorkOrderTask> UpdateTaskSchedule(TaskSchedule task);

        Task ScheduleTask(long workOrderId, IEnumerable<ScheduleWorkOrderTask> taskModels);

        Task<WorkOrderTask> UnlockTask(long taskId);

        Task<IEnumerable<WorkOrderTask>> GetTasks(TaskSearchCriteria searchCriteria);

        Task<IList<TaskView>> SearchTasks(TaskSearchCriteria searchCriteria);

        Task<bool> AreTasksCompleted(long workOrderId);

        Task<WorkOrderTask> GetMainTask(long taskId);

        Task EvaluatePermissions(WorkOrderTask task, bool full = true);
    }
}