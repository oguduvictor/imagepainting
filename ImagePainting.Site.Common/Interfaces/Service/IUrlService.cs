﻿namespace ImagePainting.Site.Common.Interfaces.Service
{
    public interface IUrlService
    {
        string GetAppBaseUrl();

        string GenerateUrl(string actionName, string controllerName, object routeValues);
    }
}
