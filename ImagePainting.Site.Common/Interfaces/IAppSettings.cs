﻿namespace ImagePainting.Site.Common.Interfaces
{
    public interface IAppSettings
    {
        string SmtpHost { get; }

        int SmtpPort { get; }

        string SmtpUsername { get; }

        string SmtpPassword { get; }

        bool SmtpEnableSsl { get; }

        string EmailSender { get; }

        string ATaskEmailAddress { get; }

        string BTaskEmailAddress { get; }

        string CTaskEmailAddress { get; }

        string AccountingEmailAddress { get; }

        string ScheduleEmailAddress { get; }

        string POEmailAddress { get; }

        string SystemEmailSender { get; }

        string[] EmailTemplateLocations { get; }
    }
}
