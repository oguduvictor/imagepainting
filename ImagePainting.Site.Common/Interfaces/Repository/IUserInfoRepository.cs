﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IUserInfoRepository
    {
        Task<List<UserInfo>> GetUsersAsync(bool isSummary = true, bool includeRole = false, bool includeInActive = false, bool includeDeleted = false);
        
        Task<UserInfo> GetUserAsync(string userId, bool includeRoles = false);

        Task CreateUserInfoAsync(UserInfo userInfo);

        Task UpdateUserInfoAsync(UserInfo userInfo);

        Task<IEnumerable<Role>> GetAllRolesAsync();

        Task<string> GetUserIdForWalkTask();

        Task<IEnumerable<UserInfo>> GetUsersInRoleAsync(UserRole userRole, bool includeActiveAndDeleteUsers = false);

        Task<UserInfo> GetUserByEmailAsync(string userEmail);

        Task DeleteUserRolesAsync(string userId);

        Task<bool> TryDeleteUserInfoAsync(string userId);

        Task DeleteUserLoginAsync(string userId);

        Task<IEnumerable<UserInfo>> GetUsersAsync(IEnumerable<string> userIds);
        
    }
}
