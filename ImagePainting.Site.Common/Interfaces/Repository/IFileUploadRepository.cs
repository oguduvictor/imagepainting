﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IFileUploadRepository
    {        
        Task<FileUpload> GetFileUpload(long id);

        Task<IEnumerable<FileUpload>> GetFileUploads(long ownerId, params FileOwnerType[] ownerType);

        Task<IEnumerable<FileUpload>> GetFileUploads(IEnumerable<long> ownersId, params FileOwnerType[] ownerType);

        Task<int> CreateFileUpload(FileUpload fileUpload);

        Task<int> CreateFileUploads(IEnumerable<FileUpload> fileUploads);

        Task<int> DeleteFileUpload(long id);

        Task<int> DeleteFileUploads(IEnumerable<long> ownersId, FileOwnerType ownerType);

        Task<bool> HasAnyFiles(long ownerId, FileOwnerType ownerType);

        Task<FileUpload> GetFirstOrDefault(long taskId, FileOwnerType fileOwner);
    }
}