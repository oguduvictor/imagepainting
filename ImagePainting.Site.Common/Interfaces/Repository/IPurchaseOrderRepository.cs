﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IPurchaseOrderRepository
    {
        Task Create(PurchaseOrder order);

        Task<PurchaseOrder> Get(long id, bool includeLocation = false, bool includeTask = false, bool includeWorkOrder = false, bool includeItems = false);

        Task<List<PurchaseOrder>> GetAll(long? taskId = null, long? workOrderId = null, 
            bool includeStoreLocation = false, bool includeItems = false, bool includeTask = false, bool? emailed = null);

        Task<List<PurchaseOrderSummary>> GetEmailedPurchaseOrders(params long[] taskIds);

        Task Update(PurchaseOrder order);

        Task Update(List<PurchaseOrderItem> items, long id);

        Task DeletePurchaseOrders(params long[] ids);

        Task DeleteUnsentPurchaseOrdersByStoreId(long paintStoreId);

        Task DeletePurchaseOrdersByTaskIds(params long[] taskIds);

        Task<bool> HasAnyPurchaseOrderEmailed(long workorderTaskId);
    }
}