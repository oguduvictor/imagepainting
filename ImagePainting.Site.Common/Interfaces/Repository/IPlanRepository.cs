﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IPlanRepository
    {
        Task<List<PlanSummary>> GetPlanSummaries(bool includeCommunities = false, bool includeBuilder = false, int? builderId = null, bool includeDeleted = false);

        Task<Plan> GetPlan(int planId, bool includeElevations = false, bool includeCommunities = false);

        Task<Plan> CreatePlan(Plan plan);

        Task<Plan> UpdatePlan(Plan plan);
    }
}
