﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ISupplyRepository
    {
        Task<Supply> GetSupply(int id);

        Task<List<int>> AddSupplyTypes(List<SupplyTypeSummary> supplyTypes);

        Task<List<Supply>> GetSupplies(int? builderId = 0, long? paintStoreId = 0);

        Task<long> GetAnyPaintStore(int? builderId);

        Task<List<Supply>> GetSupplies(int builderId, SupplyCategory category, long? storeId = 0);

        Task<List<long>> GetPaintStoreIdsByBuilder(int builderId);

        Task DeleteSuppliesByStoreId(long paintStoreId);

        Task Create(IEnumerable<Supply> supplies);

        Task Update(IEnumerable<Supply> supplies);

        Task Delete(IEnumerable<Supply> supplies);

        Task<List<SupplyType>> GetSupplyTypes(IEnumerable<int> ids = null);

        Task<List<SupplyType>> GetSupplyTypes(SupplyCategory category = SupplyCategory.All);
        
        Task<long?> GetDefaultStoreId(int builderId);
    }
}
