﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ISystemAlertRepository
    {
        Task<IList<SystemAlert>> GetAlerts(IEnumerable<Guid> alertIds);

        Task Add(SystemAlert systemAlert);

        Task AddRange(IEnumerable<SystemAlert> systemAlerts);

        Task<IList<SystemAlert>> SearchAlerts(AlertSearchCriteria searchCriteria);

        Task Update(params SystemAlert[] systemAlerts);

        Task DeleteAlerts(params SystemAlert[] systemAlerts);
    }
}