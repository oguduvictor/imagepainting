﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IPaintStoreRepository
    {
        Task<List<Store>> GetStores(bool isSummary = false, bool includeLocation = false, bool includeDeleted = false);

        Task<Store> GetStore(long id, bool includeLocations = false);

        Task<StoreLocation> GetStoreLocation(long id);

        Task<IEnumerable<StoreLocation>> GetStoreLocations(long? storeId, bool includeDeleted = false);

        Task<Store> CreateStore(Store store);

        Task<StoreLocation> SaveStoreLocation(StoreLocation location);

        Task<Store> UpdateStore(Store store, StoreLocation location = null);

        Task UpdateLocation(StoreLocation location);
    }
}
