﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IWorkOrderRepository
    {
        ExtraJobInfo GetExtraJobInfo(long id);

        Task<ExtraJobInfo> GetExtraJobInfoByWorkOrderId(long workOrderId);

        JobInfo GetJobInfo(long id);

        Task<ExtraJobInfo> CreateExtraJobInfo(long workorderId, ExtraJobInfo extraJobInfo);

        Task<JobInfo> CreateJobInfo(long workorderId, JobInfo jobInfo, string modifiedById);

        Task<WorkOrder> CreateWorkOrder(WorkOrder workOrder);

        Task<WorkOrder> GetWorkOrder(
            long workOrderId, 
            bool includeBuilder = false, 
            bool includeCommunities = false, 
            bool includeJobInfo = false, 
            bool includeExtraJobInfo = false,
            bool includeCreatedBy = false);

        Task<IEnumerable<WorkOrder>> GetWorkOrders(WorkOrderQuery summary);

        Task<PagedResult<WorkOrderView>> SearchWorkOrdersView(WorkOrderQuery workOrderQuery);

        Task<ExtraJobInfo> UpdateExtraJobInfo(ExtraJobInfo extraJobInfo, long? workOrderId = null);

        Task<JobInfo> UpdateJobInfo(JobInfo jobInfo, long? workOrderId = null, string modifiedById = null);

        Task<WorkOrder> UpdateWorkOrder(WorkOrder workOrder);

        Task<long> GetTotalWorkOrderCount(string searchString = "");

        Task<List<string>> GetSuperintendents();

        Task<List<string>> GetCustomerReps();

        Task<bool> WorkOrderExists(long workOrderId, string name);
    }
}