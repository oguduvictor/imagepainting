﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IUnAssignedCrewSettingRepository
    {
        Task AddUnAssignedCrewSettings(params UnAssignedCrewSetting[] unAssignedCrewSettings);

        Task<IList<UnAssignedCrewSetting>> GetSettings(DateTimeRange dateTimeRange, IEnumerable<string> crewIds = null);

        Task UpdateSetting(params UnAssignedCrewSetting[] unAssignedCrew);

        Task<UnAssignedCrewSetting> GetSetting(DateTime date, string crewId);
    }
}
