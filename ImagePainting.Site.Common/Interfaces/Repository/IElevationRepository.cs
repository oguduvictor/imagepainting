﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IElevationRepository
    {
        Task<Elevation> GetElevation(int elevationId);

        Task<Elevation> CreateElevation(Elevation elevation);

        Task<Elevation> UpdateElevation(Elevation elevation);

        Task<List<Elevation>> GetElevationsSummary(int? planId = null, bool includeDeleted = false);
    }
}
