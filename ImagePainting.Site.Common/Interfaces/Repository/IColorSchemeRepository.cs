﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IColorSchemeRepository
    {
        #region InteriorColorScheme

        Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemes();

        Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemeSummary(string colorCodePrefix = null);


        Task<InteriorColorScheme> GetInteriorColorScheme(int id);

        Task<bool> DeleteInteriorColorScheme(int id);

        Task<InteriorColorScheme> CreateInteriorColorScheme(InteriorColorScheme interiorColorScheme);

        Task<InteriorColorScheme> UpdateInteriorColorScheme(InteriorColorScheme interiorColorScheme);

        #endregion

        #region ExteriorColorScheme
        Task<IEnumerable<ExteriorColorScheme>> GetExteriorColorSchemes();

        Task<IEnumerable<ExteriorColorScheme>> GetExeteriorColorSchemeSummary(string colorCodePrefix = null);

        Task<ExteriorColorScheme> GetExteriorColorScheme(int id);

        Task<bool> DeleteExteriorColorScheme(int id);

        Task<ExteriorColorScheme> CreateExteriorColorScheme(ExteriorColorScheme exteriorColorScheme);

        Task<ExteriorColorScheme> UpdateExteriorColorScheme(ExteriorColorScheme exteriorColorScheme);

        #endregion
    }
}
