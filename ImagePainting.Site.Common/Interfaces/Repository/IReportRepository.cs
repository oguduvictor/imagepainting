﻿using ImagePainting.Site.Common.Models.Reports;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IReportRepository
    {
        Task<ProjectedLaborReport> GetProjectedLaborReport(ProjectedLaborReportQuery query);

        Task<ProjectedRevenueReport> GetProjectedRevenueReport(ProjectedRevenueReportQuery query);

        Task<IEnumerable<PurchaseOrderReportItem>> GetPurchaseReport(PurchaseReportQuery query);

        Task<IEnumerable<KeyValuePair<int, string>>> GetLotBlockOptions(int? communityId = null);

        Task<TasksReport> GetTasksReport(TasksReportQuery queryOptions);
    }
}