﻿using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ITaskRepository
    {
        Task CreateWorkOrderTasks(params WorkOrderTask[] tasks);

        Task DeleteWorkOrderTasks(params WorkOrderTask[] tasks);
            
        Task<List<TaskSummary>> GetTaskSummaries(long workOrderId, bool mainTaskOnly = false, string userId = null);

        Task<WorkOrderTask> GetTask(long taskId, bool includeCrew = false, bool includeWorkOrder = false, bool includeSubTasks = false, 
            bool includeCompletedBy = false, bool includBuilderAndCommunity = false, bool includeUnlockedBy = false);

        Task<WorkOrderTask> GetMainTask(long taskId);

        Task<IEnumerable<TaskSummary>> GetMainTasksForUser(long workOrderId, string user);

        Task<List<WorkOrderTask>> GetTasksByWorkOrderId(long workOrderId, bool mainTaskOnly, bool includeWalkTasks = false, bool includeWorkOrder = false);

        Task<IEnumerable<WorkOrderTask>>  GetTasks(TaskSearchCriteria searchCriteria);

        Task<IList<TaskView>> SearchTasks(TaskSearchCriteria searchCriteria);

        Task UpdateWorkOrderTasks(params WorkOrderTask[] tasks);

        Task<IEnumerable<KeyValuePair<DateTime, string>>> GetAssignedUserIds(DateTimeRange dateTimeRange);

        Task<List<long>> GetUnCompletedTaskIdsBySchemaId(int schemaId);

        Task<List<long>> GetTaskIdsByWorkOrderId(long workOrderId);
    }
}
