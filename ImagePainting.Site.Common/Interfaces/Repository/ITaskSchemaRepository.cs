﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ITaskSchemaRepository
    {
        Task<WorkOrderTaskSchema> CreateTaskSchema(WorkOrderTaskSchema schema);

        Task<WorkOrderTaskSchema> GetTaskSchema(int schemaId);

        Task<List<WorkOrderTaskSchema>> GetTaskSchemas(bool isSummary = true, bool mainTaskOnly = false);

        Task<WorkOrderTaskSchema> UpdateTaskSchema(WorkOrderTaskSchema schema);
    }
}