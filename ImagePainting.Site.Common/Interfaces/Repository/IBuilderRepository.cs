﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface IBuilderRepository
    {
        Task<Builder> GetBuilder(int builderId, bool includeCommunities = false, bool includeCosts = false, bool includePlans = false);

        Task<List<Builder>> GetBuilders(List<int> builderIds);

        Task<List<Builder>> GetBuilders(bool isSummary = true, bool includeCommunities = false, bool includeCosts = false);

        Task UpdateBuilderCosts(Builder builder);

        Task<Builder> SaveBuilder(Builder builder);

        Task<Builder> UpdateBuilder(Builder builder);

        void AddCosts(IEnumerable<Cost> costs, Builder builder);
        
        Task<List<State>> GetStates();
    }
}