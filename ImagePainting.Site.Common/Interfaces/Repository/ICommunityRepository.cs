﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ICommunityRepository
    {
        Task<Community> GetCommunity(int communityId, bool includeBuilders = false);

        Task<List<Community>> GetCommunities(bool isSummary = true, bool includeBuilders = false, long? builderId = null);

        Task<Community> CreateCommunity(Community community);

        Task<Community> UpdateCommunity(Community community);
    }
}
