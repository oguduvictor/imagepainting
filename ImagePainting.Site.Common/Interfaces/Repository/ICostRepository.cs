﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Common.Interfaces.Repository
{
    public interface ICostRepository
    {
        Task<Cost> CreateCost(Cost cost);

        Task UpdateCost(Cost cost);

        Task<CostItem> CreateCostItem(CostItem costItem);

        Task<Cost> GetCost(int id, bool includeBuilder = false, bool includeCostItem = false);

        Task DeleteCost(int id);

        Task DeleteCostItem(int id);

        Task<CostItem> GetCostItemByName(string costItemName);

        Task<CostItem> GetCostItem(int id);

        Task<List<CostItem>> GetCostItems(bool? isExtra = null);

        Task<List<Cost>> GetCosts(int builderId, bool? isExtra = false);

        Task UpdateCostItems(params CostItem[] costItems);
    }
}