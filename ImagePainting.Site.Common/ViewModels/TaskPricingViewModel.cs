﻿namespace ImagePainting.Site.Common.ViewModels
{
    public class TaskPricingViewModel
    {
        public string Name { get; set; }

        public int Quantity { get; set; }

        public decimal Cost { get; set; }

        public bool IsLumpSum { get; set; }

        public string WorkOrderName { get; set; }

        public decimal Total
        {
            get
            {
                return IsLumpSum ? Cost : Cost * Quantity;
            }
        }

        
    }
}
