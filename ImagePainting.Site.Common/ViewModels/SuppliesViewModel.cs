﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class SuppliesViewModel
    {
        public int BuilderId { get; set; }

        public string BuilderName { get; set; }

        [Required]
        public long PaintStoreId { get; set; }

        public IEnumerable<Supply> Supplies { get; set; }
    }
}
