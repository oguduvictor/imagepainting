﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.ViewModels
{
    public class WorkOrderItemViewModel<T>
    {
        public int? CommunityId = null;

        public int? BuilderId = null;

        public string BuilderAbbrevation { get; set; }

        public bool WorkOrderCompleted { get; set; }

        public IEnumerable<TaskSummary> TaskSummaries { get; set; }

        public T Item { get; set; }
    }
}
