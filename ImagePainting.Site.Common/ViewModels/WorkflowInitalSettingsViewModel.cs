﻿using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.ViewModels
{
    public class WorkflowInitalSettingsViewModel
    {
        [Required(ErrorMessage ="The Builder field is required")]
        public int? BuilderId { get; set; }

        [Required]
        public int? CommunityId { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        [Required]
        public int? Lot { get; set; }

        [Required]
        public string Block { get; set; }

        [StringLength(50)]
        public string SuperIntendent { get; set; }

        [StringLength(50)]
        public string CustomerRep { get; set; }

        [StringLength(50)]
        public string CustomerName { get; set; }

        [Required]
        public string Name { get; set; }

        public long WorkOrderId { get; set; }

        [StringLength(16)]
        public string CustomerPhoneNumber { get; set; }

        [StringLength(128)]
        public string CustomerEmail { get; set; }
    }
}