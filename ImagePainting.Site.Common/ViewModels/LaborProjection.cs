﻿using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.ViewModels
{
    public class LaborProjection
    {
        public IList<TaskPricingViewModel> BasePricings { get; set; }

        public IList<TaskPricingViewModel> ExtraPricings { get; set; }

        public decimal BaseTotal => BasePricings.Sum(x => x.Total);

        public decimal ExtraTotal => ExtraPricings.Sum(x => x.Total);

        public decimal GrandTotal => BaseTotal + ExtraTotal;
    }
}
