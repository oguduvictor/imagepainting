﻿using ImagePainting.Site.Common.Models;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.ViewModels
{
    public class PaintStoreViewModel
    {
        public long StoreId { get; set; }

        public bool Active { get; set; }

        [Required, StringLength(50)]
        [Display(Name = "Name", Prompt = "Enter Name")]
        public string StoreName { get; set; }

        public long LocationId { get; set; }

        [StringLength(50)]
        [Display(Name = "Location ID")]
        public string StoreLocationId { get; set; }

        [Required, StringLength(50)]
        [Display(Name = "Address", Prompt = "Enter Address")]
        public string StoreAddress { get; set; }

        [Required, StringLength(5)]
        [Display(Name = "Zip Code", Prompt = "Enter Zip Code")]
        public string StoreZipCode { get; set; }

        [Display(Name = "City", Prompt = "Enter City")]
        [Required, StringLength(50)]
        public string StoreCity { get; set; }

        [StringLength(256), Display(Name = "Contact")]
        public string StoreContactName { get; set; }

        [StringLength(16), Display(Name = "Phone Number")]
        public string StoreContactPhone { get; set; }

        [Required, EmailAddress, StringLength(128), Display(Name = "Email")]
        public string StoreContactEmail { get; set; }

        public StoreLocation MapToLocation()
        {
            return new StoreLocation
            {
                Id = LocationId,
                LocationId = StoreLocationId,
                StoreId = StoreId,
                StoreAddress = StoreAddress,
                StoreZipCode = StoreZipCode,
                StoreCity = StoreCity,
                StoreContactName = StoreContactName,
                StoreContactPhone = StoreContactPhone,
                StoreContactEmail = StoreContactEmail,
            };
        }

        public void MapToModel(StoreLocation storeLocation)
        {
            StoreLocationId = storeLocation.LocationId;
            StoreAddress = storeLocation.StoreAddress;
            StoreZipCode = storeLocation.StoreZipCode;
            StoreCity = storeLocation.StoreCity;
            StoreContactName = storeLocation.StoreContactName;
            StoreContactPhone = storeLocation.StoreContactPhone;
            StoreContactEmail = storeLocation.StoreContactEmail;
        }
    }
}
