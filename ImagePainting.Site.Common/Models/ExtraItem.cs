﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models
{
    public class ExtraItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal BuilderCost { get; set; }

        public decimal LaborCost { get; set; }

        public int Quantity { get; set; }

        public int? WalkQuantity { get; set; }

        public TaskType? TaskType { get; set; }

        public bool IsLumpSum { get; set; }

        public List<long> TaskIds { get; set; }

        public decimal TotalLabor
        {
            get
            {
                return IsLumpSum ? LaborCost : (WalkQuantity ?? 0) * LaborCost;
            }
        }

        public decimal TotalRevenue
        {
            get
            {
                return IsLumpSum ? BuilderCost : BuilderCost * Quantity;
            }
        }
    }
}
