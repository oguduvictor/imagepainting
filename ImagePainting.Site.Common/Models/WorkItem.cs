﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class WorkItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int WalkQuantity { get; set; }

        public decimal Cost { get; set; }

        public bool Selected { get; set; }

        public bool IsLumpSum { get; set; }

        public decimal TotoalPrice => IsLumpSum ? Price : Price * WalkQuantity;
    }
}
