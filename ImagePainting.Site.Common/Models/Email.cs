﻿namespace ImagePainting.Site.Common.Models
{
    public class Email
    {
        public enum TemplateType
        {
            None,
            TaskSchedule,
            TaskCompletion,
            RevenueProjection,
            PurchaseOrder,
            PassswordReset,
            NoTaskAssignment
        }

        public string Subject { get; set; }

        // provided by email service
        ////public string Sender { get; set; }

        public string[] Recipients { get; set; }

        public string[] Bcc { get; set; }

        public string[] CC { get; set; }

        public TemplateType EmailTemplateType { get; set; }

        public object Content { get; set; }
    }
}
