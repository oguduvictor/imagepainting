﻿using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Models
{
    public class UserInfoSummary
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public UserRole[] Roles { get; set; }
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }
    }
}
