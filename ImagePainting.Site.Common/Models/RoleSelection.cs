﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Models
{
    public class RoleSelection
    {
        public UserRole Role { get; set; }

        public bool Selected { get; set; }
        
    }
}
