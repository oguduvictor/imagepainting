﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class WorkOrderView
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int? Lot { get; set; }

        public string Block { get; set; }

        public long? JobInfoId { get; set; }

        public DateTime? Completed { get; set; }

        public DateTime Modified { get; set; }

        public DateTime Created { get; set; }

        public string CreatedById { get; set; }

        public string CreatedBy { get; set; }

        public int? BuilderId { get; set; }

        public string BuilderName { get; set; }

        public string BuilderAbbreviation { get; set; }

        public int? CommunityId { get; set; }

        public string CommunityName { get; set; }

        public string CommunityAbbreviation { get; set; }
    }
}
