﻿using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models
{
    public class PagedResult<T>
        where T : class, new()
    {
        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public int TotalCount { get; set; }

        public IList<T> Items { get; set; }
    }
}
