﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models
{
    public class AlertSearchCriteria
    {
        public Guid? TargetObjectId { get; set; }

        public IList<AlertType> AlertTypes { get; set; }

        public IList<long> ParentIds { get; set; }

        public AlertParentType? ParentType { get; set; }

        public bool? Resolved { get; set; }
    }
}
