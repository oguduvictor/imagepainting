﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Models
{
    public class SupplyTypeMap
    {
        public string Color { get; set; }
        public int Quantity { get; set; }
        public decimal Cost { get; set; }
        public string Name { get; set; }
        public string SupplyType { get; set; }
        public SupplyCategory SupplyCategory { get; set; }
    }
}
