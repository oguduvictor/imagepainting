﻿using ImagePainting.Site.Common.Helpers;
using System;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Models
{
    public class MaterialProjection
    {
        public WorkOrderTask WorkOrderTask { private get; set; }

        private const double TaxRate = 0.07;
        private const double PricePerGallon = 20;

        public string Crew
        {
            get
            {
                if (WorkOrderTask.Crew.IsNull())
                {
                    return null;
                }

                return WorkOrderTask.Crew.FirstName;
            }
        }

        public string Task
        {
            get
            {
                return WorkOrderTask.Name;
            }
        }

        private decimal TotalHouseSize
        {
            get
            {
                var firstFloor = WorkOrderTask.WorkOrder.JobInfo.FirstFloorSqft;
                var secondFloor = WorkOrderTask.WorkOrder.JobInfo.FirstFloorSqft;
                var garage = WorkOrderTask.WorkOrder.JobInfo.GarageSqft;
                var porch = WorkOrderTask.WorkOrder.JobInfo.LanaiSqft;
                return firstFloor + secondFloor + garage + porch;
            }
        }

        public decimal PaintQuantity
        {
            get
            {
                switch (WorkOrderTask.TaskType)
                {
                    case TaskType.PreCarpet:
                        return 10;
                    case TaskType.AfterCarpet:
                        return 5;
                    case TaskType.QCI:
                    case TaskType.HO:
                        return 0;
                    default:
                        return Math.Round((TotalHouseSize / 125) * 2);
                }
            }
        }

        public double MiscCost
        {
            get
            {
                switch (WorkOrderTask.TaskType)
                {
                    case TaskType.PreCarpet:
                    case TaskType.AfterCarpet:
                        return 50;
                    case TaskType.QCI:
                    case TaskType.HO:
                        return 25;
                    default:
                        return 0.05 * (double)TotalHouseSize;
                }
            }
        }

        public decimal PaintCost
        {
            get
            {
                return (decimal)PricePerGallon * PaintQuantity;
            }
        }

        public double Tax
        {
            get
            {
                return TaxRate * (MiscCost + (double)PaintCost);
            }
        }

        public double TotalCost
        {
            get
            {
                return MiscCost + (double)PaintCost + Tax;
            }
        }
    }
}