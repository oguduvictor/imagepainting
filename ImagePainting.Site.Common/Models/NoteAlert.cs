﻿namespace ImagePainting.Site.Common.Models
{
    public class NoteAlert
    {
        public NamedId<long> WorkOrder { get; set; }

        public NamedId<long> WorkOrderTask { get; set; }

        public Note Note { get; set; }
    }
}
