﻿using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class InteriorColorScheme
    {
        public int Id { get; set; }
        
        [Required, StringLength(50)]
        public string InteriorBase { get; set; }

        [Required, StringLength(50)]
        public string ColorCode { get; set; }

        [StringLength(50)]
        public string FrontDoor { get; set; }

        [StringLength(50)]
        public string Stain { get; set; }

        public string Note { get; set; }
        
        [StringLength(50)]
        public string InteriorCeiling { get; set; }

        [StringLength(50)]
        public string InteriorWoodTrimBaseCasing { get; set; } // map to colum

        [StringLength(50)]
        public string InteriorDoors { get; set; }

        [StringLength(50)]
        public string AccentWall1 { get; set; }

        [StringLength(50)]
        public string AccentWall2 { get; set; }

        [StringLength(50)]
        public string AccentWall3 { get; set; }

        [StringLength(50)]
        public string AccentRoom1 { get; set; }

        [StringLength(50)]
        public string AccentRoom2 { get; set; }

        [StringLength(50)]
        public string AccentRoom3 { get; set; }

        [StringLength(50)]
        public string Crown { get; set; }

        [StringLength(50)]
        public string Beams { get; set; }

        [StringLength(50)]
        public string Rail { get; set; }

        [StringLength(50)]
        public string NewelPosts { get; set; }

        [StringLength(50)]
        public string Treads { get; set; }

        [StringLength(50)]
        public string Risers { get; set; }
    }
}
