﻿using ImagePainting.Site.Common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ImagePainting.Site.Common.Models
{
    public class PurchaseOrder
    {
        private decimal? _total;

        public PurchaseOrder()
        {
            PurchaseOrderItems = new List<PurchaseOrderItem>();
        }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public long Id { get; set; }
        
        public string PoNumber
        {
            get
            {
                return Id.ToString().PadLeft(7 ,'0');
            }
        }

        [Required, StringLength(250)]
        public string Email { get; set; }
        
        [StringLength(5000)]
        public string Instructions { get; set; }

        [StringLength(256)]
        public string Address { get; set; }

        [StringLength(50)]
        public string DeliveryInstructions { get; set; }

        public DateTime? EmailSent { get; set; }

        [Required]
        public decimal Total
        {
            get
            {
                if (!PurchaseOrderItems.IsNullOrEmpty())
                {
                    return PurchaseOrderItems.Sum(x => x.UnitPrice * x.Quantity);
                }

                return _total ?? 0;
            }

            set
            {
                _total = value;
            }
        }        

        public virtual StoreLocation PaintStoreLocation { get; set; }

        [Required]
        public long PaintStoreLocationId { get; set; }

        public virtual List<PurchaseOrderItem> PurchaseOrderItems { get; set; }

        public virtual WorkOrderTask Task { get; set; }

        [Required]
        public long TaskId { get; set; }

        public virtual WorkOrder WorkOrder { get; set; }

        public long WorkOrderId { get; set; }
    }
}
