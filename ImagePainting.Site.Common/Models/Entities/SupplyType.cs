﻿using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Models
{
    public class SupplyType
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public SupplyCategory SupplyCategory { get; set; }
    }
}
