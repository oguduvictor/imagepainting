﻿using System;
using ImagePainting.Site.Common.Helpers;

namespace ImagePainting.Site.Common.Models
{
    public class ScheduleDateRange
    {
        public ScheduleDateRange(DateTime beginDate, DateTime? endDate = null)
        {
            BeginDate = beginDate;

            if (!endDate.HasValue || endDate.Value.IsDefaultTime())
            {
                return;
            }

            if (endDate.Value.TimeOfDay < beginDate.TimeOfDay)
            {
                throw new InvalidOperationException("End datetime cannot be earlier than the begin datetime");
            }

            EndDate = endDate;
        }

        public ScheduleDateRange(ScheduleDateRange dateRange)
        {
            BeginDate = dateRange.BeginDate;
            EndDate = dateRange.EndDate;
        }

        public ScheduleDateRange()
        {
        }

        public DateTime BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string GetDateRangeString()
        {
            string dateTimeString =  BeginDate.ToString("MMM dd yyyy");
            
            if (!BeginDate.IsDefaultTime() && !IsTimeValid(EndDate))
            {
                dateTimeString = $"{BeginDate.ToString("MMM dd yyyy ")} at { BeginDate.ToShortTimeString() }";
            }

            if (!BeginDate.IsDefaultTime() && IsTimeValid(EndDate))
            {
                dateTimeString = $"{BeginDate.ToString("MMM dd yyyy")} between {BeginDate.ToShortTimeString()} and {EndDate.Value.ToShortTimeString()}";
            }
            return dateTimeString;
        }

        private bool IsTimeValid(DateTime? date)
        {
            return !date.IsNull() && !date.Value.IsDefaultTime();
        }

        public DateTime GetDate()
        {
            return BeginDate.Date;
        }

        public void SetDate(DateTime date)
        {
            BeginDate = new DateTime(date.Year, date.Month, date.Day, BeginDate.Hour, BeginDate.Minute, BeginDate.Second);

            if (EndDate.HasValue)
            {
                var endDate = EndDate.Value;
                EndDate = new DateTime(date.Year, date.Month, date.Day, endDate.Hour, endDate.Minute, endDate.Second);
            }
        }

        public void SetTime(ScheduleDateRange dateRange)
        {
            var beginDateTime = dateRange.BeginDate;
            var endDateTime = dateRange.EndDate;

            BeginDate = new DateTime(BeginDate.Year, BeginDate.Month, BeginDate.Day, 
                            beginDateTime.Hour, beginDateTime.Minute, beginDateTime.Second);

            if (!endDateTime.HasValue || endDateTime.Value.IsDefaultTime())
            {
                EndDate = null;
                return;
            }

            if (!EndDate.HasValue)
            {
                EndDate = BeginDate.Date;
            }

            var endDate = EndDate.Value;

            EndDate = new DateTime(endDate.Year, endDate.Month, endDate.Day,
                        endDateTime.Value.Hour, endDateTime.Value.Minute, endDateTime.Value.Second);
        }

        public override bool Equals(object obj)
        {
            var dateRange = obj as ScheduleDateRange;

            if (dateRange == null)
            {
                return false;
            }

            return BeginDate.Date == dateRange.BeginDate.Date
                && BeginDate.Hour == dateRange.BeginDate.Hour
                && BeginDate.Minute == dateRange.BeginDate.Minute
                && BeginDate.Second == dateRange.BeginDate.Second
                && EndDate?.Date == dateRange.EndDate?.Date
                && EndDate?.Hour == dateRange.EndDate?.Hour
                && EndDate?.Minute == dateRange.EndDate?.Minute
                && EndDate?.Second == dateRange.EndDate?.Second;
        }

        public override int GetHashCode()
        {
            var hashCode = 13;

            hashCode += BeginDate.Date.GetHashCode() * 3;
            hashCode += BeginDate.Hour.GetHashCode() * 5;
            hashCode += BeginDate.Minute.GetHashCode() * 7;
            hashCode += BeginDate.Second.GetHashCode() * 11;

            if (EndDate.HasValue)
            {
                hashCode += EndDate.Value.Date.GetHashCode() * 13;
                hashCode += EndDate.Value.Hour.GetHashCode() * 17;
                hashCode += EndDate.Value.Minute.GetHashCode() * 19;
                hashCode += EndDate.Value.Second.GetHashCode() * 23;
            }

            return hashCode;
        }

        public static bool operator ==(ScheduleDateRange a, ScheduleDateRange b)
        {
            return a?.GetHashCode() == b?.GetHashCode();
        }

        public static bool operator !=(ScheduleDateRange a, ScheduleDateRange b)
        {
            return a?.GetHashCode() != b?.GetHashCode();
        }
    }
}
