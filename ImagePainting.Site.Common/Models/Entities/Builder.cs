using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class Builder
    {
        public Builder()
        {
            // set default state to Florida
            State = "FL";

            Communities = new List<Community>();
            Costs = new List<Cost>();
            Plans = new List<Plan>();
        }

        public int Id { get; set; }

        public bool Active { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [StringLength(50)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [Required]
        [StringLength(3)]
        public string Abbreviation { get; set; }

        public string Notes { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public long? DefaultStoreId { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<Community> Communities { get; set; }

        public virtual ICollection<Plan> Plans { get; set; }

        public virtual ICollection<Cost> Costs { get; set; }

        public virtual ICollection<Supply> Supplies { get; set; }

       // public virtual ICollection<Store> Stores { get; set; }

        public void BuilderSummary()
        {
            Communities = new List<Community>();
            Costs = new List<Cost>();
        }
    }
}