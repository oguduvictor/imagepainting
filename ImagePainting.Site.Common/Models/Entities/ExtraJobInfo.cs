﻿using ImagePainting.Site.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.Models
{
    public class ExtraJobInfo
    {
        public long Id { get; set; }
        
        public decimal Total
        {
            get
            {
                if (ExtraItems.IsNullOrEmpty())
                {
                    return 0;
                }

                return ExtraItems.Sum(x => x.BuilderCost * x.Quantity);
            }
        }

        public decimal TotalLaborCost
        {
            get
            {
                if (ExtraItems.IsNullOrEmpty())
                {
                    return 0;
                }

                return ExtraItems.Sum(extra => extra.LaborCost * extra.WalkQuantity ?? 0);
            }
        }
        
        public IEnumerable<ExtraItem> ExtraItems { get; set; }

        public string Note { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }
    }
}