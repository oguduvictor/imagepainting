﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class JobInfo
    {
        public DateTime Created { get; set; }

        [Required, Range(100, 10000000)]
        public decimal FirstFloorSqft { get; set; }

        public decimal GarageSqft { get; set; }

        public long Id { get; set; }

        public decimal LanaiSqft { get; set; }

        public DateTime Modified { get; set; }

        public int? PlanId { get; set; }

        public int? ElevationId { get; set; }

        public decimal SecondFloorSqft { get; set; }

        public decimal LivingSqft
        {
            get
            {
                return FirstFloorSqft + SecondFloorSqft;
            }
        }

        public long? PaintStoreId { get; set; }

        public InteriorColorScheme InteriorColors { get; set; }

        public ExteriorColorScheme ExteriorColors { get; set; }

        public bool Confirmed { get; set; }

        public string Note { get; set; }
        
        public IEnumerable<JobCostItem> JobCostItems { get; set; }

        public decimal TotalSize
        {
            get { return FirstFloorSqft + SecondFloorSqft + GarageSqft + LanaiSqft; }
        }

        public virtual Plan Plan { get; set; }

        public virtual Elevation Elevation { get; set; }
    }
}