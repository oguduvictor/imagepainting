﻿using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class PurchaseOrderItem
    {
        public long Id { get; set; }

        [Required, StringLength(50, ErrorMessage ="Please enter a valid item name")]
        public string Name { get; set; }

        [StringLength(50)]
        public string Color { get; set; }
        

        public decimal UnitPrice { get; set; }


        // Ignored on db
        public decimal Total {
            get
            {
                return UnitPrice * Quantity;
            }
        }

        [Required, Range(0, int.MaxValue, ErrorMessage = "Please enter a valid number")]
        public decimal Quantity { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }

        [Required]
        public long PurchaseOrderId { get; set; }

        [Required, StringLength(50, ErrorMessage = "Please enter a valid supply type")]
        public string SupplyType { get; set; }

    }
}
