﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImagePainting.Site.Common.Models
{
    public class UserInfo
    {
        public UserInfo()
        {
            Roles = new List<Role>();
        }

        public string Id { get; set; }

        [Required]
        public bool Active { get; set; }

        [StringLength(32)]
        [Display(Name = "Federal Id No")]
        public string FederalIdNo { get; set; }

        [StringLength(64)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(32)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [StringLength(32)]
        public string Title { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        [Required]
        [StringLength(32)]
        public string City { get; set; }

        [Required]
        [StringLength(2)]
        public string State { get; set; }

        [StringLength(5)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [StringLength(16)]
        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }

        [StringLength(16)]
        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }

        [StringLength(16)]
        [Required]
        [Display(Name = "Cell Phone")]
        public string CellPhone { get; set; }

        [StringLength(128)]
        public string Email { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime BirthDate { get; set; }

        public string Note { get; set; }

        public DateTime Created { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }
    }
}
