using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public partial class Cost
    {
        public int Id { get; set; }

        [Required]
        public int BuilderId { get; set; }

        [Required]
        public int CostItemId { get; set; }

        public virtual CostItem CostItem { get; set; }

        public virtual Builder Builder { get; set; }

        [Required, Range(0, 999999999.99)]
        public decimal Amount { get; set; }
    }
}
