﻿namespace ImagePainting.Site.Common.Models
{
    public partial class State
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}