﻿using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models
{
    public class Store
    {
        public Store()
        {
            Locations =  new List<StoreLocation>();
        }

        public long  Id { get; set; }

        public bool Active { get; set; }

        public string StoreName { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<StoreLocation> Locations { get; set; }

        //public virtual ICollection<Builder> Builders { get; set; }
    }
}
