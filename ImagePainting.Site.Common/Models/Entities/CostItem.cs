﻿using ImagePainting.Site.Common.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class CostItem
    {
        public int Id { get; set; }

        public bool IsExtra { get; set; }

        public bool IsLumpSum { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        public decimal LaborCost { get; set; }

        public TaskType? TaskType { get; set; }

        [Range(0, 100)]
        public decimal? PercentOfTotalSize { get; set; }

        [Range(0, 100)]
        public decimal? PercentLivingSqft { get; set; }

        [Range(0, 100)]
        public decimal? PercentGarageSqft { get; set; }

        public int Order { get; set; }
    }
}