﻿using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class Supply
    {
        public int Id { get; set; }

        public int BuilderId { get; set; }

        public long PaintStoreId { get; set; }
        
        [Required]
        public int SupplyTypeId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Required, Range(0.01, 9999999.99)]
        public decimal Cost { get; set; }

        public virtual Builder Builder { get; set; }

        public virtual Store PaintStore { get; set; }

        public virtual SupplyType SupplyType { get; set; }
    }
}
