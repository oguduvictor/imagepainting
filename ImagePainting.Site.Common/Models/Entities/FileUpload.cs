﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class FileUpload
    {
        public long Id { get; set; }

        public string Name { get; set; }

        [Required]
        public long OwnerId { get; set; }

        [Required]
        public FileOwnerType OwnerType { get; set; }

        public byte[] Content { get; set; }

        public string ContentType { get; set; }

        public string CreatedById { get; set; }

        public DateTime Created { get; set; }
    }
}
