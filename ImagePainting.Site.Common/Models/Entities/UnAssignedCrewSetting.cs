﻿using System;

namespace ImagePainting.Site.Common.Models.Entities
{
    public class UnAssignedCrewSetting
    {
        public UnAssignedCrewSetting()
        {
        }

        public UnAssignedCrewSetting(string userId, DateTime scheduledDate, bool emailed, bool busy)
        {
            UserId = userId;
            ScheduledDate = scheduledDate;
            Emailed = emailed;
            Busy = busy;
        }

        public string UserId { get; set; }
         
        public DateTime ScheduledDate { get; set; }

        public bool Emailed { get; set; }

        public bool Busy { get; set; }
    }
}
