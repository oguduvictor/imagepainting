﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class Plan
    {
        public Plan()
        {
            Elevations = new List<Elevation>();
            Communities = new List<Community>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public int BuilderId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Stories { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public virtual Builder Builder { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<Community> Communities { get; set; }
        
        public ICollection<Elevation> Elevations { get; set; }
    }
}