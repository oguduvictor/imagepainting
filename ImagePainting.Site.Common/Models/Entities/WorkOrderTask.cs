﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class WorkOrderTask
    {
        public WorkOrderTask()
        {
            Permission = new TaskPermission();
        }

        public long Id { get; set; }

        public long WorkOrderId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public bool IsLumpSum { get; set; }

        public string CrewId { get; set; }

        public int SchemaId { get; set; }

        public ScheduleDateRange ScheduleDateRange { get; set; }

        public bool DateConfirmed { get; set; }
        
        public List<TaskItem> TaskItems { get; set; }
        
        public long? PreWalkTaskId { get; set; }

        public long? CompletionWalkTaskId { get; set; }

        public WorkOrderTaskStatus Status { get; set; }

        public TaskType TaskType { get; set; }

        public DateTime? Completed { get; set; }

        public string CompletedById { get; set; }

        public DateTime? Unlocked { get; set; }

        public string UnlockedById { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public DateTime? EmailSent { get; set; }

        public bool IncludeBase { get; set; }

        public bool Deferred { get; set; }

        public virtual UserInfo Crew { get; set; }

        public virtual WorkOrder WorkOrder { get; set; }

        public virtual WorkOrderTask PreWalkTask { get; set; }

        public virtual WorkOrderTask CompletionWalkTask { get; set; }

        public virtual UserInfo CompletedBy { get; set; }

        public virtual UserInfo UnlockedBy { get; set; }

        /// <summary>
        /// Returns true if the task that this task depends on has been completed
        /// </summary>
        public bool PreceedingTaskCompleted { get; set; }

        /// <summary>
        /// Returns true if the task that depends on this task has been completed
        /// </summary>
        public bool SucceedingTaskCompleted { get; set; }
        
        public bool IsMainTask
        {
            get
            {
                return (PreWalkTaskId.HasValue 
                        || CompletionWalkTaskId.HasValue
                        || !CompletionWalkTask.IsNull()
                        || !PreWalkTask.IsNull());
            }
        }
        
        public bool? OrderPlaced { get; private set; }

        public TaskPermission Permission { get; private set; }

        public void SetOrderPlaced(bool? value)
        {
            OrderPlaced = value;
        }
        
        /// <summary>
        /// Warning: This will set Permission to null and any subsequent calls to Permission will fail.
        /// </summary>
        public void RemovePermission()
        {
            Permission = null;
        }
    }
}
