﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;
using System;

namespace ImagePainting.Site.Common.Models.Entities
{
    public class SystemAlert
    {
        private NoteAlert _note;
        private JobExtraAlert _jobExtra;
        private string _content;

        public Guid Id { get; set; }

        public long ParentId { get; set; }

        public AlertParentType ParentType { get; set; }

        public Guid? TargetObjectId { get; set; }

        public string Content
        {
            get { return _content; }
            set
            {
                _content = value;
                _note = null;
                _jobExtra = null;
            }
        }

        public AlertType AlertType { get; set; }

        public DateTime Created { get; set; }

        public string CreatedById { get; set; }

        public DateTime? Resolved { get; set; }

        public string ResolvedById { get; set; }

        public virtual UserInfo CreatedBy { get; set;}

        public virtual UserInfo ResolvedBy { get; set; }

        public NoteAlert Note
        {
            get
            {
                if (_note != null)
                {
                    return _note;
                }

                if (!Content.IsNullOrEmpty() && AlertType.IsAnyOf(AlertType.TaskNote, AlertType.JobInfoNote, 
                    AlertType.InteriorColorNote, AlertType.ExteriorColorNote, AlertType.JobExtraNote, AlertType.WorkOrderNote) )
                {
                    _note = Content.FromJson<NoteAlert>();
                }

                return _note;
            }
        }

        public JobExtraAlert JobExtra
        {
            get
            {
                if (_jobExtra != null)
                {
                    return _jobExtra;
                }

                if (AlertType == AlertType.JobExtraDifference && !Content.IsNullOrEmpty())
                {
                    _jobExtra = Content.FromJson<JobExtraAlert>();
                }

                return _jobExtra;
            }
        }
    }
}
