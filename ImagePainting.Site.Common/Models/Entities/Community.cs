using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class Community
    {
        public Community()
        {
            Builders = new List<Builder>();
            Plans = new List<Plan>();
        }

        public int Id { get; set; }

        public bool Active { get; set; }

        [Required, StringLength(64)]
        public string Name { get; set; }

        public string Notes { get; set; }

        public DateTime Created { get; set; }
        
        [Required]
        [StringLength(3)]
        public string Abbreviation { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public bool Deleted { get; set; }

        public virtual ICollection<Builder> Builders { get; set; }

        public virtual ICollection<Plan> Plans { get; set; }
        
        public void CommunitySummary()
        {
            Builders = new List<Builder>();
        }
    }
}
