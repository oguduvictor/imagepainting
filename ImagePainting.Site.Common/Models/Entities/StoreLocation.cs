﻿namespace ImagePainting.Site.Common.Models
{
    public class StoreLocation
    {
        public long  Id { get; set; }

        public long StoreId { get; set; }

        public string LocationId { get; set; }

        public string StoreAddress { get; set; }

        public string StoreZipCode { get; set; }

        public string StoreCity { get; set; }

        public string StoreContactName { get; set; }

        public bool Deleted { get; set; }

        public string StoreContactPhone { get; set; }

        public string StoreContactEmail { get; set; }

        public virtual Store Store { get; set; }
        
    }
}
