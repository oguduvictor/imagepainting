﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class WorkOrderTaskSchema
    {
        private IEnumerable<TaskItem> _taskItems;
        private string _taskItemsJson;

        public WorkOrderTaskSchema()
        {
            _taskItems = new List<TaskItem>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public bool IsLumpSum { get; set; }

        [Required(ErrorMessage = "At least one task item is required")]
        public string TaskItemsJson
        {
            get
            {
                if (_taskItemsJson.IsNullOrEmpty() && !_taskItems.IsNullOrEmpty())
                {
                    _taskItemsJson = _taskItems.ToJson();
                }

                return _taskItemsJson;
            }

            set
            {
                _taskItemsJson = value;
                _taskItems = null;
            }
        }

        // Ignore on db
        public IEnumerable<TaskItem> TaskItems
        {
            get
            {
                if (_taskItems.IsNull() && !TaskItemsJson.IsNullOrEmpty())
                {
                    _taskItems = TaskItemsJson.FromJson<List<TaskItem>>();
                }

                return _taskItems;
            }

            set
            {
                _taskItems = value;
                _taskItemsJson = null;
            }
        }

        public int? PreWalkTaskId { get; set; }

        public int? CompletionWalkTaskId { get; set; }

        [Required]
        public TaskType TaskType { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public bool Deleted { get; private set; }

        public bool IsMainTask
        {
            get
            {
                return PreWalkTaskId.HasValue || CompletionWalkTaskId.HasValue;
            }
        }

        public void Delete()
        {
            Deleted = true;
        }
    }
}