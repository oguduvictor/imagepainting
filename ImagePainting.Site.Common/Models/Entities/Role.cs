﻿namespace ImagePainting.Site.Common.Models
{
    public partial class Role
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}