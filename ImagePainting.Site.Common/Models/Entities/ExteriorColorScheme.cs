﻿using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class ExteriorColorScheme
    {
        public int Id { get; set; }

        [Required,StringLength(50)]
        public string ExteriorBase { get; set; }

        [Required, StringLength(50)]
        public string ColorCode { get; set; }
        
        [StringLength(50)]
        public string FrontDoor { get; set; }

        [StringLength(50)]
        public string GarageDoor { get; set; }

        [StringLength(50)]
        public string Stain { get; set; }
        
        [StringLength(50)]
        public string ExteriorTrim { get; set; }

        [StringLength(50)]
        public string Shutters { get; set; }

        [StringLength(50)]
        public string Corbels { get; set; }

        [StringLength(50)]
        public string Brackets { get; set; }

        [StringLength(50)]
        public string HardiSiding { get; set; }

        [StringLength(50)]
        public string ShakeSiding { get; set; }

        [StringLength(50)]
        public string BoardAndBatton { get; set; }

        [StringLength(50)]
        public string Louvre { get; set; }

        [StringLength(50)]
        public string AccentBase { get; set; }

        [StringLength(50)]
        public string Facia { get; set; }

        [StringLength(50)]
        public string Soffit { get; set; }

        public string Note { get; set; }
    }
}
