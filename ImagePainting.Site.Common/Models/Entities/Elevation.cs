﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class Elevation
    {
        public int Id { get; set; }

        [Required]
        public int PlanId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal FirstFloorSqft { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal SecondFloorSqft { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal GarageSqft { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal EntrySqft { get; set; }

        [Range(0.00, 99999999.99)]
        public decimal LanaiSqft { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public bool Deleted { get; set; }

        public virtual Plan Plan { get; set; }
    }
}
