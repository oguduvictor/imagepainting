﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class TaskSchedule
    {
        public long TaskId { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public string CrewId { get; set; } = null;

        public DateTime? ScheduleEndDate { get; set; }
    }
}
