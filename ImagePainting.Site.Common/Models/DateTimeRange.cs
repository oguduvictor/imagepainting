﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class DateTimeRange
    {
        public DateTime BeginDateTime { get; set; }

        public DateTime EndDateTime { get; set; }
    }
}
