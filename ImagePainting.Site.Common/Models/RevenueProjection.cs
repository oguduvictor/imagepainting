﻿using ImagePainting.Site.Common.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.Models
{
    public class RevenueProjection
    {
        public RevenueProjection()
        {
            CostItems = new List<JobCostItem>();
            ExtraItems = new List<ExtraItem>();
        }

        // Base
        public IEnumerable<JobCostItem> CostItems { get; set; }

        public decimal BaseTotal
        {
            get
            {
                if (CostItems.IsNullOrEmpty())
                {
                    return 0;
                }

                return CostItems.Sum(x => x.CalculateTotalRevenue(TotalHouseSqft));
            }
        }

        public decimal TotalHouseSqft { get; set; }

        // Extras
        public IEnumerable<ExtraItem> ExtraItems { get; set; }

        public decimal ExtrasTotal
        {
            get
            {
                if (ExtraItems.IsNullOrEmpty())
                {
                    return 0;
                }

                return ExtraItems.Sum(x => (x.IsLumpSum ? x.BuilderCost : x.BuilderCost * x.Quantity));
            }
        }

        public decimal GrandTotal => ExtrasTotal + BaseTotal;
    }
}
