﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class ProfitProjection
    {
        private decimal _totalPurchaseOrders;
        private decimal _totalLaborProjection;
        private decimal _totalRevenueProjection;

        public decimal TotalRevenueProjection
        {
            get => _totalRevenueProjection;
            set => _totalRevenueProjection = Math.Abs(value);
        }

        public decimal TotalLaborProjection
        {
            get => _totalLaborProjection;
            set => _totalLaborProjection = -Math.Abs(value);
        }

        public decimal TotalPurchaseOrders
        {
            get => _totalPurchaseOrders;
            set => _totalPurchaseOrders = -Math.Abs(value);
        }

        public decimal GrossProfitProjection => TotalRevenueProjection + TotalLaborProjection + TotalPurchaseOrders;
    }
}
