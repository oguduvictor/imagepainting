﻿namespace ImagePainting.Site.Common.Models
{
    public class JobCostItem
    {
        public string Name { get; set; }

        public decimal BuilderCost { get; set; }

        public decimal LaborCost { get; set; }

        public bool IsBase { get; set; }

        public bool IsLumpSum { get; set; }

        public decimal CalculateTotalRevenue(decimal totalHouserSize)
        {
            if (IsLumpSum)
            {
                return BuilderCost;
            }

            return BuilderCost * totalHouserSize;
        }
    }
}
