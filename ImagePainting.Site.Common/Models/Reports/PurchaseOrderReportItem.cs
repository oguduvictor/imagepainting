﻿using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class PurchaseOrderReportItem
    {
        public PurchaseOrder PurchaseOrder { get; set; }

        public KeyValuePair<long, string> WorkOrder { get; set; }

        public KeyValuePair<long, string> Task { get; set; }

        public string TaskType { get; set; }

        public int Lot { get; set; }

        public string Block { get; set; }

        public string Crew { get; set; }

        public KeyValuePair<long, string> Store { get; set; }

        public KeyValuePair<long, string> Builder { get; set; }

        public KeyValuePair<long, string> Community { get; set; }

        public decimal Total { get; set; }

        public DateTime? EmailSent { get; set; }
    }
}
