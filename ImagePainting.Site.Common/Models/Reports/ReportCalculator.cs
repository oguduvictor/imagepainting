﻿using ImagePainting.Site.Common.Helpers;
using System.Linq;

namespace ImagePainting.Site.Common.Models.Reports
{
    public static class ReportCalculator
    {
        public static decimal CalculateRevenue(WorkOrder workOrder)
        {
            var jobInfo = workOrder.JobInfo;
            var jobInfoCost = jobInfo.IsNull() ? 0 : jobInfo.JobCostItems.Sum(x => x.CalculateTotalRevenue(jobInfo.TotalSize));
            var totalExtraJobInfoCost = workOrder.ExtraJobInfo.IsNull() ? 0 : workOrder.ExtraJobInfo.Total;

            return jobInfoCost + totalExtraJobInfoCost;
        }

        public static decimal CalculateLaborAmount(WorkOrderTask task)
        {
            if (task == null)
            {
                return 0;
            }

            if (task.IsLumpSum)
            {
                return task.Amount;
            }

            var jobInfo = task.WorkOrder.JobInfo;

            return jobInfo.IsNull() ? 0 : (task.Amount * (int)jobInfo.TotalSize) + 
                (
                    task.WorkOrder.ExtraJobInfo?.ExtraItems?
                        .Sum(x => (x.TaskType == task.TaskType && x.WalkQuantity > 0 && x.TotalLabor != 0) ?
                                    x.TotalLabor : 0)
                ) ?? 0;
        }
    }
}