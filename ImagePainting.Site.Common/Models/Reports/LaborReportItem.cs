﻿using System;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class LaborReportItem
    {
        public string Task { get; set; }

        public long TaskId { get; set; }

        public string WorkOrder { get; set; }

        public long WorkOrderId { get; set; }

        public int Lot { get; set; }

        public string Block { get; set; }

        public string Crew { get; set; }

        public string Builder { get; set; }

        public int BuilderId { get; set; }

        public string Community { get; set; }

        public int CommunityId { get; set; }

        public DateTime? Completed { get; set; }

        public decimal Amount { get; set; }

        public DateTime? PreWalk { get; set; }

        public DateTime? CompletionWalk { get; set; }
    }
}
