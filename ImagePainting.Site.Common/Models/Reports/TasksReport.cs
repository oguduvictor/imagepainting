﻿using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class TasksReport
    {
        public TasksReport()
        {
            TaskReportItems = new List<TaskReportItem>();
        }
        public IEnumerable<TaskReportItem> TaskReportItems { get; set; }
    }
}
