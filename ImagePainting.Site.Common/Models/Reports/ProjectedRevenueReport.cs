﻿using ImagePainting.Site.Common.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class ProjectedRevenueReport
    {
        public ProjectedRevenueReport()
        {
            RevenueReportItems = new List<RevenueReportItem>();
        }

        public IEnumerable<RevenueReportItem> RevenueReportItems { get; set; }

        public decimal TotalRevenue
        {
            get
            {
                if (RevenueReportItems.IsNullOrEmpty())
                    return 0;

                return RevenueReportItems.Sum(x => x.TotalRevenue);
            }
        }
    }
}