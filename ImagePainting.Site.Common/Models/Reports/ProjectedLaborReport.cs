﻿using ImagePainting.Site.Common.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class ProjectedLaborReport
    {
        public ProjectedLaborReport()
        {
            LaborReportItems = new List<LaborReportItem>();
        }

        public IEnumerable<LaborReportItem> LaborReportItems { get; set; }

        public decimal TotalAmount
        {
            get
            {
                if (LaborReportItems.IsNullOrEmpty())
                    return 0;

                return LaborReportItems.Sum(x => x.Amount);
            }
        }
    }
}