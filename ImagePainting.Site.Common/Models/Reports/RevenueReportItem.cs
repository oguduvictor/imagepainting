﻿using System;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class RevenueReportItem
    {
        public string Block { get; set; }

        public string Builder { get; set; }

        public string Community { get; set; }

        public DateTime? Completed { get; set; }

        public int Lot { get; set; }

        public decimal TotalRevenue { get; set; }

        public long WorkOrderId { get; set; }

        public string WorkOrder { get; set; }
    }
}
