﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Models.Reports
{
    public class PurchaseReportQuery
    {
        private DateTimeRange _dateTimeRange;

        public DateTimeRange DateRange
        {
            get
            {
                if (_dateTimeRange.IsNull())
                {
                    return null;
                }

                return _dateTimeRange.ToUTCDateTimeRange();
            }
            set
            {
                _dateTimeRange = value;
                _dateTimeRange.BeginDateTime = DateTime.SpecifyKind(_dateTimeRange.BeginDateTime, DateTimeKind.Unspecified);
                _dateTimeRange.EndDateTime = DateTime.SpecifyKind(_dateTimeRange.EndDateTime, DateTimeKind.Unspecified);
            }
        }

        public long? StoreId { get; set; }

        public int? BuilderId { get; set; }

        public int? CommunityId { get; set; }
        
        public string WorkOrder { get; set; }

        public string PONumber { get; set; }

        public int? PlanId { get; set; }

        public IEnumerable<string> CrewIds { get; set; }

        public TaskType MainTaskType { get; set; }
    }
}
