﻿namespace ImagePainting.Site.Common.Models
{
    public class PurchaseOrderSummary
    {
        public long Id { get; set; }

        public long TaskId { get; set; }

        public string DeliveryInstructions { get; set; }

        public string PoNumber
        {
            get
            {
                return Id.ToString().PadLeft(7, '0');
            }
        }
    }
}
