﻿using ImagePainting.Site.Common.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace ImagePainting.Site.Common.Models
{
    public class TaskSummary
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Amount { get; set; }

        public DateTime? Completed { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public TaskType TaskType { get; set; }

        public bool Deferred { get; set; }
    }
}
