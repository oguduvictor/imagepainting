﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class ScheduleWorkOrderTask
    {
        public int SchemaId { get; set; }

        public string TaskName { get; set; }

        public long TaskId { get; set; }

        public bool Selected { get; set; }

        public DateTime? Date { get; set; }

        public string UserId { get; set; }

        public bool Confirmed { get; set; }

        public bool Completed { get; set; }

        public TaskPermission Permission { get; set; }

        public bool CantUnselect { get; set; }
    }
}
