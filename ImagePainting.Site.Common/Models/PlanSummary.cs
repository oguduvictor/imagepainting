﻿namespace ImagePainting.Site.Common.Models
{
    public class PlanSummary
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Builder Builder { get; set; }

        public int Stories { get; set; }
    }
}
