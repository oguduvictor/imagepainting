﻿namespace ImagePainting.Site.Common.Models
{
    public class JobExtraAlert
    {
        public NamedId<long> WorkOrder { get; set; }

        public ExtraItem ExtraItem { get; set; }
    }
}
