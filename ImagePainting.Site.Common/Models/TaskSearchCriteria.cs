﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.Models
{
    public class TaskSearchCriteria
    {
        public bool? IsCompleted { get; set; }

        public bool? IsOverdued { get; set; }

        public bool? IsUpcoming { get; set; }

        public int PageNumber { get; set; } = 0;
        
        public string UserId { get; set; }

        public bool? IncludeCrew { get; set; }

        public bool? IncludeWorkOrder { get; set; }
        
        public IEnumerable<long> TaskIds { get; set; }

        public bool? IncludeBuilder { get; set; }

        public bool? IncludeJobInfo { get; set; }

        public bool? IncludeCommunity { get; set; }

        public bool OrderByAscending { get; set; }

        public long? WorkOrderId { get; set; }

        public bool? IsMainTask { get; set; }

        public bool IsDeferred { get; set; }

        public IEnumerable<int> CommunityIds { get; set; }

        public IEnumerable<string> CrewIds { get; set; }
        
        public DateTimeRange DateRange { get; set; }

        public IEnumerable<long> TaskSchemaIds { get; set; }

        public string WorkOrderName { get; set; }

        // Warning: Let the default remain None to avoid unintended consequencies
        public GroupingType Grouping { get; set; }
    }
}
