﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class TaskView
    {
        public TaskView()
        {
            Permission = new TaskPermission();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public long WorkOrderId { get; set; }

        public string WorkOrderName { get; set; }

        public string WorkOrderAddress { get; set; }

        public DateTime? WorkOrderCompleted { get; set; }

        public bool JobInfoConfirmed { get; set; }

        public int CommunityId { get; set; }

        public int BuilderId { get; set; }

        public int SchemaId { get; set; }

        public DateTime ScheduleDate { get; set; }

        public DateTime? ScheduleDateEnd { get; set; }

        public DateTime? EmailSent { get; set; }

        public DateTime? Completed { get; set; }

        public string CrewId { get; set; }

        public string Crew { get; set; }

        public long? PurchaseOrderId { get; set; }

        public string DeliveryInstructions { get; set; }

        public bool IsReady { get; set; }

        public bool IsMainTask { get; set; }

        public TaskPermission Permission { get; private set; }
    }
}
