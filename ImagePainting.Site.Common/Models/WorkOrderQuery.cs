﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Models
{
    public class WorkOrderQuery
    {
        private (int? lot, string block)? LotAndBlock
        {
            get
            {
                if (SearchOption == WorkOrderSearchOption.LotAndBlock)
                {
                    return SearchString.BreakTextIntoLotAndBlock();
                }

                return null;
            }
        }

        public bool ByAddress { get; set; }
        public bool ByBuilder { get; set; } = true;
        public bool ByCommunity { get; set; } = false;
        public bool ByLastModified { get; set; } = false;
        public bool ByName { get; set; } = false;
        public bool ByStatus { get; set; } = false;
        public bool ByCreated { get; set; } = false;
        public bool IsAscending { get; set; } = false;
        public int ResultsCount { get; set; } = 50;
        public string SearchString { get; set; } = string.Empty;
        public int PageNumber { get; set; } = 0;
        public WorkOrderSearchOption SearchOption { get; set; }
        public int? Lot
        {
            get
            {
                return LotAndBlock?.lot;
            }
        }

        public string Block
        {
            get
            {
                return LotAndBlock?.block;
            }
        }
    }
}
