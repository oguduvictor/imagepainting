﻿namespace ImagePainting.Site.Common.Models
{
    public class NamedId<IdType>
    {
        public IdType Id { get; set; }

        public string Name { get; set; }

        public NamedId() { }

        public NamedId(IdType id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
