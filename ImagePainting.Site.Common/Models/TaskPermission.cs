﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class TaskPermission
    {
        private bool? _canView;
        private bool? _canSave;
        private bool? _canComplete;
        private bool? _canUnlock;
        private bool? _canAddNewItem;
        private bool? _canAddNote;
        private bool? _canChangeSchedule;
        private bool? _isTaskReady;
        private bool? _canCreatePurchaseOrder;
        private bool? _canCreateDuplicateTask;
        private bool? _canSignDocument;
        private bool? _canDefer;
        private bool? _canSaveEmailNote;

        public bool CanSave
        {
            get
            {
                return GetValue(_canSave, nameof(CanSave));
            }
        }

        public bool CanView
        {
            get
            {
                return GetValue(_canView, nameof(CanView));
            }
        }

        public bool CanComplete
        {
            get
            {
                return GetValue(_canComplete, nameof(CanComplete));
            }
        }

        public bool CanUnlock
        {
            get
            {
                return GetValue(_canUnlock, nameof(CanUnlock));
            }
        }

        public bool CanAddNewItem
        {
            get
            {
                return GetValue(_canAddNewItem, nameof(CanAddNewItem));
            }
        }

        public bool CanAddNote
        {
            get
            {
                return GetValue(_canAddNote, nameof(CanAddNote));
            }
        }

        public bool CanChangeSchedule
        {
            get
            {
                return GetValue(_canChangeSchedule, nameof(CanChangeSchedule));
            }
        }

        public bool IsTaskReady
        {
            get
            {
                return GetValue(_isTaskReady, nameof(IsTaskReady));
            }
        }

        public bool CanCreatePurchaseOrder
        {
            get
            {
                return GetValue(_canCreatePurchaseOrder, nameof(CanCreatePurchaseOrder));
            }
        }

        public bool CanCreateDuplicateTask
        {
            get
            {
                return GetValue(_canCreateDuplicateTask, nameof(CanCreateDuplicateTask));
            }
        }

        public bool CanSignDocument
        {
            get
            {
                return GetValue(_canSignDocument, nameof(CanSignDocument));
            }
        }

        public bool CanDefer
        {
            get
            {
                return GetValue(_canDefer, nameof(CanDefer));
            }
        }

        public bool CanSaveEmailNote
        {
            get
            {
                return GetValue(_canSaveEmailNote, nameof(CanSaveEmailNote));
            }
        }

        public void Update(
            bool? canView = null,
            bool? canSave = null,
            bool? canComplete = null,
            bool? canUnlock = null,
            bool? canAddNewItem = null,
            bool? canAddNote = null,
            bool? canChangeSchedule = null,
            bool? isTaskReady = null,
            bool? canCreatePurchaseOrder = null,
            bool? canCreateDuplicateTask = null,
            bool? canSignDocument = null,
            bool? canDefer = null,
            bool? canSaveEmailNote = null)
        {
            if (canView.HasValue)
            {
                _canView = canView;
            }

            if (canSave.HasValue)
            {
                _canSave = canSave;
            }

            if (canComplete.HasValue)
            {
                _canComplete = canComplete;
            }

            if (canUnlock.HasValue)
            {
                _canUnlock = canUnlock;
            }

            if (canAddNewItem.HasValue)
            {
                _canAddNewItem = canAddNewItem;
            }

            if (canAddNote.HasValue)
            {
                _canAddNote = canAddNote;
            }

            if (canChangeSchedule.HasValue)
            {
                _canChangeSchedule = canChangeSchedule;
            }

            if (isTaskReady.HasValue)
            {
                _isTaskReady = isTaskReady;
            }

            if (canCreatePurchaseOrder.HasValue)
            {
                _canCreatePurchaseOrder = canCreatePurchaseOrder;
            }

            if (canCreateDuplicateTask.HasValue)
            {
                _canCreateDuplicateTask = canCreateDuplicateTask;
            }

            if (canSignDocument.HasValue)
            {
                _canSignDocument = canSignDocument;
            }

            if (canDefer.HasValue)
            {
                _canDefer = canDefer;
            }

            if (canSaveEmailNote.HasValue)
            {
                _canSaveEmailNote = canSaveEmailNote;
            }
        }

        private bool GetValue(bool? value, string fieldName)
        {
            if (!value.HasValue)
            {
                throw new InvalidOperationException($"{fieldName} has not been set.");
            }

            return value.Value;
        }
    }
}