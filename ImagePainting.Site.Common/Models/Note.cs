﻿using System;

namespace ImagePainting.Site.Common.Models
{
    public class Note
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public string Author { get; set; }

        public DateTime Created { get; set; }
    }
}
