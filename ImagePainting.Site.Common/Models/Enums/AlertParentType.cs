﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum AlertParentType
    {
        Unknown = 0,
        Task = 1,
        WorkOrder = 2
    }
}
