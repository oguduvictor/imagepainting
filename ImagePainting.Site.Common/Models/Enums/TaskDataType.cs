﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum TaskDataType
    {
        YesNo,
        Number,
        Text,
        Note
    }
}