﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum WorkOrderTaskStatus
    {
        Created = 0,
        MissingKeyData,
        Ready
    }
}
