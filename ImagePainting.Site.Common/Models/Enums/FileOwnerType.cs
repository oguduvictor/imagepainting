﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum FileOwnerType
    {
        Unknown = 0,
        Builder = 1,
        Community = 2,
        Elevation = 3,
        Plan = 4,
        Task = 5,
        WorkOrder = 6,
        SecuredWorkOrder = 7,
        Signature = 8,
        SignedDocument = 9
    }
}
