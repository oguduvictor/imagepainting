﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum AlertType
    {
        Unknown = 0,
        JobExtraDifference = 1,
        TaskNote = 2,
        JobInfoNote = 3,
        InteriorColorNote = 4,
        ExteriorColorNote = 5,
        JobExtraNote = 6,
        WorkOrderNote = 7
    }
}
