﻿using System.ComponentModel;

namespace ImagePainting.Site.Common.Models.Enums
{
    public enum TaskType
    {
        [Description("Unknown")]
        Unknown = 0,

        [Description("Interior")]
        Interior = 1,

        [Description("Exterior")]
        Exterior = 2,

        [Description("Pre Carpet")]
        PreCarpet = 3,

        [Description("After Carpet")]
        AfterCarpet = 4,

        [Description("QCI")]
        QCI = 6,

        [Description("HO")]
        HO = 7,

        [Description("XHO")]
        XHO = 8,

        [Description("Customer Service")]
        CustomerService = 9,

        [Description("Paint Foam")]
        PaintFoam = 10,

        [Description("DryLock")]
        Drylock = 11,

        [Description("Paint Meter")]
        PaintMeter = 12,

        [Description("Stain")]
        Stain = 13,

        [Description("WallOut")]
        WallOut = 14,

        [Description("Paint Enamel")]
        PaintEnamel = 15,

        [Description("PreCarpetAfterCarpet")]
        PreCarpetAfterCarpet = 16,

        [Description("XXHO")]
        XXHO = 17,

        [Description("Repaint Exterior")]
        RepaintExterior = 18,

        [Description("Repaint Interior")]
        RepaintInterior = 19,

        [Description("Others")]
        Others = 5,

    }
}
