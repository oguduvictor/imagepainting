﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum UserRole
    {
        User = 10000,
        SubContractor = 11000,
        Employee = 11010,
        Administrator = 11100,
        Accountant = 11101
    }
}