﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum WorkOrderSearchOption
    {
        None = 0,
        WorkOrderName = 1,
        Builder = 2,
        Community = 3,
        Address = 4,
        LotAndBlock = 5,
        ExteriorBaseColor = 6,
        InteriorBaseColor = 7
    }
}
