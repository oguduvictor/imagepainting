﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.Models.Enums
{
    public enum TaskCategory
    {
        Unknown = 0,
        Prewalk = 1,
        Main = 2,
        Completion = 3
    }
}
