﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum SupplyCategory
    {
        All = 0,
        Interior = 1,
        Exterior = 2,
    }
}
