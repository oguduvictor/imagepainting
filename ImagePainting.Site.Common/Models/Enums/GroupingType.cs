﻿namespace ImagePainting.Site.Common.Models.Enums
{
    public enum GroupingType
    {
        None,
        Daily,
        Weekly,
        Monthly
    }
}
