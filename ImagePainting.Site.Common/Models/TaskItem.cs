﻿using ImagePainting.Site.Common.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace ImagePainting.Site.Common.Models
{
    public class TaskItem
    {
        [Required]
        public string Name { get; set; }

        public TaskDataType DataType { get; set; }

        public string Value { get; set; }
    }
}