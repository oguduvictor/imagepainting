﻿using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Common.Models
{
    public class SupplyTypeSummary
    {
        public string Name { get; set; }

        public SupplyCategory supplyCategory { get; set; }
    }
}
