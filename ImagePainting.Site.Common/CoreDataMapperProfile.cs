﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Linq;

namespace ImagePainting.Site.Common
{
    public class CoreDataMapperProfile : Profile
    {
        public CoreDataMapperProfile()
        {
            CreateMap<UserInfo, UserInfoSummary>()
                .ForMember(c => c.Roles,
                    x => x.ResolveUsing(y => y.Roles?.Select(role => role.Id.ToEnum<UserRole>())?.ToArray()))
                .ForMember(c => c.Active, x => x.MapFrom(f => f.Active && !f.Deleted));

            CreateMap<UserInfoSummary, UserInfo>()
                .ForMember(d => d.Roles,
                    e => e.MapFrom(f => f.Roles.Select(role => new Role { Id = ((int)role).ToString(), Name = role.ToString() })));
        }
    }
}
