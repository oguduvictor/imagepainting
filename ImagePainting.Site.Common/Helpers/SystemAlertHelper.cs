﻿using ImagePainting.Site.Common.Models.Enums;
using System;

namespace ImagePainting.Site.Common.Helpers
{
    public class SystemAlertHelper
    {
        public static Guid ComputeTargetObjectId(long parentId, AlertParentType parentType, AlertType alertType)
        {
            var parentIdSection = parentId.ToString().PadLeft(12, '0');
            var parentTypeSection = ((int)parentType).ToString().PadLeft(4, '0');
            var alertTypeSection = ((int)alertType).ToString().PadLeft(4, '0');
            var targetObjectId = $"000000000000{alertTypeSection}{parentTypeSection}{parentIdSection}";

            return Guid.Parse(targetObjectId);
        }
    }
}
