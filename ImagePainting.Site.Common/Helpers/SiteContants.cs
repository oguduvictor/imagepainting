﻿namespace ImagePainting.Site.Common.Helpers
{
    public class SiteContants
    {
        public const string BASE_PRICE = "Base Price";

        public const string LABOR_COST = "Labor_Cost";

        public const string MATERIAL_COST = "Material_Cost";

        public const string NO_SUPERINTENDENT = "No Superintendent";
    }
}
