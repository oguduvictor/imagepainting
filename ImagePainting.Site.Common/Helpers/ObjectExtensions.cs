﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ImagePainting.Site.Common.Helpers
{
    public static class ExtensionMethods
    {
        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return (source == null || !source.Any());
        }

        public static bool IsAnyOf<T>(this T source, params T[] values)
        {
            if (values.IsNullOrEmpty())
            {
                return false;
            }

            return values.Contains(source);
        }

        public static bool IsDefaultTime(this DateTime dateTime)
        {
            return dateTime.TimeOfDay == TimeSpan.Zero;
        }

        public static string SettingValue(this string source)
        {
            return ConfigurationManager.AppSettings[source];
        }

        public static string ConnectionString(this string source)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[source];

            if (connectionString != null)
            {
                return connectionString.ConnectionString;
            }

            return null;
        }

        public static bool IsOne<T>(this IEnumerable<T> source)
        {
            return (source.Count() == 1);
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
            {
                action(item);
            }
        }

        public static T Second<T>(this IEnumerable<T> source)
        {
            return source.ElementAt(1);
        }

        public static T Third<T>(this IEnumerable<T> source)
        {
            return source.ElementAt(2);
        }

        public static bool IsTrue(this string source)
        {
            if (source.IsNullOrEmpty())
            {
                return false;
            }

            return source.ToLower().IsAnyOf("1", "true", "on");
        }

        public static int ToSafeInt(this string source, int defaultValue = 0)
        {
            int value = defaultValue;
            int.TryParse(source, out value);

            return value;
        }

        public static long ToSafeLong(this string source, long defaultValue = 0)
        {
            long value = defaultValue;
            long.TryParse(source, out value);

            return value;
        }

        public static void Upsert<T, S>(this IDictionary<S, T> source, S key, T value)
        {
            if (source.ContainsKey(key))
            {
                source[key] = value;
            }
            else
            {
                source.Add(key, value);
            }
        }

        public static S TryGet<T, S>(this IDictionary<T, S> source, T key, S value)
        {
            if (source.ContainsKey(key))
                return source[key];

            return value;
        }

        public static T ToEnum<T>(this string value) where T : struct
        {
            var result = default(T);

            if (!Enum.TryParse(value, out result))
                throw new ArgumentException("value is an invalid enum type");

            return result;

        }

        public static string ToJson<T>(this T source)
        {
            return JsonConvert.SerializeObject(source, Formatting.Indented);
        }

        public static T FromJson<T>(this string source)
        {
            return JsonConvert.DeserializeObject<T>(source);
        }

        public static object FromJson(this string value, Type type)
        {
            return JsonConvert.DeserializeObject(value, type);
        }

        public static string ToParams<S>(this S obj)
        {
            var type = obj.GetType();

            if (obj == null)
                return string.Empty;

            if (type.Name == "String")
                return obj.ToString();

            if (obj is ICollection)
                return ToParams((ICollection)obj);

            var props = type.GetProperties();
            var collection = new NameValueCollection();

            foreach (var prop in props)
            {
                var value = prop.GetValue(obj);

                if (value == null)
                    continue;

                collection.Add(prop.Name, value.ToString());
            }

            return ToParams(collection);
        }

        public static string ToParams(this ICollection data)
        {
            if (data == null || data.Count == 0)
                return string.Empty;

            var sb = new StringBuilder("?");

            foreach (KeyValuePair<string, string> item in data)
            {
                if (item.Value == null)
                    continue;

                sb.Append(item.Key);
                sb.Append("=");
                sb.Append(Uri.EscapeUriString(item.Value));
                sb.Append("&");
            }

            return sb.ToString().TrimEnd('&');
        }

        public static void MergeShallow<T, S>(this T target, S source, params string[] ignoreProperties)
        {
            if (target.IsNull() || source.IsNull())
            {
                return;
            }

            var type = typeof(S);
            var props = type.GetProperties();

            foreach (var prop in props)
            {

                if (!prop.CanWrite 
                    || prop.PropertyType.IsArray 
                    || prop.PropertyType.IsInterface 
                    || (ignoreProperties != null && ignoreProperties.Contains(prop.Name)) 
                    || (prop.PropertyType.IsClass && prop.PropertyType.Name != "String")) // TODO: this is meant to exlude only the custom classes 
                {
                    continue;
                }

                var value = prop.GetValue(source);
                prop.SetValue(target, value);
            }
        }

        public static string BreakupText(this string source)
        {
            if (source.IsNullOrEmpty() || source.IsOne())
            {
                return source;
            }

            var charList = new List<char> { source[0] };

            int num = 0;
            int value = 0;
            bool fail = true;
            for (int i = 1; i < source.Length; i++)
            {
                value = i - 1;
                if (num == 1 && char.IsLower(source[value]) && char.IsUpper(source[i]) && fail == true)
                {
                    charList.Add('/');
                    fail = false;
                }

                else if (char.IsUpper(source[i]))
                {
                    charList.Add(' ');
                    num++;
                }
                charList.Add(source[i]);
            }
            return new string(charList.ToArray());
        }

        public static (int? lot, string block)? BreakTextIntoLotAndBlock(this string source)
        {
            if (source.IsNullOrEmpty())
            {
                return null;
            }

            var splitString = source.Split(new []{' '}, options: StringSplitOptions.RemoveEmptyEntries);
            var lot = default(int);
            var block = default(string);

            if (splitString.Length > 0)
            {
                lot = splitString.First().ToSafeInt();
            }

            if (splitString.Length > 1)
            {
                block = splitString.Second();
            }
            
            return (lot, block);
        }

        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            string description = null;

            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                        if (descriptionAttributes.Length > 0)
                        {
                            description = ((DescriptionAttribute)descriptionAttributes[0]).Description;
                        }

                        break;
                    }
                }
            }

            return description;
        }

        public static string FullName(this IUserContext userContext)
        {
            if (userContext.IsNull())
            {
                throw new ArgumentNullException("UserContext is null");
            }

            return $"{userContext.FirstName} {userContext.LastName}";
        }

        public static T DeepClone<T>(this T source)
        {
            var json = source.ToJson();

            return json.FromJson<T>();
        }

        public static bool IsDeserializeable<T>(this string source)
        {
            try
            {
                return !JsonConvert.DeserializeObject<T>(source).IsNull();
            }
            catch (Exception)
            {
                return false;
            }           
        }

        public static DateTimeRange ToDateRange(this TaskSearchCriteria searchCriteria)
        {
            var dateRange = new DateTimeRange();
            var today = DateTime.Today;

            if (searchCriteria.Grouping == GroupingType.Monthly)
            {
                var dateInTargeMonth = today.AddMonths(searchCriteria.PageNumber);

                dateRange.BeginDateTime = new DateTime(dateInTargeMonth.Year, dateInTargeMonth.Month, 1);
                dateRange.EndDateTime = dateRange.BeginDateTime.AddMonths(1).AddSeconds(-1);
            }

            if (searchCriteria.Grouping == GroupingType.Weekly)
            {
                var dateInTargetWeek = today.AddDays(7 * searchCriteria.PageNumber);

                dateRange.BeginDateTime = dateInTargetWeek.AddDays(-(int)DateTime.Today.DayOfWeek).Date;
                dateRange.EndDateTime = dateInTargetWeek.AddDays(7 - (int)DateTime.Today.DayOfWeek).AddSeconds(-1);
            }

            if (searchCriteria.Grouping == GroupingType.Daily)
            {
                var dayOfInterest = searchCriteria.DateRange.BeginDateTime.AddDays(searchCriteria.PageNumber).Date;

                dateRange.BeginDateTime = dayOfInterest;
                dateRange.EndDateTime = dayOfInterest.AddDays(1).AddSeconds(-1);
            }

            if (!searchCriteria.DateRange.IsNull() && searchCriteria.Grouping == GroupingType.None)
            {
                dateRange.BeginDateTime = searchCriteria.DateRange.BeginDateTime.Date;
                dateRange.EndDateTime = searchCriteria.DateRange.EndDateTime.Date.AddDays(1).AddSeconds(-1);
            }

            if (searchCriteria.IsUpcoming == true)
            {
                if (dateRange.BeginDateTime < today)
                {
                    dateRange.BeginDateTime = today;
                }
            }
            else if (searchCriteria.IsOverdued == true)
            {
                if (dateRange.EndDateTime >= today)
                {
                    dateRange.EndDateTime = today.AddDays(-1);
                }
            }

            return dateRange;
        }
    }
}
