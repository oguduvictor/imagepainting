﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;

namespace ImagePainting.Site.Common.Helpers
{
    public static class DateHelper
    {
        public static DateTimeRange BuildDateTimeRange(int? year = null, int? month = null, GroupingType? type = null, int? pageNumber = null, DateTime? date = null)
        {
            if (year.HasValue && month.HasValue)
            {
                return new DateTimeRange
                {
                    BeginDateTime = new DateTime(year.Value, month.Value, 1, 0, 0, 0),
                    EndDateTime = new DateTime(year.Value, month.Value, DateTime.DaysInMonth(year.Value, month.Value), 23, 59, 59)
                };
            }else if (date.HasValue)
            {
                return new DateTimeRange
                {
                    BeginDateTime = date ?? DateTime.Now,
                    EndDateTime = date ?? DateTime.Now
                };
            }
            else if (type.HasValue && pageNumber.HasValue)
            {
                var firstDay = default(DateTime);
                var lastDay = default(DateTime);

                switch (type)
                {
                    case GroupingType.Daily:
                        firstDay = DateTime.Today.AddDays(pageNumber.Value).Date;
                        lastDay = firstDay;
                        break;
                    case GroupingType.Weekly:
                        firstDay = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (7 * pageNumber.Value));
                        lastDay = DateTime.Today.AddDays((6 - (int)DateTime.Today.DayOfWeek) + (7 * pageNumber.Value));
                        break;
                    case GroupingType.Monthly:
                        var currentMonth = BuildDateTimeRange(DateTime.Now.Year, DateTime.Now.Month);
                        var dayInMonth = currentMonth.EndDateTime.AddMonths(pageNumber.Value);

                        firstDay = currentMonth.BeginDateTime.AddMonths(pageNumber.Value);
                        lastDay = new DateTime(dayInMonth.Year, dayInMonth.Month, 
                            DateTime.DaysInMonth(dayInMonth.Year, dayInMonth.Month));
                        break;
                    default:
                        break;
                }
                
                return new DateTimeRange
                {
                    BeginDateTime = firstDay,
                    EndDateTime = lastDay
                };
            }

            return new DateTimeRange();
        }

        public static DateTime NextBusinessDay(this DateTime date)
        {
            switch(date.DayOfWeek)
            {
                case DayOfWeek.Friday:
                    return date.AddDays(3);
                case DayOfWeek.Saturday:
                    return date.AddDays(2);
                default:
                    return date.AddDays(1);
            }
        }

        public static DateTime PreviousBusinessDay(this DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return date.AddDays(-3);
                case DayOfWeek.Sunday:
                    return date.AddDays(-2);
                default:
                    return date.AddDays(-1);
            }
        }

        public static DateTime ToLocalDateTime(this DateTime datetimeUtc)
        {
            TimeZoneInfo easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            return TimeZoneInfo.ConvertTimeFromUtc(datetimeUtc, easternTimeZone);
        }

        public static DateTime GetLocalDateToday()
        {
            return DateTime.UtcNow.ToLocalDateTime().Date;
        }

        public static string ToLocalShortDateTime(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToLocalDateTime().ToString("MMM dd yyyy, h:mm tt");
        }

        public static string ToLocalShortDate(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToLocalDateTime().ToString("MMM dd yyyy");
        }

        public static IList<DateTime> ToRange(this DateTimeRange range)
        {
            var dates = new List<DateTime>();
            var count = default(double);

            dates.Add(range.BeginDateTime.Date);

            if (range.BeginDateTime.Date == range.EndDateTime.Date)
            {
                return dates;
            }

            while (range.BeginDateTime.AddDays(++count).Date < range.EndDateTime.Date)
            {
                dates.Add(range.BeginDateTime.AddDays(count).Date);
            }

            dates.Add(range.EndDateTime.Date);

            return dates;
        }

        public static DateTime EstimatePaymentDate(this DateTime completionDateTime)
        {
            var date = completionDateTime.ToLocalDateTime();
            var paymentDate = date.AddDays(12 - (int)date.DayOfWeek).Date;

            if (date.DayOfWeek > DayOfWeek.Wednesday || (date.DayOfWeek == DayOfWeek.Wednesday && date.Hour > 11))
            {
                return paymentDate.AddDays(7);
            }

            return paymentDate;
        }

        public static DateTime ToUTC(this DateTime easternDateTime)
        {
            if (easternDateTime.Kind == DateTimeKind.Utc)
            {
                return easternDateTime;
            }

            TimeZoneInfo easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

            return TimeZoneInfo.ConvertTimeToUtc(easternDateTime, easternTimeZone);
        }

        public static DateTimeRange ToUTCDateTimeRange(this DateTimeRange localDateTimeRange)
        {
            if (localDateTimeRange.IsNull())
            {
                return null;
            }

            localDateTimeRange.BeginDateTime = localDateTimeRange.BeginDateTime.ToUTC().Date;
            localDateTimeRange.EndDateTime = localDateTimeRange.EndDateTime.ToUTC().Date.AddDays(1).AddTicks(-1);

            return localDateTimeRange;
        }
    }
}
