﻿using System;
using System.Threading.Tasks;

namespace ImagePainting.Site.Common.ServiceBus
{
    public interface IBusControl
    {
        void Register<TMessage, THandler>()
            where TMessage : IMessage
            where THandler : IHandler<TMessage>;

        void Register(Type messageType, Type handlerType);

        void RegisterHandler<TSaga>();

        void UnRegister<TMessage, THandler>()
            where TMessage : IMessage
            where THandler : IHandler<TMessage>;

        Task StartAsync();
    }
}
