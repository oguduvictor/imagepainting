﻿using System.Threading.Tasks;

namespace ImagePainting.Site.Common.ServiceBus
{
    public interface IHandler<TMessage>
        where TMessage : IMessage
    {
        Task HandleAsync(TMessage message);
    }
}
