﻿using System;

namespace ImagePainting.Site.Common.ServiceBus
{
    public interface IMessage
    {
        Guid Id { get; }

        string UserId { get; }

        DateTime Created { get; }
    }
}
