﻿using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Common.ServiceBus
{
    public class TaskSchemaUpdatedEvent : IMessage
    {
        public TaskSchemaUpdatedEvent(string userId, int schemaId, 
            IEnumerable<string> addedTaskItemNames, 
            IEnumerable<string> deletedTaskItemNames)
        {
            UserId = userId;
            SchemaId = schemaId;
            Id = Guid.NewGuid();
            Created = DateTime.UtcNow;
            AddedTaskItemNames = addedTaskItemNames?.ToList() ?? new List<string>();
            DeletedTaskItemNames = deletedTaskItemNames?.ToList() ?? new List<string>();
        }

        public Guid Id { get; private set; }

        public DateTime Created { get; private set; }

        public string UserId { get; private set; }

        public int SchemaId { get; private set; }

        public IList<string> AddedTaskItemNames { get; private set; }

        public IList<string> DeletedTaskItemNames { get; private set; }
    }
}
