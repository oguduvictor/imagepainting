﻿using System.Threading.Tasks;

namespace ImagePainting.Site.Common.ServiceBus
{
    public interface IBus
    {
        /// <summary>
        /// Sends message to all subscribers
        /// </summary>
        /// <param name="message">Message to send</param>
        Task PublishAsync(IMessage message);

        /// <summary>
        /// Sends message to only one of the subscribers
        /// </summary>
        /// <param name="message">Message to send</param>
        Task SendAsync(IMessage message);
    }
}
