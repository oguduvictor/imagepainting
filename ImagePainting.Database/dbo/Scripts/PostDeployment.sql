﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r ".\PostDeploy\001_Seed_CostItems.sql"
:r ".\PostDeploy\002_Seed_States.sql"
:r ".\PostDeploy\003_Seed_Roles.sql"
:r ".\PostDeploy\004_Seed_Supplies.sql"
