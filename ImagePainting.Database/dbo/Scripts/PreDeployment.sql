﻿--SCRIPT TO CHANGE Plans.CommunityId to Plans.BuilderId

-- STEP 1: Add CommunityPlans table for association between plans and Communities
IF NOT EXISTS(SELECT 1 FROM sys.tables 
          WHERE Object_ID = Object_ID(N'dbo.CommunityPlans'))
BEGIN
	CREATE TABLE [dbo].[CommunityPlans] (
		[CommunityId] INT NOT NULL,
		[PlanId]   INT NOT NULL,
		CONSTRAINT [PK_CommunityPlans] PRIMARY KEY CLUSTERED ([CommunityId] ASC, [PlanId] ASC),
		CONSTRAINT [FK_CommunityPlans_Communities] FOREIGN KEY ([CommunityId]) REFERENCES [dbo].[Communities] ([Id]),
		CONSTRAINT [FK_CommunityPlans_Plans] FOREIGN KEY ([PlanId]) REFERENCES [dbo].[Plans] ([Id])
	);
END

GO

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CommunityId'
          AND Object_ID = Object_ID(N'dbo.Plans'))
BEGIN
	-- STEP 2: Add BuilderId to Plans
    ALTER TABLE Plans
	ADD BuilderId INT NULL;

	-- STEP 3: Add foreign key constraint on BuilderId
	EXEC sp_executesql N'
		ALTER TABLE Plans
		ADD CONSTRAINT FK_Plans_Builders
		FOREIGN KEY (BuilderId) REFERENCES Builders(Id)';

	-- STEP 4: Populate Community Plans
	EXEC sp_executesql N'
		INSERT INTO dbo.CommunityPlans(CommunityId, PlanId) 
		SELECT CommunityId, Id 
		FROM Plans
	';

	-- STEP 5: Populate Plans.BuilderId
	EXEC sp_executesql N'
		With Plans_CTE (PlanId, BuilderId, CommunityId)
		AS 
		(
			SELECT 
				a.PlanId, c.BuilderId, c.CommunityId
			FROM BuilderCommunities c
				LEFT JOIN 
					(SELECT w.BuilderId, w.CommunityId, j.PlanId
						FROM WorkOrders w
							JOIN JobInfos j ON w.JobInfoId = j.Id
						GROUP BY w.BuilderId, w.CommunityId, j.PlanId
					) a 
				ON a.BuilderId = c.BuilderId and a.CommunityId = c.CommunityId
			GROUP BY c.BuilderId, c.CommunityId, a.PlanId
		)
		UPDATE dbo.Plans 
		SET BuilderId = ISNULL(
			(SELECT TOP 1 BuilderId from Plans_CTE WHERE PlanId = Plans.Id),
			(SElect TOP 1 BuilderId from BuilderCommunities WHERE CommunityId = Plans.CommunityId))
	';

	-- STEP 6: Drop foreign key constraint on CommunityId
	IF EXISTS (SELECT 1
		FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_Plans_Communities') 
	BEGIN
		EXEC sp_executesql N'ALTER TABLE Plans DROP CONSTRAINT FK_Plans_Communities';
	END

	-- STEP 7: Drop Plans.CommunityId
	ALTER TABLE Plans DROP COLUMN CommunityId;

	-- STEP 8: Make Plans.BuilderId not nullable
	EXEC sp_executesql N'ALTER TABLE Plans ALTER COLUMN BuilderId INT NOT NULL';
END

 -- SCRIPT TO COPY DATA FROM TABLE UnAssignedCrewEmailRegistries TO TABLE UnAssignedCrewSettings, MARK ALL THE ITEMS EMAILED AND DROP TABLE UnAssignedCrewEmailRegistries
IF EXISTS(SELECT 1 FROM sys.Tables WHERE  Name = N'UnAssignedCrewEmailRegistries' AND Type = N'U')
BEGIN

	IF NOT EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.TABLES T
        WHERE T.TABLE_SCHEMA = 'dbo'
        AND T.TABLE_NAME = 'UnAssignedCrewSettings' )
    BEGIN

        CREATE TABLE dbo.UnAssignedCrewSettings
        (
            [UserId] NVARCHAR(128) NOT NULL, 
			[ScheduledDate] DATETIME2 NOT NULL,
			[Emailed] BIT NOT NULL DEFAULT 0,
			[Busy] BIT NOT NULL DEFAULT 0,
			CONSTRAINT [PK_UnAssignedCrewSettings] PRIMARY KEY CLUSTERED ([UserId] ASC, [ScheduledDate] ASC)
        )
    END

	INSERT INTO UnAssignedCrewSettings (UserId, ScheduledDate, Emailed, Busy)
	SELECT UserId, ScheduledDate, 1, 0
	FROM UnAssignedCrewEmailRegistries

	DROP TABLE UnAssignedCrewEmailRegistries
END
