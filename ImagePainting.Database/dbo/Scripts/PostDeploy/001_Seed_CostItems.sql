﻿ALTER TABLE [Costs]
NOCHECK CONSTRAINT [FK_Costs_CostItems]

GO

DECLARE @costItems TABLE (
	Name   NVARCHAR(50) NOT NULL,  
	IsExtra BIT NOT NULL DEFAULT 1,
	IsLumpSum BIT NOT NULL DEFAULT 0,
	LaborCost NUMERIC (8, 2) NULL,
	PercentOfTotalSize NUMERIC(5, 2) NULL DEFAULT 100,
	[Order] INT NOT NULL DEFAULT 0
);  

INSERT INTO @costItems(Name, IsExtra, IsLumpSum, LaborCost, PercentOfTotalSize, [Order]) 
VALUES
('Base Price', 0, 0, 2.5, 100, 1);
--('Drylock', 0, 1, 2.5, 100, 2),
--('Custom Color', 1, 0, 2.5, 100, 3),
--('Crown Molding', 1, 0, 2.5, 100, 4),
--('Stain Railing', 1, 0, 2.5, 100, 5),
--('Paint Railing', 1, 0, 2.5, 100, 6),
--('French Doors', 1, 0, 2.5, 100, 7),
--('Shoe Molding', 1, 0, 2.5, 100, 8),
--('Exterior w/ Wood Fascia', 1, 0, 2.5, 100, 9),
--('Exterior w/ Wood Soffit', 1, 0, 2.5, 100, 10),
--('Interior w/ 5 1/4 Base', 1, 0, 2.5, 100, 11),
--('Interior w/ 7 1/4', 1, 0, 2.5, 100, 12),
--('Paint Off White Trim', 1, 0, 2.5, 100, 13),
--('Stain Treads', 1, 0, 2.5, 100, 14),
--('Paint Risers', 1, 0, 2.5, 100, 15),
--('Stain Grasp Rail', 1, 0, 2.5, 100, 16),
--('Paint Columns', 1, 0, 2.5, 100, 17),
--('Paint Sills', 1, 0, 2.5, 100, 18),
--('Paint 8'' Doors', 1, 0, 2.5, 100, 19),
--('Paint Accent Walls', 1, 0, 2.5, 100, 20),
--('Paint Accent Rooms', 1, 0, 2.5, 100, 21),
--('Paint Optional Doors', 1, 0, 2.5, 100, 22),
--('Paint Cased Windows', 1, 0, 2.5, 100, 23),
--('Stain Beams', 1, 0, 2.5, 100, 24),
--('Bead Board', 1, 0, 2.5, 100, 25),
--('Paint Pantry', 1, 0, 2.5, 100, 26),
--('Paint Tray W/ Color', 1, 0, 2.5, 100, 27),
--('2nd Story Ext', 1, 0, 2.5, 100, 28);

Merge INTO CostItems AS target
USING(SELECT Name, IsExtra, LaborCost, IsLumpSum, PercentOfTotalSize, [Order] FROM @costItems) 
		as source (Name1, IsExtra1, LaborCost1,IsLumpSum1, PercentOfTotalSize1, Order1)
	ON (target.Name = source.Name1)
WHEN NOT MATCHED BY TARGET THEN
	INSERT (Name, IsExtra, LaborCost, IsLumpSum, PercentOfTotalSize, [Order]) 
		VALUES (Name1, IsExtra1, LaborCost1, IsLumpSum1, PercentOfTotalSize1, Order1);

GO

ALTER TABLE [Costs]
CHECK CONSTRAINT [FK_Costs_CostItems]