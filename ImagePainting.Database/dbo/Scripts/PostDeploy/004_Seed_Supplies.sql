﻿AlTER TABLE [SupplyType]
NOCHECK CONSTRAINT [FK_SupplyType_SupplyCategory];

GO

DELETE SupplyCategory;

GO

INSERT INTO SupplyCategory(Id, Name)
VALUES
(1, 'Interior'),
(2, 'Exterior');

GO

DECLARE @InteriorCategory int;
SELECT @InteriorCategory = Id FROM SupplyCategory WHERE [Name] = 'Interior';

DECLARE @ExteriorCategory int;
SELECT @ExteriorCategory = Id FROM SupplyCategory WHERE [Name] = 'Exterior';
	
DECLARE @supplyTypes TABLE (
	[Name] NVARCHAR(50) NOT NULL,
	SupplyCategory int NOT NULL
);

INSERT INTO @supplyTypes (Name, SupplyCategory)
VALUES
('Base', @InteriorCategory),
('Base', @ExteriorCategory),
('Ceiling', @InteriorCategory),
('Others', @InteriorCategory),
('Others', @ExteriorCategory),
('Fascia', @InteriorCategory),
('Front Door', @ExteriorCategory),
('Garage Door', @ExteriorCategory),
('Primer', @InteriorCategory),
('Primer', @ExteriorCategory),
('Stucco', @InteriorCategory),
('Stain', @InteriorCategory),
('Trim', @InteriorCategory),
('Trim', @ExteriorCategory),
('Accent Room', @InteriorCategory),
('Accent Wall', @InteriorCategory),
('Corbels', @InteriorCategory),
('Brackets', @InteriorCategory),
('Louvre', @InteriorCategory),
('Hardi Siding', @ExteriorCategory),
('Board and Batton', @InteriorCategory),
('Shakes', @InteriorCategory);

Merge INTO SupplyType AS target
USING(SELECT [Name], SupplyCategory FROM @supplyTypes) as source (Name1, SupplyCategory1)
	ON (target.[Name] = source.Name1 AND target.SupplyCategory = source.SupplyCategory1)
WHEN NOT MATCHED BY TARGET THEN
	INSERT ([Name], SupplyCategory) VALUES (Name1, SupplyCategory1);

Go

AlTER TABLE [SupplyType]
CHECK CONSTRAINT [FK_SupplyType_SupplyCategory];