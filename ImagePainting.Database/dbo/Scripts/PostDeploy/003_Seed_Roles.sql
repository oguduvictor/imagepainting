﻿ALTER TABLE [AspNetUserRoles]
NOCHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]

GO

DELETE AspNetRoles;

GO

INSERT INTO AspNetRoles (Id, Name)
VALUES
(10000, 'User'),
(11000, 'SubContractor'),
(11010, 'Employee'),
(11100, 'Administrator'),
(11101,	'Accountant');

GO

ALTER TABLE [AspNetUserRoles]
CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId];