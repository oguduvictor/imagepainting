﻿DECLARE 
	@assembly NVARCHAR(128) = 'ImagePainting.Site.Retrofits, Version=6.0.0.0, Culture=neutral, PublicKeyToken=null',
	@type1 NVARCHAR(128) = 'ImagePainting.Site.Retrofits.Commands.RetrofitJobInfoNotesCommand',
	@type2 NVARCHAR(128) = 'ImagePainting.Site.Retrofits.Commands.RetrofitJobExtraItemsCommand',
	@type3 NVARCHAR(128) = 'ImagePainting.Site.Retrofits.Commands.RetrofitWorkOrderDescriptionCommand',
	@date NVARCHAR(50) = cast(SYSUTCDATETIME() AS NVARCHAR(50)),
	@userId NVARCHAR(128),
	@message NVARCHAR(max);
	
SELECT TOP 1 @userId = Id FROM dbo.UserInfo WHERE Active = 1;

SET @message = N'{"Id": "' + cast(NEWID() AS NVARCHAR(128)) + '","Created":"' + @date +'","UserId":"' + @userId + '"}';

INSERT [dbo].[QMessages] ([Id], [Message], [MessageTypeName], [MessageAssemblyName], [CreatedById]) 
VALUES 
(NEWID(), @message, @type1, @assembly, @userId),
(NEWID(), @message, @type2, @assembly, @userId),
(NEWID(), @message, @type3, @assembly, @userId);