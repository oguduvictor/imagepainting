﻿CREATE VIEW [dbo].[vw_Tasks]
AS 
SELECT 
	t.Id,
	t.[Name],
	t.WorkOrderId,
	t.SchemaId,
	t.ScheduleDate,
	t.ScheduleDateEnd,
	t.EmailSent,
	t.Completed,
	t.CrewId,
	(CASE
		WHEN (t.PreWalkTaskId | t.CompletionWalkTaskId) IS NULL
			THEN CAST(0 AS BIT)
		ELSE CAST(1 AS BIT)
	END) AS IsMainTask,
	(CASE
		WHEN t.PreWalkTaskId IS NOT NULL -- main task
			THEN w.JobInfoConfirmed & t.PreceedingTaskCompleted
		WHEN t.CompletionWalkTaskId IS NOT NULL -- main task
			THEN w.JobInfoConfirmed
		ELSE t.PreceedingTaskCompleted
	END) AS IsReady,
	(u.FirstName + ' ' + u.LastName) AS Crew,
	w.CommunityId,
	w.BuilderId,
	w.[Name] AS WorkOrderName,
	w.Address AS WorkOrderAddress,
	w.Completed AS WorkOrderCompleted,
	w.JobInfoConfirmed,
	p.PurchaseOrderId,
	p.DeliveryInstructions
FROM dbo.[WorkOrderTasks] t
	JOIN dbo.vw_WorkOrders w ON t.WorkOrderId = w.Id
	LEFT JOIN dbo.vw_TaskPurchaseOrders p ON t.Id = p.TaskId
	LEFT JOIN dbo.UserInfo u ON t.CrewId = u.Id
 WHERE 
	t.IsDeferred != 1
