﻿Create VIEW [dbo].[vw_TaskPurchaseOrders]
AS 
SELECT
	t.Id AS TaskId,
	MAX(p.Id) AS PurchaseOrderId,
	MIN(p.DeliveryInstructions) AS DeliveryInstructions
FROM dbo.[WorkOrderTasks] t
 LEFT JOIN dbo.PurchaseOrders p ON t.Id = p.TaskId
WHERE p.EmailSent IS NOT NULL
GROUP BY t.Id
