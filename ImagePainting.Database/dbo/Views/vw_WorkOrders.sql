﻿CREATE VIEW [dbo].[vw_WorkOrders] 
WITH SCHEMABINDING  
AS 
SELECT 
	w.Id, 
	w.[Name],   
	w.[Address],
	w.[Lot],
	w.[Block],
	w.Completed,
	w.Modified,
	w.Created,
	w.CreatedById,
	w.JobInfoId,
	w.JobInfoConfirmed,
	b.Id AS BuilderId,
	b.[Name] AS BuilderName,
	b.[Abbreviation] AS BuilderAbbreviation,
	c.[Id] AS CommunityId,
	c.[Name] AS CommunityName,
	c.[Abbreviation] AS CommunityAbbreviation,
	(u.FirstName + ' ' + u.LastName) AS CreatedBy
FROM dbo.[WorkOrders] w
	JOIN dbo.[Builders] b ON w.BuilderId = b.Id
	JOIN dbo.[Communities] c ON w.CommunityId = c.Id
	JOIN dbo.[UserInfo] u ON w.CreatedById = u.Id
Where w.Deleted = 0

GO

CREATE UNIQUE CLUSTERED INDEX IX_vw_WorkOrders  ON [dbo].[vw_WorkOrders]  (Id);  

GO

Create INDEX IX_vw_WorkOrders_LotBlock ON dbo.vw_WorkOrders (Lot, [Block], [Name])