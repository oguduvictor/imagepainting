﻿CREATE TABLE [dbo].[SupplyCategory]
(
	[Id] INT NOT NULL,
	[Name] NVARCHAR(50) NOT NULL,
    CONSTRAINT [PK_SupplyCategory] PRIMARY KEY CLUSTERED ([Id] ASC),
)
