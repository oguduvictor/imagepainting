﻿CREATE TABLE [dbo].[Communities] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Active]     BIT            DEFAULT ((1)) NOT NULL,
    [Name]       NVARCHAR (64)  NOT NULL,
    [Notes]      NVARCHAR (MAX) NULL,
    [Created] DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL,
	[Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
	[CreatedById] NVARCHAR(128) NOT NULL, 
	[ModifiedById] NVARCHAR(128) NOT NULL, 
    [Abbreviation] NVARCHAR(5) NOT NULL, 
    [Deleted] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_PCommunities_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]), 
    CONSTRAINT [FK_PCommunities_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id])
);



