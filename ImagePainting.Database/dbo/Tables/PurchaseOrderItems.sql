﻿CREATE TABLE [dbo].[PurchaseOrderItems] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [PurchaseOrderId] BIGINT         NOT NULL,
    [Name]            NVARCHAR (50)  NOT NULL,
    [SupplyType]      NVARCHAR (50)  NOT NULL,
    [Color]           NVARCHAR (50)  NULL,
    [UnitPrice]       NUMERIC (8, 2) NOT NULL,
    [Quantity]        NUMERIC(8, 2)            NOT NULL,

	-- Temporal Table Settings
	[SysStartTime] DATETIME2 (0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT DF_PurchaseOrderItems_SysStartTime DEFAULT GETDATE(),
	[SysEndTime] DATETIME2 (0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT DF_PurchaseOrderItems_SysEndTime DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
	PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime]),

    CONSTRAINT [PK_PurchaseOrderItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PurchaseOrderItems_PurchaseOrders] FOREIGN KEY ([PurchaseOrderId]) REFERENCES [dbo].[PurchaseOrders] ([Id])
)
WITH (SYSTEM_VERSIONING = ON(HISTORY_TABLE=[dbo].[PurchaseOrderItemsHistory], DATA_CONSISTENCY_CHECK=ON));









