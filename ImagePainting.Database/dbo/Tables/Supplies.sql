﻿CREATE TABLE [dbo].[Supplies] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [BuilderId]    INT            NOT NULL,
    [PaintStoreId] BIGINT         NOT NULL,
    [SupplyTypeId] INT            NOT NULL,
    [Name]         NVARCHAR (50)  NOT NULL,
    [Cost]         NUMERIC (8, 2) DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Supplies] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Supplies_Builders] FOREIGN KEY ([BuilderId]) REFERENCES [dbo].[Builders] ([Id]),
    CONSTRAINT [FK_Supplies_Stores] FOREIGN KEY ([PaintStoreId]) REFERENCES [dbo].[Stores] ([Id])
);




