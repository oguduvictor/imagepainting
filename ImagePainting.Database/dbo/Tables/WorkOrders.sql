﻿CREATE TABLE [dbo].[WorkOrders]
(
	[Id] BIGINT NOT NULL IDENTITY,
	[Name] NVARCHAR(256) NULL,
	[Address] NVARCHAR(256) NULL,
	[Description] NVARCHAR(4000) NULL,
	[BuilderId] INT NULL,
	[CommunityId] INT NULL,
    [Lot] INT NULL,
	[Block] NVARCHAR(50) NULL,
	[SuperIntendent] NVARCHAR(128) NULL, 
	[CustomerRep] NVARCHAR(128) NULL,
	[JobInfoId] BIGINT NULL,
	[ExtraJobInfoId] BIGINT NULL,
    [Completed] DATETIME2 NULL,
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(),
    [CreatedById] NVARCHAR(128) NOT NULL,
    [Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(),
    [ModifiedById] NVARCHAR(128) NOT NULL,
	[Deleted] BIT NOT NULL DEFAULT 0, 
	[JobInfoConfirmed] BIT NOT NULL DEFAULT 0,
    [CustomerName] NVARCHAR(50) NULL,
    [CustomerPhoneNumber] NVARCHAR(16) NULL, 
    [CustomerEmail] NVARCHAR(128) NULL, 

	-- Temporal Table Settings
	[SysStartTime] DATETIME2 (0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT DF_WorkOrders_SysStartTime DEFAULT GETDATE(),
	[SysEndTime] DATETIME2 (0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT DF_WorkOrders_SysEndTime DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
	PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime]),

    CONSTRAINT [PK_WorkOrders] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_WorkOrders_Builders] FOREIGN KEY ([BuilderId]) REFERENCES [Builders]([Id])
		ON DELETE SET NULL,
	CONSTRAINT [FK_WorkOrders_Communities] FOREIGN KEY ([CommunityId]) REFERENCES [Communities]([Id])
		ON DELETE SET NULL,
	CONSTRAINT [FK_WorkOrders_JobInfos] FOREIGN KEY ([JobInfoId]) REFERENCES [JobInfos]([Id])
		ON DELETE SET NULL,
	CONSTRAINT [FK_WorkOrders_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]),
    CONSTRAINT [FK_WorkOrders_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id])
)
WITH (SYSTEM_VERSIONING = ON(HISTORY_TABLE=[dbo].[WorkOrdersHistory], DATA_CONSISTENCY_CHECK=ON))

GO

CREATE INDEX [IX_WorkOrders_Name] ON [dbo].[WorkOrders] ([Name])

GO

CREATE INDEX [IX_WorkOrders_LotBlock] ON [dbo].[WorkOrders] ([Lot],[Block])
