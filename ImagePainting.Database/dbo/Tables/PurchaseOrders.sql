﻿CREATE TABLE [dbo].[PurchaseOrders] (
    [Id]                   BIGINT          IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]          BIGINT          NULL,
    [PaintStoreLocationId] BIGINT          NULL,
    [TaskId]               BIGINT          NULL,
	[DeliveryInstructions] NVARCHAR(50)    NULL,
	[Address]			   NVARCHAR(256)   NULL,
    [Instructions]         NVARCHAR (MAX)  NULL,
    [Date]                 DATETIME2 (7)   DEFAULT (sysutcdatetime()) NOT NULL,
    [Email]                NVARCHAR (250)  NULL,
	[EmailSent]			   DATETIME2 (7)   NULL ,
    [Total]                NUMERIC (18, 2) DEFAULT 0 NOT NULL,

	-- Temporal Table Settings
	[SysStartTime] DATETIME2 (0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT DF_PurchaseOrders_SysStartTime DEFAULT GETDATE(),
	[SysEndTime] DATETIME2 (0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT DF_PurchaseOrders_SysEndTime DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
	PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime]),

    CONSTRAINT [PK_PurchaseOrders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PurchaseOrders_WorkOrderTasks] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[WorkOrderTasks] ([Id])
)
WITH (SYSTEM_VERSIONING = ON(HISTORY_TABLE=[dbo].[PurchaseOrdersHistory], DATA_CONSISTENCY_CHECK=ON));








GO

CREATE INDEX [IX_PurchaseOrders_TaskId] ON [dbo].[PurchaseOrders] ([TaskId])

GO

CREATE INDEX [IX_PurchaseOrders_EmailSent] ON [dbo].[PurchaseOrders] ([EmailSent])
