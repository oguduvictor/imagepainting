﻿CREATE TABLE [dbo].[Costs] (
    [Id]            INT        IDENTITY (1, 1)	NOT NULL,
	[BuilderId]		INT							NOT NULL,
    [CostItemId]    INT	NOT NULL,
    [Amount]		NUMERIC (9, 2) DEFAULT 0	NOT NULL, 
    CONSTRAINT [PK_Costs] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Costs_CostItems] FOREIGN KEY ([CostItemId]) REFERENCES [CostItems]([Id]),
	CONSTRAINT [FK_Costs_Builders] FOREIGN KEY ([BuilderId]) REFERENCES [Builders]([Id]),
	CONSTRAINT UNQ_BuilderId_Type UNIQUE(BuilderId, [CostItemId])  
);

