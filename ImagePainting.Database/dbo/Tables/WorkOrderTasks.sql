﻿CREATE TABLE [dbo].[WorkOrderTasks]
(
	[Id] BIGINT NOT NULL IDENTITY, 
    [Name] NVARCHAR(256) NOT NULL, 
	[Amount] NUMERIC(8,2) NOT NULL DEFAULT 0,
	[IsLumpSum] BIT NOT NULL DEFAULT 0,
    [TaskItems] NVARCHAR(MAX) NOT NULL,
	[WorkOrderId] BIGINT NOT NULL,
	[CrewId] NVARCHAR(128) NULL,
	[SchemaId] INT NOT NULL,
	[PreWalkTaskId] BIGINT NULL,
	[CompletionWalkTaskId] BIGINT NULL,
	[ScheduleDate] DATETIME2 NOT NULL,
	[ScheduleDateEnd] DATETIME2 NULL,
	[DateConfirmed] BIT NOT NULL DEFAULT 0,
	[Status] INT NOT NULL DEFAULT 0,
	[TaskType] INT NOT NULL DEFAULT 0,
	[Completed] DATETIME2 NULL,
	[CompletedById] NVARCHAR(128) NULL,
	[PreceedingTaskCompleted] BIT NOT NULL DEFAULT 0,
    [SucceedingTaskCompleted] BIT NOT NULL DEFAULT 0,
	[JobInfoConfirmed] BIT NOT NULL DEFAULT 0,
	[IsDeferred] BIT NULL DEFAULT 0,
	[Created] DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL,
    [Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [CreatedById] NVARCHAR(128) NOT NULL, 
    [ModifiedById] NVARCHAR(128) NOT NULL, 
    [EmailSent] DATETIME2 NULL, 
    [Unlocked] DATETIME2 NULL, 
    [UnlockedById] NVARCHAR(128) NULL, 
    [IncludeBase] BIT NOT NULL DEFAULT 0, 

	-- Temporal Table Settings
	[SysStartTime] DATETIME2 (0) GENERATED ALWAYS AS ROW START HIDDEN CONSTRAINT DF_WorkOrderTasks_SysStartTime DEFAULT GETDATE(),
	[SysEndTime] DATETIME2 (0) GENERATED ALWAYS AS ROW END HIDDEN CONSTRAINT DF_WorkOrderTasks_SysEndTime DEFAULT CONVERT(DATETIME2 (0), '9999-12-31 23:59:59'),
	PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime]),

    CONSTRAINT [PK_WorkOrderTasks] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_WorkOrderTasks_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]), 
    CONSTRAINT [FK_WorkOrderTasks_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id]),
	CONSTRAINT [FK_WorkOrderTasks_WorkOrders] FOREIGN KEY ([WorkOrderId]) REFERENCES [WorkOrders]([Id]),
	CONSTRAINT [FK_WorkOrderTasks_UserInfo_Crew] FOREIGN KEY ([CrewId]) REFERENCES [UserInfo]([Id]),
	CONSTRAINT [FK_WorkOrderTasks_UserInfo_CompletedById] FOREIGN KEY ([CompletedById]) REFERENCES [UserInfo]([Id]),
	CONSTRAINT [FK_WorkOrderTasks_UserInfo_UnlockedById] FOREIGN KEY ([UnlockedById]) REFERENCES [UserInfo]([Id]), 
)
WITH (SYSTEM_VERSIONING = ON(HISTORY_TABLE=[dbo].[WorkOrderTasksHistory], DATA_CONSISTENCY_CHECK=ON))
GO

CREATE INDEX [IX_WorkOrderTasks_WorkOrderId] ON [dbo].[WorkOrderTasks] ([WorkOrderId])
GO

CREATE INDEX [IX_WorkOrderTasks_PreWalkTaskId] ON [dbo].[WorkOrderTasks] ([PreWalkTaskId])

GO

CREATE INDEX [IX_WorkOrderTasks_CompletionWalkTaskId] ON [dbo].[WorkOrderTasks] ([CompletionWalkTaskId])

GO

CREATE INDEX [IX_WorkOrderTasks_SchemaId] ON [dbo].[WorkOrderTasks] ([SchemaId])
