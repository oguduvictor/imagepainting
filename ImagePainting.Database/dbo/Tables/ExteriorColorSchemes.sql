﻿CREATE TABLE [dbo].[ExteriorColorSchemes]
(
	[Id] INT NOT NULL  IDENTITY (1, 1), 
	[ColorCode] NVARCHAR(50) NOT NULL, 
	[ExteriorBase] NVARCHAR(50) NOT NULL,
    [ExteriorTrim] NVARCHAR(50) NULL, 
    [FrontDoor] NVARCHAR(50) NULL, 
	[GarageDoor] NVARCHAR(50) NULL,
    [Shutters] NVARCHAR(50) NULL, 
    [Corbels] NVARCHAR(50) NULL, 
    [Brackets] NVARCHAR(50) NULL, 
    [HardiSiding] NVARCHAR(50) NULL, 
    [ShakeSiding] NVARCHAR(50) NULL, 
    [BoardAndBatton] NVARCHAR(50) NULL, 
    [Louvre] NVARCHAR(50) NULL, 
    [AccentBase] NVARCHAR(50) NULL, 
    [Stain] NVARCHAR(50) NULL, 
	[Note] NVARCHAR(MAX) NULL, 
    [Facia] NVARCHAR(50) NULL, 
    [Soffit] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_ExteriorColorSchemes] PRIMARY KEY ([Id]), 
    CONSTRAINT [AK_ExteriorColorSchemes_ColorCode] UNIQUE ([ColorCode]) 
)
