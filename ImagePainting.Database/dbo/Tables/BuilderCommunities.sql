﻿CREATE TABLE [dbo].[BuilderCommunities] (
    [CommunityId] INT NOT NULL,
    [BuilderId]   INT NOT NULL,
    CONSTRAINT [PK_BuilderCommunities] PRIMARY KEY CLUSTERED ([CommunityId] ASC, [BuilderId] ASC),
    CONSTRAINT [FK_BuilderCommunities_Builders] FOREIGN KEY ([BuilderId]) REFERENCES [dbo].[Builders] ([Id]),
    CONSTRAINT [FK_BuilderCommunities_Communities] FOREIGN KEY ([CommunityId]) REFERENCES [dbo].[Communities] ([Id])
);

