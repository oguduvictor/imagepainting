﻿CREATE TABLE [dbo].[JobInfos]
(
	[Id] BIGINT NOT NULL IDENTITY, 
	[PlanId] INT NULL,
	[ElevationId] INT NULL,
    [FirstFloorSqft] NUMERIC(18, 2) NOT NULL DEFAULT 0, 
    [SecondFloorSqft] NUMERIC(18, 2) NOT NULL DEFAULT 0, 
    [GarageSqft] NUMERIC(18, 2) NOT NULL DEFAULT 0, 
    [LanaiSqft] NUMERIC(18, 2) NOT NULL DEFAULT 0, 
	[JobCostItems] NVARCHAR(MAX) NULL,
	[PaintStoreId] BIGINT NULL,
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [InteriorColors] NVARCHAR(MAX) NULL, 
    [ExteriorColors] NVARCHAR(MAX) NULL, 
    [Confirmed] BIT NOT NULL DEFAULT 0, 
    [Note] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_JobInfos] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_JobInfos_Elevations] FOREIGN KEY ([ElevationId]) REFERENCES [Elevations]([Id]) 
		ON DELETE SET NULL,
    CONSTRAINT [FK_JobInfos_Stores] FOREIGN KEY (PaintStoreId) REFERENCES [Stores]([Id])
)
