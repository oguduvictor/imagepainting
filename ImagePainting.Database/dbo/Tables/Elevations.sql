﻿CREATE TABLE [dbo].[Elevations]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
    [PlanId] INT NOT NULL, 
    [FirstFloorSqft] NUMERIC(8, 2) NULL DEFAULT 0, 
    [SecondFloorSqft] NUMERIC(8, 2) NULL DEFAULT 0, 
    [GarageSqft] NUMERIC(8, 2) NULL DEFAULT 0, 
    [EntrySqft] NUMERIC(8, 2) NULL DEFAULT 0, 
    [LanaiSqft] NUMERIC(8, 2) NULL DEFAULT 0, 
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [CreatedById] NVARCHAR(128) NOT NULL, 
    [ModifiedById] NVARCHAR(128) NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Deleted] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Elevations_Plans] FOREIGN KEY ([PlanId]) REFERENCES [Plans]([Id]), 
    CONSTRAINT [FK_Elevations_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]), 
    CONSTRAINT [FK_Elevations_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id]) 
)
