﻿CREATE TABLE [dbo].[SystemAlerts]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
    [ParentId] BIGINT NOT NULL, 
	[ParentType] INT NOT NULL,
	[TargetObjectId] UNIQUEIDENTIFIER NULL,
    [Content] NVARCHAR(MAX) NOT NULL, 
    [AlertType] INT NOT NULL, 
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [CreatedById] NVARCHAR(128) NOT NULL, 
    [Resolved] DATETIME2 NULL, 
    [ResolvedById] NVARCHAR(128) NULL, 
    CONSTRAINT [PK_SystemAlerts] PRIMARY KEY ([Id])
)

GO

CREATE INDEX [IX_SystemAlerts_ParentId_ParentType] ON [dbo].[SystemAlerts] ([ParentId], [ParentType])
