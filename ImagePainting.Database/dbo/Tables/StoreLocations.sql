﻿CREATE TABLE [dbo].[StoreLocations] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [StoreId]           BIGINT         NOT NULL,
	[LocationId]        NVARCHAR (50)  NULL,
    [StoreAddress]      NVARCHAR (256) NOT NULL,
    [StoreZipCode]      NVARCHAR (5)   NOT NULL,
    [StoreCity]         NVARCHAR (50)  NOT NULL,
    [StoreContactName]  NVARCHAR (256) NULL,
    [StoreContactPhone] NVARCHAR (16)  NULL,
    [StoreContactEmail] NVARCHAR (128) NOT NULL,
    [Deleted] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK__StoreLoc__3214EC07D48DB427] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StoreLocations_Stores] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Stores] ([Id])
);




