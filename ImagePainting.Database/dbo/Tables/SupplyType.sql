﻿CREATE TABLE [dbo].[SupplyType] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (50) NOT NULL,
    [SupplyCategory] INT           NOT NULL,
    CONSTRAINT [PK_SupplyType_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SupplyType_SupplyCategory] FOREIGN KEY ([SupplyCategory]) REFERENCES [dbo].[SupplyCategory] ([Id])
);


