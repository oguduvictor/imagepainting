﻿CREATE TABLE [dbo].[CommunityPlans] (
	[CommunityId] INT NOT NULL,
	[PlanId]   INT NOT NULL,
	CONSTRAINT [PK_CommunityPlans] PRIMARY KEY CLUSTERED ([CommunityId] ASC, [PlanId] ASC),
	CONSTRAINT [FK_CommunityPlans_Communities] FOREIGN KEY ([CommunityId]) REFERENCES [dbo].[Communities] ([Id]),
	CONSTRAINT [FK_CommunityPlans_Plans] FOREIGN KEY ([PlanId]) REFERENCES [dbo].[Plans] ([Id])
);
