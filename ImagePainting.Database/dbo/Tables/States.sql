﻿CREATE TABLE [dbo].[States] (
    [Code]      NCHAR (2)     NOT NULL,
    [Name]		NVARCHAR (28) NOT NULL, 
    CONSTRAINT [PK_States] PRIMARY KEY ([Code])
);

