﻿CREATE TABLE [dbo].[ExtraJobInfos]
(
	[Id]  BIGINT IDENTITY(1, 1) NOT NULL,
	[ExtraItems] NVARCHAR(MAX) NULL,
	[Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [CreatedById] NVARCHAR(128) NOT NULL, 
    [Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    [ModifiedById] NVARCHAR(128) NOT NULL, 
    [Note] NVARCHAR(MAX) NULL, 
    CONSTRAINT [PK_ExtraJobInfos] PRIMARY KEY ([Id]), 
	CONSTRAINT [FK_ExtraJobInfos_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]), 
    CONSTRAINT [FK_ExtraJobInfos_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id])
)
