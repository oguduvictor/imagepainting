﻿CREATE TABLE [dbo].[Plans]
(
	[Id] INT NOT NULL IDENTITY, 
    [BuilderId] INT NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Stories] INT NOT NULL DEFAULT 1, 
	[Created] DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL,
	[Modified] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
	[CreatedById] NVARCHAR(128) NOT NULL, 
	[ModifiedById] NVARCHAR(128) NOT NULL, 
    [Deleted] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Plans_Builders] FOREIGN KEY ([BuilderId]) REFERENCES [Builders]([Id])
		ON DELETE CASCADE, 
    CONSTRAINT [PK_Plans] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_Plans_UserInfo_Created] FOREIGN KEY ([CreatedById]) REFERENCES [UserInfo]([Id]), 
    CONSTRAINT [FK_Plans_UserInfo_Modified] FOREIGN KEY ([ModifiedById]) REFERENCES [UserInfo]([Id])
)
