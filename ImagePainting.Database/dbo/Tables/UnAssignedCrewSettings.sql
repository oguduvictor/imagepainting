﻿CREATE TABLE [dbo].[UnAssignedCrewSettings] (
    [UserId]        NVARCHAR (128) NOT NULL,
    [ScheduledDate] DATETIME2 (7)  NOT NULL,
    [Emailed]       BIT            NOT NULL DEFAULT 0,
    [Busy]          BIT            NOT NULL DEFAULT 0,
    CONSTRAINT [PK_UnAssignedCrewSettings] PRIMARY KEY CLUSTERED ([UserId] ASC, [ScheduledDate] ASC)
);