﻿CREATE TABLE [dbo].[CostItems]
(
	[Id] INT NOT NULL IDENTITY, 
    [Name] VARCHAR(50) NOT NULL, 
	[IsExtra] BIT NOT NULL DEFAULT 1,
    [IsLumpSum] BIT NOT NULL DEFAULT 0, 
    [LaborCost] NUMERIC(8, 2) NOT NULL DEFAULT 0, 
	[PercentOfTotalSize] NUMERIC(5, 2) NULL DEFAULT 100, 
    [TaskType] INT NULL, 
    [Order] INT NOT NULL DEFAULT 0, 
	[PercentLivingSqft] NUMERIC(5, 2) NULL DEFAULT ((0)), 
    [PercentGarageSqft] NUMERIC(5, 2) NULL DEFAULT ((0)), 
    CONSTRAINT [PK_CostItems] PRIMARY KEY ([Id])
)
