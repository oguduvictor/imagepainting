﻿CREATE TABLE [dbo].[UserInfo] (
    [Id]            NVARCHAR(128)            NOT NULL,
    [Active]        BIT DEFAULT 1 NOT NULL,
    [FederalIdNo] NVARCHAR (32)  NULL,
    [CompanyName]  NVARCHAR (64)  NULL,
    [FirstName]    NVARCHAR (32)  DEFAULT '' NOT NULL,
    [LastName]     NVARCHAR (32)  DEFAULT '' NOT NULL,
    [Title]         NVARCHAR (32)  NULL,
    [Address]       NVARCHAR (128) NULL,
    [City]          NVARCHAR (32)  DEFAULT 'Tampa' NOT NULL,
    [State]         NCHAR (2)   DEFAULT 'FL' NOT NULL,
    [Zipcode]       NVARCHAR (5)   NULL,
    [HomePhone]    NVARCHAR (16)  NULL,
    [WorkPhone]    NVARCHAR (16)  NULL,
    [CellPhone]    NVARCHAR (16)  NULL,
    [Email]         NVARCHAR (128) NULL,
    [BirthDate]    SMALLDATETIME  NULL,
    [Note]         NVARCHAR (MAX) NULL,
    [Created]     DATETIME2 DEFAULT SYSUTCDATETIME() NOT NULL,
	[Deleted] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_UserInfo_AspNetUser] FOREIGN KEY ([Id]) REFERENCES [dbo].[AspNetUsers] ([Id]), 
    CONSTRAINT [PK_UserInfo] PRIMARY KEY ([Id])
);


GO
