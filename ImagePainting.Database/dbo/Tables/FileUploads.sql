﻿CREATE TABLE [dbo].[FileUploads]
(
	[Id] BIGINT NOT NULL IDENTITY, 
    [Name] NVARCHAR(256) NOT NULL, 
    [OwnerId] BIGINT NOT NULL, 
    [OwnerType] INT NOT NULL, 
    [Content] VARBINARY(MAX) NOT NULL, 
    [ContentType] NVARCHAR(256) NOT NULL DEFAULT '', 
    [CreatedById] NVARCHAR(128) NOT NULL, 
    [Created] DATETIME2 NOT NULL DEFAULT SYSUTCDATETIME(), 
    CONSTRAINT [PK_FileUploads] PRIMARY KEY ([Id]), 
    CONSTRAINT [FK_FileUploads_AspNetUsers] FOREIGN KEY ([CreatedById]) REFERENCES [AspNetUsers]([Id])
)

GO

CREATE INDEX [IX_FileUploads_OwnerId-OwnerType] ON [dbo].[FileUploads] ([OwnerId],[OwnerType])
