﻿using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Domain.Services;
using ImagePainting.Site.Domain.TaskManagement;
using LightInject;

namespace ImagePainting.Site.Domain
{
    public class DomainIoCModule : ICompositionRoot
    {
        public void Compose(IServiceRegistry container)
        {
            container.Register<ICommunityComposerService, CommunityComposerService>(new PerScopeLifetime());
            container.Register<IProjectionComposerService, ProjectionComposerService>(new PerScopeLifetime());
            container.Register<IWorkOrderComposerService, WorkOrderComposerService>(new PerScopeLifetime());
            container.Register<IUserInfoService, UserInfoService>(new PerScopeLifetime());
            container.Register<ITaskComposerService, TaskComposerService>(new PerScopeLifetime());
            container.Register<IPurchaseOrderService, PurchaseOrderService>(new PerScopeLifetime());
            container.Register<IEmailService, EmailService>(new PerScopeLifetime());
            container.Register<IPaintStoreService, PaintStoreService>(new PerScopeLifetime());
            container.Register<IWorkOrderTaskManager, WorkOrderTaskManager>(new PerScopeLifetime());
            container.Register<IWorkOrderMailer, WorkOrderMailer>(new PerScopeLifetime());
            container.Register<IUserInfoMailer, UserInfoMailer>(new PerScopeLifetime());
            container.Register<IPurchaseOrderService, PurchaseOrderService>(new PerScopeLifetime());
            container.Register<WorkOrderTaskUpdater>(new PerScopeLifetime());
            container.Register<ISystemAlertService, SystemAlertService>(new PerScopeLifetime());
            container.Register<ITaskPdfGenerator, TaskPdfGenerator>(new PerScopeLifetime());
            container.Register<ICacheService, MemoryCacheService>(new PerContainerLifetime());
            container.Register<IUserCacheService, UserCacheService>(new PerScopeLifetime());
        }
    }
}
