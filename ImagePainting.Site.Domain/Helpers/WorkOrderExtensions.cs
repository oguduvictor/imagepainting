﻿using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Domain
{
    public static class WorkOrderExtensions
    {
        public static decimal EstimateItemQuantity(this CostItem costItem, JobInfo jobInfo)
        {
            if (costItem.PercentLivingSqft.HasValue && costItem.PercentLivingSqft > 0)
            {
                return costItem.PercentLivingSqft.Value / 100 * jobInfo.LivingSqft;
            }

            if (costItem.PercentGarageSqft.HasValue && costItem.PercentGarageSqft > 0)
            {
                return costItem.PercentGarageSqft.Value / 100 * jobInfo.GarageSqft;
            }

            if (costItem.PercentOfTotalSize.HasValue && costItem.PercentOfTotalSize > 0)
            {
                return costItem.PercentOfTotalSize.Value / 100 * jobInfo.TotalSize;
            }

            return 0;
        }

        public static Guid GenerateBaseIdForWorkItem(long workOrderTaskId, long workOrderId)
        {
            var workOrderSection = workOrderId.ToString().PadLeft(12, '0');
            var taskSection = ((int)workOrderId).ToString().PadLeft(8, '0');
            var targetObjectId = $"000000000000{taskSection}{workOrderSection}";

            return Guid.Parse(targetObjectId);
        }
    }
}
