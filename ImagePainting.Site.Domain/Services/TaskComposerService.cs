﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Common.ServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class TaskComposerService : ITaskComposerService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ITaskSchemaRepository _taskSchemaRepository;
        private readonly IWorkOrderTaskManager _taskManager;
        private readonly IUserContext _userContext;
        private readonly IWorkOrderMailer _workOrderMailer;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IUserInfoRepository _userInfoRepository;
        private readonly IUnAssignedCrewSettingRepository _assignedCrewSettingRepository;
        private readonly IBus _bus;
        private readonly ITaskPdfGenerator _taskPdfGenerator;
        private readonly IFileUploadRepository _fileUploadRepository;
        private readonly IWorkOrderComposerService _workOrderComposerService;
        private readonly IUserCacheService _userCacheService;

        public TaskComposerService(
            ITaskRepository taskRepository,
            ITaskSchemaRepository taskSchemaRepository,
            IWorkOrderTaskManager taskManager,
            IUserContext userContext,
            IWorkOrderMailer workOrderMailer,
            IPurchaseOrderRepository purchaseOrderRepository,
            IUserInfoRepository userInfoRepository,
            IUnAssignedCrewSettingRepository assignedCrewSettingRepository,
            IBus bus, ITaskPdfGenerator taskPdfGenerator, 
            IFileUploadRepository fileUploadRepository, 
            IWorkOrderComposerService workOrderComposerService,
            IUserCacheService userCacheService)
        {
            _taskRepository = taskRepository;
            _taskSchemaRepository = taskSchemaRepository;
            _taskManager = taskManager;
            _userContext = userContext;
            _workOrderMailer = workOrderMailer;
            _purchaseOrderRepository = purchaseOrderRepository;
            _userInfoRepository = userInfoRepository;
            _assignedCrewSettingRepository = assignedCrewSettingRepository;
            _bus = bus;
            _taskPdfGenerator = taskPdfGenerator;
            _fileUploadRepository = fileUploadRepository;
            _workOrderComposerService = workOrderComposerService;
            _userCacheService = userCacheService;
        }

        public Task<List<WorkOrderTaskSchema>> GetTaskSchemas(bool isSummary = false, bool mainTaskOnly = false)
        {
            return _taskSchemaRepository.GetTaskSchemas(isSummary, mainTaskOnly);
        }

        public async Task<IEnumerable<WorkOrderTask>> GetWorkOrderTasks(TaskSearchCriteria searchCriteria)
        {
            var userId = _userContext.UserId;
            var isAdmin = _userContext.IsAdmin();

            searchCriteria.UserId = isAdmin ? null : userId;

            var workOrderTasks = await _taskManager.GetTasks(searchCriteria);

            if (!workOrderTasks.IsNullOrEmpty())
            {
                var taskIds = workOrderTasks.Select(x => x.Id).ToArray();
                var tasksWithEmailedPOs = await _purchaseOrderRepository.GetEmailedPurchaseOrders(taskIds);

                if (!tasksWithEmailedPOs.IsNullOrEmpty())
                {
                    foreach (var task in workOrderTasks)
                    {
                        var hasAnyPurchaseOrderEmailed = tasksWithEmailedPOs.Any(x => x.TaskId == task.Id);
                        task.SetOrderPlaced(hasAnyPurchaseOrderEmailed);
                    }
                }
            }

            return workOrderTasks;
        }

        public async Task<IList<TaskView>> SearchWorkOrderTasks(TaskSearchCriteria searchCriteria)
        {
            var userId = _userContext.UserId;
            var isAdmin = _userContext.IsAdmin();

            searchCriteria.UserId = isAdmin ? null : userId;

            return await _taskManager.SearchTasks(searchCriteria);
        }

        public async Task<WorkOrderTaskSchema> SaveTaskSchema(WorkOrderTaskSchema task)
        {
            var userId = _userContext.UserId;

            task.Modified = DateTime.UtcNow;
            task.ModifiedById = userId;

            if (task.Id == default(int))
            {
                task.Created = DateTime.UtcNow;
                task.CreatedById = userId;

                return await _taskSchemaRepository.CreateTaskSchema(task);
            }
            
            var dbTask = await _taskSchemaRepository.GetTaskSchema(task.Id);
            var addedTaskItemNames = task.TaskItems.Where(n => !dbTask.TaskItems.Any(o => o.Name == n.Name)).Select(t => t.Name).ToList();
            var deletedTaskItemNames = dbTask.TaskItems.Where(n => !task.TaskItems.Any(o => o.Name == n.Name)).Select(t => t.Name).ToList();

            dbTask.MergeShallow(task, nameof(task.Created), nameof(task.CreatedById));

            await _taskSchemaRepository.UpdateTaskSchema(dbTask);

            await _bus.PublishAsync(new TaskSchemaUpdatedEvent(userId, dbTask.Id, addedTaskItemNames, deletedTaskItemNames));

            return dbTask;
        }

        public async Task<WorkOrderTaskSchema> GetWorkSchemaTask(int taskId)
        {
            return taskId == default(int) ? new WorkOrderTaskSchema()
                                          : await _taskSchemaRepository.GetTaskSchema(taskId);
        }

        public async Task SendWorkOrderMails(IEnumerable<long> ids, string note)
        {
            if (ids.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(ids));
            }

            var searchCriteria = new TaskSearchCriteria
            {
                IncludeBuilder = true,
                IncludeCommunity = true,
                IncludeCrew = true,
                IncludeJobInfo = true,
                IncludeWorkOrder = true,
                TaskIds = ids
            };

            var workordertasks = await _taskRepository.GetTasks(searchCriteria);
            workordertasks = workordertasks.OrderBy(x => ids.ToList().IndexOf(x.Id)).ToList();
            await _workOrderMailer.EmailSchedules(workordertasks, note);
            
            foreach(var task in workordertasks)
            {
                if (task.Crew.IsNull())
                {
                    continue;
                }

                task.EmailSent = DateTime.UtcNow;
            }
            
            await _taskRepository.UpdateWorkOrderTasks(workordertasks.ToArray());
        }

        public async Task DeleteTaskSchema(int id)
        {
            var taskSchema = await GetWorkSchemaTask(id);

            taskSchema.Delete();

            await _taskSchemaRepository.UpdateTaskSchema(taskSchema);
        }

        public async Task SendNoTaskEmailAsync(IEnumerable<string> userIds, DateTime date, string note)
        {
            var unAssignedCrews = await _userInfoRepository.GetUsersAsync(userIds);
            await _workOrderMailer.EmailFreeCrewsSchedule(unAssignedCrews, date, note);
            
            await UpdateUnAssignedCrewSetting(userIds, date, email: true);
        }

        public async Task<IDictionary<DateTime, IEnumerable<UserInfoSummary>>> GetUnAssignedSubContractors(DateTimeRange dateRange)
        {
            var allUsers = _userCacheService.GetUsers();
            var assignedSchedules = await _taskRepository.GetAssignedUserIds(dateRange);
            var subContractors = allUsers.Where(x => x.Active && x.Roles.Contains(UserRole.SubContractor)).ToList();
            var unAssignedSubContractorWithDate = new Dictionary<DateTime, IEnumerable<UserInfoSummary>>(subContractors.Count());
            var dates = dateRange.ToRange();

            foreach (var date in dates)
            {
                var assignedUserIds = assignedSchedules
                                           .Where(x => x.Key == date)
                                           .Select(x => x.Value).ToList();

                var unassignedContractors = subContractors.Where(x => !assignedUserIds.Contains(x.Id)).ToList();

                unAssignedSubContractorWithDate[date] = unassignedContractors;
            }

            return unAssignedSubContractorWithDate;
        }

        public async Task<IList<UnAssignedCrewSetting>> GetEmailedUnAssignedCrewsInRange(DateTimeRange dateRange)
        {
            return await _assignedCrewSettingRepository.GetSettings(dateTimeRange: dateRange);
        }

        public async Task UpdateUnAssignedCrewSetting(IEnumerable<string> userIds, DateTime date, bool? email = null, bool? busy = null)
        {
            var newDate = new DateTimeRange
            {
                BeginDateTime = date,
                EndDateTime = date
            };

            var crewIds = userIds as string[] ?? userIds.ToArray(); 
            var savedUnAssignedCrewSettings = await _assignedCrewSettingRepository.GetSettings(newDate, crewIds);
            var nonExistingSettingIds = crewIds.Except(savedUnAssignedCrewSettings.Select(x => x.UserId));
            var existingUserSettings = savedUnAssignedCrewSettings.Where(x => !nonExistingSettingIds.Contains(x.UserId));

            if (!nonExistingSettingIds.IsNullOrEmpty())
            {
                var model = new UnAssignedCrewSetting[nonExistingSettingIds.Count()];

                for (int i = 0, len = nonExistingSettingIds.Count(); i < len; i++)
                {
                    var id = nonExistingSettingIds.ElementAt(i);

                    model[i] = new UnAssignedCrewSetting(id, date, email ?? false, busy ?? false);
                }

                await _assignedCrewSettingRepository.AddUnAssignedCrewSettings(model);
            }

            if (!existingUserSettings.IsNullOrEmpty())
            {
                existingUserSettings.ForEach(settings =>
                {
                    settings.Emailed = email ?? settings.Emailed;
                    settings.Busy = busy ?? settings.Busy;
                });

                await _assignedCrewSettingRepository.UpdateSetting(existingUserSettings.ToArray());
            }
        }

        public async Task<UserInfo> GetBusyUser(string crewId, DateTime date)
        {
            var crewSetting = await _assignedCrewSettingRepository.GetSetting(date, crewId);

            if (crewSetting.IsNull() || !crewSetting.Busy)
            {
                return null;
            }

            return await _userInfoRepository.GetUserAsync(crewId);
        }

        public async Task CreatePdf(long taskId, string encodedSignature)
        {
            var task = await GetTaskForPdf(taskId);
            var oldSignature = await _fileUploadRepository.GetFileUploads(taskId, FileOwnerType.Signature);

            
            var content = _taskPdfGenerator.CreateTaskPdf(task, encodedSignature);

            var pdfFile = new FileUpload
            {
                Content = content,
                ContentType = "application/pdf",
                Name = $"EPO_{DateTime.UtcNow.ToString("yyyyMMdd")}.pdf",
                OwnerId = taskId,
                OwnerType = FileOwnerType.SignedDocument, 
                CreatedById = _userContext.UserId,
                Created = DateTime.UtcNow,
            };

            await _fileUploadRepository.CreateFileUpload(pdfFile);
            await UploadSignature(encodedSignature, taskId);

            if (!oldSignature.IsNullOrEmpty())
            {
                await  _fileUploadRepository.DeleteFileUpload(oldSignature.First().Id);
            }
        }

        public async Task<string> GetTaskPdfTemplate(long taskId)
        {
            var task = await GetTaskForPdf(taskId);

            return _taskPdfGenerator.GetTaskPdfTemplate(task);

        }

        public async Task<byte[]> GetTaskSignedPdf(long taskId)
        {
            var task = await GetTaskForPdf(taskId);
            var billingInfo = new Dictionary<string, decimal>();
            string signature;
            
            var workItems = _workOrderComposerService.GetWorkItems(taskId);
            billingInfo.Add(SiteContants.LABOR_COST, workItems.Item.Where(x => x.Selected).Sum(x => x.TotoalPrice));

            var purchaseOrders = await _purchaseOrderRepository.GetAll(taskId, includeItems: true);
            billingInfo.Add(SiteContants.MATERIAL_COST, purchaseOrders.Sum(x => x.Total));

            var savedSignature = await _fileUploadRepository.GetFirstOrDefault(taskId, FileOwnerType.Signature);
            signature = savedSignature.IsNull() ? string.Empty : Encoding.ASCII.GetString(savedSignature.Content);

            var signedDate = signature == string.Empty ? DateTime.UtcNow : savedSignature.Created;

            return _taskPdfGenerator.CreateTaskPdf(task, signature, completeCopy: true, billingInfo: billingInfo, signedDate: signedDate);
        }

        private async Task UploadSignature(string encodedSignature, long taskId)
        {
            var signaturefile = new FileUpload
            {
                Content = Encoding.ASCII.GetBytes(encodedSignature),
                ContentType = string.Empty,
                Name = "Customer Signature",
                OwnerType = FileOwnerType.Signature,
                CreatedById = _userContext.UserId,
                OwnerId = taskId,
                Created = DateTime.UtcNow
            };

            await _fileUploadRepository.CreateFileUpload(signaturefile);
        }

        private  Task<WorkOrderTask> GetTaskForPdf( long taskId)
        {
            return _taskRepository.GetTask(taskId, includeCrew: true, includeWorkOrder: true, includBuilderAndCommunity: true);
        }

        public Task<IEnumerable<WorkOrderTask>> GetDeferredTasks()
        {
            var searchCriteria = new TaskSearchCriteria
            {
                IsDeferred = true,
                IncludeWorkOrder = true,
                IsMainTask = true
            };

            return _taskRepository.GetTasks(searchCriteria);
        }

        public async Task UnDeferTasks(params long[] taskIds)
        {
            foreach (var id in taskIds)
            {
                await SetTaskDeferState(id, false);
            }
        }

        public async Task SetTaskDeferState(long taskId, bool? deferred = null)
        {
            var mainTask = await _taskRepository.GetMainTask(taskId);

            await _taskManager.EvaluatePermissions(mainTask);

            if (!CanDefer(mainTask))
            {
                return;
            }

            mainTask.Deferred = deferred ?? !mainTask.Deferred;

            if (!mainTask.PreWalkTask.IsNull())
            {
                mainTask.PreWalkTask.Deferred = mainTask.Deferred;
            }

            if (!mainTask.CompletionWalkTask.IsNull())
            {
                mainTask.CompletionWalkTask.Deferred = mainTask.Deferred;
            }

            await _taskRepository.UpdateWorkOrderTasks(mainTask);
        }

        private bool CanDefer(WorkOrderTask mainTask)
        {
            var defer = new List<bool>(3);

            if (mainTask.Permission.CanDefer)
            {
                defer.Add(mainTask.Permission.CanDefer);
            }

            if(!mainTask.PreWalkTask.IsNull()) {
                defer.Add(mainTask.PreWalkTask.Permission.CanDefer);
            }

            if(!mainTask.CompletionWalkTask.IsNull())
            {
                defer.Add(mainTask.CompletionWalkTask.Permission.CanDefer);
            }
            return defer.All(x => x == true);
        }

    }
}
