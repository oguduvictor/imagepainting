﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class ProjectionComposerService : IProjectionComposerService
    {
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;

        public ProjectionComposerService(IWorkOrderRepository workOrderRepository, ITaskRepository taskRepository, IPurchaseOrderRepository purchaseOrderRepository)
        {
            _workOrderRepository = workOrderRepository;
            _taskRepository = taskRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
        }
        
        public async Task<IEnumerable<MaterialProjection>> GetWorkOrder(long id)
        {
            var projection  = new List<MaterialProjection>();
            var task = await _taskRepository.GetTasksByWorkOrderId(id, true, false, true);
            var itemProjections = task.Select(y => new MaterialProjection
                                        {
                                            WorkOrderTask = y
                                        });

            projection.AddRange(itemProjections);

            return projection;
        }

        public async Task<LaborProjection> GetLaborProjection(long workOrderId)
        {
            var taskSearch = new TaskSearchCriteria { WorkOrderId = workOrderId, IsMainTask = true };
            var mainTasks = await _taskRepository.GetTasks(taskSearch);
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true, includeExtraJobInfo: true);
            var extraItems = workOrder.ExtraJobInfo.ExtraItems;
            
            var projection = new LaborProjection
            {
                BasePricings = new List<TaskPricingViewModel>(),
                ExtraPricings = new List<TaskPricingViewModel>()
            };

            mainTasks.ForEach(task =>
            {
                var allExtraItemsForTask = extraItems.Where(x => !(x.TaskIds.IsNull() || !x.TaskIds.Contains(task.Id)));

                if (task.IncludeBase)
                {
                    projection.BasePricings.Add(new TaskPricingViewModel
                    {
                        Cost = task.Amount,
                        IsLumpSum = task.IsLumpSum,
                        Name = task.Name,
                        Quantity = (int)workOrder.JobInfo.TotalSize
                    });
                }

                if (allExtraItemsForTask.IsNullOrEmpty()) return;

                allExtraItemsForTask.ForEach(extraItem =>
                {
                    projection.ExtraPricings.Add(new TaskPricingViewModel
                    {
                        Cost = extraItem.LaborCost,
                        IsLumpSum = extraItem.IsLumpSum,
                        Name = extraItem.Name,
                        Quantity = extraItem.WalkQuantity ?? 0
                    });
                });
            });

            return projection;
        }

        public Task<RevenueProjection> GetRevenueProjection(long workOrderId)
        {
            return _workOrderRepository
                    .GetWorkOrder(workOrderId, includeJobInfo: true, includeExtraJobInfo: true)
                    .ContinueWith(x =>
                    {
                        var workOrder = x.Result;

                        return new RevenueProjection
                        {
                            TotalHouseSqft = workOrder.JobInfo?.TotalSize ?? 0,
                            CostItems = workOrder.JobInfo?.JobCostItems,
                            ExtraItems = workOrder.ExtraJobInfo?.ExtraItems
                        };
                    });
        }

        public async Task<ProfitProjection> GetProfitProjection(long workOrderId)
        {
            var revenueProjection = await GetRevenueProjection(workOrderId);
            var laborProjection = await GetLaborProjection(workOrderId);
            var purchaseOrders = await _purchaseOrderRepository.GetAll(workOrderId: workOrderId);

            var sumTotal = purchaseOrders.Sum(p => p.Total);

            return new ProfitProjection
            {
                TotalRevenueProjection = revenueProjection.GrandTotal,
                TotalLaborProjection = laborProjection.GrandTotal,
                TotalPurchaseOrders = sumTotal,
            };
        }
    }
}
