﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Common.ViewModels;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class WorkOrderComposerService : IWorkOrderComposerService
    {
        private readonly ICostRepository _costRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly ITaskSchemaRepository _taskSchemaRepository;
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly IUserContext _userContext;
        private readonly IWorkOrderTaskManager _taskManager;
        private readonly IFileUploadRepository _fileRepository;
        private readonly ISystemAlertService _systemAlertService;

        public WorkOrderComposerService(
            ICostRepository costRepository,
            ITaskRepository taskRepository,
            ITaskSchemaRepository taskSchemaRepository,
            IUserContext userContext,
            IWorkOrderRepository workOrderRepository,
            IWorkOrderTaskManager workOrderTaskManager,
            IFileUploadRepository fileRepository,
            ISystemAlertService systemAlertService)
        {
            _costRepository = costRepository;
            _taskRepository = taskRepository;
            _taskSchemaRepository = taskSchemaRepository;
            _userContext = userContext;
            _workOrderRepository = workOrderRepository;
            _taskManager = workOrderTaskManager;
            _fileRepository = fileRepository;
            _systemAlertService = systemAlertService;
        }

        public async Task<WorkOrder> SaveInitialSettings(WorkflowInitalSettingsViewModel model)
        {
            var userId = _userContext.UserId;

            var workOrder = await _workOrderRepository.GetWorkOrder(model.WorkOrderId) ?? new WorkOrder();
            var originalInitialSettingNotes = workOrder.Description?.DeepClone();

            if (_userContext.IsAdmin())
            {
                workOrder.Description = JSONifyNote(workOrder.Description, model.Description);
            }

            workOrder.BuilderId = model.BuilderId;
            workOrder.CommunityId = model.CommunityId;
            workOrder.Address = model.Address;
            workOrder.Name = model.Name;
            workOrder.Lot = model.Lot;
            workOrder.Block = model.Block;
            workOrder.SuperIntendent = model.SuperIntendent;
            workOrder.CustomerRep = model.CustomerRep;
            workOrder.CustomerName = model.CustomerName;
            workOrder.CustomerEmail = model.CustomerEmail;
            workOrder.CustomerPhoneNumber = model.CustomerPhoneNumber;

            await SaveWorkOrder(workOrder, userId);

            await _systemAlertService.SaveWorkOrderNoteAlert(workOrder, originalInitialSettingNotes);

            await PopulateWorkorderTasks(workOrder);

            return workOrder;
        }

        public async Task<WorkOrderItemViewModel<WorkflowInitalSettingsViewModel>> GetInitialSettingsModel(long workOrderId)
        {
            var model = new WorkflowInitalSettingsViewModel();
            var workOrder = default(WorkOrder);

            if (workOrderId > 0)
            {
                workOrder = await _workOrderRepository.GetWorkOrder(workOrderId);
            }

            if (!workOrder.IsNull())
            {
                model.CommunityId = workOrder.CommunityId;
                model.BuilderId = workOrder.BuilderId;
                model.Address = workOrder.Address;
                model.Description = workOrder.Description;
                model.WorkOrderId = workOrderId;
                model.Block = workOrder.Block;
                model.Lot = workOrder.Lot;
                model.Name = workOrder.Name;
                model.SuperIntendent = workOrder.SuperIntendent;
                model.CustomerRep = workOrder.CustomerRep;
                model.CustomerName = workOrder.CustomerName;
                model.CustomerEmail = workOrder.CustomerEmail;
                model.CustomerPhoneNumber = workOrder.CustomerPhoneNumber;
            }

            return new WorkOrderItemViewModel<WorkflowInitalSettingsViewModel>
            {
                Item = model,
                WorkOrderCompleted = workOrder.IsNull() ? false : workOrder.Completed.HasValue
            };
        }

        public async Task<WorkOrder> GetWorkOrder(long workOrderId, bool includeTasks = false)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId);

            if (!includeTasks)
            {
                return workOrder;
            }

            await PopulateWorkorderTasks(workOrder);

            return workOrder;
        }

        public async Task<bool> WorkOrderExists(long workOrderId, string name)
        {
            return await _workOrderRepository.WorkOrderExists(workOrderId, name);
        }

        public async Task<IEnumerable<WorkOrder>> GetWorkOrders(WorkOrderQuery query)
        {
            return await _workOrderRepository.GetWorkOrders(query);
        }

        public async Task<PagedResult<WorkOrderView>> SearchWorkOrders(WorkOrderQuery query)
        {
            return await _workOrderRepository.SearchWorkOrdersView(query);
        }

        public Task<long> GetTotalWorkOrderCount(string searchString = "")
        {
            return _workOrderRepository.GetTotalWorkOrderCount(searchString);
        }

        public async Task DeleteWorkOrder(long workOrderId)
        {
            var dbWorkOrder = await _workOrderRepository.GetWorkOrder(workOrderId);

            if (!dbWorkOrder.IsNull())
            {
                dbWorkOrder.Deleted = true;

                await _workOrderRepository.UpdateWorkOrder(dbWorkOrder);

                await _fileRepository.DeleteFileUploads(new long[] { workOrderId }, FileOwnerType.WorkOrder);
            }
            else
            {
                throw new Exception("Deleting work order failed!");
            }
        }

        public Task<WorkOrderTask> GetWorkOrderTask(long taskId)
        {
            return _taskManager.GetTaskWithPermissions(taskId);
        }

        public async Task SaveWorkOrderTask(WorkOrderTask model)
        {
            var dbtask = await _taskRepository.GetTask(model.Id);
            var userId = _userContext.UserId;

            dbtask.Modified = DateTime.UtcNow;
            dbtask.ModifiedById = userId;
            dbtask.TaskItems = model.TaskItems;

            await _taskRepository.UpdateWorkOrderTasks(dbtask);
        }

        public async Task<long> CloneWorkOrder(long workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true, includeExtraJobInfo: true);
            var newWorkOrder = WorkOrderBuilder.Clone(workOrder, _userContext.UserId);

            await _workOrderRepository.CreateWorkOrder(newWorkOrder);

            if (!newWorkOrder.JobInfo.IsNull())
            {
                await SaveJobInfo(newWorkOrder.Id, newWorkOrder.JobInfo);
            }

            if (!newWorkOrder.ExtraJobInfo.IsNull())
            {
                await SaveExtraJobInfo(newWorkOrder.Id, newWorkOrder.ExtraJobInfo);
            }

            return newWorkOrder.Id;
        }

        public async Task<IEnumerable<TaskSummary>> ScheduleTask(
            long workOrderId,
            IEnumerable<ScheduleWorkOrderTask> taskModels)
        {
            await _taskManager.ScheduleTask(workOrderId, taskModels);

            return await _taskRepository.GetTaskSummaries(workOrderId, true);
        }

        public async Task<WorkOrderItemViewModel<ExtraJobInfo>> GetExtraJobInfo(long workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeExtraJobInfo: true, includeJobInfo: true);

            if (workOrder == null)
            {
                throw new InvalidOperationException($"{workOrderId} is an invalid work order ID.");
            }

            await PopulateWorkorderTasks(workOrder);

            return new WorkOrderItemViewModel<ExtraJobInfo>
            {
                Item = workOrder.ExtraJobInfo ?? new ExtraJobInfo { ExtraItems = new List<ExtraItem>() },
                WorkOrderCompleted = workOrder.Completed.HasValue,
                TaskSummaries = workOrder.Tasks ?? new List<TaskSummary>()
            };
        }

        public async Task<IEnumerable<ExtraItem>> GetAllJobExtras(long workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeExtraJobInfo: true, includeJobInfo: true);

            if (workOrder == null)
            {
                throw new InvalidOperationException($"{workOrderId} is an invalid work order ID.");
            }

            var selectedItems = workOrder.ExtraJobInfo?.ExtraItems ?? new List<ExtraItem>();
            var costs = await _costRepository.GetCosts(workOrder.BuilderId ?? 0, true);

            // sort by order
            costs.Sort((x, y) => x.CostItem.Order.CompareTo(y.CostItem.Order));

            var extraJobItems = costs.Select(cost => new ExtraItem
            {
                Name = cost.CostItem.Name,
                BuilderCost = cost.Amount,
                LaborCost = cost.CostItem.LaborCost,
                TaskType = cost.CostItem.TaskType,
                IsLumpSum = cost.CostItem.IsLumpSum,
                Quantity = (int)cost.CostItem.EstimateItemQuantity(workOrder.JobInfo),
            }).ToList();

            // add items that don't exist in the original list
            foreach (var selectedItem in selectedItems)
            {
                // This is to remove any ExtraItem TaskId with value 0
                selectedItem.TaskIds?.RemoveAll(id => id == 0);

                var item = extraJobItems.FirstOrDefault(x => x.Name == selectedItem.Name);
                if (item != null)
                {
                    item.BuilderCost = selectedItem.BuilderCost;
                    item.Quantity = selectedItem.Quantity;
                    item.WalkQuantity = selectedItem.WalkQuantity;
                    item.BuilderCost = selectedItem.BuilderCost;
                    item.TaskType = selectedItem.TaskType;
                    item.TaskIds = selectedItem.TaskIds;
                }
                else
                {
                    extraJobItems.Add(selectedItem);
                }
            }

            return extraJobItems.Distinct() ;
        }

        public async Task<ExtraJobInfo> SaveExtraJobInfo(long workOrderId, ExtraJobInfo model)
        {
            var userId = _userContext.UserId;
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeExtraJobInfo: true);
            var originalJobExtra = workOrder.ExtraJobInfo?.DeepClone();

            // needed to prevent saving null extraitems
            model.ExtraItems = model.ExtraItems ?? new List<ExtraItem>();

            // need to populate workorder with tasks for setting extraItem taskId with most recent task of same type
            await PopulateWorkorderTasks(workOrder);

            workOrder.ExtraJobInfo = workOrder.ExtraJobInfo ?? new ExtraJobInfo();
            workOrder.ExtraJobInfo.Note = JSONifyNote(workOrder.ExtraJobInfo.Note, model.Note);

            var previouslySavedExtraItems = workOrder.ExtraJobInfo?.ExtraItems ?? new List<ExtraItem>();

            var deleteExtraItems = previouslySavedExtraItems.Where(o => !model.ExtraItems.Any(n => n.Id == o.Id));
            
            UpdateExtraItems(previouslySavedExtraItems, model.ExtraItems, workOrder.Tasks.OrderByDescending(x => x.Id));

            workOrder.ExtraJobInfo.MergeShallow(model,
                nameof(model.Id),
                nameof(model.Created),
                nameof(model.CreatedById),
                nameof(model.Total),
                nameof(model.Note));
            
            workOrder.ExtraJobInfo.ExtraItems = model.ExtraItems;

            workOrder.ExtraJobInfo.Modified = DateTime.UtcNow;
            workOrder.ExtraJobInfo.ModifiedById = userId;
            
            if (workOrder.ExtraJobInfo.Id == default(int))
            {
                workOrder.ExtraJobInfo.Created = DateTime.UtcNow;
                workOrder.ExtraJobInfo.CreatedById = userId;

                var savedExtraJob = await _workOrderRepository.CreateExtraJobInfo(workOrder.Id, workOrder.ExtraJobInfo);
                await _systemAlertService.SaveJobExtraNoteAlert(workOrder, null);

                return savedExtraJob;
            }

            var updatedExtraJob = await _workOrderRepository.UpdateExtraJobInfo(workOrder.ExtraJobInfo, workOrder.Id);
            await _systemAlertService.SaveJobExtraNoteAlert(workOrder, originalJobExtra);

            // delete alerts for deleted extra items
            if (deleteExtraItems.Any())
            {
                var alerts = deleteExtraItems.Select(x => new SystemAlert {
                    TargetObjectId = x.Id,
                    AlertType = AlertType.JobExtraDifference,
                    ParentId = workOrder.Id,
                    ParentType = AlertParentType.WorkOrder
                }).ToArray();

                await _systemAlertService.DeleteAlerts(alerts);
            }

            return updatedExtraJob;
        }

        public async Task<JobInfo> GetJobInformation(long workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true).ConfigureAwait(false);

            return workOrder.JobInfo;
        }

        public async Task<WorkOrderItemViewModel<JobInfo>> GetJobInfo(long workOrderId)
        {
            var jobInfo = new JobInfo();
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true, includeBuilder:true);

            if (workOrder == null)
            {
                throw new InvalidOperationException($"{workOrderId} is an invalid work order ID.");
            }

            if (workOrder.JobInfo != null)
            {
                var itemModel = new WorkOrderItemViewModel<JobInfo>
                {
                    CommunityId = workOrder.CommunityId,
                    BuilderId = workOrder.BuilderId,
                    BuilderAbbrevation = workOrder.Builder.Abbreviation,
                    Item = workOrder.JobInfo,
                    WorkOrderCompleted = workOrder.Completed.HasValue
                    
                };
                return itemModel;
            }

            var baseCosts = await _costRepository.GetCosts(workOrder.BuilderId ?? 0);
            var jobCostItems = new List<JobCostItem>();

            foreach (var baseCost in baseCosts)
            {
                jobCostItems.Add(new JobCostItem
                {
                    Name = baseCost.CostItem.Name,
                    BuilderCost = baseCost.Amount,
                    LaborCost = baseCost.CostItem.LaborCost,
                    IsBase = baseCost.CostItem.Name == SiteContants.BASE_PRICE,
                    IsLumpSum = baseCost.CostItem.IsLumpSum
                });
            }

            return new WorkOrderItemViewModel<JobInfo>
            {
                CommunityId = workOrder.CommunityId,
                BuilderId = workOrder.BuilderId,
                BuilderAbbrevation = workOrder.Builder.Abbreviation,

                Item = new JobInfo
                {
                    JobCostItems = jobCostItems,
                    InteriorColors = new InteriorColorScheme(),
                    ExteriorColors = new ExteriorColorScheme()
                },
                WorkOrderCompleted = false
            };
        }

        public async Task<JobInfo> SaveJobInfo(long workOrderId, JobInfo model)
        {
            var userId = _userContext.UserId;
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true);
            var originalJobInfo = workOrder.JobInfo?.DeepClone();
            var today = DateTime.UtcNow;

            workOrder.JobInfo = workOrder.JobInfo.IsNull() ? new JobInfo() : workOrder.JobInfo;

            workOrder.JobInfo.Note = JSONifyNote(workOrder.JobInfo.Note, model.Note);
            
            workOrder.JobInfo.MergeShallow(model, nameof(model.Id), nameof(model.Created), nameof(model.Note));

            workOrder.JobInfo.ExteriorColors = model.ExteriorColors;
            workOrder.JobInfo.InteriorColors = model.InteriorColors;
            workOrder.JobInfo.JobCostItems = model.JobCostItems;

            workOrder.JobInfo.Modified = today;
            
            if (workOrder.JobInfo.Id == default(int))
            {
                workOrder.JobInfo.Created = today;

                await _workOrderRepository.CreateJobInfo(workOrder.Id, workOrder.JobInfo, userId);
                
                await _systemAlertService.SaveJobInfoNoteAlert(workOrder, null);

                return workOrder.JobInfo;
            }

            await _workOrderRepository.UpdateJobInfo(workOrder.JobInfo, workOrder.Id, userId);
            await _systemAlertService.SaveJobInfoNoteAlert(workOrder, originalJobInfo);

            return workOrder.JobInfo;
        }

        public async Task CompleteWorkOrder(long id)
        {
            var userId = _userContext.UserId;
            var workOrder = await GetWorkOrder(id);
            if (workOrder.Completed.HasValue)
            {
                workOrder.Completed = null;
                await SaveWorkOrder(workOrder, userId);
                return;
            }
            var areAllCompleted = await _taskManager.AreTasksCompleted(id);
            if (!areAllCompleted)
            {
                throw new InvalidOperationException("All Tasks Are Not Commpleted Yet");
            }

            workOrder.Completed = DateTime.UtcNow;
            await SaveWorkOrder(workOrder, userId);
        }

        public IEnumerable<TaskPricingViewModel> GetTaskPricing(long taskId)
        {
            var workOrderTask = _taskRepository.GetTask(taskId).GetAwaiter().GetResult();
            var workOrder = _workOrderRepository.GetWorkOrder(workOrderTask.WorkOrderId, includeExtraJobInfo: true, includeJobInfo: true).GetAwaiter().GetResult();
            var model = new List<TaskPricingViewModel>();
            var jobInfo = workOrder.JobInfo;

            var taskType = workOrderTask.TaskType;
            
            var extraItems = workOrder.ExtraJobInfo.ExtraItems.Where(x => !(x.TaskIds.IsNull() || !x.TaskIds.Contains(taskId)));

            if (workOrderTask.IncludeBase)
            {
                model.Add(new TaskPricingViewModel
                {
                    Name = SiteContants.BASE_PRICE,
                    Cost = workOrderTask.Amount,
                    Quantity = (int)jobInfo.TotalSize,
                    IsLumpSum = workOrderTask.IsLumpSum,
                    WorkOrderName = workOrder.Name
                });
            }
            
            model.AddRange(extraItems?.Select(extraItem => new TaskPricingViewModel
            {
                Name = extraItem.Name,
                Cost = extraItem.LaborCost,
                Quantity = extraItem.WalkQuantity ?? 0,
                IsLumpSum = extraItem.IsLumpSum,
                WorkOrderName = workOrder.Name
            }));

            return model;
        }

        public async Task<List<string>> GetColorSchemes(long workOrderId,
            SupplyCategory category = SupplyCategory.All)
        {
            Type myType = null;
            IList<PropertyInfo> props = null;

            List<string> colors = new List<string>();
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId, includeJobInfo: true).ConfigureAwait(false);
            if (category == SupplyCategory.Exterior || category == SupplyCategory.All)
            {
                myType = workOrder.JobInfo.ExteriorColors.GetType();
                props = new List<PropertyInfo>(myType.GetProperties());
                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name != "Id" && prop.Name != "ColorCode")
                    {
                        var color = prop.GetValue(workOrder.JobInfo.ExteriorColors, null);
                        if (color is string && !string.IsNullOrEmpty((string)color))
                        {
                            colors.Add((string)color);
                        }
                    }

                }
            }
            if (category == SupplyCategory.Interior || category == SupplyCategory.All)
            {
                myType = workOrder.JobInfo.InteriorColors.GetType();
                props = new List<PropertyInfo>(myType.GetProperties());
                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name != "Id" && prop.Name != "ColorCode")
                    {
                        var color = prop.GetValue(workOrder.JobInfo.InteriorColors, null);
                        if (color is string && !string.IsNullOrEmpty((string)color))
                        {
                            colors.Add((string)color);
                        }
                    }     
                }
            }

            return colors;
        }

        public Task<List<string>> GetSuperintendents()
        {
            return _workOrderRepository.GetSuperintendents();
        }

        public Task<List<string>> GetCustomerReps()
        {
            return _workOrderRepository.GetCustomerReps();
        }

        public async Task<IDictionary<string, IList<Note>>> GetNotes(long workorderId)
        {
            var notesDictionary = new Dictionary<string, IList<Note>>();
            var workorder = await _workOrderRepository.GetWorkOrder(workorderId, includeJobInfo: true, includeExtraJobInfo: true, includeCreatedBy: true);
            var workOrderTasks = await _taskRepository.GetTasks(new TaskSearchCriteria { WorkOrderId = workorderId });
            var jobInfo = workorder.JobInfo;
            var jobExtra = workorder.ExtraJobInfo;
            var tasks = workOrderTasks.OrderBy(x => x.ScheduleDateRange.BeginDate);

            // add initialSettings notes
            if (!string.IsNullOrWhiteSpace(workorder.Description))
            {
                notesDictionary.Add("Initial Settings", workorder.Description.FromJson<List<Note>>());
            }

            // add job info notes
            if (!string.IsNullOrWhiteSpace(jobInfo?.Note))
            {
                notesDictionary.Add("Job Information", jobInfo.Note.FromJson<List<Note>>());
            }

            // add job extra notes
            if (!string.IsNullOrWhiteSpace(jobExtra?.Note))
            {
                notesDictionary.Add("Job Extras Notes", jobExtra.Note.FromJson<List<Note>>());
            }

            // add interior colors notes
            if (!string.IsNullOrWhiteSpace(jobInfo?.InteriorColors?.Note))
            {
                var id = SystemAlertHelper.ComputeTargetObjectId(workorderId, AlertParentType.WorkOrder, AlertType.InteriorColorNote);
                var interiorColorNotes = new Note { Id = id, Author = workorder.CreatedBy.FullName,
                    Created = jobInfo.Created, Text = jobInfo.InteriorColors.Note };

                notesDictionary.Add("Interior Colors", new List<Note> { interiorColorNotes });
            }

            // add exterior colors notes
            if (!string.IsNullOrWhiteSpace(jobInfo?.ExteriorColors?.Note))
            {
                var id = SystemAlertHelper.ComputeTargetObjectId(workorderId, AlertParentType.WorkOrder, AlertType.ExteriorColorNote);
                var exteriorColorNotes = new Note { Id = id, Author = workorder.CreatedBy.FullName,
                    Created = jobInfo.Created, Text = jobInfo.ExteriorColors.Note };

                notesDictionary.Add("Exterior Colors", new List<Note> { exteriorColorNotes });
            }

            // add task notes
            foreach (var task in tasks)
            {
                var items = task.TaskItems?.Where(x => x.DataType == TaskDataType.Note && !x.Value.IsNullOrEmpty());

                if (items.IsNullOrEmpty())
                {
                    continue;
                }

                var notes = items.SelectMany(x => x.Value.FromJson<List<Note>>()).OrderBy(x => x.Created);

                notesDictionary.Add($"Task: {task.Name}", notes.ToList());
            }

            return notesDictionary;
        }

        public async Task ChangeExtraItemTaskType(long workOrderId, string extraItemName, TaskType taskType, List<long> taskIds)
        {
            var extraJobInfo = await _workOrderRepository.GetExtraJobInfoByWorkOrderId(workOrderId);

            var existingExtraItem = extraJobInfo?.ExtraItems?.FirstOrDefault(x => x.Name == extraItemName);

            if (existingExtraItem.IsNull())
            {
                throw new Exception($"Extra Item does not exist for workorder with id {workOrderId}");
            }

            existingExtraItem.TaskType = taskType;
            existingExtraItem.TaskIds = taskIds;

            extraJobInfo.ExtraItems = extraJobInfo.ExtraItems;


            await _workOrderRepository.UpdateExtraJobInfo(extraJobInfo);
        }

        public WorkOrderItemViewModel<List<WorkItem>> GetWorkItems(long taskId)
        {
            var workOrderTask = _taskRepository.GetTask(taskId).GetAwaiter().GetResult();

            if (!workOrderTask.IsMainTask)
            {
                throw new Exception("Only main tasks have workItems");
            }

            var workOrder = _workOrderRepository.GetWorkOrder(workOrderTask.WorkOrderId, includeExtraJobInfo: true, includeJobInfo: true).GetAwaiter().GetResult();
            var extraItemsForTask = workOrder.ExtraJobInfo.ExtraItems.Where(x => x.TaskType == workOrderTask.TaskType);

            var workItems = new List<WorkItem>(extraItemsForTask.Count() + 1)
            {
                new WorkItem
                {
                    Id = WorkOrderExtensions.GenerateBaseIdForWorkItem(workOrderTask.Id, workOrder.Id),
                    Name = SiteContants.BASE_PRICE,
                    Cost = workOrderTask.Amount,
                    Quantity = (int)workOrder.JobInfo.TotalSize,
                    Price = workOrderTask.Amount,
                    WalkQuantity = (int)workOrder.JobInfo.TotalSize,
                    Selected = workOrderTask.IncludeBase
                }
            };

            workItems.AddRange(extraItemsForTask.Select(x => new WorkItem
            {
                Cost = x.LaborCost,
                Id = x.Id,
                Name = x.Name,
                Price = x.BuilderCost,
                IsLumpSum = x.IsLumpSum,
                Quantity = x.Quantity,
                WalkQuantity = x.WalkQuantity ?? 0,
                Selected = !(x.TaskIds.IsNull() || !x.TaskIds.Contains(taskId))
            }));

            return new WorkOrderItemViewModel<List<WorkItem>>
            {
                Item = workItems,
                WorkOrderCompleted = workOrder.Completed.HasValue
            };
        }

        public async Task SaveWorkItems(long taskId, IEnumerable<WorkItem> workItems)
        {
            var task = await _taskRepository.GetMainTask(taskId);

            if (!task.IsMainTask)
            {
                throw new Exception($"Can not add workitems to {task.Name}, since it is not a main task");
            }

            var extraJobInfo = await _workOrderRepository.GetExtraJobInfoByWorkOrderId(task.WorkOrderId);
            var extraItems = extraJobInfo.ExtraItems;
            var basePriceId = WorkOrderExtensions.GenerateBaseIdForWorkItem(task.Id, task.WorkOrderId);
            var userId = _userContext.UserId;
            var today = DateTime.UtcNow;

            foreach(var item in workItems)
            {
                var extraItem = extraItems.FirstOrDefault(x => x.Id == item.Id);

                if (extraItem.IsNull() && item.Id == basePriceId)
                {
                    task.IncludeBase = item.Selected;
                    task.Modified = today;
                    task.ModifiedById = userId;

                    await _taskRepository.UpdateWorkOrderTasks(task);

                    continue;
                }

                extraItem.TaskIds = extraItem.TaskIds ?? new List<long>();

                if (item.Selected)
                {
                    extraItem.TaskIds.Add(task.Id);
                }

                if (!item.Selected)
                {
                    extraItem.TaskIds.Remove(task.Id);
                }

                // This is to remove any ExtraItem TaskId with value 0
                extraItem.TaskIds?.RemoveAll(id => id == 0);
            }
            
            extraJobInfo.Modified = today;
            extraJobInfo.ModifiedById = userId;

            await _workOrderRepository.UpdateExtraJobInfo(extraJobInfo);
        }

        private void UpdateExtraItems(IEnumerable<ExtraItem> previouslySavedExtraItems, IEnumerable<ExtraItem> modelExtraItems, IEnumerable<TaskSummary> taskSummaries)
        {
            modelExtraItems?.ForEach(item =>
            {
                item.TaskIds = item.TaskIds ?? new List<long>();

                // add ids to newly added items
                if (item.Id == Guid.Empty)
                {
                    item.Id = Guid.NewGuid();
                }

                // This is to remove any ExtraItem TaskId with value 0
                item.TaskIds.RemoveAll(id => id == 0);

                var isExisting = previouslySavedExtraItems.Any(x => x.Id == item.Id && x.TaskType == item.TaskType);

                if (isExisting)
                {
                    return;
                }

                var taskId = taskSummaries.FirstOrDefault(x => x.TaskType == item.TaskType)?.Id;

                if (!taskId.HasValue || item.TaskIds.Any(id => taskId == id))
                {
                    return;
                }

                item.TaskIds.Add(taskId.Value);
            });
        }

        private async Task<WorkOrder> SaveWorkOrder(WorkOrder workOrder, string userId)
        {
            workOrder.Modified = DateTime.UtcNow;
            workOrder.ModifiedById = userId;

            if (workOrder.Id == default(int))
            {
                workOrder.Created = DateTime.UtcNow;
                workOrder.CreatedById = userId;

                return await _workOrderRepository.CreateWorkOrder(workOrder);
            }

            return await _workOrderRepository.UpdateWorkOrder(workOrder);
        }

        private async Task PopulateWorkorderTasks(WorkOrder workOrder)
        {
            if (workOrder.Tasks.IsNullOrEmpty())
            {
                workOrder.Tasks = _userContext.IsAdmin()
                    ? await _taskRepository.GetTaskSummaries(workOrder.Id, true)
                    : await _taskRepository.GetMainTasksForUser(workOrder.Id, _userContext.UserId);
            }
        }

        // This method is created for appending text from UI to the Note data structure
        private string JSONifyNote(string oldJSONNotes, string newTextNote)
        {
            if (newTextNote.IsNullOrEmpty())
            {
                return oldJSONNotes;
            }

            var username = _userContext.FullName();

            var existingNotes = oldJSONNotes.IsNullOrEmpty()
                                ? new List<Note>()
                                : oldJSONNotes.FromJson<List<Note>>();
            
            existingNotes.Add(new Note
            {
                Id = Guid.NewGuid(),
                Text = newTextNote,
                Author = username,
                Created = DateTime.UtcNow
            });

            return existingNotes.ToJson();
        }

        public async Task<Dictionary<string, IEnumerable<FileUpload>>> GetFileUploads(long workOrderId)
        {
            var workorder = await GetWorkOrder(workOrderId, true);
            var taskIds = workorder.Tasks.Select(x => x.Id);
            var ids = taskIds.Append(workOrderId);

            var files = await _fileRepository.GetFileUploads(ids, FileOwnerType.WorkOrder, FileOwnerType.Task, FileOwnerType.SecuredWorkOrder, FileOwnerType.SignedDocument);

            var model = new Dictionary<string, IEnumerable<FileUpload>>();

            var initialSettingFiles = files.Where(file => file.OwnerId == workOrderId && file.OwnerType.IsAnyOf(FileOwnerType.SecuredWorkOrder));

            if (initialSettingFiles.Any())
            {
                model.Add("Initial Settings", initialSettingFiles);
            }

            var jobInfoFiles = files.Where(file => file.OwnerId == workOrderId && file.OwnerType.IsAnyOf(FileOwnerType.WorkOrder));

            if (jobInfoFiles.Any())
            {
                model.Add("Job Information", jobInfoFiles);
            }

            workorder.Tasks.ForEach(task =>
            {
                var taskFiles = files.Where(file => file.OwnerId == task.Id && file.OwnerType.IsAnyOf(FileOwnerType.Task, FileOwnerType.SignedDocument));

                if (taskFiles.Any())
                {
                    model.Add(task.Name, taskFiles);
                }
            });

            return model;
        }
    }
}
