﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Domain.Services
{
    public class WorkOrderBuilder
    {
        public static WorkOrder Clone(WorkOrder workOrder, string userId)
        {
            var newWorkOrder = new WorkOrder();

            newWorkOrder.MergeShallow(workOrder, 
                            nameof(WorkOrder.Id),
                            nameof(WorkOrder.Name),
                            nameof(WorkOrder.JobInfo),
                            nameof(WorkOrder.JobInfoId),
                            nameof(WorkOrder.ExtraJobInfo),
                            nameof(WorkOrder.ExtraJobInfoId),
                            nameof(WorkOrder.Completed),
                            nameof(WorkOrder.Tasks),
                            nameof(WorkOrder.JobInfoConfirmed),
                            nameof(WorkOrder.Deleted));

            var nameParts = workOrder.Name.Split(' ');

            if (nameParts.Length > 1)
            {
                newWorkOrder.Name = $"{nameParts[0]} {nameParts[1]}";
            }
            else
            {
                newWorkOrder.Name = "Unknown";
            }

            newWorkOrder.Lot = 9999999;
            newWorkOrder.Block = "Please Change";
            newWorkOrder.Created = DateTime.UtcNow;
            newWorkOrder.Modified = DateTime.UtcNow;
            newWorkOrder.CreatedById = userId;
            newWorkOrder.ModifiedById = userId;


            AddJobInfo(newWorkOrder, workOrder.JobInfo);
            AddExtraJobInfo(newWorkOrder, workOrder.ExtraJobInfo);

            return newWorkOrder;
        }

        private static void AddJobInfo(WorkOrder workOrder, JobInfo jobInfo)
        {
            if (jobInfo.IsNull())
            {
                return;
            }

            workOrder.JobInfo = new JobInfo
            {
                Modified = workOrder.Modified,
                Created = workOrder.Created,
                FirstFloorSqft = jobInfo.FirstFloorSqft,
                GarageSqft = jobInfo.GarageSqft,
                LanaiSqft = jobInfo.LanaiSqft,
                PlanId = jobInfo.PlanId,
                ElevationId = jobInfo.ElevationId,
                SecondFloorSqft = jobInfo.SecondFloorSqft,
                PaintStoreId = jobInfo.PaintStoreId,
                InteriorColors = jobInfo.InteriorColors,
                ExteriorColors = jobInfo.ExteriorColors,
                JobCostItems = jobInfo.JobCostItems
            };
        }

        private static void AddExtraJobInfo(WorkOrder workOrder, ExtraJobInfo extra)
        {
            if (extra.IsNull())
            {
                return;
            }

            workOrder.ExtraJobInfo = new ExtraJobInfo
            {
                ExtraItems = extra.ExtraItems,
                Created = workOrder.Created,
                CreatedById = workOrder.CreatedById,
                Modified = workOrder.Modified,
                ModifiedById = workOrder.ModifiedById
            };
        }
    }
}
