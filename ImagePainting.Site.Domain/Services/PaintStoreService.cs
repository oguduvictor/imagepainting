﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using ImagePainting.Site.Common.Interfaces.Repository;

namespace ImagePainting.Site.Domain.Services
{
    public class PaintStoreService : IPaintStoreService
    {
        private readonly IPaintStoreRepository _storeRepository;

        public PaintStoreService(IPaintStoreRepository paintStoreRepository)
        {
            _storeRepository = paintStoreRepository;
        }

        public async Task<PaintStoreViewModel> GetStore(long id, long locationId)
        {
            var paintStore = new PaintStoreViewModel { Active = true, };
            if (id == default(long) && locationId == default(long))
            {
                return paintStore;
            }

            if (id != default(long) && locationId == default(long))
            {
                var dbstore = await _storeRepository.GetStore(id);
                paintStore.StoreId = id;
                paintStore.StoreName = dbstore.StoreName;
                return paintStore;
            }

            var storeLocation = await _storeRepository.GetStoreLocation(locationId);
            var store = await _storeRepository.GetStore(id);

            paintStore.StoreId = store.Id;
            paintStore.StoreName = store.StoreName;
            paintStore.Active = store.Active;
            paintStore.LocationId = storeLocation.Id;

            paintStore.MapToModel(storeLocation);

            return paintStore;
        }

        public async Task<IEnumerable<Store>> GetStores()
        {
            var stores = await _storeRepository.GetStores(includeLocation: true);
            stores.ForEach(x =>
            {
                x.Locations = x.Locations.Where(y => !y.Deleted).ToList();
            });

            return stores;
        }

        public Task<IEnumerable<StoreLocation>> GetStoreLocations(long? storeId = null, bool includeDeleted = false)
        {
            return _storeRepository.GetStoreLocations(storeId, includeDeleted);
        }

        public async Task<StoreLocation> GetStoreLocation(long locationId)
        {
            return await _storeRepository.GetStoreLocation(locationId);
        }

        public Task<List<Store>> GetStoreSummary(bool includeDeleted)
        {
            return _storeRepository.GetStores(isSummary: true, includeDeleted: includeDeleted);
        }

        public async Task<Store> SaveStore(PaintStoreViewModel store)
        {
            var paintStore = new Store();
            var storeLocation = new StoreLocation();

            if (store.StoreId == default(long))
            {
                paintStore.Active = store.Active;
                paintStore.StoreName = store.StoreName;
                storeLocation.Deleted = false;
                storeLocation = store.MapToLocation();

                paintStore.Locations.Add(storeLocation);                

                return await _storeRepository.CreateStore(paintStore);
            }

            if (store.StoreId != default(long) && store.LocationId == default(long))
            {
                storeLocation = store.MapToLocation();

                await _storeRepository.SaveStoreLocation(storeLocation);
                return paintStore;
            }

            storeLocation = await _storeRepository.GetStoreLocation(store.LocationId);
            paintStore = await _storeRepository.GetStore(store.StoreId);
            
            paintStore.StoreName = store.StoreName;
            paintStore.Active = store.Active;
            paintStore.Id = store.StoreId;

            storeLocation.StoreAddress = store.StoreAddress;
            storeLocation.StoreZipCode = store.StoreZipCode;
            storeLocation.StoreCity = store.StoreCity;
            storeLocation.StoreContactName = store.StoreContactName;
            storeLocation.StoreContactPhone = store.StoreContactPhone;
            storeLocation.StoreContactEmail = store.StoreContactEmail;
            storeLocation.LocationId = store.StoreLocationId;

            return await _storeRepository.UpdateStore(paintStore, storeLocation);
        }

        public async Task DeleteLocation(long locationId)
        {
            var location = await _storeRepository.GetStoreLocation(locationId);
            if (!location.IsNull())
            {
                location.Deleted = true;
                await _storeRepository.UpdateLocation(location);
            }
        }

        public async Task DeleteStore(long storeId)
        {
            var store = await _storeRepository.GetStore(storeId, true);
            
            if(!store.IsNull())
            {
                store.Deleted = true;

                store.Locations.ForEach(location => location.Deleted = true);

                await _storeRepository.UpdateStore(store);
            }
        }
    }
}
