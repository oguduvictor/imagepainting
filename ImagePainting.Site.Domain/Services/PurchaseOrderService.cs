﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class PurchaseOrderService : IPurchaseOrderService
    {
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IPaintStoreRepository _paintStoreRepository;
        private readonly ISupplyRepository _suppliesRepository;
        private readonly IWorkOrderTaskManager _taskManager;
        private readonly IWorkOrderComposerService _workOrderComposerService;
        private readonly ICommunityComposerService _communityComposerService;
        private readonly IWorkOrderMailer _worderOrderMailer;
        private readonly ILogger _logger;

        public PurchaseOrderService(
            IPurchaseOrderRepository purchaseOrderRepository, 
            IPaintStoreRepository paintStoreRepository,
            IWorkOrderTaskManager workOrderTaskManager, 
            IWorkOrderComposerService workOrderComposerService,
            ISupplyRepository suppliesRepository, 
            ICommunityComposerService communityComposerService,
            IWorkOrderMailer workOrderMailer,
            ILogger logger)
        {
            _purchaseOrderRepository = purchaseOrderRepository;
            _paintStoreRepository = paintStoreRepository;
            _taskManager = workOrderTaskManager;
            _workOrderComposerService = workOrderComposerService;
            _suppliesRepository = suppliesRepository;
            _communityComposerService = communityComposerService;
            _worderOrderMailer = workOrderMailer;
            _logger = logger;
        }

        public Task<List<PurchaseOrder>> GetPurchaseOrders(long? taskId = null, long? workOrderId = null,
            bool includeStoreLocation = false, bool includeItems = false, bool? emailed = null)
        {
            return _purchaseOrderRepository.GetAll(taskId, workOrderId, includeStoreLocation, includeItems, emailed: emailed);
        }

        public async Task<PurchaseOrder> GetPurchaseOrder(long id, bool complete = false)
        {
            if (complete)
            {
                return await _purchaseOrderRepository.Get(
                    id,
                    includeItems: true,
                    includeLocation: true,
                    includeWorkOrder: true,
                    includeTask: true
                    );
            }

            return await _purchaseOrderRepository.Get(id, includeLocation: true, includeItems: true);
        }

        public async Task UpdatePurchaseOrderEmailSent(long id, string emailAddress)
        {
            var purchaseOrder = await _purchaseOrderRepository.Get(id);
            
            if(!purchaseOrder.IsNull())
            {
                purchaseOrder.Email = emailAddress;
                purchaseOrder.EmailSent = DateTime.UtcNow;
            }

            try
            {
                await _purchaseOrderRepository.Update(purchaseOrder);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void DeleteUnsentPurchaseOrdersByStoreId(long paintStoreId)
        {
            try
            {
                _purchaseOrderRepository.DeleteUnsentPurchaseOrdersByStoreId(paintStoreId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task DeletePurchaseOrder(long id)
        {
            try
            {
                await _purchaseOrderRepository.DeletePurchaseOrders(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task<PurchaseOrder> GetPurchaseOrder(long taskId, long id, long? paintStoreId)
        {
            try
            {

                if (id != default(int))
                {
                    var purchaseOrder = await _purchaseOrderRepository
                        .Get(id, includeLocation: true, includeItems: true, includeTask: true, includeWorkOrder: true);
                    if (paintStoreId != purchaseOrder.PaintStoreLocation.StoreId && paintStoreId != default(long))
                    {
                        id = default(int);
                    }
                    else
                    {
                        return purchaseOrder;
                    }
                }
                else
                {
                    var task = await _taskManager.GetTaskWithPermissions(taskId, true);
                    var jobInfo = await _workOrderComposerService.GetJobInfo(task.WorkOrderId);

                    var builderId = task.WorkOrder?.BuilderId ?? 0;
                    var items = new List<PurchaseOrderItem>();
                    var suppliesItems = new List<PurchaseOrderItem>();

                    var storeId = (paintStoreId == default(long)) ? jobInfo.Item?.PaintStoreId ?? await GetDefaultBuilderPaintStoreId(builderId) : 0;
                    if (task.TaskType == TaskType.Interior)
                    {
                        suppliesItems = MapSuppliesToItems(await GetSupplies(builderId, SupplyCategory.Interior, storeId));
                        items.AddRange(InteriorMapToItems(suppliesItems, jobInfo.Item.InteriorColors, jobInfo.Item.TotalSize));
                    }
                    if (task.TaskType == TaskType.Exterior)
                    {
                        suppliesItems = MapSuppliesToItems(await GetSupplies(builderId, SupplyCategory.Exterior, storeId));
                        items.AddRange(ExteriorMapToItems(suppliesItems, jobInfo.Item.ExteriorColors, jobInfo.Item.TotalSize));
                    }

                    var paintStoreLocation =
                        (await _paintStoreRepository.GetStoreLocations(storeId, includeDeleted: false)).FirstOrDefault();
                    var paintStoreLocationId = default(long);
                    if (paintStoreLocation != null)
                    {
                        paintStoreLocation.StoreId = storeId;
                        paintStoreLocationId = paintStoreLocation.Id;
                    }

                    var purchaseOrder =  new PurchaseOrder
                    {
                        TaskId = taskId,
                        PurchaseOrderItems = items,
                        PaintStoreLocationId = paintStoreLocationId,
                        PaintStoreLocation = paintStoreLocation ?? new StoreLocation(),
                        WorkOrder = task.WorkOrder,
                        WorkOrderId = task.WorkOrderId,
                        Address = task.WorkOrder.Address,
                        Email = paintStoreLocation?.StoreContactEmail,
                        Date = DateTime.UtcNow,
                        Task = task
                    };

                    purchaseOrder.WorkOrder.JobInfo = jobInfo.Item;

                    return purchaseOrder;
                }

                return await _purchaseOrderRepository.Get(id, includeLocation: true, includeItems: true, includeTask: true, includeWorkOrder: true);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return default(PurchaseOrder);
            }
        }

        public async Task<PurchaseOrder> SavePurchaseOrder(PurchaseOrder order, bool sendEmail = false)
        {
            var storeLocation = await _paintStoreRepository.GetStoreLocation(order.PaintStoreLocationId);

            order.PaintStoreLocation = storeLocation;

            if (sendEmail)
            {
                order.EmailSent = DateTime.UtcNow;
            }

            var result = order.Id == default(int) ? await CreatePurchaseOrder(order) : await UpdatePurchaseOrder(order);

            if (!sendEmail)
            {
                return result;
            }

            try
            {
                await _worderOrderMailer.EmailPurchaseOrder(result.Id, result.Email);
            }
            catch (Exception)
            {
                // if sending emails failed, revert the emailSent changes
                order.EmailSent = null;
                await UpdatePurchaseOrder(order);
            }

            return result;
        }

        public async Task<IEnumerable<PurchaseOrder>> WorkOrderPurchaseOrders(long workOrderId)
        {
            return await _purchaseOrderRepository.GetAll(workOrderId: workOrderId, includeItems: true, includeTask:true);
        }

        public async Task UnlockPurchaseOrder(long id)
        {
            var purchaseOrder = await _purchaseOrderRepository.Get(id);

            if (!purchaseOrder.IsNull() && purchaseOrder.EmailSent.HasValue)
            {
                purchaseOrder.EmailSent = null;

                await _purchaseOrderRepository.Update(purchaseOrder);
            }
        }

        private List<PurchaseOrderItem> ExteriorMapToItems(List<PurchaseOrderItem> suppliesItems, ExteriorColorScheme exteriorColors = null, decimal totalSize = 0)
        {
            var items = new List<PurchaseOrderItem>();

            var supplyTypes = _communityComposerService.GetSupplyTypes(SupplyCategory.Exterior).Result;

            suppliesItems.ForEach(x =>
            {
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Base")?.Name)
                {
                    x.Color = exteriorColors?.ExteriorBase;
                    x.Quantity = (int)Math.Ceiling(totalSize / 125);
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Trim")?.Name)
                {
                    x.Color = exteriorColors?.ExteriorTrim;
                    x.Quantity = (int)Math.Ceiling((double)totalSize / 1000);
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Primer")?.Name)
                {
                    x.Quantity = (int)Math.Ceiling(totalSize / 125);
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Shakes")?.Name)
                {
                    x.Color = exteriorColors?.ShakeSiding;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Board and Batton")?.Name)
                {
                    x.Color = exteriorColors?.BoardAndBatton;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Garage Door")?.Name)
                {
                    x.Color = exteriorColors?.GarageDoor;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Front Door")?.Name)
                {
                    x.Color = exteriorColors?.FrontDoor;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Soffit")?.Name)
                {
                    x.Color = exteriorColors?.Soffit;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Shutters")?.Name)
                {
                    x.Color = exteriorColors?.Shutters;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Hardi Siding")?.Name)
                {
                    x.Color = exteriorColors?.HardiSiding;
                }
                x.SupplyType = $"Ext. {x.SupplyType}";
            });

            return suppliesItems;
        }

        private List<PurchaseOrderItem> InteriorMapToItems(List<PurchaseOrderItem> suppliesItems, InteriorColorScheme interiorColors = null, decimal totalSize = 0)
        {
            var items = new List<PurchaseOrderItem>();
            
            var supplyTypes = _communityComposerService.GetSupplyTypes(SupplyCategory.Interior).Result;

            suppliesItems.ForEach(x =>
            {
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Base")?.Name)
                {
                    x.Color = interiorColors?.InteriorBase;
                    x.Quantity = (int)Math.Ceiling((double)totalSize / 125) + 5;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Trim")?.Name)
                {
                    x.Color = interiorColors?.InteriorWoodTrimBaseCasing;
                    x.Quantity = (int)Math.Ceiling((double)totalSize / 1000);
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Ceiling")?.Name)
                {
                    x.Color = interiorColors?.InteriorCeiling;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Accent Room")?.Name)
                {
                    x.Color = interiorColors?.AccentRoom1;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Accent Wall")?.Name)
                {
                    x.Color = interiorColors?.AccentWall1;
                }

                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Beams")?.Name)
                {
                    x.Color = interiorColors?.Beams;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Crown")?.Name)
                {
                    x.Color = interiorColors?.Crown;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Front Door")?.Name)
                {
                    x.Color = interiorColors?.FrontDoor;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Treads")?.Name)
                {
                    x.Color = interiorColors?.Treads;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name.Contains("Newel"))?.Name)
                {
                    x.Color = interiorColors?.NewelPosts;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Rail")?.Name)
                {
                    x.Color = interiorColors?.Rail;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name == "Risers")?.Name)
                {
                    x.Color = interiorColors?.Risers;
                }
                if (x.SupplyType == supplyTypes.FirstOrDefault(y => y.Name.Contains("Stain"))?.Name)
                {
                    x.Color = interiorColors?.Stain;
                }
                x.SupplyType = $"Int. {x.SupplyType}";
            });

            return suppliesItems;
        }

        private async Task<PurchaseOrder> CreatePurchaseOrder(PurchaseOrder order)
        {
            await _purchaseOrderRepository.Create(order);

            return order;
        }

        private async Task<PurchaseOrder> UpdatePurchaseOrder(PurchaseOrder order)
        {
            var dbOrder = await _purchaseOrderRepository.Get(order.Id, true);

            dbOrder.MergeShallow(order, nameof(order.Id));

            await _purchaseOrderRepository.Update(dbOrder);

            await _purchaseOrderRepository.Update(order.PurchaseOrderItems, order.Id);

            return order;
        }

        private async Task<List<Supply>> GetSupplies(int builderId, SupplyCategory category, long paintStoreId)
        {
            return await _suppliesRepository.GetSupplies(builderId, category, paintStoreId);
        }

        private async Task<long> GetDefaultBuilderPaintStoreId(int builderId)
        {
            return await _suppliesRepository.GetDefaultStoreId(builderId) 
                ?? (await _suppliesRepository.GetPaintStoreIdsByBuilder(builderId)).FirstOrDefault();
        }

        private List<PurchaseOrderItem> MapSuppliesToItems(List<Supply> supplies)
        {
            var items = new List<PurchaseOrderItem>();

            supplies.ForEach(x =>
                items.Add(new PurchaseOrderItem
                {
                    Name = x.Name,
                    Color = string.Empty,
                    UnitPrice = x.Cost,
                    SupplyType = x.SupplyType.Name
                })
            );
            return items;
        }
        
    }
}
