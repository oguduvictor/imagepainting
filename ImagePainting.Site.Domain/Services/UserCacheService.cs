﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class UserCacheService : IUserCacheService
    {
        private readonly ICacheService _cacheService;
        private readonly IUserInfoRepository _userInfoRepository;
        private const string CacheKeyPrefix = "User::";

        public UserCacheService(ICacheService cacheService, IUserInfoRepository userInfoRepository)
        {
            _cacheService = cacheService;
            _userInfoRepository = userInfoRepository;
        }
        
        public void CacheUser(UserInfoSummary userInfo)
        {
            _cacheService.AddOrUpdate(GetKey(userInfo.Id), userInfo, new TimeSpan(0, 15, 0));
        }

        public void ClearAll()
        {
            _cacheService.Clear(CacheKeyPrefix);
        }

        public IList<UserInfoSummary> GetUsers()
        {
            var key = GetKey();

            var users = _cacheService.Get<IList<UserInfoSummary>>(key, true);

            if (users.IsNullOrEmpty())
            {
                var dbUsers = _userInfoRepository.GetUsersAsync().GetAwaiter().GetResult();
                users = Mapper.Map<IList<UserInfoSummary>>(dbUsers);

                _cacheService.AddOrUpdate(key, users);
            }

            return users;
        }

        public async Task<IList<UserInfoSummary>> GetUsersAsync(IEnumerable<string> userIds)
        {
            var keys = userIds.Select(id => GetKey(id));
            var users = _cacheService.GetList<UserInfoSummary>(keys, CacheKeyPrefix);
            var userIdsFound = users.Select(u => u.Id);
            var userIdsNotFound = userIds.Except(userIdsFound);

            if (userIdsNotFound.Any())
            {
                var dbUsers = await _userInfoRepository.GetUsersAsync(userIdsNotFound);
                var dbUsersSummary = Mapper.Map<IList<UserInfoSummary>>(dbUsers);

                dbUsersSummary.ForEach(user => {
                    CacheUser(user);
                    users.Add(user);
                });
            }
            
            return users;
        }

        public UserInfoSummary GetUser(string userId)
        {
            var key = GetKey(userId);
            var user = _cacheService.Get<UserInfoSummary>(key, true);

            if (user.IsNull())
            {
                var dbUser = _userInfoRepository.GetUserAsync(userId, true).GetAwaiter().GetResult();

                user = Mapper.Map<UserInfoSummary>(dbUser);

                CacheUser(user);
            }

            return user;
        }
        
        public IList<UserInfoSummary> GetUsersInRoles(params UserRole[] userRoles)
        {
            var users = GetUsers();

            return users.Where(x => x.Roles.Any(role => userRoles.Contains(role))).ToList();
        }

        public void Remove(string userId)
        {
            _cacheService.Remove(GetKey(userId));
        }

        public void UpdateUserInCache(UserInfo user)
        {
            var key = GetKey(user.Id);
            var userInCache = _cacheService.Get<UserInfoSummary>(key, true);

            if (!userInCache.IsNull())
            {
                _cacheService.Remove(key);
            }

            var userSummary = Mapper.Map<UserInfoSummary>(user);

            _cacheService.AddOrUpdate(key, userSummary);
        }

        private string GetKey(string userId = null) => $"{CacheKeyPrefix}{(userId ?? string.Empty)}";
    }
}
