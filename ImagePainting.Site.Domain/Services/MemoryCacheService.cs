﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace ImagePainting.Site.Domain.Services
{
    public class MemoryCacheService : ICacheService
    {
        private readonly MemoryCache _memoryCache;
        private readonly TimeSpan DefaultCacheDuration = TimeSpan.FromMinutes(15);

        public MemoryCacheService()
        {
            _memoryCache = MemoryCache.Default;
        }

        public void AddOrUpdate<TObject>(string key, TObject value, TimeSpan cacheDuration = default(TimeSpan))
        {
            var offset = cacheDuration == default(TimeSpan) ? DefaultCacheDuration : cacheDuration;

            var expiredTime = DateTime.Now.AddMinutes(offset.TotalMinutes);
          
            _memoryCache.Set(key, value, expiredTime);
        }

        public void Remove(string key)
        {
            _memoryCache.Remove(key);
        }
    
        public TObject Get<TObject>(string key, bool defaultIfNotFound = false)
        {
            TObject value = default(TObject);

            value = (TObject)_memoryCache.Get(key);

            if (value.IsNull() && !defaultIfNotFound)
            {
                throw new Exception($"No entry found for key {key}");
            }

            return value;
        }

        public IList<TObject> GetList<TObject>(IEnumerable<string> keys, string prefix)
        {
            var values = new List<TObject>(keys.Count());
            
            keys.ForEach(key =>
            {
                var item = Get<TObject>(key, true);
                
                values.Add(item);
            });

            return values;
        }

        public void Clear(string keyPrefix)
        {
            var matchingKeyObjects = _memoryCache.Where(x => x.Key.Contains(keyPrefix));

            matchingKeyObjects.ForEach(item => _memoryCache.Remove(item.Key));
        }
    }
}
