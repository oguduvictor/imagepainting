﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ImagePainting.Site.Common.Interfaces.Repository;

namespace ImagePainting.Site.Domain.Services
{
    public class UserInfoService : IUserInfoService
    {
        private IUserInfoRepository _userInfoRepo;
        private IUserCacheService _userCacheService;

        public UserInfoService(IUserInfoRepository userInfoRepo, IUserCacheService userCacheService)
        {
            _userInfoRepo = userInfoRepo;
            _userCacheService = userCacheService;
        }

        public Task<List<UserInfo>> GetUsersSummary(bool includeDeleted = false, bool includeInActive = false)
        {
            var users = _userInfoRepo.GetUsersAsync(
                includeDeleted: includeDeleted, 
                includeInActive: includeInActive,
                includeRole: true);

            if (includeDeleted || includeInActive)
            {
                return users;
            }

            var approvedRoles = new string[]
            {
                ((int)UserRole.Administrator).ToString(),
                ((int)UserRole.Accountant).ToString(),
                ((int)UserRole.SubContractor).ToString(),
                ((int)UserRole.Employee).ToString()
            };

            return users.ContinueWith(x =>
            {
                return x.Result.Where(user => user.Roles.Any(role => approvedRoles.Contains(role.Id))).ToList();
            });
        }

        public async Task<IEnumerable<UserInfo>> GetUsers()
        {
            return await _userInfoRepo.GetUsersAsync(false, true, true);
        }

        public async Task SaveUserInfo(UserInfo userInfo)
        {
            var dbUserInfo = await _userInfoRepo.GetUserAsync(userInfo.Id);

            if (!dbUserInfo.IsNull())
            {
                dbUserInfo.MergeShallow(userInfo, nameof(userInfo.Id), nameof(userInfo.Email), nameof(userInfo.Created), nameof(userInfo.FullName));

                await _userInfoRepo.UpdateUserInfoAsync(dbUserInfo);
            }
            else
            {
                await _userInfoRepo.CreateUserInfoAsync(userInfo);
            }

            _userCacheService.UpdateUserInCache(dbUserInfo ?? userInfo);
        }

        public Task<UserInfo> GetUserAsync(string userId)
        {
            return _userInfoRepo.GetUserAsync(userId);
        }

        public async Task<UserInfo> GetUserByEmailAsync(string userEmail)
        {
            return await _userInfoRepo.GetUserByEmailAsync(userEmail);
        }

        public async Task<UserInfo> GetUserRoles(string id)
        {
            return await _userInfoRepo.GetUserAsync(id, includeRoles: true); 
        }

        public async Task AssignRolesToUser(string userId, IEnumerable<RoleSelection> model)
        {
            var dbUser = await _userInfoRepo.GetUserAsync(userId, includeRoles: true);
            var dbRoles = await _userInfoRepo.GetAllRolesAsync();
            var addedRole = false;
            var deleted = false;

            model.ForEach(x =>
            {
                var role = dbRoles.First(z => z.Id == ((int)x.Role).ToString());

                if (x.Selected && !dbUser.Roles.Any(y => y.Id == role.Id))
                {
                    dbUser.Roles.Add(role);
                    addedRole = true;
                }

                else if (!x.Selected && dbUser.Roles.Any(y => y.Id == role.Id)) 
                {
                    dbUser.Roles.Remove(role);
                    deleted = true;
                }

            });
       
            if (addedRole || deleted)
            {
                await _userInfoRepo.UpdateUserInfoAsync(dbUser);

                _userCacheService.UpdateUserInCache(dbUser);
            }
        }

        public async Task DeleteUser(string id)
        {
            // attempt to permanently delete user from the system
            await _userInfoRepo.DeleteUserRolesAsync(id);

            if (await _userInfoRepo.TryDeleteUserInfoAsync(id))
            {
                await _userInfoRepo.DeleteUserLoginAsync(id);

                _userCacheService.Remove(id);

                return;
            }

            // if can't permanently delete user, set the deleted flag to true
            var user = await GetUserAsync(id);

            if (user.IsNull())
            {
                throw new ArgumentException($"No user exists for ID={id}");
            }

            user.Deleted = true;
            user.Active = false;

            await _userInfoRepo.UpdateUserInfoAsync(user);

            _userCacheService.UpdateUserInCache(user);
        }
    }
}
