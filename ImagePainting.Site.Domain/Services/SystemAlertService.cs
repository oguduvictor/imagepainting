﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class SystemAlertService : ISystemAlertService
    {
        private readonly ISystemAlertRepository _systemAlertRepository;
        private readonly IUserContext _userContext;
        private readonly ITaskRepository _taskRepository;

        public SystemAlertService(ISystemAlertRepository systemAlertRepository, IUserContext userContext, ITaskRepository taskRepository)
        {
            _systemAlertRepository = systemAlertRepository;
            _userContext = userContext;
            _taskRepository = taskRepository;
        }

        public Task<IList<SystemAlert>> SearchAlerts(AlertSearchCriteria searchCriteria)
        {
            return _systemAlertRepository.SearchAlerts(searchCriteria);
        }

        public async Task<IList<SystemAlert>> GetNoteAlerts(long workOrderId)
        {
            var alerts = new List<SystemAlert>();
            var taskIds = await _taskRepository.GetTaskIdsByWorkOrderId(workOrderId);
            var taskNoteSearch = new AlertSearchCriteria
            {
                ParentIds = taskIds,
                ParentType = AlertParentType.Task,
                AlertTypes = new[] { AlertType.TaskNote }
            };
            var workorderNotesSearch = new AlertSearchCriteria
            {
                ParentIds = new[] { workOrderId },
                ParentType = AlertParentType.WorkOrder,
                AlertTypes = new[] { AlertType.ExteriorColorNote, AlertType.InteriorColorNote, AlertType.JobInfoNote, AlertType.JobExtraNote, AlertType.WorkOrderNote }
            };

            var taskNoteAlerts = await SearchAlerts(taskNoteSearch);
            var workorderRelatedNoteAlerts = await SearchAlerts(workorderNotesSearch);

            // combine the alerts
            alerts.AddRange(taskNoteAlerts);
            alerts.AddRange(workorderRelatedNoteAlerts);

            return alerts;
        }

        public async Task ResolveAlerts(params Guid[] ids)
        {
            var alerts = await GetAlerts(ids);
            var userId = _userContext.UserId;
            var currentUtcTime = DateTime.UtcNow;

            alerts.ForEach(alert =>
            {
                alert.ResolvedById = userId;
                alert.Resolved = currentUtcTime;
            });

            await _systemAlertRepository.Update(alerts.ToArray());
        }
        
        public async Task DeleteAlerts(params SystemAlert[] alerts)
        {
            foreach (var alert in alerts)
            {
                var searchCriteria = CreateSearchCriteria(alert);
                var dbAlerts = await _systemAlertRepository.SearchAlerts(searchCriteria);

                if (dbAlerts.Any())
                {
                    if (dbAlerts.Count > 1)
                    {
                        throw new ApplicationException($"More than a single entry matches a search for alert: {alert.ToJson()}");
                    }

                    await _systemAlertRepository.DeleteAlerts(dbAlerts.First());
                }
            }
        }

        private async Task SaveNoteAlertForInitialSettingsAndJobInfo(WorkOrder workOrder, string newNoteModel, string originalNote, AlertType alertType)
        {
            var newAlerts = new List<SystemAlert>();
            if (!string.IsNullOrWhiteSpace(newNoteModel))
            {
                var originalNotes = originalNote?.FromJson<List<Note>>();
                var newNotes = newNoteModel.FromJson<List<Note>>();

                if ((newNotes?.Count ?? 0) > (originalNotes?.Count ?? 0))
                {
                    var newestNote = newNotes.OrderBy(x => x.Created).Last();

                    newAlerts.Add(CreateWorkOrderNoteAlert(workOrder, newestNote.Text, alertType, targetObjectId: newestNote.Id));
                }
            }

            if (newAlerts.Any())
            {
                foreach (var alert in newAlerts)
                {
                    await SaveAlert(alert);
                }
            }
        }

        public async Task SaveWorkOrderNoteAlert(WorkOrder workOrder, string originalDescription)
        {
            await SaveNoteAlertForInitialSettingsAndJobInfo(workOrder, workOrder.Description, originalDescription, AlertType.WorkOrderNote);
        }

        public async Task SaveJobInfoNoteAlert(WorkOrder workOrder, JobInfo originalJobInfo)
        {
            var newJobInfo = workOrder.JobInfo;
            var newAlerts = new List<SystemAlert>();
            var deletedAlerts = new List<SystemAlert>();

            if (!string.IsNullOrWhiteSpace(newJobInfo?.Note))
            {
                var originalNotes = originalJobInfo?.Note?.FromJson<List<Note>>();
                var newNotes = newJobInfo.Note.FromJson<List<Note>>();

                if ((newNotes?.Count ?? 0) > (originalNotes?.Count ?? 0))
                {
                    var newestNote = newNotes.OrderBy(x => x.Created).Last();

                    newAlerts.Add(CreateWorkOrderNoteAlert(workOrder, newestNote.Text, AlertType.JobInfoNote, targetObjectId: newestNote.Id));
                }
            }

            if (originalJobInfo?.InteriorColors?.Note != newJobInfo?.InteriorColors?.Note)
            {
                if (!string.IsNullOrWhiteSpace(newJobInfo?.InteriorColors?.Note))
                {
                    newAlerts.Add(CreateWorkOrderNoteAlert(workOrder, newJobInfo.InteriorColors.Note, AlertType.InteriorColorNote));
                }
                else if (!string.IsNullOrWhiteSpace(originalJobInfo?.InteriorColors?.Note))
                {
                    deletedAlerts.Add(CreateWorkOrderNoteAlert(workOrder, originalJobInfo.InteriorColors.Note, AlertType.InteriorColorNote, false));
                }
            }

            if (originalJobInfo?.ExteriorColors?.Note != newJobInfo?.ExteriorColors?.Note)
            {
                if (!string.IsNullOrWhiteSpace(newJobInfo?.ExteriorColors?.Note))
                {
                    newAlerts.Add(CreateWorkOrderNoteAlert(workOrder, newJobInfo.ExteriorColors.Note, AlertType.ExteriorColorNote));
                }
                else if (!string.IsNullOrWhiteSpace(originalJobInfo?.ExteriorColors?.Note))
                {
                    deletedAlerts.Add(CreateWorkOrderNoteAlert(workOrder, originalJobInfo.ExteriorColors.Note, AlertType.ExteriorColorNote, false));
                }
            }

            if (newAlerts.Any())
            {
                foreach (var alert in newAlerts)
                {
                    await SaveAlert(alert);
                }
            }

            if (deletedAlerts.Any())
            {
                await DeleteAlerts(deletedAlerts.ToArray());
            }
        }

        public Task SaveTaskNoteAlert(WorkOrder workOrder, WorkOrderTask task, Note note)
        {
            var noteAlert = new NoteAlert
            {
                Note = note,
                WorkOrder = new NamedId<long>(task.WorkOrderId, workOrder?.Name),
                WorkOrderTask = new NamedId<long>(task.Id, task.Name)
            };

            var alert = new SystemAlert
            {
                Id = Guid.NewGuid(),
                ParentId = task.Id,
                ParentType = AlertParentType.Task,
                TargetObjectId = note.Id,
                AlertType = AlertType.TaskNote,
                CreatedById = _userContext.UserId,
                Created = DateTime.UtcNow,
                Content = noteAlert.ToJson()
            };

            return SaveAlert(alert);
        }

        public async Task SaveExtraItemAlerts(IEnumerable<ExtraItem> extraItems, WorkOrder workOrder)
        {
            if (extraItems.IsNullOrEmpty())
            {
                return;
            }

            foreach (var extraItem in extraItems)
            {
                if (extraItem.WalkQuantity <= extraItem.Quantity)
                {
                    continue;
                }

                var jobExtraAlert = new JobExtraAlert
                {
                    WorkOrder = new NamedId<long>(workOrder.Id, workOrder.Name),
                    ExtraItem = extraItem
                };

                var alert = new SystemAlert
                {
                    Id = Guid.NewGuid(),
                    ParentId = workOrder.Id,
                    ParentType = AlertParentType.WorkOrder,
                    TargetObjectId = extraItem.Id,
                    AlertType = AlertType.JobExtraDifference,
                    CreatedById = _userContext.UserId,
                    Created = DateTime.UtcNow,
                    Content = jobExtraAlert.ToJson()
                };

                await SaveAlert(alert);
            }
        }

        public async Task SaveJobExtraNoteAlert(WorkOrder workOrder, ExtraJobInfo originalJobExtra)
        {
            await SaveNoteAlertForInitialSettingsAndJobInfo(workOrder, workOrder.ExtraJobInfo?.Note, originalJobExtra?.Note, AlertType.JobExtraNote);
        }

        private Task<IList<SystemAlert>> GetAlerts(params Guid[] ids)
        {
            return _systemAlertRepository.GetAlerts(ids);
        }

        private async Task SaveAlert(SystemAlert systemAlert)
        {
            var searchCriteria = CreateSearchCriteria(systemAlert);
            var searchResult = await SearchAlerts(searchCriteria);

            if (!searchResult.IsNullOrEmpty())
            {
                // if alert exists delete it
                if (searchResult.IsOne())
                {
                    await _systemAlertRepository.DeleteAlerts(searchResult[0]);
                }
                else
                {
                    throw new InvalidOperationException($"More than one alert exists for alert: {systemAlert.ToJson()}");
                }
            }

            await _systemAlertRepository.Add(systemAlert);
        }

        private AlertSearchCriteria CreateSearchCriteria(SystemAlert alert)
        {
            var searchCriteria = new AlertSearchCriteria();

            if (alert.TargetObjectId.HasValue)
            {
                searchCriteria.TargetObjectId = alert.TargetObjectId;
                return searchCriteria;
            }

            searchCriteria.ParentIds = new[] { alert.ParentId };
            searchCriteria.ParentType = alert.ParentType;
            searchCriteria.AlertTypes = new[] { alert.AlertType };

            return searchCriteria;
        }

        private SystemAlert CreateWorkOrderNoteAlert(WorkOrder workOrder, string note, AlertType alertType, bool addId = true, Guid? targetObjectId = null)
        {
            var userId = _userContext.UserId;
            var author = _userContext.FullName();
            var noteId = targetObjectId ?? SystemAlertHelper.ComputeTargetObjectId(workOrder.Id, AlertParentType.WorkOrder, alertType);
            var noteAlert = new NoteAlert
            {
                Note = new Note { Id = noteId, Author = author, Created = DateTime.UtcNow, Text = note },
                WorkOrder = new NamedId<long>(workOrder.Id, workOrder.Name)
            };

            var alert = new SystemAlert
            {
                TargetObjectId = noteId,
                ParentId = workOrder.Id,
                ParentType = AlertParentType.WorkOrder,
                AlertType = alertType,
                Created = DateTime.UtcNow,
                CreatedById = userId,
                Content = noteAlert.ToJson()
            };

            if (addId)
            {
                alert.Id = targetObjectId ?? Guid.NewGuid();
            }

            return alert;
        }
    }
}
