﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Domain.Models;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class UserInfoMailer : IUserInfoMailer
    {
        private readonly IEmailService _emailService;

        public UserInfoMailer(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public async  Task EmailForgotPassWord(UserInfo user, string url)
        {
            var email = new Email
            {
                EmailTemplateType = Email.TemplateType.PassswordReset,
                Content = new UserEmailModel { User = user, Url = url },
                Recipients = new string[] { user.Email },
                Subject = "Image Painting - Password Reset"
            };

            await _emailService.SendAsync(email);
        }
    }
}
