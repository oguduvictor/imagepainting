﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.Domain.Services
{
    public class CommunityComposerService : ICommunityComposerService
    {
        private const decimal DEFAULT_COST = 1.50m;

        private readonly IBuilderRepository _builderRepository;
        private readonly ICommunityRepository _communityRepository;
        private readonly ICostRepository _costRepository;
        private readonly IUserContext _userContext;
        private readonly IPlanRepository _planRepository;
        private readonly IElevationRepository _elevationRepository;
        private readonly ILogger _logger;
        private readonly IColorSchemeRepository _colorSchemeRepository;
        private readonly ISupplyRepository _supplyRepository;
        private readonly IFileUploadRepository _fileRepository;

        public CommunityComposerService(
            IBuilderRepository builderRepository,
            ICommunityRepository communityRepository,
            ICostRepository costRepository,
            IUserContext userContext,
            IPlanRepository planRepository,
            IElevationRepository elevationRepository,
            ILogger logger,
            IColorSchemeRepository colorSchemeRepository,
            ISupplyRepository supplyRepository,
            IFileUploadRepository fileRepository)
        {
            _builderRepository = builderRepository;
            _communityRepository = communityRepository;
            _costRepository = costRepository;
            _userContext = userContext;
            _planRepository = planRepository;
            _elevationRepository = elevationRepository;
            _logger = logger;
            _colorSchemeRepository = colorSchemeRepository;
            _supplyRepository = supplyRepository;
            _fileRepository = fileRepository;
        }

        #region Communities

        public Task<List<Community>> GetCommunitiesSummary(long? builderId = null)
        {
            return _communityRepository.GetCommunities(true, false, builderId);
        }

        public async Task<List<Community>> GetCommunities()
        {
            return await _communityRepository.GetCommunities(false, true);
        }

        public async Task<Community> GetCommunity(int communityId)
        {
            return communityId > 0
                   ? await _communityRepository.GetCommunity(communityId, true)
                   : new Community { Active = true };
        }

        public async Task<Community> SaveCommunity(Community community)
        {
            var userId = _userContext.UserId;

            community.Modified = DateTime.UtcNow;
            community.ModifiedById = userId;

            if (community.Id == default(int))
            {
                community.Created = DateTime.UtcNow;
                community.CreatedById = userId;

                return await _communityRepository.CreateCommunity(community);
            }

            var dbCommunity = await _communityRepository.GetCommunity(community.Id, true);

            dbCommunity.MergeShallow(community, nameof(community.CreatedById), nameof(community.Created));
            dbCommunity.Builders = community.Builders;

            return await _communityRepository.UpdateCommunity(dbCommunity);
        }

        public async Task<bool> DeleteCommunity(int communityId, int builderId = 0)
        {
            var community = await _communityRepository.GetCommunity(communityId, true);

            if (builderId > 0 && community.Builders.Count(x => !x.Deleted) > 1)
            {
                var builder = community.Builders.SingleOrDefault(x => x.Id == builderId);

                community.Builders.Remove(builder);

                await _communityRepository.UpdateCommunity(community);

                return true;
            }

            //var communityPlans = await _planRepository.GetPlanSummaries(communityId: community.Id);

            //if (community.Plans.Count(x => !x.Deleted) > 1)
            //{
            //    foreach (var plan in community.Plans)
            //    {
            //        await DeletePlan(plan.Id);
            //    }
            //}

            
            community.Active = false;
            community.Deleted = true;

            await _communityRepository.UpdateCommunity(community);

            await _fileRepository.DeleteFileUploads(new long[] { communityId }, FileOwnerType.Community);

            return true;
        }

        #endregion

        #region Builders

        public Task<List<Builder>> GetBuildersSummary()
        {
            return _builderRepository.GetBuilders();
        }

        public async Task<List<Builder>> GetBuilders()
        {
            return await _builderRepository.GetBuilders(false);
        }

        public async Task<List<Builder>> GetBuilders(List<int> builderIds)
        {
            return await _builderRepository.GetBuilders(builderIds);
        }

        public async Task<Builder> GetBuilder(int? builderId, bool includeCost = false, bool includePlans = false)
        {
            var builder = new Builder { Active = true };

            if (builderId > 0)
            {
                builder = await _builderRepository.GetBuilder(builderId.Value, false, includeCost, includePlans);
                builder.Costs = builder.Costs.Where(x => x.BuilderId == builderId).ToList();
            }

            return builder;
        }

        public async Task<Builder> SaveBuilder(Builder builder, int buiderCopiedCostId)
        {
            var userId = _userContext.UserId;

            builder.ModifiedById = userId;
            builder.Modified = DateTime.UtcNow;

            if (builder.Id == default(int))
            {
                if (buiderCopiedCostId != default(int))
                {
                    await AddCopiedCosts(builder, buiderCopiedCostId);
                }
                else
                {
                    await AddDefaultCosts(builder);
                }
                
                builder.Created = DateTime.UtcNow;
                builder.CreatedById = userId;

                builder = await _builderRepository.SaveBuilder(builder);
            }
            else
            {
                var dbBuilder = await _builderRepository.GetBuilder(builder.Id, false, true);

                dbBuilder.MergeShallow(builder, nameof(builder.CreatedById), nameof(builder.Created));

                await AddDefaultCosts(dbBuilder);

                builder = await _builderRepository.UpdateBuilder(dbBuilder);
            }
 
            return builder;
        }

        public async Task<bool> DeleteBuilder(int builderId)
        {
            var builderWasDeleted = false;
            var builder = await _builderRepository.GetBuilder(builderId);

            if (!builder.IsNull())
            {
                var builderCommunities = await _communityRepository.GetCommunities(isSummary: true, builderId: builder.Id);

                //builder.Costs = await _costRepository.GetCosts(builder.Id);

                //foreach(var cost in builder.Costs)
                //{
                //    await DeleteCost(cost.Id);
                //}

                foreach (var community in builderCommunities)
                {
                    await DeleteCommunity(community.Id, builderId);
                }

                builder.Deleted = true;
                builder.Active = false;

                await _builderRepository.UpdateBuilder(builder);

                await _fileRepository.DeleteFileUploads(new long[] { builderId }, FileOwnerType.Builder);

                builderWasDeleted = true;
            }

            return builderWasDeleted;
        }

        public async Task UpdateBuilderCosts(Builder builder)
        {
            await _builderRepository.UpdateBuilderCosts(builder);
        }

        public async Task<Cost> GetCostForBuilder(int builderId, int costId)
        {
            var cost = default(Cost);

            if (builderId == default(int))
            {
                var costItem = await _costRepository.GetCostItem(costId);

                if (!costItem.IsNull())
                {
                    cost = new Cost
                    {
                        Id = costItem.Id,
                        CostItemId = costItem.Id,
                        CostItem = costItem,
                        Amount = costItem.LaborCost,
                        Builder = new Builder()
                    };
                }
                else
                {
                    cost = new Cost { CostItem = new CostItem(), Builder = new Builder() };
                }

                return cost;
            }

            if (costId > 0)
            {
                cost = await _costRepository.GetCost(costId, true, true);
            }
            else
            {
                var builder = await _builderRepository.GetBuilder(builderId);

                cost = new Cost { Builder = builder, BuilderId = builder.Id, CostItem = new CostItem { PercentOfTotalSize = 100 } };
            }

            return cost;
        }

        public Task<List<CostItem>> GetCostItems(int builderId = 0)
        {
            if (builderId > 0)
            {
                return _costRepository.GetCosts(builderId)
                    .ContinueWith(task =>
                    {
                        return task.Result.Select(x => x.CostItem).ToList();
                    });
            }

            return _costRepository.GetCostItems();
        }

        public Task<IEnumerable<CostItem>> GetCostItemsNotInBuilder(int builderId)
        {
            IEnumerable<CostItem> allCostItems = null;

            return _costRepository.GetCostItems()
                    .ContinueWith(t1 =>
                    {
                        allCostItems = t1.Result;

                        return _costRepository.GetCosts(builderId, null);
                    })
                    .ContinueWith(t2 =>
                    {
                        var builderCostItems = t2.Result.Result.Select(x => x.CostItem);

                        return allCostItems.Where(x => !builderCostItems.Any(y => y.Id == x.Id));
                    });
        }

        public async Task<Cost> SaveCost(Cost cost)
        {
            if (cost.Id > 0)
            {
                var dbCost = await _costRepository.GetCost(cost.Id, includeCostItem: true);

                dbCost.Amount = cost.Amount;
                dbCost.CostItem.IsExtra = cost.CostItem.IsExtra;
                dbCost.CostItem.IsLumpSum = cost.CostItem.IsLumpSum;
                dbCost.CostItem.TaskType = cost.CostItem.TaskType;
                dbCost.CostItem.PercentOfTotalSize = cost.CostItem.PercentOfTotalSize;
                dbCost.CostItem.PercentGarageSqft = cost.CostItem.PercentGarageSqft;
                dbCost.CostItem.PercentLivingSqft = cost.CostItem.PercentLivingSqft;

                if (!cost.CostItem.Name.IsNullOrEmpty())
                {
                    dbCost.CostItem.Name = cost.CostItem.Name;
                }

                await _costRepository.UpdateCost(dbCost);
                return dbCost;
            }

            var costItem = default(CostItem);

            if (cost.CostItemId > 0)
            {
                costItem = await _costRepository.GetCostItem(cost.CostItemId);
            }

            if (costItem.IsNull() && !cost.CostItem.Name.IsNullOrEmpty())
            {
                costItem = await _costRepository.GetCostItemByName(cost.CostItem.Name);
            }
           
            if (!costItem.IsNull())
            {
                //costItem.IsExtra = cost.CostItem.IsExtra;
                //costItem.IsLumpSum = cost.CostItem.IsLumpSum;
                cost.CostItem = costItem;
            }
            else
            {
                await SaveCostItem(cost.CostItem);
                cost.CostItemId = cost.CostItem.Id;
            }

            await _costRepository.CreateCost(cost);

            return cost;
        }

        public async Task DeleteCost(int id)
        {
            var cost = await _costRepository.GetCost(id, includeCostItem: true);

            if (cost == null)
            {
                throw new ArgumentException($"No cost matches the ID {nameof(id)}");
            }

            if (cost.CostItem.Name == SiteContants.BASE_PRICE)
            {
                throw new InvalidOperationException($"{SiteContants.BASE_PRICE} cannot be deleted");
            }

            await _costRepository.DeleteCost(id);
        }

        public async Task DeleteCostItem(int id)
        {
            var costItem = await _costRepository.GetCostItem(id);

            if (costItem == null)
            {
                throw new ArgumentException($"No cost item matches the ID {nameof(id)}");
            }

            if (costItem.Name == SiteContants.BASE_PRICE)
            {
                throw new InvalidOperationException($"{SiteContants.BASE_PRICE} cannot be deleted");
            }

            await _costRepository.DeleteCostItem(id);
        }

        public async Task UpdateCostItems(IEnumerable<CostItem> costItems, bool laborCostOnly = false)
        {
            var dbCostItems = await _costRepository.GetCostItems();

            dbCostItems.ForEach(item =>
            {
                var costItem = costItems.FirstOrDefault(x => x.Id == item.Id);

                if (costItem == null)
                {
                    return;
                }

                item.LaborCost = costItem.LaborCost;
                item.Order = costItem.Order;

                if (!laborCostOnly)
                {
                    item.IsLumpSum = costItem.IsLumpSum;
                    item.IsExtra = costItem.IsExtra;
                    item.Name = costItem.Name;   
                }
            });

            await _costRepository.UpdateCostItems(dbCostItems.ToArray());
        }
        #endregion
        
        #region Plans

        public async Task<IEnumerable<PlanSummary>> GetPlans()
        {
            return await _planRepository.GetPlanSummaries();
        }

        public Task<List<PlanSummary>> GetPlansSummary(int? builderId)
        {
            return _planRepository.GetPlanSummaries(builderId: builderId);
        }
        
        public async Task<Plan> GetPlan(int planId, bool includeElevations = false, bool includeCommunities = false)
        {
            if (planId > 0)
            {
                var plan = await _planRepository.GetPlan(planId, includeElevations, includeCommunities);
                plan.Elevations = plan.Elevations.Where(x => x.Deleted == false).ToList();

                return plan;
            }

            return new Plan { Id = 0 };
        }

        public async Task<Plan> SavePlan(Plan plan)
        {
            var userId = _userContext.UserId;

            plan.Modified = DateTime.UtcNow;
            plan.ModifiedById = userId;

            if (plan.Id == default(int))
            {
                plan.Created = DateTime.UtcNow;
                plan.CreatedById = userId;

                plan = await _planRepository.CreatePlan(plan);
            }
            else
            {
                var dbPlan = await _planRepository.GetPlan(plan.Id);

                dbPlan.Name = plan.Name;
                dbPlan.Stories = plan.Stories;
                dbPlan.BuilderId = plan.BuilderId;
                if (!plan.Communities.IsNull())
                {
                    foreach (var community in plan.Communities)
                    {
                        var dbCommunity = await _communityRepository.GetCommunity(community.Id);
                        dbPlan.Communities.Add(dbCommunity);
                    }
                }

                plan = await _planRepository.UpdatePlan(dbPlan);
            }

            return plan;
        }

        public async Task<bool> DeletePlan(int planId)
        {
            var planWasDeleted = false;

            var plan = await _planRepository.GetPlan(planId, includeCommunities:true);

            if (!plan.IsNull())
            {
                var planElevations = await _elevationRepository.GetElevationsSummary(planId: plan.Id);

                foreach (var elevation in planElevations)
                {
                    await DeleteElevation(elevation.Id);
                }

                if (plan.Communities.Count(x => !x.Deleted) > 0)
                {
                    plan.Communities = null;
                }

                plan.Deleted = true;
                await _planRepository.UpdatePlan(plan);

                await _fileRepository.DeleteFileUploads(new long[] { planId }, FileOwnerType.Plan);

                planWasDeleted = true;
            }

            return planWasDeleted;
        }

        public async Task<bool> DeletePlanCommunity(int planId, int communityId)
        {
            var dbplan = await _planRepository.GetPlan(planId, includeCommunities: true);
            if(!dbplan.IsNull())
            {
                var community = dbplan.Communities.FirstOrDefault(x => x.Id == communityId);
                if(!community.IsNull())
                {
                    dbplan.Communities.Remove(community);
                    await _planRepository.UpdatePlan(dbplan);
                    return true;
                }
                return false;
            }

            return false;
        }

        #region Elevation
        public Task<List<Elevation>> GetElevationsSummary(int? planId = null)
        {
            return _elevationRepository.GetElevationsSummary(planId);
        }

        public async Task<Elevation> DeleteElevation(int elevationId)
        {
            var elevation = await _elevationRepository.GetElevation(elevationId);

            if (!elevation.IsNull())
            {
                elevation.Deleted = true;
                await _elevationRepository.UpdateElevation(elevation);

                await _fileRepository.DeleteFileUploads(new long[] { elevationId }, FileOwnerType.Elevation);
            }

            return elevation;
        }

        public async Task<Elevation> GetElevation(int elevationId)
        {
            return (elevationId == default(int)) ? new Elevation { Id = 0 } :
                await _elevationRepository.GetElevation(elevationId);
        }

        public async Task<Elevation> SaveElevation(Elevation elevation)
        {
            var userId = _userContext.UserId;

            elevation.ModifiedById = userId;
            elevation.Modified = DateTime.UtcNow;

            if (elevation.Id == default(int))
            {
                elevation.Created = DateTime.UtcNow;
                elevation.CreatedById = userId;

                return await _elevationRepository.CreateElevation(elevation);
            }

            var dbElevation = await _elevationRepository.GetElevation(elevation.Id);

            dbElevation.MergeShallow(elevation, nameof(elevation.Id), nameof(elevation.CreatedById));

            return await _elevationRepository.UpdateElevation(dbElevation);
        }

        #endregion

        #endregion

        #region InteriorColorSchemes

        public async Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemes()
        {
            return await _colorSchemeRepository.GetInteriorColorSchemes();
        }

        public async Task<InteriorColorScheme> GetInteriorColorScheme(int colorSchemeId)
        {
            if (colorSchemeId > 0)
            {
                return await _colorSchemeRepository.GetInteriorColorScheme(colorSchemeId);
            }

            return new InteriorColorScheme { Id = 0 };
        }

        public async Task<InteriorColorScheme> SaveInteriorColorScheme(InteriorColorScheme colorScheme)
        {
            if (colorScheme.Id == 0)
            {
                return await _colorSchemeRepository.CreateInteriorColorScheme(colorScheme);
            }

            var dbColorScheme = await _colorSchemeRepository.GetInteriorColorScheme(colorScheme.Id);

            dbColorScheme.MergeShallow(colorScheme, nameof(colorScheme.Id));

            return await _colorSchemeRepository.UpdateInteriorColorScheme(dbColorScheme);
        }

        public async Task<bool> DeleteInteriorColorScheme(int colorSchemeId)
        {
            return await _colorSchemeRepository.DeleteInteriorColorScheme(colorSchemeId);
        }

        public Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemeSummary(string colorCodePrefix)
        {
            return _colorSchemeRepository.GetInteriorColorSchemeSummary(colorCodePrefix);
        }
        #endregion

        #region ExteriorColorScheme

        public async Task<IEnumerable<ExteriorColorScheme>> GetExteriorColorSchemes()
        {
            return await _colorSchemeRepository.GetExteriorColorSchemes();
        }

        public async Task<ExteriorColorScheme> GetExteriorColorScheme(int colorSchemeId)
        {
            if (colorSchemeId > 0)
            {
                return await _colorSchemeRepository.GetExteriorColorScheme(colorSchemeId);
            }

            return new ExteriorColorScheme { Id = 0 };
        }

        public async Task<ExteriorColorScheme> SaveExteriorColorScheme(ExteriorColorScheme colorScheme)
        {
            if (colorScheme.Id == 0)
            {
                return await _colorSchemeRepository.CreateExteriorColorScheme(colorScheme);
            }

            var dbColorScheme = await _colorSchemeRepository.GetExteriorColorScheme(colorScheme.Id);

            dbColorScheme.MergeShallow(colorScheme, nameof(colorScheme.Id));

            return await _colorSchemeRepository.UpdateExteriorColorScheme(dbColorScheme);
        }

        public async Task<bool> DeleteExteriorColorScheme(int colorSchemeId)
        {
            return await _colorSchemeRepository.DeleteExteriorColorScheme(colorSchemeId);
        }
        
        public Task<IEnumerable<ExteriorColorScheme>> GetExeteriorColorSchemeSummary(string colorCodePrefix)
        {
            return _colorSchemeRepository.GetExeteriorColorSchemeSummary(colorCodePrefix);
        }

        #endregion

        #region Supplies
        public Task<List<Supply>> GetBuilderSupplies(int builderId , long storeId)
        {
            return _supplyRepository.GetSupplies(builderId, storeId);
        }

        public async Task<SuppliesViewModel> GetSupplies(int builderId, long storeId = 0)
        {
            if (storeId == default(long))
            {
                storeId = await _supplyRepository.GetDefaultStoreId(builderId) 
                            ?? await _supplyRepository.GetAnyPaintStore(builderId);
            }

            var supplies = await _supplyRepository.GetSupplies(builderId, storeId);

            if (supplies.Count() == 0 && storeId > 0)
            {
                supplies = PrefillSupplies(builderId);
            }
            
            var builder = await _builderRepository.GetBuilder(builderId);

            var viewModel = new SuppliesViewModel { BuilderId = builderId, BuilderName = builder.Name, Supplies = supplies, PaintStoreId = storeId };

            return viewModel;
        }

        public async Task<List<int>> AddSupplyTypes(List<SupplyTypeSummary> supplyTypes)
        {
            return await _supplyRepository.AddSupplyTypes(supplyTypes);
        }

        public async Task SaveSupplies(SuppliesViewModel viewModel)
        {
            var affectedSupplies = viewModel.Supplies.Where(x => x.SupplyTypeId == 0);
            var nonAffectedSupplies = viewModel.Supplies.Except(affectedSupplies);
            var supplyTypesToAdd = affectedSupplies
                .Select(x => new SupplyTypeSummary { Name = x.SupplyType.Name, supplyCategory = x.SupplyType.SupplyCategory }).ToList();

            var mappings = await AddSupplyTypes(supplyTypesToAdd);
            var index = 0;
            affectedSupplies.ForEach(x =>
            {
                x.SupplyTypeId = mappings[index];
                index++;
            });


            viewModel.Supplies = await AssignSupplyTypes(affectedSupplies.Concat(nonAffectedSupplies));

            var dbSupplies = await _supplyRepository.GetSupplies(viewModel.BuilderId, paintStoreId : viewModel.PaintStoreId);

            if (!viewModel.Supplies.IsNullOrEmpty() || !dbSupplies.IsNullOrEmpty())
            {
                var supplies = viewModel.Supplies.IsNullOrEmpty() ? new List<Supply>() : viewModel.Supplies;

                var supplyIds = supplies.Select(x => x.Id);
                
                var removedSupplies = dbSupplies.Where(x => !supplyIds.Contains(x.Id));

                var newSupplies = supplies.Where(x => x.Id == 0);

                var existingSupplies = new List<Supply>();                

                supplies.ForEach(supply => {
                    var existingSupply = dbSupplies.FirstOrDefault(x => x.Id == supply.Id);

                    if (!existingSupply.IsNull())
                    {
                        existingSupply.MergeShallow(supply, nameof(supply.Id), nameof(supply.BuilderId));
                        existingSupplies.Add(existingSupply);
                    }
                });

                if (newSupplies.Count() > 0)
                {
                    newSupplies.ForEach(x =>
                    {
                        x.SupplyType = null;
                    });
                    await _supplyRepository.Create(newSupplies);
                }

                if (existingSupplies.Count() > 0)
                {
                    await _supplyRepository.Update(existingSupplies);
                }

                if(removedSupplies.Count() > 0)
                {
                    await _supplyRepository.Delete(removedSupplies);
                }
            }

            await SetDefaultBuilderStore(viewModel.BuilderId, viewModel.PaintStoreId);
        }

        public Task<List<SupplyType>> GetSupplyTypes(SupplyCategory category = SupplyCategory.All)
        {
            return _supplyRepository.GetSupplyTypes(category);
        }

        public void DeleteSuppliesByStoreId(long paintStoreId)
        {
            try
            {
                _supplyRepository.DeleteSuppliesByStoreId(paintStoreId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            
        }
        #endregion

        #region Helpers

        public Task<List<State>> GetStates()
        {
            return _builderRepository.GetStates();
        }

        public async Task<CostItem> SaveCostItem(CostItem costItem)
        {
            if (costItem.Id < 1)
            {
                await _costRepository.CreateCostItem(costItem);

                return costItem;
            }

            var dbCostItem = await _costRepository.GetCostItem(costItem.Id);

            if (dbCostItem.IsNull())
            {
                throw new InvalidOperationException("cost item doesn't exist in the system");
            }

            dbCostItem.IsExtra = costItem.IsExtra;
            dbCostItem.IsLumpSum = costItem.IsLumpSum;
            dbCostItem.Name = costItem.Name ?? dbCostItem.Name;

            await _costRepository.UpdateCostItems(dbCostItem);

            return dbCostItem;
        }

        private async Task AddDefaultCosts(Builder builder)
        {
            if (!builder.Costs.IsNullOrEmpty())
            {
                return;
            }

            var costItems = await _costRepository.GetCostItems();
            var costs = new List<Cost>();

            foreach (var costItem in costItems)
            {
                var cost = new Cost
                {
                    Amount = DEFAULT_COST,
                    BuilderId = builder.Id,
                    CostItemId = costItem.Id
                };

                costs.Add(cost);
            }

            _builderRepository.AddCosts(costs, builder);
        }

        private async Task AddCopiedCosts(Builder builder, int builderCopiedCostId)
        {
            var copiedBuilder = await GetBuilder(builderCopiedCostId, true);
            var copiedCosts = copiedBuilder.Costs;

            _builderRepository.AddCosts(copiedCosts, builder);
        }

        private async Task<IEnumerable<Supply>> AssignSupplyTypes(IEnumerable<Supply> supplies)
        {
            if (supplies.IsNullOrEmpty())
            {
                return new List<Supply>();
            }

            var supplyTypes = await _supplyRepository.GetSupplyTypes(supplies.Select(x => x.SupplyTypeId));

            supplyTypes.ForEach(x =>
            {
                supplies.FirstOrDefault(y => y.SupplyTypeId == x.Id).SupplyType = x;
            });

            return supplies;
        }

        private List<Supply> PrefillSupplies(int builderId)
        {
            var supplyTypes = GetSupplyTypes().Result;
            
            List<Supply> prefilledSupplies = new List<Supply>();

            supplyTypes.ForEach(x =>
            {
                prefilledSupplies.Add(new Supply
                {
                    Cost = 0,
                    SupplyType = x,
                    SupplyTypeId = x.Id,
                    BuilderId = builderId,
                    Id = 0,
                    Name = x.Name

                });
            });

            return prefilledSupplies;
        }

        private async Task SetDefaultBuilderStore(int builderId, long storeId)
        {
            var builder = await _builderRepository.GetBuilder(builderId);

            builder.DefaultStoreId = storeId;

            await _builderRepository.UpdateBuilder(builder);
        }

        #endregion
    }
}