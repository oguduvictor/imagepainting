﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class WorkOrderMailer : IWorkOrderMailer
    {
        private readonly IEmailService _emailService;
        private readonly IUrlService _urlService;
        private readonly ILogger _logger;
        private readonly IProjectionComposerService _projectionService;
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IUserInfoRepository _userRepository;
        private readonly IUserContext _userContext;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IAppSettings _appSettings;

        public WorkOrderMailer(
            IEmailService emailService,
            IUrlService urlService,
            IProjectionComposerService projectionService,
            IWorkOrderRepository workOrderRepository,
            IUserInfoRepository userRepository,
            ITaskRepository taskRepository,
            IUserContext userContext,
            IPurchaseOrderRepository purchaseOrderRepository,
            ILogger logger,
            IAppSettings appSettings
            )
        {
            _emailService = emailService;
            _urlService = urlService;
            _projectionService = projectionService;
            _workOrderRepository = workOrderRepository;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _logger = logger;
            _userContext = userContext;
            _purchaseOrderRepository = purchaseOrderRepository;
            _appSettings = appSettings;
        }

        public async Task EmailSchedules(IEnumerable<WorkOrderTask> tasks, string note)
        {
            var userId = _userContext.UserId;
            var initiator = await _userRepository.GetUserAsync(userId);
         
            if (tasks.IsNullOrEmpty())
            {
                return;
            }

            var taskGroups = tasks.GroupBy(x => x.CrewId);

            foreach (var taskGroup in taskGroups)
            {
                if(taskGroup.First().Crew.IsNull())
                {
                    continue;
                }
                
                var taskList = taskGroup.ToList();
                var model = GetTaskScheduleModels(taskList, initiator.FullName, note);
                var recipient = taskList.First().Crew.Email;
                
                try
                {
                    var email = new Email
                    {
                        EmailTemplateType = Email.TemplateType.TaskSchedule,
                        Subject = "Scheduled Tasks",
                        Recipients = new string[] { recipient },
                        CC = new string[] { _appSettings.ScheduleEmailAddress },
                        Content = model
                    };

                    await _emailService.SendAsync(email);
                }
                catch(Exception ex)
                {
                    _logger.Error(ex);
                }
            }
        }

        public async Task EmailProjection(long workOrderId)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId);
            var projection = await _projectionService.GetRevenueProjection(workOrderId);

            if (projection.IsNull() || projection.ExtraItems.IsNull() || workOrder.IsNull())
            {
                return;
            }

            if (!workOrder.JobInfoId.HasValue || !workOrder.BuilderId.HasValue)
            {
                throw new InvalidOperationException("Not enough information to send projection");
            }
            
            var emailModel = new ProjectionEmailModel
            {
                WorkOrderName = workOrder.Name,
                Projection = projection
            };

            var recipients = new string[] { _appSettings.AccountingEmailAddress };

            try
            {
                var email = new Email
                {
                    Subject = $"Image Painting Projection for {workOrder.Name}",
                    Recipients = recipients,
                    Content = emailModel,
                    EmailTemplateType = Email.TemplateType.RevenueProjection
                };

                await _emailService.SendAsync(email);
            }
            catch(Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task EmailTaskCompletion(long workOrderTaskId, TaskCategory taskCategory)
        {
            var task = await _taskRepository.GetTask(workOrderTaskId, includeCrew: true, includeWorkOrder: true);
            var adminUsers = await _userRepository.GetUsersInRoleAsync(UserRole.Administrator);
            string recipientAddress = string.Empty;

            switch (taskCategory)
            {
                case TaskCategory.Prewalk:
                    recipientAddress = _appSettings.ATaskEmailAddress;
                    break;
                case TaskCategory.Main:
                    recipientAddress = _appSettings.BTaskEmailAddress;
                    break;
                case TaskCategory.Completion:
                    recipientAddress = _appSettings.CTaskEmailAddress;
                    break;
                default:
                    break;
            }

            var mailModel = new WorkOrderCompletionMailModel
            {
                TaskName = task.Name,
                WorkOrderName = task.WorkOrder.Name,
                Completed = task.Completed.Value.ToLocalShortDateTime(),
                WorkOrderUrl = _urlService.GenerateUrl("WorkOrder", "WorkOrders", new { id = task.WorkOrderId }),
                TaskUrl = _urlService.GenerateUrl("ViewTask", "Tasks", new { id = task.Id }),
                CrewName = task.Crew?.FullName
            };

            try
            {
                var email = new Email
                {
                    Subject = $"{task.Name} completed by {task.Crew.FullName}",
                    Recipients =  new string[] { recipientAddress },
                    Content = mailModel,
                    EmailTemplateType = Email.TemplateType.TaskCompletion
                };

                await _emailService.SendAsync(email);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task EmailPurchaseOrder(long orderId, string emailAddress)
        {
            var purchaseOrder = await _purchaseOrderRepository.Get(
                    orderId,
                    includeItems: true,
                    includeLocation: true,
                    includeTask: true);
            var model = await GetPurchaseOrderEmailOrder(purchaseOrder, orderId);

            try
            {
                var email = new Email
                {
                    Subject = $"{purchaseOrder.Task.TaskType} Paint for {model.WorkOrder.Name}",
                    Recipients = new string[] { emailAddress },
                    CC = new string[] { _appSettings.POEmailAddress },
                    Content = model,
                    EmailTemplateType = Email.TemplateType.PurchaseOrder
                };

                await _emailService.SendAsync(email);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public async Task EmailFreeCrewsSchedule(IEnumerable<UserInfo> freeCrews, DateTime taskDate, string note)
        {
            if (freeCrews.IsNullOrEmpty())
            {
                return;
            }

            foreach (var crew in freeCrews)
            {
                var model = new FreeCrewScheduleMailModel
                {
                    CrewName = crew.FirstName,
                    taskDate = taskDate.Date.ToString("MMM dd yyyy"), 
                    Note = note
                };

                try
                {
                    var email = new Email
                    {
                        EmailTemplateType = Email.TemplateType.NoTaskAssignment,
                        Subject = "No Scheduled Tasks",
                        Recipients = new string[] { crew.Email },
                        CC = new string[] { _appSettings.ScheduleEmailAddress },
                        Content = model
                    };

                    await _emailService.SendAsync(email);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
        }

        public async Task<MailMessage> PreviewPurchaseOrder(long orderId)
        {
            var sender = _userContext.Email;

            MailMessage messageBody = null;

            var purchaseOrder = await _purchaseOrderRepository.Get(
                    orderId,
                    includeItems: true,
                    includeLocation: true,
                    includeTask: true).ConfigureAwait(false);
            var model = await GetPurchaseOrderEmailOrder(purchaseOrder, orderId).ConfigureAwait(false);
            try
            {
                var email = new Email
                {
                    Subject = $"{purchaseOrder.Task.TaskType} Paint for {model.WorkOrder.Name}",
                    Recipients = new string[] { purchaseOrder.Email },
                    CC = new string[] { sender },
                    Content = model,
                    EmailTemplateType = Email.TemplateType.PurchaseOrder
                };

               messageBody =  _emailService.PreviewEmail(email);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return messageBody;
        }

        private async Task<PurchaseOrderEmailModel> GetPurchaseOrderEmailOrder(PurchaseOrder purchaseOrder, long orderId)
        {
            var colorScheme = string.Empty;
            
            var workOrder = await _workOrderRepository.GetWorkOrder(
                    purchaseOrder.Task.WorkOrderId,
                    includeBuilder: true,
                    includeCommunities: true,
                    includeJobInfo: true);

            if (purchaseOrder.Task.TaskType == TaskType.Interior)
            {
                colorScheme = workOrder.JobInfo.InteriorColors.ColorCode;
            }
            else if (purchaseOrder.Task.TaskType == TaskType.Exterior)
            {
                colorScheme = workOrder.JobInfo.ExteriorColors.ColorCode;
            }

            var model = new PurchaseOrderEmailModel
            {
                ColorScheme = colorScheme,
                WorkOrder = workOrder,
                PurchaseOrder = purchaseOrder
            };

            return model;
        }

        private IEnumerable<WorkOrderScheduleMailModel> GetTaskScheduleModels(IEnumerable<WorkOrderTask> tasks, string initiatorName, string note)
        {
            var mailModels = new List<WorkOrderScheduleMailModel>();

            foreach (var task in tasks)
            {
                var model = new WorkOrderScheduleMailModel();

                mailModels.Add(model);
                
                model.CrewName = task.Crew.FullName;
                model.InitiatorName = initiatorName;
                model.ScheduleDate = task.ScheduleDateRange.GetDateRangeString();
                model.TaskName = task.Name;
                model.TaskUrl = _urlService.GenerateUrl("ViewTask", "Tasks", new { id = task.Id });
                model.WorkOrderName = task.WorkOrder.Name;
                model.WorkOrderAddress = task.WorkOrder.Address;
                model.Lot = task.WorkOrder.Lot.HasValue ? task.WorkOrder.Lot.ToString() : string.Empty;
                model.Block = task.WorkOrder.Block;
                model.BuilderName = task.WorkOrder.Builder.Name;
                model.CommunityName = task.WorkOrder.Community.Name;
                model.ColorSchemeType = task.TaskType.IsAnyOf(TaskType.Interior, TaskType.Exterior) ? task.TaskType.ToString() : string.Empty;
                model.Note = note == string.Empty ? null : note;
                
                if (!task.IsMainTask)
                {
                    continue;
                }

                if (task.TaskType == TaskType.Interior)
                {
                    model.ColorSchemeName = task.WorkOrder.JobInfo.InteriorColors.ColorCode;
                    model.Colors = GetInteriorColors(task.WorkOrder.JobInfo.InteriorColors);
                }
                else if (task.TaskType == TaskType.Exterior)
                {
                    model.ColorSchemeName = task.WorkOrder.JobInfo.ExteriorColors.ColorCode;
                    model.Colors = GetExteriorColors(task.WorkOrder.JobInfo.ExteriorColors);
                }
            }

            return mailModels;
        }
        
        private IDictionary<string, string> GetExteriorColors(ExteriorColorScheme exteriorColors)
        {
            var colors = new Dictionary<string, string>();

            if (!exteriorColors.ExteriorBase.IsNullOrEmpty())
            {
                colors.Add("Exterior Base", exteriorColors.ExteriorBase);
            }

            if (!exteriorColors.AccentBase.IsNullOrEmpty())
            {
                colors.Add("Accent Base", exteriorColors.AccentBase);
            }

            if (!exteriorColors.ExteriorTrim.IsNullOrEmpty())
            {
                colors.Add("Exterior Trim", exteriorColors.ExteriorTrim);
            }

            if (!exteriorColors.FrontDoor.IsNullOrEmpty())
            {
                colors.Add("Front Door", exteriorColors.FrontDoor);
            }

            if (!exteriorColors.Shutters.IsNullOrEmpty())
            {
                colors.Add("Shutters", exteriorColors.Shutters);
            }

            if (!exteriorColors.GarageDoor.IsNullOrEmpty())
            {
                colors.Add("Garage Door", exteriorColors.GarageDoor);
            }

            if (!exteriorColors.HardiSiding.IsNullOrEmpty())
            {
                colors.Add("Hardi Siding", exteriorColors.HardiSiding);
            }

            if (!exteriorColors.BoardAndBatton.IsNullOrEmpty())
            {
                colors.Add("Board and Batten", exteriorColors.BoardAndBatton);
            }

            if (!exteriorColors.ShakeSiding.IsNullOrEmpty())
            {
                colors.Add("Shake Siding", exteriorColors.ShakeSiding);
            }

            if (!exteriorColors.Corbels.IsNullOrEmpty())
            {
                colors.Add("Corbels", exteriorColors.Corbels);
            }

            if (!exteriorColors.Brackets.IsNullOrEmpty())
            {
                colors.Add("Brackets", exteriorColors.Brackets);
            }

            if (!exteriorColors.Louvre.IsNullOrEmpty())
            {
                colors.Add("Louvre", exteriorColors.Louvre);
            }

            if (!exteriorColors.Stain.IsNullOrEmpty())
            {
                colors.Add("Stain", exteriorColors.Stain);
            }

            if (!exteriorColors.Facia.IsNullOrEmpty())
            {
                colors.Add("Facia", exteriorColors.Facia);
            }

            if (!exteriorColors.Soffit.IsNullOrEmpty())
            {
                colors.Add("Soffit", exteriorColors.Soffit);
            }
            
            if (!exteriorColors.Note.IsNullOrEmpty())
            {
                colors.Add("Note", exteriorColors.Note);
            }
           
            return colors;
        }

        private IDictionary<string, string> GetInteriorColors(InteriorColorScheme interiorColors)
        {
            var colors = new Dictionary<string, string>();

            if (!interiorColors.InteriorBase.IsNullOrEmpty())
            {
                colors.Add("Interior Base", interiorColors.InteriorBase);
            }

            if (!interiorColors.InteriorWoodTrimBaseCasing.IsNullOrEmpty())
            {
                colors.Add("Interior Trim Base/Casing", interiorColors.InteriorWoodTrimBaseCasing);
            }

            if (!interiorColors.InteriorDoors.IsNullOrEmpty())
            {
                colors.Add("Interior Doors", interiorColors.InteriorDoors);
            }

            if (!interiorColors.InteriorCeiling.IsNullOrEmpty())
            {
                colors.Add("Interior Ceiling", interiorColors.InteriorCeiling);
            }

            if (!interiorColors.Crown.IsNullOrEmpty())
            {
                colors.Add("Crown", interiorColors.Crown);
            }

            if (!interiorColors.AccentRoom1.IsNullOrEmpty())
            {
                colors.Add("Accent Room 1", interiorColors.AccentRoom1);
            }

            if (!interiorColors.AccentRoom2.IsNullOrEmpty())
            {
                colors.Add("Accent Room 2", interiorColors.AccentRoom2);
            }

            if (!interiorColors.AccentRoom3.IsNullOrEmpty())
            {
                colors.Add("Accent Room 3", interiorColors.AccentRoom3);
            }

            if (!interiorColors.AccentWall1.IsNullOrEmpty())
            {
                colors.Add("Accent Wall 1", interiorColors.AccentWall1);
            }

            if (!interiorColors.AccentWall2.IsNullOrEmpty())
            {
                colors.Add("Accent Wall 2", interiorColors.AccentWall2);
            }

            if (!interiorColors.AccentWall3.IsNullOrEmpty())
            {
                colors.Add("Accent Wall 3", interiorColors.AccentWall3);
            }

            if (!interiorColors.Stain.IsNullOrEmpty())
            {
                colors.Add("Stained Rail", interiorColors.Stain);
            }

            if (!interiorColors.Rail.IsNullOrEmpty())
            {
                colors.Add("Painted Rail", interiorColors.Rail);
            }

            if (!interiorColors.Treads.IsNullOrEmpty())
            {
                colors.Add("Treads", interiorColors.Treads);
            }

            if (!interiorColors.Risers.IsNullOrEmpty())
            {
                colors.Add("Risers", interiorColors.Risers);
            }

            if (!interiorColors.NewelPosts.IsNullOrEmpty())
            {
                colors.Add("Newel Posts", interiorColors.NewelPosts);
            }
            
            if (!interiorColors.Beams.IsNullOrEmpty())
            {
                colors.Add("Beams", interiorColors.Beams);
            }

            if (!interiorColors.FrontDoor.IsNullOrEmpty())
            {
                colors.Add("Front Door", interiorColors.FrontDoor);
            }

            if (!interiorColors.Note.IsNullOrEmpty())
            {
                colors.Add("Note", interiorColors.Note);
            }

            return colors;
        }
    }
}
