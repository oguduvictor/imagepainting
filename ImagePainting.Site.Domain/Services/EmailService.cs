﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.Services
{
    public class EmailService : IEmailService
    {
        private readonly SmtpClient _smtpClient;
        private readonly ILogger _logger;
        private readonly IViewCreator _viewCreator;
        private readonly IUrlService _urlService;
        private readonly IAppSettings _appSettings;

        public EmailService(
            SmtpClient smtpClient, 
            IViewCreator viewCreator, 
            ILogger logger,
            IUrlService urlService,
            IAppSettings appSettings)
        {
            smtpClient.Port = appSettings.SmtpPort;
            smtpClient.Host = appSettings.SmtpHost;
            smtpClient.UseDefaultCredentials = appSettings.SmtpUsername.IsNullOrEmpty();
            smtpClient.EnableSsl = appSettings.SmtpEnableSsl;

            if (!smtpClient.UseDefaultCredentials)
            {
                smtpClient.Credentials = new NetworkCredential(appSettings.SmtpUsername, appSettings.SmtpPassword);
            }

            _smtpClient = smtpClient;
            _viewCreator = viewCreator;
            _urlService = urlService;
            _logger = logger;
            _appSettings = appSettings;
        }

        public void Send(Email email)
        {
            try
            {
                var message = GetMessage(email);

                _smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public MailMessage PreviewEmail(Email email)
        {
            if (email.EmailTemplateType == Email.TemplateType.None || email.Content.IsNull())
                throw new ArgumentException("Content and EmailTemplateType are required");

            MailMessage messageBody = null;
            try
            {
                messageBody = GetMessage(email);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return messageBody;
        }

        public async Task SendAsync(Email email)
        {
            if (email.EmailTemplateType == Email.TemplateType.None || email.Content.IsNull())
                throw new ArgumentException("Content and EmailTemplateType are required");

            try
            {
                var message = GetMessage(email);

                await _smtpClient.SendMailAsync(message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        private MailMessage GetMessage(Email email)
        {
            var message = new MailMessage();
            var layoutData = GetLayoutData();

            message.Subject = email.Subject;
            message.Body = _viewCreator.CreateView(email.EmailTemplateType, email.Content, layoutData);
            message.From = new MailAddress(_appSettings.SystemEmailSender);
            message.Sender = new MailAddress(_appSettings.EmailSender);
            message.IsBodyHtml = true;

            email.Recipients.ForEach(recipient => message.To.Add(recipient));

            if (!email.Bcc.IsNullOrEmpty())
            {
                email.Bcc.ForEach(recipient => message.Bcc.Add(recipient));
            }

            if (!email.CC.IsNullOrEmpty())
            {
                email.CC.ForEach(recipient => message.Bcc.Add(recipient));
            }

            return message;
        }

        private IDictionary<string, object> GetLayoutData()
        {
            var baseUrl = _urlService.GetAppBaseUrl();

            return new Dictionary<string, object>
            {
                {"AppBaseUrl", baseUrl }
            };
        }
    }
}
