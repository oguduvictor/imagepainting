﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.Models
{
    public class PurchaseOrderEmailModel
    {
        public PurchaseOrder PurchaseOrder { get; set; }

        public string ColorScheme { get; set; }

        public WorkOrder WorkOrder { get; set; }
    }
}
