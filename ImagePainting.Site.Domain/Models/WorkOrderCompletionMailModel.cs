﻿using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Domain.Models
{
    public class WorkOrderCompletionMailModel
    {
        public string TaskName { get; set; }
       
        public string WorkOrderName { get; set; }

        public string Completed { get; set; }

        public string WorkOrderUrl { get; set; }

        public string TaskUrl { get; set; }
       
        public string CrewName { get; set; }
        
    }
}
