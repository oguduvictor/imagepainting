﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.Models
{
    public class ProjectionEmailModel
    {
        public string WorkOrderName { get; set; }

        public RevenueProjection Projection { get; set; }
    }
}
