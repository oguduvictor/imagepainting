﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.Models
{
    public class UserEmailModel
    { 
        public UserInfo User { get; set; }

        public string Url { get; set; }
    }
}
