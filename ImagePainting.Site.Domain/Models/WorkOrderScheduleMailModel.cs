﻿using System.Collections.Generic;

namespace ImagePainting.Site.Domain.Models
{
    public class WorkOrderScheduleMailModel
    {
        public string TaskName { get; set; }

        public string TaskUrl { get; set; }

        public string WorkOrderName { get; set; }

        public string WorkOrderAddress { get; set; }

        public string BuilderName { get; set; }

        public string CommunityName { get; set; }

        public string Lot { get; set; }

        public string Block { get; set; }

        public string ColorSchemeType { get; set; }

        public string ColorSchemeName { get; set; }

        public IDictionary<string, string> Colors { get; set; }

        public string InitiatorName { get; set; }

        public string CrewName { get; set; }

        public string ScheduleDate { get; set; }

        public string Note { get; set; }
    }
}
