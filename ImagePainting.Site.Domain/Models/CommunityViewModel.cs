﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Domain.Models
{
    public class CommunityViewModel : Community
    {
        [EnsureMinimumElements(1, ErrorMessage = "At least one Builder is required")]
        public List<int> BuilderIds { get; set; }

        public Community MapFromViewModel()
        {
            return new Community
            {
                Active = Active,
                Builders = Builders,
                Created = Created,
                Id = Id,
                Name = Name,
                Notes = Notes,
                Abbreviation = Abbreviation
            };
        }

        public void MapToViewModel(Community community)
        {
            Active = community.Active;
            Builders = community.Builders;
            BuilderIds = community.Builders.Select(x => x.Id).ToList();
            Created = community.Created;
            Id = community.Id;
            Name = community.Name;
            Notes = community.Notes;
            Abbreviation = community.Abbreviation;
        }
    }
}
