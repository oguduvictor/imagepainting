﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.Models
{
    public class WorkOrderTaskPdfModel
    {
        public string TaskName { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhoneNumber { get; set; }
        
        public string CustomerEmail { get; set; }

        public string CustomerServiceRep { get; set; }

        public string WorkOrderName { get; set; }

        public string WorkOrderAddress { get; set; }

        public string BuilderName { get; set; }

        public string CommunityName { get; set; }

        public string ScheduleDate { get; set; }

        public string SignedDate { get; set; }

        public List<KeyValuePair<string, List<Note>>> NoteItems { get; set; }
            
        public List<KeyValuePair<string, string>> OtherTaskItems { get; set; }

        public decimal LaborCost { get; set; }

        public decimal MaterialCost { get; set; }

        public decimal TotalCost => LaborCost + MaterialCost;

        public bool IncludeSignature { get; set; }

        public string Signature { get; set; }

        public bool CompleteCopy { get; set; }

        public string Signatory { get; set; }

        public string SignOffDescription;

        public string TaskDescription { get; set; }
    }
}
