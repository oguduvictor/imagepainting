﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.TaskManagement
{
    public class WorkOrderTaskManager : IWorkOrderTaskManager
    {
        private readonly IUserContext _userContext;
        private readonly ITaskRepository _taskRepository;
        private readonly ITaskSchemaRepository _taskSchemaRepository;
        private readonly IUserInfoRepository _userInfoRepository;
        private readonly IWorkOrderRepository _workOrderRepository;
        private readonly IWorkOrderMailer _workOrderMailer;
        private readonly IFileUploadRepository _fileRepository;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly ICostRepository _costRepository;
        private readonly ISystemAlertService _systemAlertService;

        public WorkOrderTaskManager(
            ITaskRepository taskRepository,
            ITaskSchemaRepository taskSchemaRepository,
            IUserInfoRepository userInfoRepository,
            IWorkOrderRepository workOrderRepository,
            IWorkOrderMailer workOrderMailer,
            IUserContext userContext,
            IFileUploadRepository fileRepository,
            IPurchaseOrderRepository purchaseOrderRepository,
            ICostRepository costRepository,
            ISystemAlertService systemAlertService)
        {
            _taskRepository = taskRepository;
            _taskSchemaRepository = taskSchemaRepository;
            _userInfoRepository = userInfoRepository;
            _workOrderRepository = workOrderRepository;
            _workOrderMailer = workOrderMailer;
            _userContext = userContext;
            _fileRepository = fileRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _costRepository = costRepository;
            _systemAlertService = systemAlertService;
        }

        public async Task ScheduleTask(long workOrderId, IEnumerable<ScheduleWorkOrderTask> taskModels)
        {
            var userId = _userContext.UserId;
            var tasks = await _taskRepository.GetTasksByWorkOrderId(workOrderId, mainTaskOnly: true, includeWalkTasks: true);
            var schemas = await _taskSchemaRepository.GetTaskSchemas(isSummary: false);
            var addedTasks = new List<WorkOrderTask>();
            var deletedTasks = new List<WorkOrderTask>();
            var updatedTasks = new List<WorkOrderTask>();
            var jobExtraInfo = (await _workOrderRepository.GetExtraJobInfoByWorkOrderId(workOrderId)) ?? new ExtraJobInfo();
            var jobExtraUpdated = false;

            foreach (var taskModel in taskModels)
            {
                var task = default(WorkOrderTask);

                // new selection
                if (taskModel.TaskId == default(int))
                {
                    if (!taskModel.Selected)
                    {
                        continue;
                    }

                    task = await CreateTask(schemas, taskModel.SchemaId, workOrderId, taskModel.UserId, taskModel.Date.Value, taskModel.TaskName);

                    // Needed only if there are existing tasks
                    if (!tasks.IsNullOrEmpty())
                    {
                        SetTaskItemsWalkQuantity(jobExtraInfo.ExtraItems, task.TaskItems);
                        SetTaskItemsWalkQuantity(jobExtraInfo.ExtraItems, task.PreWalkTask?.TaskItems);
                        SetTaskItemsWalkQuantity(jobExtraInfo.ExtraItems, task.CompletionWalkTask?.TaskItems);
                    }

                    await EvaluatePermissions(task);

                    await SetSchedules(task, new ScheduleDateRange(taskModel.Date.Value), taskModel.UserId);

                    addedTasks.Add(task);
                    continue;
                }

                task = tasks.Find(y => y.Id == taskModel.TaskId);

                await EvaluatePermissions(task);

                if (!task.Permission.CanChangeSchedule) continue;

                // unselected (removed)
                if (!taskModel.Selected)
                {
                    // don't remove if any of the associated tasks don't allow change of schedule
                    if ((!task.PreWalkTask.IsNull() && !task.PreWalkTask.Permission.CanChangeSchedule)
                        || (!task.CompletionWalkTask.IsNull() && !task.CompletionWalkTask.Permission.CanChangeSchedule))
                    {
                        continue;
                    }

                    deletedTasks.Add(task);

                    if (!task.PreWalkTask.IsNull()) deletedTasks.Add(task.PreWalkTask);

                    if (!task.CompletionWalkTask.IsNull()) deletedTasks.Add(task.CompletionWalkTask);

                    continue;
                }

                // for existing task
                var taskUpdated = false;

                // if task assigned to another user, reset emailSent
                if (task.CrewId != taskModel.UserId)
                {
                    task.EmailSent = null;
                    task.CrewId = taskModel.UserId;
                    taskUpdated = true;
                }

                // update only when date has changed
                if (task.ScheduleDateRange.BeginDate.Date != taskModel.Date.Value.Date)
                {
                    await SetSchedules(task, new ScheduleDateRange(taskModel.Date.Value), taskModel.UserId);
                    taskUpdated = task.IsMainTask; // Non main-tasks are updated in SetSchedules
                }

                if (taskUpdated)
                {
                    updatedTasks.Add(task);
                }
            }

            // This is created to have reference to the newly added task ids when assigning taskId to extraItems
            var addedTasksArray = addedTasks.ToArray();

            await _taskRepository.CreateWorkOrderTasks(addedTasksArray);
            await _taskRepository.UpdateWorkOrderTasks(updatedTasks.ToArray());

            if (!deletedTasks.IsNullOrEmpty())
            {
                var taskIds = deletedTasks.Select(y => y.Id).ToArray();

                await _fileRepository.DeleteFileUploads(taskIds, FileOwnerType.Task);
                await _purchaseOrderRepository.DeletePurchaseOrdersByTaskIds(taskIds);
                await _taskRepository.DeleteWorkOrderTasks(deletedTasks.ToArray());
            }

            // Assign extra items for the newly added tasks
            foreach (var addedTask in addedTasksArray)
            {
                if (AssignExtraItemsToTask(addedTask, jobExtraInfo))
                {
                    jobExtraUpdated = true;
                }
            }

            foreach (var deletedTask in deletedTasks)
            {
                if (RemoveExtraItemsFromTask(deletedTask, jobExtraInfo))
                {
                    jobExtraUpdated = true;
                }
            }

            if (jobExtraUpdated)
            {
                await _workOrderRepository.UpdateExtraJobInfo(jobExtraInfo);
            }
        }

        public async Task<IEnumerable<ScheduleWorkOrderTask>> GetTaskSchedules(long workOrderId)
        {
            var taskSchedules = new List<ScheduleWorkOrderTask>();
            var tasks = await _taskRepository.GetTasksByWorkOrderId(workOrderId, true, true);
            var workOrder = await _workOrderRepository.GetWorkOrder(workOrderId);
            var schemas = await _taskSchemaRepository.GetTaskSchemas(mainTaskOnly: true);

            // add already scheduled tasks
            foreach (var task in tasks)
            {
                var scheduledTask = await ToTaskSchedule(task);

                taskSchedules.Add(scheduledTask);
            }

            // add tasks that are not scheduled yet
            foreach (var schema in schemas)
            {
                if (!tasks.Any(y => y.SchemaId == schema.Id || y.Name == schema.Name))
                {
                    taskSchedules.Add(new ScheduleWorkOrderTask
                    {
                        SchemaId = schema.Id,
                        TaskName = schema.Name
                    });
                }
            }

            return taskSchedules;
        }

        public async Task<WorkOrderTask> GetTaskWithPermissions(long taskId, bool includeWorkOrder = false)
        {
            var task = await _taskRepository.GetTask(taskId, includeSubTasks: true, includeCompletedBy: true, includeWorkOrder: includeWorkOrder, includeUnlockedBy: true);

            await EvaluatePermissions(task);

            return task;
        }

        public async Task<WorkOrderTask> UnlockTask(long taskId)
        {
            var mainTask = await _taskRepository.GetMainTask(taskId);
            var hasChanges = false;
            var userId = _userContext.UserId;
            var today = DateTime.UtcNow;

            await EvaluatePermissions(mainTask);

            // if task is prewalk
            if (!mainTask.PreWalkTask.IsNull() && mainTask.PreWalkTask.Id == taskId && mainTask.PreWalkTask.Permission.CanUnlock)
            {
                mainTask.PreWalkTask.Completed = null;
                mainTask.PreWalkTask.CompletedById = null;
                mainTask.PreceedingTaskCompleted = false;
                mainTask.PreWalkTask.UnlockedById = userId;
                mainTask.PreWalkTask.Unlocked = today;
                hasChanges = true;
            }

            // if main task
            if (mainTask.Id == taskId && mainTask.Permission.CanUnlock)
            {
                mainTask.Completed = null;
                mainTask.CompletedById = null;
                mainTask.Unlocked = today;
                mainTask.UnlockedById = userId;

                if (!mainTask.PreWalkTask.IsNull())
                {
                    mainTask.PreWalkTask.SucceedingTaskCompleted = false;
                }

                if (!mainTask.CompletionWalkTask.IsNull())
                {
                    mainTask.CompletionWalkTask.PreceedingTaskCompleted = false;
                }

                hasChanges = true;
            }

            // if task is completion walk
            if (!mainTask.CompletionWalkTask.IsNull() && mainTask.CompletionWalkTask.Id == taskId && mainTask.CompletionWalkTask.Permission.CanUnlock)
            {
                mainTask.CompletionWalkTask.Completed = null;
                mainTask.CompletionWalkTask.CompletedById = null;
                mainTask.CompletionWalkTask.Unlocked = today;
                mainTask.CompletionWalkTask.UnlockedById = userId;
                mainTask.SucceedingTaskCompleted = false;
                hasChanges = true;
            }

            if (hasChanges)
            {
                await _taskRepository.UpdateWorkOrderTasks(mainTask);
            }

            return await _taskRepository.GetTask(taskId);
        }

        public async Task<WorkOrderTask> CompleteTask(WorkOrderTask task)
        {
            await SaveWorkOrderTask(task);

            var mainTask = await _taskRepository.GetMainTask(task.Id);
            var today = DateTime.UtcNow;
            var nextTaskDate = DateHelper.GetLocalDateToday().NextBusinessDay();
            var taskCategory = TaskCategory.Unknown;
            var hasChanges = false;
            var userId = _userContext.UserId;

            await EvaluatePermissions(mainTask);

            // if task is prewalk
            if (!mainTask.PreWalkTask.IsNull() && mainTask.PreWalkTask.Id == task.Id && mainTask.PreWalkTask.Permission.CanComplete)
            {
                mainTask.PreWalkTask.Completed = today;
                mainTask.PreWalkTask.CompletedById = userId;
                mainTask.PreWalkTask.UnlockedById = null;
                mainTask.PreWalkTask.Unlocked = null;
                mainTask.PreceedingTaskCompleted = true;

                // push to next business day if earlier
                if (mainTask.ScheduleDateRange.GetDate() < nextTaskDate)
                {
                    mainTask.ScheduleDateRange.SetDate(nextTaskDate);

                    await SetSchedules(mainTask, mainTask.ScheduleDateRange, mainTask.CrewId, true, true);
                }

                taskCategory = TaskCategory.Prewalk;
                hasChanges = true;
            }

            // if main task
            if (mainTask.Id == task.Id && mainTask.Permission.CanComplete)
            {
                mainTask.Completed = today;
                mainTask.CompletedById = userId;
                mainTask.UnlockedById = null;
                mainTask.Unlocked = null;

                if (!mainTask.PreWalkTask.IsNull())
                {
                    mainTask.PreWalkTask.SucceedingTaskCompleted = true;
                }

                if (!mainTask.CompletionWalkTask.IsNull())
                {
                    mainTask.CompletionWalkTask.PreceedingTaskCompleted = true;

                    // push to next business day if earlier
                    if (mainTask.CompletionWalkTask.ScheduleDateRange.GetDate() < nextTaskDate)
                    {
                        mainTask.CompletionWalkTask.ScheduleDateRange.SetDate(nextTaskDate);

                        await SetSchedules(mainTask.CompletionWalkTask, mainTask.CompletionWalkTask.ScheduleDateRange, 
                            mainTask.CompletionWalkTask.CrewId, true, true);
                    }
                }

                taskCategory = TaskCategory.Main;
                hasChanges = true;
            }

            // if task is completion walk
            if (!mainTask.CompletionWalkTask.IsNull() && mainTask.CompletionWalkTask.Id == task.Id && mainTask.CompletionWalkTask.Permission.CanComplete)
            {
                mainTask.CompletionWalkTask.Completed = today;
                mainTask.CompletionWalkTask.CompletedById = userId;
                mainTask.CompletionWalkTask.UnlockedById = null;
                mainTask.CompletionWalkTask.Unlocked = null;
                mainTask.SucceedingTaskCompleted = true;

                taskCategory = TaskCategory.Completion;
                hasChanges = true;
            }

            if (hasChanges)
            {
                await _taskRepository.UpdateWorkOrderTasks(mainTask);
                await _workOrderMailer.EmailTaskCompletion(task.Id, taskCategory);
            }

            return await _taskRepository.GetTask(task.Id);
        }

        public async Task<WorkOrderTask> UpdateTaskSchedule(TaskSchedule task)
        {
            var dbTask = await GetTaskWithPermissions(task.TaskId);
            var newDateRange = new ScheduleDateRange(task.ScheduleDate ?? dbTask.ScheduleDateRange.BeginDate, 
                task.ScheduleEndDate ?? dbTask.ScheduleDateRange.EndDate);

            // if task assigned to another user, reset emailSent
            if (task.CrewId != dbTask.CrewId)
            {
                dbTask.CrewId = task.CrewId;
                dbTask.EmailSent = null;

                await AssignCrewIdToCompletionWalk(dbTask);
            }

            await SetSchedules(dbTask, newDateRange, task.CrewId);

            await _taskRepository.UpdateWorkOrderTasks(dbTask);

            return dbTask;
        }

        public async Task<WorkOrderTask> SaveWorkOrderTask(WorkOrderTask task)
        {
            var dbtask = await GetTaskWithPermissions(task.Id);
            var userId = _userContext.UserId;
            var user = await _userInfoRepository.GetUserAsync(userId);

            dbtask.Modified = DateTime.UtcNow;
            dbtask.ModifiedById = userId;

            // if task assigned to another user, reset emailSent
            if (task.CrewId != dbtask.CrewId)
            {
                dbtask.EmailSent = null;
                await AssignCrewIdToCompletionWalk(task);
            }

            await UpdateTaskItems(dbtask, task, user.FullName);

            await SetSchedules(dbtask, task.ScheduleDateRange, task.CrewId);

            if (!dbtask.IsMainTask)
            {
                await UpdateWalkQuantities(dbtask);
            }

            await _taskRepository.UpdateWorkOrderTasks(dbtask);

            return dbtask;
        }

        public async Task<IEnumerable<WorkOrderTask>> GetTasks(TaskSearchCriteria searchCriteria)
        {
            var tasks = await _taskRepository.GetTasks(searchCriteria);

            foreach (var task in tasks)
            {
                await EvaluatePermissions(task, false);
            }

            return tasks;
        }

        public async Task<IList<TaskView>> SearchTasks(TaskSearchCriteria searchCriteria)
        {
            var tasks = await _taskRepository.SearchTasks(searchCriteria);
            var userId = _userContext.UserId;
            var userRoles = _userContext.GetRoles(userId);

            foreach (var task in tasks)
            {
                EvaluatePermissions(task, userId, userRoles);
            }

            return tasks;
        }

        public async Task<bool> AreTasksCompleted(long workOrderId)
        {
            var tasks = await _taskRepository.GetTasksByWorkOrderId(workOrderId, mainTaskOnly: false);
            return tasks.All(x => x.Completed.HasValue);
        }

        public Task<WorkOrderTask> GetMainTask(long taskId)
        {
            return _taskRepository.GetMainTask(taskId);
        }

        private async Task<WorkOrderTask> CreateTask(IEnumerable<WorkOrderTaskSchema> schemas, int schemaId, long workOrderId, string crewId, DateTime date, string taskName)
        {
            var scheduleDate = date.Date;
            var schema = schemas.First(x => x.Id == schemaId);
            var walkCrewId = await _userInfoRepository.GetUserIdForWalkTask();
            var now = DateTime.UtcNow;
            var today = DateHelper.GetLocalDateToday();

            var task = new WorkOrderTask
            {
                SchemaId = schema.Id,
                Name = taskName,
                Amount = schema.Amount,
                TaskItems = schema.TaskItems.ToList(),
                IsLumpSum = schema.IsLumpSum,
                TaskType = schema.TaskType,
                WorkOrderId = workOrderId,
                CrewId = crewId,
                CreatedById = _userContext.UserId,
                Created = now,
                Modified = now,
                ModifiedById = _userContext.UserId,
                // assign today if the date is earlier than today
                ScheduleDateRange = scheduleDate < today ? new ScheduleDateRange(today, null) : new ScheduleDateRange(scheduleDate, null),
            };

            if (schema.PreWalkTaskId.HasValue)
            {
                var name = GetTaskName(schemas, schema.PreWalkTaskId.Value, taskName);

                task.PreWalkTask = await CreateTask(schemas, schema.PreWalkTaskId.Value, workOrderId, null, GetEffectivePreviousBusinessDay(scheduleDate), name);
                task.PreWalkTask.PreceedingTaskCompleted = true;
            }

            if (schema.CompletionWalkTaskId.HasValue)
            {
                var name = GetTaskName(schemas, schema.CompletionWalkTaskId.Value, taskName);

                task.CompletionWalkTask = await CreateTask(schemas, schema.CompletionWalkTaskId.Value, workOrderId, null, scheduleDate.NextBusinessDay(), name);
            }

            // If task is the first in the series assign base work item to it
            task.IncludeBase = task.IsMainTask && !Regex.IsMatch(task.Name, @"\(\d+\)$");

            return task;
        }

        private string GetTaskName(IEnumerable<WorkOrderTaskSchema> schemas, int taskId, string mainTaskname)
        {
            var schema = schemas.First(x => x.Id == taskId);

            if (mainTaskname.EndsWith(")"))
            {
                var openingBracketIndex = mainTaskname.LastIndexOf("(");
                var closingBracketIndex = mainTaskname.LastIndexOf(")");

                return $"{schema.Name}{mainTaskname.Substring(openingBracketIndex)}";
            }

            return schema.Name;
        }

        private async Task UpdateTaskItems(WorkOrderTask existingTask, WorkOrderTask newTask, string username)
        {
            var permission = existingTask.Permission;

            if (!(permission.CanSave || permission.CanAddNote || permission.CanAddNewItem))
            {
                return;
            }

            var existingNoteItems = existingTask.TaskItems.FindAll(item => item.DataType == TaskDataType.Note);
            var newNoteItems = newTask.TaskItems.FindAll(item => item.DataType == TaskDataType.Note);
            var nonNoteItems = permission.CanSave || permission.CanAddNewItem
                            ? newTask.TaskItems.FindAll(item => item.DataType != TaskDataType.Note).ToList()
                            : existingTask.TaskItems.FindAll(item => item.DataType != TaskDataType.Note).ToList();
            var notes = existingNoteItems;
            var newlyAddedNotes = new List<Note>();

            if (permission.CanAddNote)
            {
                newNoteItems.ForEach(newNoteItem =>
                {
                    var existingNoteItem = existingNoteItems.FirstOrDefault(x => x.Name == newNoteItem.Name);

                    if (newNoteItem.Value.IsNullOrEmpty())
                    {
                        newNoteItem.Value = existingNoteItem?.Value;
                        return;
                    }

                    var existingNotes = string.IsNullOrWhiteSpace(existingNoteItem?.Value)
                                        ? new List<Note>()
                                        : existingNoteItem.Value.FromJson<List<Note>>();

                    var newNote = new Note
                    {
                        Id = Guid.NewGuid(),
                        Created = DateTime.UtcNow,
                        Author = username,
                        Text = newNoteItem.Value
                    };

                    newlyAddedNotes.Add(newNote);
                    existingNotes.Add(newNote);

                    // add Ids to notes without valid ids
                    existingNotes.ForEach(note =>
                    {
                        if (note.Id == Guid.Empty)
                        {
                            note.Id = Guid.NewGuid();
                        }
                    });

                    newNoteItem.Value = existingNotes.ToJson();
                });

                notes = newNoteItems;
            }

            var items = new List<TaskItem>(nonNoteItems);

            items.AddRange(notes);

            existingTask.TaskItems = items;

            await AddNoteAlerts(newlyAddedNotes, existingTask);
        }

        private bool AssignExtraItemsToTask(WorkOrderTask task, ExtraJobInfo extraJobInfo)
        {
            var updated = false;
            var extraItems = extraJobInfo.ExtraItems;

            if (!task.IsMainTask || extraItems.IsNullOrEmpty())
            {
                return updated;
            }

            var matchingExtraItems = extraItems.Where(x => x.TaskType == task.TaskType);

            if (!matchingExtraItems.Any())
            {
                return updated;
            }

            foreach (var extraItem in matchingExtraItems)
            {
                if (extraItem.TaskIds.IsNullOrEmpty())
                {
                    extraItem.TaskIds = new List<long> { task.Id };
                    updated = true;
                }
            }

            if (updated)
            {
                extraJobInfo.ExtraItems = extraItems;
            }

            return updated;
        }

        private bool RemoveExtraItemsFromTask(WorkOrderTask task, ExtraJobInfo extraJobInfo)
        {
            var extraItems = extraJobInfo.ExtraItems?.Where(x => x.TaskType == task.TaskType
                       && (x.TaskIds?.Any(y => y == task.Id) ?? false));

            if (!extraItems.IsNullOrEmpty())
            {
                extraItems.ForEach(x => x.TaskIds.RemoveAll(y => y == task.Id));
                return true;
            }

            return false;
        }

        private async Task SetSchedules(WorkOrderTask task, ScheduleDateRange dateRange, string userId,
            bool ignorePreceedingTasks = false, bool ignorePermissions = false)
        {
            if (!ignorePermissions && !task.Permission.CanChangeSchedule)
            {
                return;
            }

            task.CrewId = userId;
            task.Crew = null; // reset

            if (task.ScheduleDateRange == dateRange)
            {
                return;
            }

            task.ScheduleDateRange = new ScheduleDateRange(dateRange);

            var scheduleDate = dateRange.GetDate();

            // main task
            if (task.IsMainTask)
            {
                if (!ignorePreceedingTasks && !task.PreWalkTask.IsNull() && !task.PreWalkTask.Completed.HasValue)
                {
                    var newDate = GetEffectivePreviousBusinessDay(scheduleDate);

                    task.PreWalkTask.ScheduleDateRange.SetDate(newDate);
                }

                if (!task.CompletionWalkTask.IsNull() && !task.CompletionWalkTask.Completed.HasValue)
                {
                    var newDate = scheduleDate.Date.NextBusinessDay();

                    task.CompletionWalkTask.ScheduleDateRange.SetDate(newDate);
                }

                return;
            }

            var mainTask = await _taskRepository.GetMainTask(task.Id);

            if (mainTask.Completed.HasValue)
            {
                return;
            }

            // prewalk task
            if (mainTask.PreWalkTaskId == task.Id)
            {
                var newDate = scheduleDate.NextBusinessDay();

                mainTask.ScheduleDateRange.SetDate(newDate);
                mainTask.ScheduleDateRange.SetTime(dateRange);

                if (!mainTask.CompletionWalkTask.IsNull() && !mainTask.CompletionWalkTask.Completed.HasValue)
                {
                    newDate = mainTask.ScheduleDateRange.GetDate().NextBusinessDay();

                    mainTask.CompletionWalkTask.ScheduleDateRange.SetDate(newDate);
                }
            }

            // completion walk task
            if (!ignorePreceedingTasks && mainTask.CompletionWalkTaskId == task.Id)
            {
                var newDate = GetEffectivePreviousBusinessDay(scheduleDate);

                mainTask.ScheduleDateRange.SetDate(newDate);

                if (!mainTask.PreWalkTask.IsNull() && !mainTask.PreWalkTask.Completed.HasValue)
                {
                    newDate = GetEffectivePreviousBusinessDay(mainTask.ScheduleDateRange.GetDate());

                    mainTask.PreWalkTask.ScheduleDateRange.SetDate(newDate);
                }
            }

            await _taskRepository.UpdateWorkOrderTasks(mainTask);
        }

        private async Task<EvaluatorParameters> GetEvaluatorParameters(WorkOrderTask task)
        {
            var userId = _userContext.UserId;
            var workOrder = task.WorkOrder;
            var userRoles = _userContext.GetRoles(userId);

            if (workOrder.IsNull())
            {
                workOrder = await _workOrderRepository.GetWorkOrder(task.WorkOrderId);
            }

            return new EvaluatorParameters
            {
                UserId = userId,
                UserRoles = userRoles,
                JobInfoConfirmed = workOrder.JobInfoConfirmed,
                WorkOrderCompleted = workOrder.Completed.HasValue
            };
        }

        public async Task EvaluatePermissions(WorkOrderTask task, bool full = true)
        {
            var evaluatorParameters = await GetEvaluatorParameters(task);

            var permissionEvaluators = new List<IPermissionEvaluator>
            {
                new ViewPermissionEvaluator(evaluatorParameters),
                new IsReadyPermissionEvaluator(evaluatorParameters),
                new SavePermissionEvaluator(evaluatorParameters),
                new ChangeSchedulePermissionEvaluator(evaluatorParameters),
                new CanCreateDuplicatePermissionEvaluator(evaluatorParameters)
            };

            if (full)
            {
                if (task.IsMainTask)
                {
                    evaluatorParameters.TaskHasSignature =
                        await _fileRepository.HasAnyFiles(task.Id, FileOwnerType.SignedDocument);
                }

                permissionEvaluators.AddRange(
                    new List<IPermissionEvaluator>
                    {
                        new CreatePurchaseOrderPermissionEvaluator(evaluatorParameters),
                        new CompletePermissionEvaluator(evaluatorParameters),
                        new SignDocumentPermissionEvaluator(evaluatorParameters),
                        new UnlockPermissionEvaluator(evaluatorParameters),
                        new NewItemPermissionEvaluator(evaluatorParameters),
                        new AddNotePermissionEvaluator(evaluatorParameters)
                    }
                );
            }

            permissionEvaluators.ForEach(x => x.Execute(task));
        }

        private void EvaluatePermissions(TaskView task, string userId, IEnumerable<UserRole> userRoles)
        {
            var evaluatorParameters = new EvaluatorParameters
            {
                UserId = userId,
                UserRoles = userRoles,
                JobInfoConfirmed = task.JobInfoConfirmed,
                WorkOrderCompleted = task.WorkOrderCompleted.HasValue
            };

            var permissionEvaluators = new List<IPermissionEvaluator>
            {
                new ViewPermissionEvaluator(evaluatorParameters),
                new IsReadyPermissionEvaluator(evaluatorParameters),
                new SavePermissionEvaluator(evaluatorParameters),
                new ChangeSchedulePermissionEvaluator(evaluatorParameters),
                new CanCreateDuplicatePermissionEvaluator(evaluatorParameters)
            };

            permissionEvaluators.ForEach(x => x.Execute(task));
        }

        private async Task<ScheduleWorkOrderTask> ToTaskSchedule(WorkOrderTask task)
        {
            var taskSchedule = new ScheduleWorkOrderTask
            {
                SchemaId = task.SchemaId,
                TaskName = task.Name,
                Selected = true,
                Date = task.ScheduleDateRange.BeginDate,
                UserId = task.CrewId,
                TaskId = task.Id,
                Confirmed = task.DateConfirmed,
                Completed = task.Completed.HasValue
            };

            await EvaluatePermissions(task, false);

            taskSchedule.Permission = task.Permission;

            // cannot unselect if any of the tasks does not allow change of schedule
            var cantUnselect = !task.Permission.CanChangeSchedule
                || (!task.PreWalkTask.IsNull() && !task.PreWalkTask.Permission.CanChangeSchedule)
                || (!task.CompletionWalkTask.IsNull() && !task.CompletionWalkTask.Permission.CanChangeSchedule);

            // check for sent Purchase orders
            if (!cantUnselect && task.Id > 0)
            {
                cantUnselect = await _purchaseOrderRepository.HasAnyPurchaseOrderEmailed(task.Id);
            }

            taskSchedule.CantUnselect = cantUnselect;

            return taskSchedule;
        }

        private DateTime GetEffectivePreviousBusinessDay(DateTime date)
        {
            var prevBusinesDay = date.Date.PreviousBusinessDay();
            var today = DateHelper.GetLocalDateToday();

            if (prevBusinesDay.Date < today)
            {
                return date.Date;
            }

            return prevBusinesDay;
        }

        private async Task UpdateWalkQuantities(WorkOrderTask task)
        {
            var allWorkOrderTasks = await _taskRepository.GetTasksByWorkOrderId(task.WorkOrderId, false);
            var walkQuantityItems = GetWalkQuantityItems(task);

            if (!walkQuantityItems.Any())
                return;

            await UpdateExtrasWalkQuantity(task, walkQuantityItems, allWorkOrderTasks);
            await UpdateWalkQuantitiesForWalks(walkQuantityItems, allWorkOrderTasks);
        }

        private async Task UpdateWalkQuantitiesForWalks(IDictionary<string, TaskItem> walkQuantityItems, IEnumerable<WorkOrderTask> allWorkOrderTasks)
        {
            var tasksToUpdate = allWorkOrderTasks.Where(x => !x.IsMainTask);

            foreach (var walkTask in tasksToUpdate)
            {
                var updated = false;
                var walkTaskItems = walkTask.TaskItems ?? new List<TaskItem>();

                foreach (var item in walkQuantityItems)
                {
                    var walkTaskItem = FindTaskItem(item.Key, walkTaskItems);

                    if (!walkTaskItem.IsNull() && walkTaskItem.Value != item.Value.Value && walkTaskItem.DataType == TaskDataType.Number)
                    {
                        walkTaskItem.Value = item.Value.Value;
                        updated = true;
                    }
                }

                if (updated)
                {
                    walkTask.TaskItems = walkTaskItems;
                    await _taskRepository.UpdateWorkOrderTasks(walkTask);
                }
            }
        }

        private async Task UpdateExtrasWalkQuantity(WorkOrderTask task, IDictionary<string, TaskItem> walkQuantityItems, IEnumerable<WorkOrderTask> allWorkOrderTasks)
        {
            var workOrder = await _workOrderRepository.GetWorkOrder(task.WorkOrderId, includeExtraJobInfo: true, includeJobInfo: true);
            var extraItems = workOrder.ExtraJobInfo?.ExtraItems?.ToList() ?? new List<ExtraItem>();
            var modifiedExtraItems = new List<ExtraItem>();
            var builderCostItems = default(List<Cost>);

            foreach (var item in walkQuantityItems)
            {
                var itemValue = item.Value.Value.ToSafeInt();
                var extraItem = extraItems.FirstOrDefault(x => x.Name?.Trim().ToLower() == item.Key.ToLower());

                // if selected items list doesn't contain the item
                // let's look for it from all the builder extra items and add it if found
                if (extraItem.IsNull())
                {
                    // if item value is zero, do nothing and move on.
                    if (itemValue == 0) continue;

                    if (builderCostItems.IsNull())
                    {
                        builderCostItems = await _costRepository.GetCosts(workOrder.BuilderId.Value, true);
                    }

                    var cost = builderCostItems.FirstOrDefault(x => x.CostItem.Name == item.Key);

                    // if builder doesn't have the item, do nothing and move on.
                    if (cost.IsNull()) continue;

                    // get the most recent main task that has the same task type
                    var mainTask = allWorkOrderTasks.Where(x => x.TaskType == cost.CostItem.TaskType && x.IsMainTask)
                                                    .OrderBy(x => x.Created)
                                                    .LastOrDefault();

                    var newItem = new ExtraItem
                    {
                        Id = Guid.NewGuid(),
                        Name = cost.CostItem.Name,
                        BuilderCost = cost.Amount,
                        IsLumpSum = cost.CostItem.IsLumpSum,
                        LaborCost = cost.CostItem.LaborCost,
                        Quantity = (int)cost.CostItem.EstimateItemQuantity(workOrder.JobInfo),
                        TaskType = cost.CostItem.TaskType,
                        WalkQuantity = itemValue,
                        TaskIds = mainTask.IsNull() ? null : new List<long> { mainTask.Id }
                    };

                    modifiedExtraItems.Add(newItem);
                    extraItems.Add(newItem);
                }
                else
                {
                    if (itemValue == extraItem.WalkQuantity)
                        continue;

                    extraItem.WalkQuantity = itemValue;

                    if (extraItem.Id == Guid.Empty)
                    {
                        extraItem.Id = Guid.NewGuid();
                    }

                    modifiedExtraItems.Add(extraItem);
                }
            }

            if (modifiedExtraItems.Any())
            {
                workOrder.ExtraJobInfo.ExtraItems = extraItems;
                await _workOrderRepository.UpdateExtraJobInfo(workOrder.ExtraJobInfo);
                await _systemAlertService.SaveExtraItemAlerts(modifiedExtraItems, workOrder);
            }
        }

        private IDictionary<string, TaskItem> GetWalkQuantityItems(WorkOrderTask task)
        {
            var collection = new Dictionary<string, TaskItem>();
            var numberItems = task.TaskItems.Where(x => x.DataType == TaskDataType.Number);

            if (!numberItems.Any())
                return collection;

            foreach (var item in numberItems)
            {
                if (item.Value.IsNullOrEmpty())
                    continue;

                var extraItemName = ExtractExtraItemName(item.Name);

                if (!extraItemName.IsNull() && !collection.ContainsKey(extraItemName))
                {
                    collection.Add(extraItemName, item);
                }
            }

            return collection;
        }

        private void SetTaskItemsWalkQuantity(IEnumerable<ExtraItem> extraItems, IEnumerable<TaskItem> taskItems)
        {
            if (extraItems.IsNullOrEmpty() || taskItems.IsNullOrEmpty())
            {
                return;
            }

            foreach (var taskItem in taskItems)
            {
                var extraItemName = default(string);

                if (taskItem.DataType == TaskDataType.Number &&
                    (extraItemName = ExtractExtraItemName(taskItem.Name)) != null)
                {
                    var value = extraItems.FirstOrDefault(x => x.Name == extraItemName)?.WalkQuantity;

                    if (value.HasValue)
                    {
                        taskItem.Value = value.ToString();
                    }
                }
            }
        }

        private TaskItem FindTaskItem(string extraItemName, IEnumerable<TaskItem> taskItems)
        {
            if (extraItemName.IsNullOrEmpty())
            {
                return null;
            }

            var pattern = $"\"{extraItemName}\"".ToLower();

            return taskItems.FirstOrDefault(x => !x.Name.IsNullOrEmpty() && x.Name.ToLower().Contains(pattern));
        }

        private string ExtractExtraItemName(string TaskItemName)
        {
            // get text within double quotes ("")
            var nameParts = TaskItemName.Split('"');

            if (nameParts.Length == 3)
            {
                return nameParts[1].Trim();
            }

            return null;
        }

        private async Task AddNoteAlerts(IEnumerable<Note> notes, WorkOrderTask task)
        {
            if (notes.IsNullOrEmpty())
            {
                return;
            }

            var workOrder = task.WorkOrder;

            if (workOrder == null)
            {
                workOrder = await _workOrderRepository.GetWorkOrder(task.WorkOrderId);
            }

            foreach (var note in notes)
            {
                await _systemAlertService.SaveTaskNoteAlert(workOrder, task, note);
            }
        }

        private async Task AssignCrewIdToCompletionWalk(WorkOrderTask task)
        {
            if (task.IsMainTask)
            {
                return;
            }

            var mainTask = await _taskRepository.GetMainTask(task.Id);
            var completionWalkTask = mainTask?.CompletionWalkTask;

            if (completionWalkTask.IsNull())
            {
                return;
            }

            // do only when the current task is a prewalk
            if (mainTask.PreWalkTaskId == task.Id)
            {
                completionWalkTask.CrewId = task.CrewId;

                await _taskRepository.UpdateWorkOrderTasks(completionWalkTask);
            }
        }
    }
}
