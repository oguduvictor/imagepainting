﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public abstract class PermissionEvaluator : IPermissionEvaluator
    {
        protected readonly EvaluatorParameters _parameters;

        public PermissionEvaluator(EvaluatorParameters parameters)
        {
            _parameters = parameters;
        }

        public virtual void Execute(WorkOrderTask task)
        {
            EvaluatePermissions(task);

            if (!task.PreWalkTask.IsNull())
            {
                EvaluatePermissions(task.PreWalkTask);
            }

            if (!task.CompletionWalkTask.IsNull())
            {
                EvaluatePermissions(task.CompletionWalkTask);
            }
        }

        public void Execute(TaskView task)
        {
            EvaluatePermissions(task);
        }

        protected abstract void EvaluatePermissions(WorkOrderTask task);

        protected virtual void EvaluatePermissions(TaskView task)
        {
            throw new NotImplementedException();
        }
    }
}
