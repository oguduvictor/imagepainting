﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public interface IPermissionEvaluator
    {
        void Execute(WorkOrderTask task);

        void Execute(TaskView task);
    }
}
