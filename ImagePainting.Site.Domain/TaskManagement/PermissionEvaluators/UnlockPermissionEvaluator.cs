﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class UnlockPermissionEvaluator : PermissionEvaluator
    {
        public UnlockPermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var hasAccess = task.Completed.HasValue
                            && _parameters.IsUserAdmin
                            && !_parameters.WorkOrderCompleted;
            //&& !task.SucceedingTaskCompleted;

            task.Permission.Update(canUnlock: hasAccess);
        }
    }
}
