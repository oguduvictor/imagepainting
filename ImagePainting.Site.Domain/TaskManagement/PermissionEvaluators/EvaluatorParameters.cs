﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class EvaluatorParameters
    {
        private bool? _workOrderCompleted;
        private string _userId;
        private bool? _jobInfoConfirmed;
        private bool? _taskHasSignature;

        public string UserId
        {
            get
            {
                if (_userId.IsNullOrEmpty())
                {
                    throw new InvalidOperationException($"{nameof(UserId)} has not been set.");
                }

                return _userId;
            }

            set { _userId = value; }
        }

        public bool WorkOrderCompleted
        {
            get { return _workOrderCompleted.Value; }
            set { _workOrderCompleted = value; }
        }

        public bool JobInfoConfirmed
        {
            get { return _jobInfoConfirmed.Value; }
            set { _jobInfoConfirmed = value; }
        }

        public IEnumerable<UserRole> UserRoles { get; set; }

        public bool CanUserAccessTask(WorkOrderTask task)
        {
            if (task.CrewId == UserId || IsUserAdmin)
            {
                return true;
            }

            return UserRoles.Contains(UserRole.Employee) && 
                    (task.PreWalkTask?.CrewId == UserId || task.CompletionWalkTask?.CrewId == UserId);
        }

        public bool CanUserAccessTask(TaskView task)
        {
            if (task.CrewId == UserId || IsUserAdmin)
            {
                return true;
            }

            return UserRoles.Contains(UserRole.Employee);
        }

        public bool IsUserAdmin
        {
            get
            {
                if (UserRoles.IsNullOrEmpty())
                {
                    throw new InvalidOperationException($"{nameof(UserRoles)} has not been set.");
                }

                return UserRoles.Contains(UserRole.Administrator);
            }
        }
        
        public bool IsEditable(WorkOrderTask task)
        {
            return !(WorkOrderCompleted || task.Completed.HasValue);
        }

        public bool IsEditable(TaskView task)
        {
            return !(WorkOrderCompleted || task.Completed.HasValue);
        }

        public bool TaskHasSignature
        {
            get
            {
                if (!_taskHasSignature.HasValue)
                {
                    throw new InvalidOperationException($"{nameof(TaskHasSignature)} has not been set.");
                }

                return _taskHasSignature.Value;
            }
            set
            {
                _taskHasSignature = value;
            }
        }

    }
}
