﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Linq;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class CompletePermissionEvaluator : PermissionEvaluator
    {
        public CompletePermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var checkLists = task.TaskItems.Where(x => x.DataType == TaskDataType.YesNo);
            var allChecked = checkLists.All(x => x.Value.IsTrue());
            var canComplete = _parameters.IsEditable(task)
                              && allChecked
                              && _parameters.CanUserAccessTask(task)
                              && !task.Deferred
                              && !task.CrewId.IsNullOrEmpty();

            if (task.IsMainTask)
            {
                canComplete = canComplete && _parameters.TaskHasSignature;
            }

            task.Permission.Update(canComplete: canComplete);
        }
    }
}
