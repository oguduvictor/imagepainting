﻿using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Linq;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class CreatePurchaseOrderPermissionEvaluator : PermissionEvaluator
    {
        public CreatePurchaseOrderPermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var hasAccess = _parameters.IsEditable(task) 
                                && _parameters.CanUserAccessTask(task)
                                && (_parameters.IsUserAdmin || _parameters.UserRoles.Contains(UserRole.Employee));

            task.Permission.Update(canCreatePurchaseOrder: hasAccess);
        }
    }
}
