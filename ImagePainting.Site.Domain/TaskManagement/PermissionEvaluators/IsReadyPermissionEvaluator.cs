﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class IsReadyPermissionEvaluator : PermissionEvaluator
    {
        public IsReadyPermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var isReady = task.PreceedingTaskCompleted;

            if (task.IsMainTask)
            {
                if (!task.PreWalkTaskId.HasValue)
                {
                    isReady = _parameters.JobInfoConfirmed;
                }
                else
                {
                    isReady = isReady && _parameters.JobInfoConfirmed;
                }
            }

            isReady = isReady && !task.Deferred;

            task.Permission.Update(isTaskReady: isReady);
        }

        protected override void EvaluatePermissions(TaskView task)
        {
            task.Permission.Update(isTaskReady: task.IsReady);
        }
    }
}
