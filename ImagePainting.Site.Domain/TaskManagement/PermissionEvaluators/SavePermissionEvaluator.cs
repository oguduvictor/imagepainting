﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class SavePermissionEvaluator : PermissionEvaluator
    {
        public SavePermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var canSave = true;
            var preceedingTaskCompleted = (task.IsMainTask && !task.PreWalkTaskId.HasValue) || task.PreceedingTaskCompleted;

            if (!_parameters.CanUserAccessTask(task))
            {
                canSave = false;
            }
            else if (!_parameters.IsEditable(task))
            {
                canSave = false;
            }
            else if (task.Deferred)
            {
                canSave = false;
            }
            else if (_parameters.IsUserAdmin)
            {
                canSave = true; // admin can save even if task is not ready
            }
            else if (!preceedingTaskCompleted)
            {
                canSave = false;
            }
            else if (task.IsMainTask && !_parameters.JobInfoConfirmed)
            {
                canSave = false;
            }
            else if (task.ScheduleDateRange.BeginDate.Date > DateTime.UtcNow.ToLocalDateTime().Date)
            {
                canSave = false;
            }

            task.Permission.Update(
                canSave: canSave, 
                canDefer: CanDefer(task),
                canSaveEmailNote: CanSaveEmailNote(task));
        }

        protected override void EvaluatePermissions(TaskView task)
        {
            var canSave = true;

            if (!_parameters.CanUserAccessTask(task))
            {
                canSave = false;
            }
            else if (!_parameters.IsEditable(task))
            {
                canSave = false;
            }
            else if (!task.IsReady)
            {
                canSave = false;
            }
            else if (task.ScheduleDate.Date > DateTime.UtcNow.ToLocalDateTime().Date)
            {
                canSave = false;
            }

            task.Permission.Update(canSave: canSave);
        }

        private bool CanDefer(WorkOrderTask task)
        {
            return !task.Completed.HasValue && _parameters.IsUserAdmin;
        }

        private bool CanSaveEmailNote(WorkOrderTask task)
        {
            return _parameters.IsUserAdmin && !_parameters.WorkOrderCompleted;
        }
    }
}
