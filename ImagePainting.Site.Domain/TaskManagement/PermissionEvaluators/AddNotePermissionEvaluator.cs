﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class AddNotePermissionEvaluator : PermissionEvaluator
    {
        public AddNotePermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var canAddNote = _parameters.CanUserAccessTask(task) && _parameters.IsEditable(task);

            task.Permission.Update(canAddNote: canAddNote);
        }
    }
}
