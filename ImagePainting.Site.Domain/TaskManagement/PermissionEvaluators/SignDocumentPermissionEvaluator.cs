﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Helpers;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    class SignDocumentPermissionEvaluator : PermissionEvaluator
    {
        public SignDocumentPermissionEvaluator(EvaluatorParameters parameters) :base(parameters)
        {
            
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var canSignDocument = task.IsMainTask && !_parameters.TaskHasSignature && !task.CrewId.IsNullOrEmpty();

            task.Permission.Update(canSignDocument: canSignDocument);
        }
    }
}
