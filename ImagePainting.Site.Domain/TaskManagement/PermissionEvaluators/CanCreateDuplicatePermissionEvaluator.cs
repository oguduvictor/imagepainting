﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class CanCreateDuplicatePermissionEvaluator : PermissionEvaluator
    {
        public CanCreateDuplicatePermissionEvaluator(EvaluatorParameters parameters)
            : base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var canCreateDuplicate = _parameters.IsUserAdmin
                                        && task.IsMainTask
                                        && task.Completed.HasValue;

            task.Permission.Update(canCreateDuplicateTask: canCreateDuplicate);
        }

        protected override void EvaluatePermissions(TaskView task)
        {
            var canCreateDuplicate = _parameters.IsUserAdmin
                                        && task.IsMainTask
                                        && task.Completed.HasValue;

            task.Permission.Update(canCreateDuplicateTask: canCreateDuplicate);
        }
    }
}
