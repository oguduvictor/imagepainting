﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class ViewPermissionEvaluator : PermissionEvaluator
    {
        public ViewPermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            task.Permission.Update(canView: _parameters.CanUserAccessTask(task));
        }

        protected override void EvaluatePermissions(TaskView task)
        {
            task.Permission.Update(canView: _parameters.CanUserAccessTask(task));
        }
    }
}
