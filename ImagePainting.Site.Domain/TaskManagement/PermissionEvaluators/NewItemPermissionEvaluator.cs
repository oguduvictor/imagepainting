﻿using ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Domain.TaskManagement.PermissionEvaluators
{
    public class NewItemPermissionEvaluator : PermissionEvaluator
    {
        public NewItemPermissionEvaluator(EvaluatorParameters parameters)
            :base(parameters)
        {
        }

        protected override void EvaluatePermissions(WorkOrderTask task)
        {
            var hasAccess = _parameters.IsUserAdmin && _parameters.IsEditable(task);

            task.Permission.Update(canAddNewItem: hasAccess);
        }
    }
}
