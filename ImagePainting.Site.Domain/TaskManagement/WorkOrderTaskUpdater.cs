﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Domain.TaskManagement
{
    public class WorkOrderTaskUpdater
    {
        private readonly ITaskSchemaRepository _taskSchemaRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IUserContext _userContext;

        public WorkOrderTaskUpdater(ITaskSchemaRepository taskSchemaRepository,
            ITaskRepository taskRepository,
            IUserContext userContext)
        {
            _taskSchemaRepository = taskSchemaRepository;
            _taskRepository = taskRepository;
            _userContext = userContext;
        }

        public async Task UpdateAllTasksFromSchema(int taskSchemaId, 
            IEnumerable<string> addedTaskItemNames, IEnumerable<string> deletedTaskItemNames)
        {
            var taskSchema = await _taskSchemaRepository.GetTaskSchema(taskSchemaId);
            var taskIds = await _taskRepository.GetUnCompletedTaskIdsBySchemaId(taskSchemaId);
            var addedTaskItems = taskSchema.TaskItems.Where(x => addedTaskItemNames.Contains(x.Name)).ToList();

            if (taskIds.IsNullOrEmpty())
            {
                return;
            }

            var userId = _userContext.UserId;

            foreach (var taskId in taskIds)
            {
                var workOrderTask = await _taskRepository.GetTask(taskId);

                await UpdateWithSchema(workOrderTask, taskSchema, userId, addedTaskItems, deletedTaskItemNames);
            }
        }

        private async Task UpdateWithSchema(WorkOrderTask workOrderTask, WorkOrderTaskSchema taskSchema, string userId,
            IEnumerable<TaskItem> addedTaskItems, IEnumerable<string> deletedTaskItemNames)
        {
            workOrderTask.Name = taskSchema.Name;
            workOrderTask.Amount = taskSchema.Amount;
            workOrderTask.IsLumpSum = taskSchema.IsLumpSum;
            workOrderTask.TaskType = taskSchema.TaskType;
            workOrderTask.Modified = DateTime.UtcNow;
            workOrderTask.ModifiedById = userId;

            var taskItems = workOrderTask.TaskItems.ToList();

            taskSchema.TaskItems.ForEach(item =>
            {
                var taskItem = taskItems.FirstOrDefault(x => x.Name == item.Name);
                
                if (!taskItem.IsNull() && taskItem.DataType != item.DataType)
                {
                    taskItem.Value = null;
                    taskItem.DataType = item.DataType;
                }
            });

            foreach(var taskItem in workOrderTask.TaskItems)
            {
                if (deletedTaskItemNames.Contains(taskItem.Name))
                {
                    taskItems.Remove(taskItem);
                }
            }

            taskItems.AddRange(addedTaskItems);
            
            workOrderTask.TaskItems = taskItems;

            System.Diagnostics.Trace.WriteLine($"UpdateWorkOrderTasks being called for {workOrderTask.Name}");

            await _taskRepository.UpdateWorkOrderTasks(workOrderTask);
        }
    }
}
