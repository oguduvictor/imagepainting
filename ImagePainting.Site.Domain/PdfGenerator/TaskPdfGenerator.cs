﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Domain.Models;
using NReco.PdfGenerator;

namespace ImagePainting.Site.Domain
{
    public class TaskPdfGenerator : ITaskPdfGenerator
    {
        private readonly IViewCreator _viewCreator;
        private readonly IUrlService _urlService;

        public TaskPdfGenerator(
            IViewCreator viewCreator, 
            IUrlService urlService)
        {
            _viewCreator = viewCreator;
            _urlService = urlService;
        }
        

        public  byte[] CreateTaskPdf(WorkOrderTask task, 
            string encodedSignature = null, 
            bool completeCopy = false, 
            IDictionary<string, decimal> billingInfo = null,
            DateTime? signedDate = null)
        {
            var content =  GetTaskPdfTemplate(task, encodedSignature, completeCopy, billingInfo, signedDate);

            var generator = new NReco.PdfGenerator.HtmlToPdfConverter();
            generator.Margins = new PageMargins { Left = 25.4f, Bottom = 25.4f, Right = 25.4f, Top = 25.4f };
            generator.Size = NReco.PdfGenerator.PageSize.A4;

            return generator.GeneratePdf(content);
        }
        
        public string GetTaskPdfTemplate(WorkOrderTask task, 
            string encodedSignature = null, 
            bool completeCopy = false, 
            IDictionary<string, decimal> billingInfo = null,
            DateTime? signedDate = null)
        {
            var model = new WorkOrderTaskPdfModel
            {
                TaskName = task.Name,
                CustomerName = task.WorkOrder?.CustomerName,
                WorkOrderName = task.WorkOrder.Name,
                WorkOrderAddress = task.WorkOrder.Address,
                BuilderName = task.WorkOrder.Builder.Name,
                CommunityName = task.WorkOrder.Community.Name,
                ScheduleDate = task.ScheduleDateRange.GetDateRangeString(),
                SignedDate = !signedDate.IsNull() ? signedDate.Value.ToShortDateString() : DateTime.UtcNow.ToShortDateString(),
                CustomerEmail = task.WorkOrder.CustomerEmail,
                CustomerServiceRep = task.WorkOrder.CustomerRep,
                CustomerPhoneNumber = task.WorkOrder.CustomerPhoneNumber,
                TaskDescription = task.TaskType.GetDescription(),

                Signatory = task.TaskType == TaskType.CustomerService
                ? task.WorkOrder.CustomerName
                : task?.Crew.FullName,

                SignOffDescription = task.TaskType == TaskType.CustomerService
                ? "Work has been completed as per work order and to customers satisfaction."
                : "Work has been completed as per work order.",

        };

            if (completeCopy)
            {
                model.MaterialCost = billingInfo[SiteContants.MATERIAL_COST];
                model.LaborCost = billingInfo[SiteContants.LABOR_COST];
                model.CompleteCopy = true;
            }

            if (!encodedSignature.IsNull())
            {
                model.IncludeSignature = true;
                model.Signature = encodedSignature;
            }

            AddTaskItems(model, task.TaskItems);

           return _viewCreator.CreateView("WorkOrderTask", model, GetLayoutData(), addLayout: false);
        }
        
        private void AddTaskItems(WorkOrderTaskPdfModel model, List<TaskItem> taskItems)
        {
            var otherItems = taskItems.Where(x =>
                    x.DataType.IsAnyOf(TaskDataType.Number, TaskDataType.Text, TaskDataType.YesNo))
                    .OrderByDescending(x => x.DataType == TaskDataType.YesNo);

            var notesitems = taskItems.Where(x => x.DataType == TaskDataType.Note).ToList();

            model.OtherTaskItems = otherItems.Select(x =>
            {
                if (x.DataType == TaskDataType.YesNo)
                {
                    var value = x.Value.IsTrue() ? "Yes" : "No";
                    return new KeyValuePair<string, string>(x.Name, value);
                }

                return new KeyValuePair<string, string>(x.Name, x.Value);

            }).ToList();

            model.NoteItems = notesitems.Select(x =>
                new KeyValuePair<string, List<Note>>(x.Name, x.Value.IsNullOrEmpty() ? new List<Note>() : x.Value.FromJson<List<Note>>())).ToList();

        }

        private IDictionary<string, object> GetLayoutData()
        {
            var baseUrl = _urlService.GetAppBaseUrl();
            var layoutData = new Dictionary<string, object>
            {
                {"AppBaseUrl", baseUrl }
            };
            
            return layoutData;
        }
    }
}
