﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.IntegrationTests.Helpers
{
    public static class Categories
    {
        public const string IntegrationTest = "IntegrationTest";

        public const string UnitTest = "UnitTest";
    }
}
