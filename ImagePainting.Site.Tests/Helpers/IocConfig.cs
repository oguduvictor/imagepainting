﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Data;
using ImagePainting.Site.Domain.Services;
using ImagePainting.Site.Domain.TaskManagement;
using ImagePainting.Site.Web.Providers;
using LightInject;
using Moq;
using System.IO;
using System.Net.Mail;

namespace ImagePainting.Site.IntegrationTests.Helpers
{
    public static class IocConfig
    {
        public static Mock<IUserContext> UserContextMock;

        public static ServiceContainer Register()
        {
            var container = new ServiceContainer();
            var appSettingsMock = new Mock<IAppSettings>();
            var emailTemplateLocations = new[] { Path.GetFullPath(@"..\..\..\ImagePainting.Site.Web\Views\Emails") };

            UserContextMock = new Mock<IUserContext>();

            appSettingsMock.SetupGet(x => x.EmailSender).Returns("user@example.com");
            appSettingsMock.SetupGet(x => x.SystemEmailSender).Returns("user@example.com");
            appSettingsMock.SetupGet(x => x.ATaskEmailAddress).Returns("A_Task@example.com");
            appSettingsMock.SetupGet(x => x.BTaskEmailAddress).Returns("B_Task@example.com");
            appSettingsMock.SetupGet(x => x.CTaskEmailAddress).Returns("C_Task@example.com");
            appSettingsMock.SetupGet(x => x.AccountingEmailAddress).Returns("Accounting@example.com");
            appSettingsMock.SetupGet(x => x.ScheduleEmailAddress).Returns("Schedule@example.com");
            appSettingsMock.SetupGet(x => x.POEmailAddress).Returns("PO@example.com");
            appSettingsMock.SetupGet(x => x.SmtpHost).Returns("127.0.0.1");
            appSettingsMock.SetupGet(x => x.SmtpPort).Returns(25);
            appSettingsMock.SetupGet(x => x.EmailTemplateLocations).Returns(emailTemplateLocations);

            UserContextMock.SetupGet(x => x.Email).Returns("user@example.com");
            UserContextMock.SetupGet(x => x.UserId).Returns("501D4B83-8B07-49B3-A277-601A4C9D9D41");
            UserContextMock.Setup(x => x.IsAdmin()).Returns(true);

            container.RegisterFrom<DataIoCModule>();
            container.Register<SmtpClient>(new PerScopeLifetime());
            
            container.Register<ICommunityComposerService, CommunityComposerService>(new PerScopeLifetime());
            container.Register<IProjectionComposerService, ProjectionComposerService>(new PerScopeLifetime());
            container.Register<IWorkOrderComposerService, WorkOrderComposerService>(new PerScopeLifetime());
            container.Register<IUserInfoService, UserInfoService>(new PerScopeLifetime());
            container.Register<ITaskComposerService, TaskComposerService>(new PerScopeLifetime());
            container.Register<IPurchaseOrderService, PurchaseOrderService>(new PerScopeLifetime());
            container.Register<IEmailService, Domain.Services.EmailService>(new PerScopeLifetime());
            container.Register<IPaintStoreService, PaintStoreService>(new PerScopeLifetime());

            container.Register<ILogger, Logger>(new PerScopeLifetime());
            container.Register<IWorkOrderTaskManager, WorkOrderTaskManager>(new PerScopeLifetime());
            container.Register<IWorkOrderMailer, WorkOrderMailer>(new PerScopeLifetime());
            container.Register<IUserInfoMailer, UserInfoMailer>(new PerScopeLifetime());
            container.Register<IPurchaseOrderService, PurchaseOrderService>(new PerScopeLifetime());
            container.Register<IViewCreator, ViewCreator>(new PerContainerLifetime());
            container.Register<IUrlService, UrlProvider>(new PerContainerLifetime());
            container.RegisterInstance<IAppSettings>(appSettingsMock.Object);
            container.RegisterInstance<IUserContext>(UserContextMock.Object);

            return container;
        }
    }
}
