﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePainting.Site.IntegrationTests.Helpers
{
    public class MockData
    {
        public static string UserId
        {
            get
            {
                return "89DF2BDB-F17A-4437-ABF7-96EE40E9FE00";
            }
        }

        public static List<WorkOrder> WorkOrderDB 
        {
            get
            {
                return new List<WorkOrder> {
                    new WorkOrder
                    {
                        Id = 1,
                        Created = new DateTime(2017, 04, 20),
                        Description = "The Description",
                        CreatedBy = new UserInfo { LastName = "Moses", FirstName = "Jackson"},
                        Name = "Described1",
                        Deleted = false,
                        Lot = 1,
                        SuperIntendent = "John Smith"
                    },
                    new WorkOrder
                    {
                        Id = 2,
                        Created = new DateTime(2011, 05, 12),
                        Description = "The Description for the second one",
                        Name = "Described2",
                        Deleted = false,
                        Lot = 102,
                        SuperIntendent = "Adebayomi Farinde"
                    }
                 };
            }
        }

        public static List<WorkOrderTask> WorkOrderTasks
        {
            get
            {
                return new List<WorkOrderTask>
                {
                    new WorkOrderTask
                    {
                        Id = 1,
                        SchemaId = 4,
                        Amount = 30,
                        IsLumpSum = true,
                        Name = "Task Name",
                        TaskType = TaskType.Interior,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Clean the room", DataType = TaskDataType.YesNo, Value = true.ToString() }
                        },
                        ModifiedById = UserId,
                        WorkOrderId = 1
                    },
                    new WorkOrderTask
                    {
                        Id = 2,
                        SchemaId = 1,
                        Amount = 30,
                        IsLumpSum = true,
                        TaskType = TaskType.Interior,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Clean the room", DataType = TaskDataType.Number, Value = true.ToString() },
                            new TaskItem { Name = "Buy Paints", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
                        },
                        ModifiedById = UserId,
                        WorkOrderId = 1
                    },
                    new WorkOrderTask
                    {
                        Id = 3,
                        SchemaId = 2,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Need Custom Color", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Quantities Needed", DataType = TaskDataType.Number, Value = "10" },
                            new TaskItem { Name = "Buy Custom Color", DataType = TaskDataType.Text, Value = "Buy Paints from market" },
                            new TaskItem { Name = "Charge Client", DataType = TaskDataType.Note, Value = null }
                        },
                        ModifiedById = UserId,
                        WorkOrderId = 2
                    },
                    new WorkOrderTask
                    {
                        Id = 4,
                        SchemaId = 3,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Need Custom Color", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Quantities Needed", DataType = TaskDataType.Number, Value = "10" },
                            new TaskItem { Name = "Buy Custom Color", DataType = TaskDataType.Text, Value = "Buy Paints from market" },
                            new TaskItem { Name = "Charge Client", DataType = TaskDataType.Number, Value = "13000" }
                        },
                        ModifiedById = UserId,
                        WorkOrderId = 2
                    }
                };
            }
        }

        public static List<WorkOrderTaskSchema> TaskSchemas
        {
            get
            {
                return new List<WorkOrderTaskSchema>
                {
                    new WorkOrderTaskSchema
                    {
                        Id = 1,
                        Amount = 30,
                        IsLumpSum = true,
                        TaskType = TaskType.Interior,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Clean the room", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Buy Paints", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
                        }
                    },
                    new WorkOrderTaskSchema
                    {
                        Id = 2,
                        Amount = 100,
                        IsLumpSum = false,
                        TaskType = TaskType.Exterior,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Need Custom Color", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Quantities Needed", DataType = TaskDataType.Number, Value = "10" },
                            new TaskItem { Name = "Buy Custom Color", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
                        }
                    },
                    new WorkOrderTaskSchema
                    {
                        Id = 3,
                        Amount = 100,
                        IsLumpSum = false,
                        TaskType = TaskType.Exterior,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Need Custom Color", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Quantities Needed", DataType = TaskDataType.Number, Value = "10" },
                            new TaskItem { Name = "Buy Custom Color", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
                        }
                    },
                    new WorkOrderTaskSchema
                    {
                        Id = 4,
                        Amount = 8000,
                        IsLumpSum = false,
                        Name = "Updated Schema Name",
                        TaskType = TaskType.PaintEnamel,
                        TaskItems = new List<TaskItem>
                        {
                            new TaskItem { Name = "Clean the room", DataType = TaskDataType.YesNo, Value = true.ToString() },
                            new TaskItem { Name = "Buy Paints", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
                        }
                    },
                };
            }
        }

        public static List<WorkOrder> WorkOrders
        {
            get
            {
                return new List<WorkOrder>
                {
                    new WorkOrder
                    {
                        Id = 1,
                        Name = "WorkOrder 1",
                        ExtraJobInfo = new ExtraJobInfo
                        {
                            Id = 1,
                            ExtraItems = new List<ExtraItem>
                            {
                                new ExtraItem { Id = Guid.Parse("7CA16B0B-F3FD-4A2A-ADCD-FAC32D76ECBE") },
                                new ExtraItem { Id = Guid.Parse("63B5281F-6E2E-4C5E-969A-611C446E85B1") },
                                new ExtraItem { Id = Guid.Parse("BC9370EE-0F6E-4632-9BFC-5747030C55EB") }
                            }
                        },
                        ExtraJobInfoId = 1
                    },
                    new WorkOrder
                    {
                        Id = 2,
                        Name = "WorkOrder 2",
                        ExtraJobInfo = new ExtraJobInfo
                        {
                            Id = 2,
                            ExtraItems = new List<ExtraItem>
                            {
                                new ExtraItem { Id = Guid.Parse("L0A1670B-F3FD-4A2A-ADCD-F11C4D76ECBE") },
                                new ExtraItem { Id = Guid.Parse("P3B5681F-6E2E-4C5E-969A-6CBC446E85B1") },
                                new ExtraItem { Id = Guid.Parse("B02JGQEE-0F6E-4632-9BFC-5D76E30C55EB") }
                            }
                        },
                        ExtraJobInfoId = 2
                    }
                };
            }
        }

        public static List<ExtraJobInfo> ExtraJobInfos
        {
            get
            {
                return new List<ExtraJobInfo>
                {
                    new ExtraJobInfo
                    {
                        Id = 1,
                        ExtraItems = new List<ExtraItem>
                        {
                            new ExtraItem { Id = Guid.Parse("7CA16B0B-F3FD-4A2A-ADCD-FAC32D76ECBE") },
                            new ExtraItem { Id = Guid.Parse("63B5281F-6E2E-4C5E-969A-611C446E85B1") },
                            new ExtraItem { Id = Guid.Parse("BC9370EE-0F6E-4632-9BFC-5747030C55EB") }
                        }
                    },
                    new ExtraJobInfo
                    {
                        Id = 2,
                        ExtraItems = new List<ExtraItem>
                        {
                            new ExtraItem { Id = Guid.Parse("L0A1670B-F3FD-4A2A-ADCD-F11C4D76ECBE") },
                            new ExtraItem { Id = Guid.Parse("P3B5681F-6E2E-4C5E-969A-6CBC446E85B1") },
                            new ExtraItem { Id = Guid.Parse("B02JGQEE-0F6E-4632-9BFC-5D76E30C55EB") }
                        }
                    }
                };
            }
        }
    }
}
