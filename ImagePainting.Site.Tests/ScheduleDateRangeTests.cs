﻿using ImagePainting.Site.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class ScheduleDateRangeTests
    {
        [TestMethod]
        public void SchduleDateRange_Equality_Is_Correct()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var date2 = new DateTime(2018, 10, 10, 6, 6, 6);
            var date3 = new DateTime(2018, 10, 10, 7, 7, 7);

            var dateRange1 = new ScheduleDateRange(date1, date2);
            var dateRange2 = new ScheduleDateRange(date1, date2);
            var dateRange3 = new ScheduleDateRange(date1);
            var dateRange4 = new ScheduleDateRange(date1);
            var dateRange5 = new ScheduleDateRange(date2, date3);

            Assert.IsTrue(dateRange1 == dateRange2);
            Assert.IsTrue(dateRange1.Equals(dateRange2));

            Assert.IsTrue(dateRange3 == dateRange4);
            Assert.IsTrue(dateRange3.Equals(dateRange4));

            Assert.IsFalse(dateRange1 == dateRange5);
            Assert.IsFalse(dateRange1.Equals(dateRange5));

            Assert.IsFalse(dateRange1 == dateRange3);
            Assert.IsFalse(dateRange1.Equals(dateRange3));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Invalid_Date_Range_Should_Throw()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var date2 = new DateTime(2018, 10, 10, 6, 6, 6);

            var dateRange1 = new ScheduleDateRange(date2, date1);
        }

        [TestMethod]
        public void GetDate_Should_Return_Date()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var date2 = new DateTime(2018, 10, 10, 6, 6, 6);
            var dateTimeRange = new ScheduleDateRange(date1, date2);

            var expectedDate = new DateTime(2018, 10, 10);

            var result = dateTimeRange.GetDate();

            Assert.AreEqual(expectedDate, result);
        }

        [TestMethod]
        public void SetDate_Should_Set_Date()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var date2 = new DateTime(2018, 10, 10, 6, 6, 6);
            var dateTimeRange = new ScheduleDateRange(date1, date2);

            var expectedDate = new DateTime(2018, 12, 12);

            dateTimeRange.SetDate(expectedDate);

            Assert.AreEqual(expectedDate, dateTimeRange.BeginDate.Date);
            Assert.AreEqual(date1.Hour, dateTimeRange.BeginDate.Hour);
            Assert.AreEqual(date1.Minute, dateTimeRange.BeginDate.Minute);
            Assert.AreEqual(date1.Second, dateTimeRange.BeginDate.Second);

            Assert.AreEqual(expectedDate, dateTimeRange.EndDate.Value.Date);
            Assert.AreEqual(date2.Hour, dateTimeRange.EndDate.Value.Hour);
            Assert.AreEqual(date2.Minute, dateTimeRange.EndDate.Value.Minute);
            Assert.AreEqual(date2.Second, dateTimeRange.EndDate.Value.Second);
        }

        [TestMethod]
        public void SetDate_Should_Set_Date_With_Null_EndDate()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var dateTimeRange = new ScheduleDateRange(date1);

            var expectedDate = new DateTime(2018, 12, 12);

            dateTimeRange.SetDate(expectedDate);

            Assert.AreEqual(expectedDate, dateTimeRange.BeginDate.Date);
            Assert.AreEqual(date1.Hour, dateTimeRange.BeginDate.Hour);
            Assert.AreEqual(date1.Minute, dateTimeRange.BeginDate.Minute);
            Assert.AreEqual(date1.Second, dateTimeRange.BeginDate.Second);

            Assert.IsNull(dateTimeRange.EndDate);
        }

        [TestMethod]
        public void SetTime_Should_Set_Time()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var date2 = new DateTime(2018, 10, 10, 6, 6, 6);
            var dateTimeRange = new ScheduleDateRange(date1, date2);

            var date3 = new DateTime(2016, 5, 5, 10, 9, 8);
            var date4 = new DateTime(2016, 5, 5, 11, 10, 9);
            var dateTimeRange2 = new ScheduleDateRange(date3, date4);

            dateTimeRange.SetTime(dateTimeRange2);

            Assert.AreEqual(date1.Date, dateTimeRange.BeginDate.Date);
            Assert.AreEqual(date3.Hour, dateTimeRange.BeginDate.Hour);
            Assert.AreEqual(date3.Minute, dateTimeRange.BeginDate.Minute);
            Assert.AreEqual(date3.Second, dateTimeRange.BeginDate.Second);

            Assert.AreEqual(date2.Date, dateTimeRange.EndDate.Value.Date);
            Assert.AreEqual(date4.Hour, dateTimeRange.EndDate.Value.Hour);
            Assert.AreEqual(date4.Minute, dateTimeRange.EndDate.Value.Minute);
            Assert.AreEqual(date4.Second, dateTimeRange.EndDate.Value.Second);
        }

        [TestMethod]
        public void SetTime_Should_Set_Time_With_Null_EndDate()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var dateTimeRange = new ScheduleDateRange(date1);

            var date3 = new DateTime(2016, 5, 5, 10, 9, 8);
            var date4 = new DateTime(2016, 5, 5, 11, 10, 9);
            var dateTimeRange2 = new ScheduleDateRange(date3, date4);

            dateTimeRange.SetTime(dateTimeRange2);

            Assert.AreEqual(date1.Date, dateTimeRange.BeginDate.Date);
            Assert.AreEqual(date3.Hour, dateTimeRange.BeginDate.Hour);
            Assert.AreEqual(date3.Minute, dateTimeRange.BeginDate.Minute);
            Assert.AreEqual(date3.Second, dateTimeRange.BeginDate.Second);

            Assert.AreEqual(date1.Date, dateTimeRange.EndDate.Value.Date);
            Assert.AreEqual(date4.Hour, dateTimeRange.EndDate.Value.Hour);
            Assert.AreEqual(date4.Minute, dateTimeRange.EndDate.Value.Minute);
            Assert.AreEqual(date4.Second, dateTimeRange.EndDate.Value.Second);
        }

        [TestMethod]
        public void SetTime_Should_Set_Time_With_No_EndTime()
        {
            var date1 = new DateTime(2018, 10, 10, 5, 5, 5);
            var dateTimeRange = new ScheduleDateRange(date1, new DateTime(2018, 10, 10));

            var date3 = new DateTime(2016, 5, 5, 10, 9, 8);
            var date4 = new DateTime(2016, 5, 5, 11, 10, 9);
            var dateTimeRange2 = new ScheduleDateRange(date3, date4);

            dateTimeRange.SetTime(dateTimeRange2);

            Assert.AreEqual(date1.Date, dateTimeRange.BeginDate.Date);
            Assert.AreEqual(date3.Hour, dateTimeRange.BeginDate.Hour);
            Assert.AreEqual(date3.Minute, dateTimeRange.BeginDate.Minute);
            Assert.AreEqual(date3.Second, dateTimeRange.BeginDate.Second);

            Assert.AreEqual(date1.Date, dateTimeRange.EndDate.Value.Date);
            Assert.AreEqual(date4.Hour, dateTimeRange.EndDate.Value.Hour);
            Assert.AreEqual(date4.Minute, dateTimeRange.EndDate.Value.Minute);
            Assert.AreEqual(date4.Second, dateTimeRange.EndDate.Value.Second);
        }
    }
}
