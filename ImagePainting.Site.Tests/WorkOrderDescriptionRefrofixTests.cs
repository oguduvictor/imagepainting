﻿using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.IntegrationTests.Helpers;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Retrofits;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ImagePainting.Site.IntegrationTests.Helpers.MockData;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class WorkOrderDescriptionRefrofixTests
    {
        private static Mock<IWorkOrderRepository> _workOrderRepository;
        private WorkOrderDescriptionRefrofix _workOrderDescriptionRetrofix;

        private static List<WorkOrder> WorkOrders;

        [ClassInitialize]
        public static void BeforeAll(TestContext context)
        {
            _workOrderRepository = new Mock<IWorkOrderRepository>();
        }

        [TestInitialize]
        public void BeforeEach()
        {
            _workOrderDescriptionRetrofix = new WorkOrderDescriptionRefrofix(_workOrderRepository.Object);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public async Task Should_Update_Description_ToJson_When_FixDescriptionInWorkOrders_Is_Called_And_The_Properties_Unchanged_Remained_Unchanged()
        {
            _workOrderRepository.Setup(x => x.GetWorkOrders(It.IsAny<WorkOrderQuery>()))
                .ReturnsAsync(
                (WorkOrderQuery query) => {
                    var workOrders = WorkOrderDB.Skip(query.PageNumber * query.ResultsCount).Take(query.ResultsCount).ToList();

                    // This is done to prevent setting the workorders list as empty list
                    if (workOrders.Any())
                    {
                        WorkOrders = workOrders;
                    }

                    return workOrders;
                });

            await _workOrderDescriptionRetrofix.FixDescriptionInWorkOrders();

            var expectedWorkOrder = new List<WorkOrder> {
                new WorkOrder {
                    Id = 1,
                    Name = "Described1" ,
                    Created = new DateTime(2017, 04, 20),
                    Deleted = false,
                    Lot = 1,
                    SuperIntendent = "John Smith",
                    CreatedBy = new UserInfo
                    { LastName = "Moses", FirstName = "Jackson"
                    }
                },
                new WorkOrder {
                    Id = 2,
                    Name = "Described2",
                    Created = new DateTime(2011, 05, 12),
                    Deleted = false,
                    Lot = 102,
                    SuperIntendent = "Adebayomi Farinde"
                }
            };

            for (int i = 0; i < expectedWorkOrder.Count; i++)
            {
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).Name, WorkOrders.ElementAt(i).Name);
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).Created, WorkOrders.ElementAt(i).Created);
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).Deleted, WorkOrders.ElementAt(i).Deleted);
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).SuperIntendent, WorkOrders.ElementAt(i).SuperIntendent);
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).Lot, WorkOrders.ElementAt(i).Lot);
                Assert.AreEqual(expectedWorkOrder.ElementAt(i).Id, WorkOrders.ElementAt(i).Id);
            }
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public async Task Should_Update_Description_ToJson_When_FixDescriptionInWorkOrders_Is_Called_And_The_Properties_Changed_Remained_Changed()
        {
            _workOrderRepository.Setup(x => x.GetWorkOrders(It.IsAny<WorkOrderQuery>()))
                .ReturnsAsync(
                (WorkOrderQuery query) => {
                    var workOrders = WorkOrderDB.Skip(query.PageNumber * query.ResultsCount).Take(query.ResultsCount).ToList();

                    // This is done to prevent setting the workorders list as empty list
                    if (workOrders.Any())
                    {
                        WorkOrders = workOrders;
                    }

                    return workOrders;
                });

            await _workOrderDescriptionRetrofix.FixDescriptionInWorkOrders();

            var expectedNotes = new List<Note> {
                new Note {
                    Author = "Jackson Moses",
                    Text = "The Description",
                    Created = new DateTime(2017, 04, 20)
                },

                new Note {
                    Author = null,
                    Text = "The Description for the second one",
                    Created = new DateTime(2011, 05, 12)

                }
            };

            for (int i = 0; i < expectedNotes.Count; i++)
            {
                Assert.AreEqual(expectedNotes.ElementAt(i).Author, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Author);
                Assert.AreEqual(expectedNotes.ElementAt(i).Text, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Text);
                Assert.AreEqual(expectedNotes.ElementAt(i).Created, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Created);
            }
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public async Task Should_Not_Update_Description_ToJson_When_FixDescriptionInWorkOrders_Is_Called_On_Description_In_JsonFormat()
        {
            var WorkOrderDbWithJSONDescription = new List<WorkOrder>
            {
                new WorkOrder
                    {
                        Id = 4,
                        Created = new DateTime(2016, 10, 13),
                        Description = "[\r\n  {\r\n    \"Id\": \"f62559a7-091a-45ca-9221-7d91ecf33821\",\r\n    \"Text\": \"The Description\",\r\n    \"Author\": \"Adegboyega Lakes\",\r\n    \"Created\": \"2016-10-13T00:00:00\"\r\n  }\r\n]",
                        CreatedBy = new UserInfo { LastName = "Lakes", FirstName = "Adegboyega" },
                        Name ="Describe4",
                        Deleted = false,
                        Lot = 20, SuperIntendent = "Joe Isaac"
                    }
            };

            _workOrderRepository.Setup(x => x.GetWorkOrders(It.IsAny<WorkOrderQuery>()))
                .ReturnsAsync(
                (WorkOrderQuery query) => {
                    var workOrders = WorkOrderDbWithJSONDescription.Skip(query.PageNumber * query.ResultsCount).Take(query.ResultsCount).ToList();

                    // This is done to prevent setting the workorders list as empty list
                    if (workOrders.Any())
                    {
                        WorkOrders = workOrders;
                    }

                    return workOrders;
                });


            await _workOrderDescriptionRetrofix.FixDescriptionInWorkOrders();

            var expectedNotes = new List<Note> {

                new Note
                {
                    Author = "Adegboyega Lakes",
                    Text = "The Description",
                    Created = new DateTime(2016, 10, 13)
                }
            };

            for (int i = 0; i < expectedNotes.Count; i++)
            {
                Assert.AreEqual(expectedNotes.ElementAt(i).Author, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Author);
                Assert.AreEqual(expectedNotes.ElementAt(i).Text, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Text);
                Assert.AreEqual(expectedNotes.ElementAt(i).Created, WorkOrders.ElementAt(i).Description.FromJson<List<Note>>().First().Created);
            }
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public async Task Should_Not_Update_Description_ToJson_When_FixDescriptionInWorkOrders_Is_Called_On_Description_Of_NullOrWhiteSpace()
        {
            var WorkOrderDbWithJSONDescription = new List<WorkOrder>
            {
                new WorkOrder
                    {
                        Id = 3,
                        Created = new DateTime(2010, 12, 22),
                        Description = "",
                        Name = "Described3",
                        Deleted = false,
                        Lot = 25,
                        SuperIntendent = "Onwuzulike Emeka"
                    }
            };

            _workOrderRepository.Setup(x => x.GetWorkOrders(It.IsAny<WorkOrderQuery>()))
                .ReturnsAsync(
                (WorkOrderQuery query) => {
                    var workOrders = WorkOrderDbWithJSONDescription.Skip(query.PageNumber * query.ResultsCount).Take(query.ResultsCount).ToList();

                    // This is done to prevent setting the workorders list as empty list
                    if (workOrders.Any())
                    {
                        WorkOrders = workOrders;
                    }

                    return workOrders;
                });


            await _workOrderDescriptionRetrofix.FixDescriptionInWorkOrders();

            var expectedDescription = string.Empty;

            for (int i = 0; i < WorkOrders.Count; i++)
            {
                Assert.AreEqual(expectedDescription, WorkOrders.ElementAt(i).Description);
            }
        }
    }
}
