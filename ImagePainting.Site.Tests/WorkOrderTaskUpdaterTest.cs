﻿using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Domain.TaskManagement;
using ImagePainting.Site.IntegrationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Interfaces.Repository;
using static ImagePainting.Site.IntegrationTests.Helpers.MockData;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class WorkOrderTaskUpdaterTest
    {
        private static Mock<ITaskSchemaRepository> taskSchemaRepository;
        private static Mock<ITaskRepository> taskRepository;
        private static Mock<IUserContext> userContext;
        private WorkOrderTaskUpdater workOrderTaskUpdater;

        private static WorkOrderTask workOrderTask;

        [ClassInitialize]
        public static void BeforeAll(TestContext context)
        {
            taskSchemaRepository = new Mock<ITaskSchemaRepository>();
            taskRepository = new Mock<ITaskRepository>();
            userContext = new Mock<IUserContext>();

            taskRepository.Setup(x => x.GetUnCompletedTaskIdsBySchemaId(It.IsAny<int>()))
                .ReturnsAsync((int schemaId) => WorkOrderTasks.Where(x => x.SchemaId == schemaId).Select(x => x.Id).ToList());

            taskSchemaRepository.Setup(x => x.GetTaskSchema(It.IsAny<int>()))
                .ReturnsAsync((int id) => TaskSchemas.FirstOrDefault(x => x.Id == id));

            taskRepository.Setup(x => x.GetTask(It.IsAny<long>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync((long input, bool includeCrew, bool includeWorkOrder, bool includeSubTasks, bool includeCompletedBy, bool includeBuilderAndCommunity, bool includeUnlockedBy) =>
                {
                    workOrderTask = WorkOrderTasks.FirstOrDefault(x => x.Id == input);
                    return workOrderTask;
                });

            userContext.Setup(x => x.UserId).Returns(UserId);
        }

        [TestInitialize]
        public void BeforeEach()
        {
            workOrderTaskUpdater = new WorkOrderTaskUpdater(taskSchemaRepository.Object, taskRepository.Object, userContext.Object);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public async Task Should_Update_WorkOrderTask_Amount_IsLumpSum_And_TaskType_When_TaskSchema_Is_Updated()
        {
            await workOrderTaskUpdater.UpdateAllTasksFromSchema(4, new List<string>(), new List<string>());

            Assert.AreEqual("Updated Schema Name", workOrderTask.Name);
            Assert.AreEqual(8000, workOrderTask.Amount);
            Assert.AreEqual(TaskType.PaintEnamel, workOrderTask.TaskType);
            Assert.AreEqual(false, workOrderTask.IsLumpSum);
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Add_Schema_Task_Item_To_WorkOrderTask_Items_When_Not_Found_In_Task_Items()
        {
            var addedTaskItemNames = new List<string> { "Buy Paints" };

            await workOrderTaskUpdater.UpdateAllTasksFromSchema(4, addedTaskItemNames, new List<string>());

            var expectedTaskItems = TaskSchemas.ElementAt(3).TaskItems;
            
            Assert.AreEqual(2, workOrderTask.TaskItems.Count);

            for (int i = 0, length = expectedTaskItems.Count(); i < length; i++)
            {
                var actualTaskItem = workOrderTask.TaskItems.ElementAt(i);
                var expectedTaskItem = expectedTaskItems.ElementAt(i);

                Assert.AreEqual(expectedTaskItem.Name, actualTaskItem.Name);
                Assert.AreEqual(expectedTaskItem.DataType, actualTaskItem.DataType);
                Assert.AreEqual(expectedTaskItem.Value, actualTaskItem.Value);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Update_Task_Item_DataType_And_Set_Value_As_Null_In_WorkOrderTask_When_Task_DataType_Differs_From_Schema_Items()
        {
            var itemsList = new List<string>();
            await workOrderTaskUpdater.UpdateAllTasksFromSchema(1, itemsList, itemsList);

            var expectedTaskItems = new List<TaskItem>
            {
                new TaskItem { Name = "Clean the room", DataType = TaskDataType.YesNo, Value = null },
                new TaskItem { Name = "Buy Paints", DataType = TaskDataType.Text, Value = "Buy Paints from market" }
            };

            Assert.AreEqual(TaskDataType.YesNo, workOrderTask.TaskItems.First().DataType);
            Assert.IsNull(workOrderTask.TaskItems.First().Value);

            for (int i = 0, length = expectedTaskItems.Count(); i < length; i++)
            {
                var actualTaskItem = workOrderTask.TaskItems.ElementAt(i);
                var expectedTaskItem = expectedTaskItems.ElementAt(i);

                Assert.AreEqual(expectedTaskItem.Name, actualTaskItem.Name);
                Assert.AreEqual(expectedTaskItem.DataType, actualTaskItem.DataType);
                Assert.AreEqual(expectedTaskItem.Value, actualTaskItem.Value);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Remove_WorkOrderTask_Item_If_Not_Found_In_Schema_Items_And_Not_Answered()
        {
            var deletedTaskNames = new List<string> { "Charge Client" };

            await workOrderTaskUpdater.UpdateAllTasksFromSchema(2, new List<string>(), deletedTaskNames);

            Assert.AreEqual(3, workOrderTask.TaskItems.Count);

            var expectedTaskItems = TaskSchemas.ElementAt(1).TaskItems;

            for (int i = 0, length = expectedTaskItems.Count(); i < length; i++)
            {
                var actualTaskItem = workOrderTask.TaskItems.ElementAt(i);
                var expectedTaskItem = expectedTaskItems.ElementAt(i);

                Assert.AreEqual(expectedTaskItem.Name, actualTaskItem.Name);
                Assert.AreEqual(expectedTaskItem.DataType, actualTaskItem.DataType);
                Assert.AreEqual(expectedTaskItem.Value, actualTaskItem.Value);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Leave_WorkOrderTask_Item_If_Not_Found_In_Schema_Items_But_Answered()
        {
            var itemsList = new List<string>();
            await workOrderTaskUpdater.UpdateAllTasksFromSchema(3, itemsList, itemsList);

            Assert.AreEqual(4, workOrderTask.TaskItems.Count);

            var expectedTaskItems = TaskSchemas.ElementAt(2).TaskItems as IList<TaskItem>;
            expectedTaskItems.Add(new TaskItem { Name = "Charge Client", DataType = TaskDataType.Number, Value = "13000" });

            for (int i = 0, length = expectedTaskItems.Count(); i < length; i++)
            {
                var actualTaskItem = workOrderTask.TaskItems.ElementAt(i);
                var expectedTaskItem = expectedTaskItems.ElementAt(i);

                Assert.AreEqual(expectedTaskItem.Name, actualTaskItem.Name);
                Assert.AreEqual(expectedTaskItem.DataType, actualTaskItem.DataType);
                Assert.AreEqual(expectedTaskItem.Value, actualTaskItem.Value);
            }
        }
    }
}
