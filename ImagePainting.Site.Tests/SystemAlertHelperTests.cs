﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models.Enums;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class SystemAlertHelperTests
    {
        [TestMethod]
        public void ComputeTargetObjectId_Should_Generate_Valid_Guid()
        {
            var parentId = 12345;
            var parentType = AlertParentType.WorkOrder;
            var alertType = AlertType.JobInfoNote;
            var expectedGuid = Guid.Parse("00000000000000030002000000012345");

            var guid = SystemAlertHelper.ComputeTargetObjectId(parentId, parentType, alertType);

            Assert.AreEqual(expectedGuid, guid);
        }
    }
}
