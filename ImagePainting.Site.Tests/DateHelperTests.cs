﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.IntegrationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class DateHelperTests
    {
        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Split_Should_Give_Correct_Number_Of_Dates_In_Range()
        {
            var dateRange = new DateTimeRange
            {
                BeginDateTime = DateTime.Today,
                EndDateTime = DateTime.Today.AddDays(4)
            };

            var dates = dateRange.ToRange();

            Assert.AreEqual(5, dates.Count);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Split_Should_Give_Correct_Dates_In_Range()
        {
            var dateRange = new DateTimeRange
            {
                BeginDateTime = DateTime.Today,
                EndDateTime = DateTime.Today.AddDays(5)
            };

            var dates = dateRange.ToRange();
            var startDate = dateRange.BeginDateTime.Date;

            Assert.AreEqual(startDate, dates[0]);
            Assert.AreEqual(startDate.AddDays(1), dates[1]);
            Assert.AreEqual(startDate.AddDays(2), dates[2]);
            Assert.AreEqual(startDate.AddDays(3), dates[3]);
            Assert.AreEqual(startDate.AddDays(4), dates[4]);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Following_Friday_For_Task_Completed_On_Monday()
        {
            var dateCompleted = new DateTime(2018, 4, 2);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 13);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Following_Friday_For_Task_Completed_On_Tuesday()
        {
            var dateCompleted = new DateTime(2018, 4, 3);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 13);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate); 
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Following_Friday_For_Task_Completed_On_Wednessday_Before_Noon()
        {
            var dateCompleted = new DateTime(2018, 4, 4, 10, 0, 0);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 13);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Upper_Friday_For_Task_Completed_On_Wednessday_After_Noon()
        {
            var dateCompleted = new DateTime(2018, 4, 4, 16, 0, 0, DateTimeKind.Utc);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 20);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Upper_Friday_For_Task_Completed_On_Thursday()
        {
            var dateCompleted = new DateTime(2018, 4, 5);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 20);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Upper_Friday_For_Task_Completed_On_Friday()
        {
            var dateCompleted = new DateTime(2018, 4, 6);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 20);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Upper_Friday_For_Task_Completed_On_Saturday()
        {
            var dateCompleted = new DateTime(2018, 4, 7);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 20);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Return_Upper_Friday_For_Task_Completed_On_Sunday()
        {
            var dateCompleted = new DateTime(2018, 4, 8);
            var estimatePaymentDate = dateCompleted.EstimatePaymentDate();
            var espectedPaymentDate = new DateTime(2018, 4, 20);
            Assert.AreEqual(espectedPaymentDate, estimatePaymentDate);
        }

        [TestMethod]
        public void Should_Convert_Eastern_Time_To_UTC()
        {
            var easternDateTime = new DateTime(2018, 4, 21, 7, 30, 50);

            var utcDateTime = easternDateTime.ToUTC();

            Assert.AreEqual(DateTimeKind.Unspecified, easternDateTime.Kind);
            Assert.AreEqual(DateTimeKind.Utc, utcDateTime.Kind);
            Assert.AreEqual(2018, utcDateTime.Year);
            Assert.AreEqual(4, utcDateTime.Month);
            Assert.AreEqual(21, utcDateTime.Day);
            Assert.AreEqual(11, utcDateTime.Hour);
            Assert.AreEqual(30, utcDateTime.Minute);
            Assert.AreEqual(50, utcDateTime.Second);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Should_Convert_DateTimeRange_To_UTC()
        {
            var dateRange = new DateTimeRange
            {
                BeginDateTime = new DateTime(2018, 4, 1),
                EndDateTime = new DateTime(2018, 4, 30)
            };

            var utcDateRange = dateRange.ToUTCDateTimeRange();

            var expectedBeginDateTime = new DateTime(2018, 4, 1, 0, 0, 0);
            var expectedEndDateTime = new DateTime(2018, 4, 30, 23, 59, 59);

            Assert.AreEqual(expectedBeginDateTime.Year, utcDateRange.BeginDateTime.Year);
            Assert.AreEqual(expectedBeginDateTime.Month, utcDateRange.BeginDateTime.Month);
            Assert.AreEqual(expectedBeginDateTime.Day, utcDateRange.BeginDateTime.Day);
            Assert.AreEqual(expectedBeginDateTime.Hour, utcDateRange.BeginDateTime.Hour);
            Assert.AreEqual(expectedBeginDateTime.Minute, utcDateRange.BeginDateTime.Minute);
            Assert.AreEqual(expectedBeginDateTime.Second, utcDateRange.BeginDateTime.Second);

            Assert.AreEqual(expectedEndDateTime.Year, utcDateRange.EndDateTime.Year);
            Assert.AreEqual(expectedEndDateTime.Month, utcDateRange.EndDateTime.Month);
            Assert.AreEqual(expectedEndDateTime.Day, utcDateRange.EndDateTime.Day);
            Assert.AreEqual(expectedEndDateTime.Hour, utcDateRange.EndDateTime.Hour);
            Assert.AreEqual(expectedEndDateTime.Minute, utcDateRange.EndDateTime.Minute);
            Assert.AreEqual(expectedEndDateTime.Second, utcDateRange.EndDateTime.Second);
        }
    }
}
