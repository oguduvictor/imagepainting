﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.IntegrationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ImagePainting.Site.IntegrationTests
{
    /// <summary>
    /// Summary description for ObjectExtensionTest
    /// </summary>
    [TestClass]
    public class ObjectExtensionTest
    {
        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Can_BreakUpText_Giving_Spaces()
        {
            var word = "TryingThis";
            string result = word.BreakupText();
            Assert.AreEqual("Trying This", result);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Can_Break_Up_Text_Giving_Spaces_And_Forward_Splash()
        {
            var word = "CheckThisForGood";
            string result = word.BreakupText();
            Assert.AreEqual("Check This/For Good", result);
        }

        [TestMethod, TestCategory(Categories.UnitTest)]
        public void Can_Break_Text_Into_Lot_And_Block()
        {
            var lotAndBlock1 = string.Empty;
            var lotAndBlock2 = "83";
            var lotAndBlock3 = "901 D8";
            var lotAndBlock4 = "728  EF9";

            var result1 = lotAndBlock1.BreakTextIntoLotAndBlock();
            var result2 = lotAndBlock2.BreakTextIntoLotAndBlock();
            var result3 = lotAndBlock3.BreakTextIntoLotAndBlock();
            var result4 = lotAndBlock4.BreakTextIntoLotAndBlock();

            Assert.AreEqual(null, result1?.lot);
            Assert.AreEqual(null, result1?.block);

            Assert.AreEqual(83, result2?.lot);
            Assert.IsNull(result2?.block);

            Assert.AreEqual(901, result3?.lot);
            Assert.AreEqual("D8", result3?.block);

            Assert.AreEqual(728, result4?.lot);
            Assert.AreEqual("EF9", result4?.block);
        }
    }
}
