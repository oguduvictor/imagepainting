﻿using AutoMapper;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Data;
using ImagePainting.Site.IntegrationTests.Helpers;
using LightInject;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.IntegrationTests
{
    [TestClass]
    public class EmailTests
    {
        private static ServiceContainer _container;

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            _container = IocConfig.Register();
            Mapper.Initialize(c =>
            {
                c.AddProfile<SqlDataMapperProfile>();
            });
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Password_Reset_Email()
        {
            using (_container.BeginScope())
            {
                var userInfoMailer = _container.GetInstance(typeof(IUserInfoMailer)) as IUserInfoMailer;
                var userInfo = new UserInfo
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Email = "john.doe@example.com"
                };

                await userInfoMailer.EmailForgotPassWord(userInfo, "http://localhost:5554/");
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Revenue_Projection_Email()
        {
            using (_container.BeginScope())
            {
                var workorderService = _container.GetInstance(typeof(IWorkOrderComposerService)) as IWorkOrderComposerService;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;
                var query = new WorkOrderQuery { ResultsCount = 1 };
                var workorders = await workorderService.GetWorkOrders(query);

                await mailer.EmailProjection(workorders.First().Id);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Task_Completion_Email_For_Prewalk_Task()
        {
            var taskCategory = TaskCategory.Prewalk;

            using (_container.BeginScope())
            {
                var taskRepo = _container.GetInstance(typeof(ITaskRepository)) as ITaskRepository;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;

                var searchCriteria = new TaskSearchCriteria
                {
                    IncludeCrew = true,
                    IncludeWorkOrder = true,
                    OrderByAscending = false,
                    PageNumber = 0, 
                    IsCompleted = true
                };

                var tasks = await taskRepo.GetTasks(searchCriteria);

                var mainTask = tasks.FirstOrDefault(x => x.IsMainTask && x.PreWalkTaskId.HasValue);
                var preWalk = tasks.FirstOrDefault(x => mainTask.PreWalkTaskId == x.Id);

                await mailer.EmailTaskCompletion(preWalk.Id, taskCategory);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Task_Completion_Email_For_Main_Task()
        {
            var taskCategory = TaskCategory.Main;

            using (_container.BeginScope())
            {
                var taskRepo = _container.GetInstance(typeof(ITaskRepository)) as ITaskRepository;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;

                var searchCriteria = new TaskSearchCriteria
                {
                    IncludeCrew = true,
                    IncludeWorkOrder = true,
                    OrderByAscending = false,
                    PageNumber = 0,
                    IsCompleted = true
                };
                
                var tasks = await taskRepo.GetTasks(searchCriteria);
                var task = tasks.FirstOrDefault(x => x.IsMainTask);

                await mailer.EmailTaskCompletion(task.Id, taskCategory);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Task_Completion_Email_For_Completion_Task()
        {
            var taskCategory = TaskCategory.Completion;

            using (_container.BeginScope())
            {
                var taskRepo = _container.GetInstance(typeof(ITaskRepository)) as ITaskRepository;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;

                var searchCriteria = new TaskSearchCriteria
                {
                    IncludeCrew = true,
                    IncludeWorkOrder = true,
                    OrderByAscending = false,
                    PageNumber = 0,
                    IsCompleted = true
                };

                var tasks = await taskRepo.GetTasks(searchCriteria);

                var mainTask = tasks.LastOrDefault(x => x.IsMainTask && x.CompletionWalkTaskId.HasValue);
                var completionWalk = tasks.FirstOrDefault(x => mainTask.CompletionWalkTaskId == x.Id);

                await mailer.EmailTaskCompletion(completionWalk.Id, taskCategory);
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Task_purchaserOrder_Email()
        {
            using (_container.BeginScope())
            {
                var purchaseOrderService = _container.GetInstance(typeof(IPurchaseOrderService)) as IPurchaseOrderService;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;

                var purchaseOrders = await purchaseOrderService.GetPurchaseOrders();

                await mailer.EmailPurchaseOrder(purchaseOrders.First().Id, "test@example.com");
            }
        }

        [TestMethod, TestCategory(Categories.IntegrationTest)]
        public async Task Should_Send_Task_Scheduled_Email()
        {
            using (_container.BeginScope())
            {
                var taskRepo = _container.GetInstance(typeof(ITaskRepository)) as ITaskRepository;
                var mailer = _container.GetInstance(typeof(IWorkOrderMailer)) as IWorkOrderMailer;

                var query = new TaskSearchCriteria { IsMainTask = true, IncludeWorkOrder = true, IncludeJobInfo = true, IncludeCrew = true, IncludeCommunity = true, IncludeBuilder = true };
                var tasks = await taskRepo.GetTasks(query);

                var task = tasks.OrderByDescending(x => x.Created).FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.CrewId));

                IocConfig.UserContextMock.SetupGet(x => x.UserId).Returns(task.CreatedById);

                if (task == null)
                {
                    Assert.Fail("Get a task that has a crew");
                }

                await mailer.EmailSchedules(new WorkOrderTask[] { task }, "sample note");
            }
        }
    }
}
