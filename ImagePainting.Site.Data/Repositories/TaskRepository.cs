﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class TaskRepository : ITaskRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public TaskRepository(ImagePaintingContext context)
        {
            _dbContext = context;
        }
        
        public async Task<IEnumerable<WorkOrderTask>> GetTasks(TaskSearchCriteria searchCriteria)
        {
            var today = DateTime.Now.Date;
            var query = _dbContext.WorkOrderTasks.AsQueryable();

            if (searchCriteria.IsDeferred)
            {
                query = query.Where(x => x.Deferred);
            }

            if (searchCriteria.Grouping == GroupingType.Monthly)
            {
                query = query.Where(x => (x.ScheduleDate.Month + 
                (x.ScheduleDate.Year - today.Year) * 12 - today.Month) == searchCriteria.PageNumber);
            }

            if (searchCriteria.Grouping == GroupingType.Weekly)
            {
                var sunday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (7 * searchCriteria.PageNumber));
                var saturday = DateTime.Today.AddDays((6 - (int)DateTime.Today.DayOfWeek) + (7 * searchCriteria.PageNumber));

                // make sure it's the last second of the day
                saturday = saturday.AddDays(1).AddSeconds(-1);

                query = query.Where(x => (x.ScheduleDate >= sunday && x.ScheduleDate <= saturday));
            }

            if (searchCriteria.Grouping == GroupingType.Daily)
            {
                var dayOfInterest = searchCriteria.DateRange.BeginDateTime.AddDays(searchCriteria.PageNumber).Date;

                query = query.Where(x => DbFunctions.TruncateTime(x.ScheduleDate) == dayOfInterest);
            }

            if (searchCriteria.IsUpcoming.HasValue)
            {
                query = query.Where(x => x.Completed == null && x.ScheduleDate >= today);
            }

            else if (searchCriteria.IsCompleted.HasValue)
            {
                query = query.Where(x => searchCriteria.IsCompleted.Value ?  x.Completed.HasValue : x.Completed == null);
            }

            else if (searchCriteria.IsOverdued.HasValue)
            {
                query = query.Where(x => (x.Completed == null) && (x.ScheduleDate < today));
            }

            if (searchCriteria.IncludeWorkOrder.HasValue)
            {
                query = query.Include(x => x.WorkOrder);
            }

            if (!searchCriteria.CommunityIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.CommunityIds.Contains(x.WorkOrder.CommunityId.Value));
            }
            
            if (!searchCriteria.TaskSchemaIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.TaskSchemaIds.Contains(x.SchemaId));
            }

            if (!searchCriteria.CrewIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.CrewIds.Contains(x.CrewId));
            }

            if (!searchCriteria.DateRange.IsNull() && searchCriteria.Grouping == GroupingType.None)
            {
                query = query.Where(x => searchCriteria.DateRange.BeginDateTime <= x.ScheduleDate
                            && searchCriteria.DateRange.EndDateTime >= x.ScheduleDate);
            }

            if (searchCriteria.IncludeJobInfo.HasValue)
            {
                query = query.Include(x => x.WorkOrder.JobInfo);
            }

            if (searchCriteria.IncludeCommunity.HasValue)
            {
                query = query.Include(x => x.WorkOrder.Community);
            }

            if (searchCriteria.IncludeBuilder.HasValue)
            {
                query = query.Include(x => x.WorkOrder.Builder);
            }

            if (searchCriteria.IncludeCrew.HasValue)
            {
                query = query.Include(x => x.Crew);
            }

            if (!searchCriteria.TaskIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.TaskIds.Contains(x.Id));
            }

            if (searchCriteria.WorkOrderId.HasValue)
            {
                query = query.Where(x => x.WorkOrderId == searchCriteria.WorkOrderId);
            }

            if (!searchCriteria.WorkOrderName.IsNullOrEmpty())
            {
                query = query.Where(x => x.WorkOrder.Name.Contains(searchCriteria.WorkOrderName));
            }

            if (searchCriteria.IsMainTask.HasValue)
            {
                query = query.Where(x => x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0);
            }

            query = query.Where(x => !x.WorkOrder.Deleted);

            if (!searchCriteria.UserId.IsNull())
            {
                return await query.Where(x => x.CrewId == searchCriteria.UserId).ToListAsync()
                    .ContinueWith(task => Mapper.Map<List<WorkOrderTask>>(task.Result));
            }

            return await query.ToListAsync()
                .ContinueWith(task => Mapper.Map<List<WorkOrderTask>>(task.Result));
        }

        public async Task<IList<TaskView>> SearchTasks(TaskSearchCriteria searchCriteria)
        {
            var today = DateTime.Now.Date;
            var dateRange = searchCriteria.ToDateRange();
            var query = _dbContext.Set<TaskView>()
                            .AsNoTracking()
                            .Where(x => x.ScheduleDate >= dateRange.BeginDateTime && x.ScheduleDate <= dateRange.EndDateTime);

            if (searchCriteria.IsUpcoming.HasValue)
            {
                query = query.Where(x => x.Completed == null);
            }
            else if (searchCriteria.IsCompleted.HasValue)
            {
                query = query.Where(x => searchCriteria.IsCompleted.Value ? x.Completed.HasValue : x.Completed == null);
            }
            else if (searchCriteria.IsOverdued.HasValue)
            {
                query = query.Where(x => x.Completed == null);
            }

            if (!searchCriteria.CommunityIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.CommunityIds.Contains(x.CommunityId));
            }

            if (!searchCriteria.TaskSchemaIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.TaskSchemaIds.Contains(x.SchemaId));
            }

            if (!searchCriteria.CrewIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.CrewIds.Contains(x.CrewId));
            }

            if (!searchCriteria.TaskIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.TaskIds.Contains(x.Id));
            }

            if (searchCriteria.WorkOrderId.HasValue)
            {
                query = query.Where(x => x.WorkOrderId == searchCriteria.WorkOrderId);
            }

            if (!searchCriteria.WorkOrderName.IsNullOrEmpty())
            {
                query = query.Where(x => x.WorkOrderName.Contains(searchCriteria.WorkOrderName));
            }

            if (searchCriteria.IsMainTask.HasValue)
            {
                query = query.Where(x => x.IsMainTask);
            }

            if (!searchCriteria.UserId.IsNull())
            {
                return await query.Where(x => x.CrewId == searchCriteria.UserId).ToListAsync();
            }

            return await query.ToListAsync();
        }

        public Task<WorkOrderTask> GetTask(long taskId, 
            bool includeCrew = false, 
            bool includeWorkOrder = false, 
            bool includeSubTasks = false,
            bool includeCompletedBy = false,
            bool includBuilderAndCommunity = false,
            bool includeUnlockedBy = false)
        {
            var query = _dbContext.WorkOrderTasks.AsNoTracking().AsQueryable();

            if (includeCrew)
            {
                query = query.Include(x => x.Crew);
            }

            if (includeCompletedBy)
            {
                query = query.Include(x => x.CompletedBy);
            }

            if (includeUnlockedBy)
            {
                query = query.Include(x => x.UnlockedBy);
            }

            if (includeWorkOrder)
            {
                query = query.Include(x => x.WorkOrder);
            }

            if (includBuilderAndCommunity)
            {
                query = query.Include(x => x.WorkOrder.Builder).Include(x => x.WorkOrder.Community);
            }

            if (includeSubTasks)
            {
                query = query.Include(x => x.PreWalkTask)
                            .Include(x => x.CompletionWalkTask);

                if (includeCompletedBy)
                {
                    query = query.Include(x => x.PreWalkTask.CompletedBy)
                                .Include(x => x.CompletionWalkTask.CompletedBy);
                }

                if (includeUnlockedBy)
                {
                    query = query.Include(x => x.PreWalkTask.UnlockedBy)
                                .Include(x => x.CompletionWalkTask.UnlockedBy);
                }
            }

            return query.FirstOrDefaultAsync(x => x.Id == taskId)
                .ContinueWith(task => Mapper.Map<WorkOrderTask>(task.Result));
        }

        public Task<WorkOrderTask> GetMainTask(long taskId)
        {
            return _dbContext.WorkOrderTasks
                            .Include(x => x.PreWalkTask)
                            .Include(x => x.CompletionWalkTask)
                            .SingleOrDefaultAsync(
                                x => (x.Id == taskId && (x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0)) || 
                                x.PreWalkTaskId == taskId || 
                                x.CompletionWalkTaskId == taskId)
                                .ContinueWith(task => Mapper.Map<WorkOrderTask>(task.Result));
        }

        public async Task<IEnumerable<TaskSummary>> GetMainTasksForUser(long workOrderId, string user)
        {
            var userTaskIds = await _dbContext.WorkOrderTasks
                                    .Where(x => x.WorkOrderId == workOrderId && x.CrewId == user)
                                    .Select(x => x.Id)
                                    .ToListAsync();

            var mainTasks = await _dbContext.WorkOrderTasks
                                .Where(x => (userTaskIds.Contains(x.PreWalkTaskId.Value) 
                                            || userTaskIds.Contains(x.CompletionWalkTaskId.Value)
                                            || userTaskIds.Contains(x.Id))
                                    && (x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0))
                                .OrderBy(x => x.ScheduleDate)
                                .ThenBy(x => x.Name)
                                .Select(x => new TaskSummary
                                {
                                    Amount = x.Amount,
                                    Completed = x.Completed,
                                    Id = x.Id,
                                    Name = x.Name,
                                    TaskType = x.TaskType
                                })
                                .ToListAsync();

            return mainTasks;
        }

        public async Task UpdateWorkOrderTasks(params WorkOrderTask[] tasks)
        {
            if (tasks.IsNullOrEmpty())
            {
                return;
            }

            var allUpdatedTasks = new List<WorkOrderTask>();

            foreach (var task in tasks)
            {
                allUpdatedTasks.Add(task);

                if (!task.PreWalkTask.IsNull())
                {
                    allUpdatedTasks.Add(task.PreWalkTask);
                }

                if (!task.CompletionWalkTask.IsNull())
                {
                    allUpdatedTasks.Add(task.CompletionWalkTask);
                }
            }

            foreach (var task in allUpdatedTasks)
            {
                var dbTask = await _dbContext.WorkOrderTasks.FirstOrDefaultAsync(x => x.Id == task.Id);
                var entityTask = Mapper.Map<Entities.WorkOrderTask>(task);

                dbTask.MergeShallow(entityTask,
                        nameof(entityTask.Id),
                        nameof(entityTask.WorkOrder),
                        nameof(entityTask.CompletionWalkTask),
                        nameof(entityTask.PreWalkTask),
                        nameof(entityTask.CompletedBy),
                        nameof(entityTask.Crew),
                        nameof(entityTask.Created),
                        nameof(entityTask.CreatedById));

                _dbContext.Entry(dbTask).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task CreateWorkOrderTasks(params WorkOrderTask[] tasks)
        {
            if (tasks.IsNullOrEmpty())
            {
                return;
            }

            var tasksEntities = Mapper.Map<Entities.WorkOrderTask[]>(tasks);
            
            _dbContext.WorkOrderTasks.AddRange(tasksEntities);

            await _dbContext.SaveChangesAsync();

            // Assigning newly saved task ids to the tasks models
            tasksEntities.ForEach(entity =>
            {
                var task = tasks.FirstOrDefault(x => x.Name == entity.Name && x.TaskType == entity.TaskType);

                task.Id = entity.Id;
            });
        }

        public async Task DeleteWorkOrderTasks(params WorkOrderTask[] tasks)
        {
            if (tasks.IsNullOrEmpty())
            {
                return;
            }

            var tasksToRemove = _dbContext.WorkOrderTasks.Local.Where(x => tasks.Any(y => y.Id == x.Id));

            _dbContext.WorkOrderTasks.RemoveRange(tasksToRemove);

            await _dbContext.SaveChangesAsync();
        }
                
        public async Task<List<WorkOrderTask>> GetTasksByWorkOrderId(long workOrderId, bool mainTaskOnly = false, bool includeWalkTasks = false, bool includeWorkOrder = false)
        {
            var query = _dbContext.WorkOrderTasks.Where(x => x.WorkOrderId == workOrderId);

            if (mainTaskOnly)
            {
                query = query.Where(x => x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0);
            }

            if (includeWorkOrder)
            {
                query = query
                        .Include(x => x.Crew)
                        .Include(x => x.CompletedBy)
                        .Include(x => x.WorkOrder)
                        .Include(x => x.WorkOrder.JobInfo);
            }

            if (includeWalkTasks)
            {
                query = query.Include(x => x.CompletionWalkTask).Include(x => x.PreWalkTask);
            }

            return await query.OrderBy(x => x.ScheduleDate).ThenBy(x => x.Name).ToListAsync()
                .ContinueWith(task => Mapper.Map<List<WorkOrderTask>>(task.Result));
        }

        public Task<List<TaskSummary>> GetTaskSummaries(long workOrderId, bool mainTaskOnly = false, string userId = null)
        {
            var query = _dbContext.WorkOrderTasks.Where(x => x.WorkOrderId == workOrderId);

            if (mainTaskOnly)
            {
                query = query.Where(x => x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0);
            }

            if (!userId.IsNull())
            {
                query = query.Where(x => x.CrewId == userId);
            }

            return query
                    .OrderBy(x => x.ScheduleDate)
                    .ThenBy(x => x.Name)
                    .Select(x => new TaskSummary {
                        Id = x.Id,
                        Name = x.Name,
                        Amount = x.Amount,
                        Completed = x.Completed,
                        TaskType = x.TaskType,
                        Deferred = x.Deferred,
                    })
                    .ToListAsync();
        }

        public async Task<IEnumerable<KeyValuePair<DateTime, string>>> GetAssignedUserIds(DateTimeRange dateTimeRange)
        {
            var tasks = await _dbContext.WorkOrderTasks.Where(x => !x.WorkOrder.Deleted &&
                        x.CrewId != default(string)
                        && x.ScheduleDate >= dateTimeRange.BeginDateTime
                        && x.ScheduleDate <= dateTimeRange.EndDateTime)
                        .Select(x => new { x.ScheduleDate, x.CrewId })
                        .Distinct()
                        .ToListAsync();


            return tasks.Select(x => new KeyValuePair<DateTime, string>(x.ScheduleDate.Date, x.CrewId));
        }

        public Task<List<long>> GetUnCompletedTaskIdsBySchemaId(int schemaId)
        {
            return _dbContext.WorkOrderTasks.Where(x => x.SchemaId == schemaId && !x.Completed.HasValue).Select(x => x.Id).ToListAsync();
        }

        public Task<List<long>> GetTaskIdsByWorkOrderId(long workOrderId)
        {
            return _dbContext.WorkOrderTasks.Where(x => x.WorkOrderId == workOrderId)
                .Select(x => x.Id)
                .ToListAsync();
        }
    }
}
