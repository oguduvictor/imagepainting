﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using MoreLinq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class SupplyRepository : ISupplyRepository
    {
        private ImagePaintingContext _dbContext;

        public SupplyRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Supply> GetSupply(int id)
        {
            return await _dbContext.Supplies.FindAsync(id);
        }
        public async Task<List<int>> AddSupplyTypes(List<SupplyTypeSummary> supplyTypes)
        {
            List<SupplyType> dbSuppliesToAdd = new List<SupplyType>();
            List<int> supplyTypeIds = new List<int>();
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            var uniqueEntries = supplyTypes.DistinctBy(x => x.Name.ToLower() + x.supplyCategory);
            var currentSupplies = await _dbContext.SupplyType.Select(x => new SupplyTypeSummary
            {
                Name = x.Name.ToLower(),
                supplyCategory = x.SupplyCategory
            }).ToListAsync();

            var suppliesToAdd = uniqueEntries.Except(currentSupplies);
            var maximumId = await _dbContext.SupplyType.Select(x =>x.Id).MaxAsync();
            foreach (var type in suppliesToAdd)
            {
                dbSuppliesToAdd.Add(new SupplyType
                {
                    Id = maximumId + 1,
                    Name = myTI.ToTitleCase(type.Name),
                    SupplyCategory = type.supplyCategory
                });
                maximumId++;
            }

            _dbContext.SupplyType.AddRange(dbSuppliesToAdd);

            _dbContext.SaveChanges();

            var ids = new List<int>();

            supplyTypes.ForEach(x =>
            {
                ids.Add((_dbContext.SupplyType.
                    FirstOrDefault(y => y.Name.ToLower() == x.Name.ToLower() && y.SupplyCategory == x.supplyCategory))?.Id ?? 0);
            });

            return ids;

        }

        public Task<List<Supply>> GetSupplies(int? builderId = 0, long? storeId = 0)
        {
            return _dbContext.Supplies.Where(x => x.BuilderId == builderId && x.PaintStoreId == storeId)
                .Include(x => x.SupplyType).ToListAsync();
        }

        public async Task<long> GetAnyPaintStore(int? builderId)
        {
            return (await _dbContext.Supplies
                .Join(_dbContext.Stores, x => x.PaintStoreId, y => y.Id, (x, y) => new { Supplies = x, Stores = y })
                .FirstOrDefaultAsync(Supplies_Stores => Supplies_Stores.Supplies.BuilderId == builderId 
                    && !Supplies_Stores.Stores.Deleted))
                ?.Supplies.PaintStoreId ?? 0;
        }

        public async Task<IEnumerable<Supply>> GetSupplies(int builderId, long storeId)
        {
            var supplyTypes = await _dbContext.SupplyType.ToListAsync();
            var supplies = new List<Supply>();
            supplyTypes.ForEach(x =>
            {
                supplies.Add(new Supply
                {
                    Cost = 0,
                    BuilderId = builderId,
                    Name = x.Name,
                    PaintStoreId = storeId,
                    SupplyTypeId = x.Id
                });
            });
            return supplies.AsEnumerable();
       }

        public async Task<List<Supply>> GetSupplies(int builderId, SupplyCategory category, long? storeId = 0)
        {
            return (storeId == 0) ? await _dbContext.Supplies.Include(x => x.SupplyType).Where(x => x.BuilderId == builderId && x.SupplyType.SupplyCategory == category).ToListAsync()
                : await _dbContext.Supplies.Include(x => x.SupplyType).Where(x => x.BuilderId == builderId && x.SupplyType.SupplyCategory == category && x.PaintStoreId == storeId).ToListAsync();
        }

        public async Task<List<long>> GetPaintStoreIdsByBuilder(int builderId)
        {
            return await _dbContext.Supplies
                .Where(x => x.BuilderId == builderId).Select(x => x.PaintStoreId).Distinct().ToListAsync();
        }

        public async Task DeleteSuppliesByStoreId(long paintStoreId)
        {
            var suppliesToRemove = _dbContext.Supplies.Where(x => x.PaintStoreId == paintStoreId).ToList();

            _dbContext.Supplies.RemoveRange(suppliesToRemove);

            await _dbContext.SaveChangesAsync();

        }

        public async Task Create(IEnumerable<Supply> supplies)
        {
            _dbContext.Supplies.AddRange(supplies);

            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(IEnumerable<Supply> supplies)
        {
            foreach(var dbSupply in supplies)
            {
                _dbContext.Entry(dbSupply).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(IEnumerable<Supply> supplies)
        {
            _dbContext.Supplies.RemoveRange(supplies);

            await _dbContext.SaveChangesAsync();
        }

        public Task<List<SupplyType>> GetSupplyTypes(SupplyCategory category = SupplyCategory.All)
        {
            var query = _dbContext.SupplyType.AsQueryable();

            if (category == SupplyCategory.All)
            {
                return query.ToListAsync();
            }
            return query.Where(x => x.SupplyCategory == category).ToListAsync();
        }

        public Task<List<SupplyType>> GetSupplyTypes(IEnumerable<int> ids = null)
        {
            var query = _dbContext.SupplyType.AsQueryable();

            if (ids.IsNullOrEmpty())
            {
                var task = query.ToListAsync();
                task.Wait();

                return task;
            }

            return query.Where(x => ids.Contains(x.Id)).ToListAsync();
        }
        
        public Task<long?> GetDefaultStoreId(int builderId)
        {
            return _dbContext.Builders.Where(x => x.Id == builderId).Select(x => x.DefaultStoreId).SingleOrDefaultAsync();
        }
    }
}
