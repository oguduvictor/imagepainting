﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class UserInfoRepository : IUserInfoRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public UserInfoRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;

            // Hack: to stop EF from using UserInfo attribute validations
            // TODO: remove when UserInfo is fixed
            _dbContext.Configuration.ValidateOnSaveEnabled = false;
        }
        
        public Task<List<UserInfo>> GetUsersAsync(
            bool isSummary = true,
            bool includeRole = false,
            bool includeInActive = false,
            bool includeDeleted = false)
        {
            var query = _dbContext.UserInfos.AsQueryable();

            if (!includeDeleted)
            {
                query = query.Where(x => !x.Deleted);
            }

            if (!includeInActive)
            {
                query = query.Where(x => x.Active);
            }

            if (includeRole)
            {
                query = query.Include(x => x.Roles);
            }

            if (!isSummary)
            {
                return query.OrderBy(y => y.FirstName)
                            .ThenBy(y => y.LastName).ToListAsync();
            }

            return query
                    .Select(x => new { x.Id, x.FirstName, x.LastName, x.Email, x.Active, x.Roles })
                    .OrderBy(y => y.FirstName)
                    .ThenBy(y => y.LastName)
                    .ToListAsync()
                    .ContinueWith(x =>
                    {
                        return x.Result.Select(user => new UserInfo
                        {
                            Id = user.Id,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            Active = user.Active,
                            Roles = user.Roles
                        }).ToList();
                    });
        }

        public Task<UserInfo> GetUserAsync(string userId, bool includeRoles = false)
        {
            var query = _dbContext.UserInfos.AsQueryable();

            if (includeRoles)
            {
                query = query.Include(x => x.Roles);
            }

            return query.FirstOrDefaultAsync(x => x.Id == userId);
        }

        public async Task CreateUserInfoAsync(UserInfo userInfo)
        {
            _dbContext.UserInfos.Add(userInfo);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUserInfoAsync(UserInfo userInfo)
        {
            _dbContext.Entry(userInfo).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Role>> GetAllRolesAsync()
        {
            return await _dbContext.Roles.ToListAsync();
        }
        
        public async Task<string> GetUserIdForWalkTask()
        {
            // TODO: implement with a configuration setting 
            var adminId = ((int)UserRole.Administrator).ToString();

            var userId = await _dbContext.UserInfos
                            .Where(x => x.Roles.Any(t => t.Id == adminId))
                            .Select(x => x.Id)
                            .FirstOrDefaultAsync();

            if (userId.IsNullOrEmpty())
            {
                userId = await _dbContext.UserInfos.Select(x => x.Id).FirstOrDefaultAsync();
            }

            return userId;
        }

        public async Task<IEnumerable<UserInfo>> GetUsersInRoleAsync(UserRole userRole, bool includeActiveAndDeleteUsers = false)
        {
            var role = (int)userRole;
            var query = _dbContext.UserInfos
                .Where(x => x.Roles.Any(r => r.Id == role.ToString()));
                

            if (!includeActiveAndDeleteUsers)
            {
                query = query.Where(u => u.Active && !u.Deleted);
            }

            return await query.OrderBy(y => y.FirstName).ThenBy(y => y.LastName).ToListAsync();
        }

        public async Task<UserInfo> GetUserByEmailAsync(string userEmail)
        {
            return await _dbContext.UserInfos.FirstOrDefaultAsync(x => x.Email == userEmail);
        }

        public async Task DeleteUserRolesAsync(string userId)
        {
            var userInfo = await _dbContext
                            .UserInfos.Include(x => x.Roles)
                            .FirstOrDefaultAsync(x => x.Id == userId);

            if (userInfo.Roles.IsNullOrEmpty())
            {
                return;
            }

            // remove roles starting from the last
            for (int i = userInfo.Roles.Count -1; i >= 0; i--)
            {
                var role = userInfo.Roles.ElementAt(i);
                userInfo.Roles.Remove(role);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> TryDeleteUserInfoAsync(string userId)
        {
            var userInfo = _dbContext.UserInfos.Local.FirstOrDefault(x => x.Id == userId);

            if (userInfo == null)
            {
                new UserInfo { Id = userId };
                _dbContext.UserInfos.Attach(userInfo);
            }

            _dbContext.UserInfos.Remove(userInfo);

            try
            {
                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task DeleteUserLoginAsync(string userId)
        {
            var sqlCommand = "Delete [AspNetUserClaims] where UserId = {0}; ";
            sqlCommand += "Delete [AspNetUserLogins] where UserId = {0}; ";
            sqlCommand += "Delete [AspNetUserRoles] where UserId = {0}; ";
            sqlCommand += "Delete [AspNetUsers] where Id = {0};";

            await _dbContext.Database.ExecuteSqlCommandAsync(sqlCommand, userId);
        }

        public async Task<IEnumerable<UserInfo>> GetUsersAsync(IEnumerable<string> userIds)
        {
            return await _dbContext.UserInfos.Where(x => userIds.Contains(x.Id)).ToListAsync();
        }
    }
}
