﻿using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System;
using ImagePainting.Site.Common.Interfaces.Repository;

namespace ImagePainting.Site.Data.Repositories
{
    internal class PlanRepository : IPlanRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public PlanRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<PlanSummary>> GetPlanSummaries(
            bool includeCommunities = false,
            bool includeBuilder = false,
            int? builderId = null,
            bool includeDeleted = false
            )
        {
            var query = _dbContext.Plans.AsQueryable();
            
            if (builderId.HasValue)
            {
                query = query.Where(x => x.BuilderId == builderId);
            
                var task = query.Select(x => new { Id = x.Id, Name = x.Name }).ToListAsync();

                task.Wait();

                return Task.FromResult(task.Result.Select(x => new PlanSummary { Id = x.Id, Name = x.Name }).ToList());
            }

            if (includeCommunities)
            {
                query = query.Include(x => x.Communities);
            }

            if (includeBuilder)
            {
                query = query.Include(x => x.Builder);
            }

            if (!includeDeleted)
            {
                query = query.Where(x => x.Deleted == includeDeleted);
            }

            return query.Select(x => new PlanSummary
            {
                Id = x.Id,
                Name = x.Name,
                Builder = x.Builder,
                Stories = x.Stories
            }).ToListAsync();
        }

        public async Task<Plan> GetPlan(int planId, bool includeElevations = false, bool includeCommunities = false)
        {
            var query = _dbContext.Plans.AsQueryable();

            if (includeElevations)
            {
                query = query.Include(x => x.Elevations);
            }

            if (includeCommunities)
            {
                query = query.Include(x => x.Communities);
            }

            return await query.FirstOrDefaultAsync(x => x.Id == planId);
        }

        public async Task<Plan> CreatePlan(Plan plan)
        {
            _dbContext.Plans.Add(plan);

            await _dbContext.SaveChangesAsync();

            return plan;
        }

        public async Task<Plan> UpdatePlan(Plan plan)
        {
            _dbContext.Entry(plan).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
            return plan;
        }
        
    }
}
