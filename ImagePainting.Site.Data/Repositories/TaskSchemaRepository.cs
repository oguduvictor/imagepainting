﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class TaskSchemaRepository : ITaskSchemaRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public TaskSchemaRepository(ImagePaintingContext context)
        {
            _dbContext = context;
        }

        public Task<WorkOrderTaskSchema> GetTaskSchema(int schemaId)
        {
            return _dbContext.WorkOrderTaskSchemas.AsNoTracking().FirstOrDefaultAsync(x => x.Id == schemaId);
        }

        public Task<List<WorkOrderTaskSchema>> GetTaskSchemas(bool isSummary = true, bool mainTaskOnly = false)
        {
            var query = _dbContext.WorkOrderTaskSchemas.Where(x => !x.Deleted);

            if (mainTaskOnly)
            {
                query = query.Where(x => x.PreWalkTaskId > 0 || x.CompletionWalkTaskId > 0);
            }

            if (isSummary)
            {
                return query.Select(x => new { x.Id, x.Name, x.PreWalkTaskId, x.CompletionWalkTaskId })
                    .ToListAsync()
                    .ContinueWith(items => {
                        return items.Result.Select(x => new WorkOrderTaskSchema {
                            Id = x.Id,
                            Name = x.Name,
                            PreWalkTaskId = x.PreWalkTaskId,
                            CompletionWalkTaskId = x.CompletionWalkTaskId
                        }).ToList();
                    });
            }

            return query.ToListAsync();
        }

        public async Task<WorkOrderTaskSchema> CreateTaskSchema(WorkOrderTaskSchema schema)
        {
            _dbContext.WorkOrderTaskSchemas.Add(schema);

            await _dbContext.SaveChangesAsync();

            return schema;
        }

        public async Task<WorkOrderTaskSchema> UpdateTaskSchema(WorkOrderTaskSchema schema)
        {
            var dbTaskSchema = _dbContext.WorkOrderTaskSchemas.Local.FirstOrDefault(x => x.Id == schema.Id);

            if (dbTaskSchema == null)
            {
                dbTaskSchema = schema;
                _dbContext.WorkOrderTaskSchemas.Attach(dbTaskSchema);
            }
            else
            {
                dbTaskSchema.MergeShallow(schema, nameof(schema.TaskItems));
            }

            _dbContext.Entry(schema).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return schema;
        }
    }
}
