﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class WorkOrderRepository : IWorkOrderRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public WorkOrderRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<WorkOrder> CreateWorkOrder(WorkOrder workOrder)
        {
            var entityWorkOrder = Mapper.Map<Entities.WorkOrder>(workOrder);

            _dbContext.WorkOrders.Add(entityWorkOrder);

            await _dbContext.SaveChangesAsync();

            // This is to attach newly created id to the workorder from the domain
            workOrder.Id = entityWorkOrder.Id;

            return workOrder;
        }

        public async Task<IEnumerable<WorkOrder>> GetWorkOrders(WorkOrderQuery summary)
        {
            var query = _dbContext.WorkOrders
                            .Include(x => x.Builder)
                            .Include(x => x.Community)
                            .Include(x => x.CreatedBy)
                            .Include(x => x.JobInfo)
                            .Where(x => x.Deleted == false);

            var searchString = summary.SearchString;
            
            if (!string.IsNullOrEmpty(searchString))
            {
                switch (summary.SearchOption)
                {
                    case WorkOrderSearchOption.WorkOrderName:
                        query = query.Where(x => x.Name.Contains(searchString));
                        break;
                    case WorkOrderSearchOption.Builder:
                        query = query.Where(x => x.Builder.Abbreviation.Contains(searchString) || x.Builder.Name.Contains(searchString));
                        break;
                    case WorkOrderSearchOption.Community:
                        query = query.Where(x => x.Community.Abbreviation.Contains(searchString) || x.Community.Name.Contains(searchString));
                        break;
                    case WorkOrderSearchOption.Address:
                        query = query.Where(x => x.Address.Contains(searchString));
                        break;
                    case WorkOrderSearchOption.LotAndBlock:
                        if (summary.Lot > 0)
                        {
                            query = query.Where(x => x.Lot == summary.Lot);
                        }

                        if (!summary.Block.IsNullOrEmpty())
                        {
                            query = query.Where(x => x.Block.Contains(summary.Block));
                        }
                        
                        break;
                    case WorkOrderSearchOption.ExteriorBaseColor:
                        query = query.Where(x => x.JobInfo.ExteriorColors.Contains(searchString));
                        break;
                    case WorkOrderSearchOption.InteriorBaseColor:
                        query = query.Where(x => x.JobInfo.InteriorColors.Contains(searchString));
                        break;
                    default:
                        throw new Exception("Search option is not selected");
                }
            }

            if (summary.ByName)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
            }
            else if (summary.ByBuilder)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Builder.Abbreviation) : query.OrderByDescending(x => x.Builder.Abbreviation);
            }
            else if (summary.ByCommunity)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Community.Abbreviation) : query.OrderByDescending(x => x.Community.Abbreviation);
            }
            else if (summary.ByStatus)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Completed) : query.OrderByDescending(x => x.Completed);
            }
            else if (summary.ByLastModified)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Modified) : query.OrderByDescending(x => x.Modified);
            }
            else if (summary.ByCreated)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.CreatedBy.FirstName).ThenBy(x => x.CreatedBy.LastName)
                    : query.OrderByDescending(x => x.CreatedBy.FirstName).ThenByDescending(x => x.CreatedBy.LastName);
            }
            else if (summary.ByAddress)
            {
                query = summary.IsAscending ? query.OrderBy(x => x.Address) : query.OrderByDescending(x => x.Address);
            }

            query = query.Skip(summary.PageNumber * summary.ResultsCount).Take(summary.ResultsCount);

            return await query.ToListAsync()
                .ContinueWith(task => Mapper.Map<IEnumerable<WorkOrder>>(task.Result));
        }

        public Task<bool> WorkOrderExists(long workOrderId, string name)
        {
            return _dbContext.WorkOrders.AnyAsync(x => !x.Deleted && x.Id != workOrderId && x.Name == name);
        }

        public Task<long> GetTotalWorkOrderCount(string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return _dbContext.WorkOrders.LongCountAsync(x => !x.Deleted);
            }
            else
            {
                return _dbContext.WorkOrders
                    .Include(x => x.Builder)
                    .Include(x => x.Community)
                    .Include(x => x.CreatedBy)
                    .LongCountAsync(x => !x.Deleted && (x.Name.Contains(searchString)
                    || x.Builder.Name.Contains(searchString)
                    || x.Community.Name.Contains(searchString)));
            }
        }

        public async Task<WorkOrder> GetWorkOrderSummary(long id)
        {
            return await _dbContext.WorkOrders.FindAsync(id)
                .ContinueWith(task => Mapper.Map<WorkOrder>(task.Result));
        }

        public Task<WorkOrder> GetWorkOrder(
            long workOrderId,
            bool includeBuilder = false,
            bool includeCommunities = false,
            bool includeJobInfo = false,
            bool includeExtraJobInfo = false,
            bool includeCreatedBy = false)
        {
            var query = _dbContext.WorkOrders.AsQueryable();

            if (includeBuilder)
            {
                query = query.Include(x => x.Builder);
            }

            if (includeCommunities)
            {
                query = query.Include(x => x.Community);
            }
            
            if (includeJobInfo)
            {
                query = query.Include(x => x.JobInfo);
            }

            if (includeExtraJobInfo)
            {
                query = query.Include(x => x.ExtraJobInfo);
            }

            if (includeCreatedBy)
            {
                query = query.Include(x => x.CreatedBy);
            }

            return query.FirstOrDefaultAsync(x => x.Id == workOrderId)
                .ContinueWith(task => Mapper.Map<WorkOrder>(task.Result));
        }
        
        public async Task<WorkOrder> UpdateWorkOrder(WorkOrder workOrder)
        {
            var entityWorkorder = Mapper.Map<Entities.WorkOrder>(workOrder);
            var dbWorkorder = await _dbContext.WorkOrders.FirstOrDefaultAsync(x => x.Id == workOrder.Id);

            dbWorkorder.MergeShallow(entityWorkorder,
                nameof(entityWorkorder.Id),
                nameof(entityWorkorder.Created),
                nameof(entityWorkorder.CreatedBy),
                nameof(entityWorkorder.CreatedById),
                nameof(entityWorkorder.ExtraJobInfo),
                nameof(entityWorkorder.JobInfo),
                nameof(entityWorkorder.Community),
                nameof(entityWorkorder.Builder));

            _dbContext.Entry(dbWorkorder).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return workOrder;
        }

        public async Task<PagedResult<WorkOrderView>> SearchWorkOrdersView(WorkOrderQuery workOrderQuery)
        {
            var query = _dbContext.Set<WorkOrderView>().AsNoTracking().AsQueryable();
            var searchString = workOrderQuery.SearchString;
            var result = new PagedResult<WorkOrderView> { PageNumber = workOrderQuery.PageNumber, PageSize = workOrderQuery.ResultsCount };

            if (!string.IsNullOrEmpty(searchString))
            {
                switch (workOrderQuery.SearchOption)
                {
                    case WorkOrderSearchOption.WorkOrderName:
                        query = query.Where(x => x.Name.Contains(searchString));
                        break;

                    case WorkOrderSearchOption.Builder:
                        query = query.Where(x => x.BuilderAbbreviation.Contains(searchString) || x.BuilderName.Contains(searchString));
                        break;

                    case WorkOrderSearchOption.Community:
                        query = query.Where(x => x.CommunityAbbreviation.Contains(searchString) || x.CommunityName.Contains(searchString));
                        break;

                    case WorkOrderSearchOption.Address:
                        query = query.Where(x => x.Address.Contains(searchString));
                        break;

                    case WorkOrderSearchOption.LotAndBlock:
                        if (workOrderQuery.Lot > 0)
                        {
                            query = query.Where(x => x.Lot == workOrderQuery.Lot);
                        }

                        if (!workOrderQuery.Block.IsNullOrEmpty())
                        {
                            query = query.Where(x => x.Block.Contains(workOrderQuery.Block));
                        }

                        break;

                    case WorkOrderSearchOption.ExteriorBaseColor:
                        var infoIdsForExt = await _dbContext.JobInfos.Where(x => x.ExteriorColors.Contains(searchString)).Select(x => x.Id).ToListAsync();

                        if (infoIdsForExt.Any())
                        {
                            query = query.Where(x => infoIdsForExt.Contains(x.JobInfoId.Value));
                        }
                        break;

                    case WorkOrderSearchOption.InteriorBaseColor:
                        var infoIdsForInt = await _dbContext.JobInfos.Where(x => x.InteriorColors.Contains(searchString)).Select(x => x.Id).ToListAsync();

                        if (infoIdsForInt.Any())
                        {
                            query = query.Where(x => infoIdsForInt.Contains(x.JobInfoId.Value));
                        }
                        break;

                    default:
                        throw new Exception("Search option is not selected");
                }
            }

            result.TotalCount = await query.CountAsync();

            if (workOrderQuery.ByName)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
            }
            else if (workOrderQuery.ByBuilder)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.BuilderAbbreviation) : query.OrderByDescending(x => x.BuilderAbbreviation);
            }
            else if (workOrderQuery.ByCommunity)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.CommunityAbbreviation) : query.OrderByDescending(x => x.CommunityAbbreviation);
            }
            else if (workOrderQuery.ByStatus)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.Completed) : query.OrderByDescending(x => x.Completed);
            }
            else if (workOrderQuery.ByLastModified)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.Modified) : query.OrderByDescending(x => x.Modified);
            }
            else if (workOrderQuery.ByCreated)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.CreatedBy)
                    : query.OrderByDescending(x => x.CreatedBy).ThenByDescending(x => x.CreatedBy);
            }
            else if (workOrderQuery.ByAddress)
            {
                query = workOrderQuery.IsAscending ? query.OrderBy(x => x.Address) : query.OrderByDescending(x => x.Address);
            }

            query = query.Skip(workOrderQuery.PageNumber * workOrderQuery.ResultsCount).Take(workOrderQuery.ResultsCount);

            result.Items = await query.ToListAsync();

            return result;
        }

        #region JobInfo
        public async Task<JobInfo> CreateJobInfo(long workorderId, JobInfo jobInfo, string modifiedById)
        {
            var entityJobInfo = Mapper.Map<Entities.JobInfo>(jobInfo);
            var workorder = await _dbContext.WorkOrders.FirstOrDefaultAsync(x => x.Id == workorderId);

            _dbContext.JobInfos.Add(entityJobInfo);

            workorder.JobInfoId = entityJobInfo.Id;
            workorder.JobInfoConfirmed = jobInfo.Confirmed;

            SetWorkOrderModified(workorder, jobInfo.Modified, modifiedById);

            await _dbContext.SaveChangesAsync();

            // This is done so that the returned jobinfo has id set
            jobInfo.Id = entityJobInfo.Id;

            return jobInfo;
        }

        public async Task<JobInfo> UpdateJobInfo(JobInfo jobInfo, long? workOrderId = null, string modifiedById = null)
        {
            var entityJobInfo = Mapper.Map<Entities.JobInfo>(jobInfo);
            var dbJobInfo = await _dbContext.JobInfos.FirstOrDefaultAsync(x => x.Id == jobInfo.Id);
            
            dbJobInfo.MergeShallow(entityJobInfo,
                nameof(entityJobInfo.Id),
                nameof(entityJobInfo.Created),
                nameof(entityJobInfo.Plan),
                nameof(entityJobInfo.Elevation));

            _dbContext.Entry(dbJobInfo).State = EntityState.Modified;

            if (workOrderId.HasValue && !modifiedById.IsNullOrEmpty())
            {
                var workorder = await _dbContext.WorkOrders.FirstOrDefaultAsync(x => x.Id == workOrderId);

                workorder.JobInfoConfirmed = jobInfo.Confirmed;
                SetWorkOrderModified(workorder, jobInfo.Modified, modifiedById);
            }

            await _dbContext.SaveChangesAsync();

            return jobInfo;
        }

        public JobInfo GetJobInfo(long id)
        {
            return Mapper.Map<JobInfo>(_dbContext.JobInfos.Find(id));
        }

        #endregion
        
        #region ExtraJobInfo
        public ExtraJobInfo GetExtraJobInfo(long id)
        {
            return Mapper.Map<ExtraJobInfo>(_dbContext.ExtraJobInfos.Find(id));
        }

        public Task<ExtraJobInfo> GetExtraJobInfoByWorkOrderId(long workOrderId)
        {
            return _dbContext.WorkOrders
                    .Where(x => x.Id == workOrderId)
                    .Select(x => x.ExtraJobInfo)
                    .FirstOrDefaultAsync()
                    .ContinueWith(task => Mapper.Map<ExtraJobInfo>(task.Result));
        }

        public async Task<ExtraJobInfo> CreateExtraJobInfo(long workorderId, ExtraJobInfo extraJobInfo)
        {
            var entityExtraJob = Mapper.Map<Entities.ExtraJobInfo>(extraJobInfo);
            var workorder = await _dbContext.WorkOrders.FirstOrDefaultAsync(x => x.Id == workorderId);

            _dbContext.ExtraJobInfos.Add(entityExtraJob);

            workorder.ExtraJobInfoId = entityExtraJob.Id;

            SetWorkOrderModified(workorder, extraJobInfo.Modified, extraJobInfo.ModifiedById);

            await _dbContext.SaveChangesAsync();

            // This is done so that the returned extrajob has id set
            extraJobInfo.Id = entityExtraJob.Id;

            return extraJobInfo;
        }
        
        public async Task<ExtraJobInfo> UpdateExtraJobInfo(ExtraJobInfo extraJobInfo, long? workOrderId = null)
        {
            var entityExtraJob = Mapper.Map<Entities.ExtraJobInfo>(extraJobInfo);
            var dbExtraJobInfo = await _dbContext.ExtraJobInfos.FirstOrDefaultAsync(x => x.Id == extraJobInfo.Id);

            dbExtraJobInfo.MergeShallow(entityExtraJob,
                nameof(entityExtraJob.Id),
                nameof(entityExtraJob.Created),
                nameof(entityExtraJob.CreatedById));

            _dbContext.Entry(dbExtraJobInfo).State = EntityState.Modified;

            if (workOrderId.HasValue)
            {
                var workorder = await _dbContext.WorkOrders.FirstOrDefaultAsync(x => x.Id == workOrderId);

                SetWorkOrderModified(workorder, extraJobInfo.Modified, extraJobInfo.ModifiedById);
            }

            await _dbContext.SaveChangesAsync();

            return extraJobInfo;
        }
        #endregion

        public Task<List<string>> GetSuperintendents()
        {
            var superintendents = _dbContext.WorkOrders
                .Select(x => x.SuperIntendent ?? SiteContants.NO_SUPERINTENDENT)
                .Distinct();

            return superintendents.ToListAsync();
        }

        public Task<List<string>> GetCustomerReps()
        {
            var customerReps = _dbContext.WorkOrders
                .Select(x => x.CustomerRep)
                .Distinct();

            return customerReps.ToListAsync();
        }

        /// <summary>
        /// This is used for JobInfo and ExtraJobInfo whenever there is persistence of data
        /// </summary>
        /// <param name="workOrder"></param>
        /// <param name="modified"></param>
        /// <param name="modifiedById"></param>
        private void SetWorkOrderModified(Entities.WorkOrder workOrder, DateTime modified, string modifiedById)
        {
            workOrder.Modified = modified;
            workOrder.ModifiedById = modifiedById;
        }
    }
}
