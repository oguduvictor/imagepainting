﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class PaintStoreRepository : IPaintStoreRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public PaintStoreRepository(ImagePaintingContext context)
        {
            _dbContext = context;
        }

        public async Task<Store> GetStore(long id, bool includeLocations = false)
        {
            var query = _dbContext.Stores.AsQueryable();

            if (includeLocations)
            {
                query = query.Include(x => x.Locations);
            }

            return await query.FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<List<Store>> GetStores(bool isSummary = false, bool includeLocation = false, bool includeDeleted = false)
        {
            var query = _dbContext.Stores.AsQueryable();
            if (!includeDeleted)
            {
                query = query.Where(x => !x.Deleted).AsQueryable();
            }

            if (isSummary)
            {
                var task = query.Select(x => new { Id = x.Id, StoreName = x.StoreName }).ToListAsync();

                task.Wait();

                return Task.FromResult(task.Result.Select(x => new Store { Id = x.Id, StoreName = x.StoreName }).ToList());
            }

            if (includeLocation)
            {
                query = query.Include(x => x.Locations);
            }

            return query.ToListAsync();
        }

        public async Task<Store> CreateStore(Store store)
        {
            _dbContext.Stores.Add(store);
            await _dbContext.SaveChangesAsync();

            return store;
        }

        public async Task<StoreLocation> SaveStoreLocation(StoreLocation location)
        {
            _dbContext.StoreLocations.Add(location);
            await _dbContext.SaveChangesAsync();

            return location;
        }

        public async Task<Store> UpdateStore(Store store, StoreLocation location = null)
        {
            _dbContext.Entry(store).State = EntityState.Modified;

            if (!location.IsNull())
            {
                _dbContext.Entry(location).State = EntityState.Modified;
            }

            await _dbContext.SaveChangesAsync();

            return store;
        }

        public async Task<StoreLocation> GetStoreLocation(long id)
        {
            return await _dbContext.StoreLocations.FindAsync(id);
        }

        public Task<IEnumerable<StoreLocation>> GetStoreLocations(long? storeId, bool includeDeleted = false)
        {
            var query = _dbContext.StoreLocations.AsQueryable();

            if (storeId.HasValue)
            {
                query = query.Where(x => x.StoreId == storeId);
            }

            if (!includeDeleted)
            {
                query = query.Where(x => !x.Deleted);
            }

            var task = query.Select(x => new
            {
                Id = x.Id,
                City = x.StoreCity,
                ZipCode = x.StoreZipCode,
                Address = x.StoreAddress,
                Email = x.StoreContactEmail,
                LocationId = x.LocationId
                
            }).ToListAsync();

            return Task.FromResult(task.Result.Select(x => new StoreLocation
            {
                Id = x.Id,
                StoreCity = x.City,
                StoreAddress = x.Address,
                StoreZipCode = x.ZipCode,
                StoreContactEmail = x.Email,
                LocationId = x.LocationId
            }));
        }

        public async Task UpdateLocation(StoreLocation location)
        {
            _dbContext.Entry(location).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }
    }
}
