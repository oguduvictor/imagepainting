﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class PurchaseOrderRepository : IPurchaseOrderRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public PurchaseOrderRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Create(PurchaseOrder order)
        {
            var entityPO = Mapper.Map<Entities.PurchaseOrder>(order);

            _dbContext.PurchaseOrders.Add(entityPO);

            await _dbContext.SaveChangesAsync();

            // Sets id of the PO since the PO sent is different from the one saved
            order.Id = entityPO.Id;
        }

        public async Task Update(PurchaseOrder order)
        {
            var dbPurchaseOrder = await _dbContext.PurchaseOrders.FirstOrDefaultAsync(x => x.Id == order.Id);

            dbPurchaseOrder.MergeShallow(Mapper.Map<Entities.PurchaseOrder>(order),
                nameof(Entities.PurchaseOrder.Id),
                nameof(Entities.PurchaseOrder.PurchaseOrderItems));

            _dbContext.Entry(dbPurchaseOrder).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(List<PurchaseOrderItem> items, long orderId)
        {
            var itemIds = items.Select(y => y.Id);

            var itemsToUpdate = items.Where(x => x.Id > 0);
            var itemsToDelete = _dbContext.PurchaseOrderItems.Where(x => !itemIds.Contains(x.Id) && x.PurchaseOrderId == orderId).ToList();
            var itemsToAdd = items.Where(x => x.Id == 0);

            foreach (var item in itemsToUpdate)
            {
                var entityItem = Mapper.Map<Entities.PurchaseOrderItem>(item);
                var dbItem = _dbContext.PurchaseOrderItems.Find(item.Id);
                dbItem.MergeShallow(entityItem, nameof(entityItem.Id));
                _dbContext.Entry(dbItem).State = EntityState.Modified;
            }

            _dbContext.PurchaseOrderItems.RemoveRange(itemsToDelete);

            _dbContext.PurchaseOrderItems.AddRange(itemsToAdd.Select(x => new Entities.PurchaseOrderItem
            {
                Color = x.Color,
                Name = x.Name,
                PurchaseOrderId = x.PurchaseOrderId,
                Quantity = x.Quantity,
                UnitPrice = x.UnitPrice,
                SupplyType = x.SupplyType
            }));

            await _dbContext.SaveChangesAsync();
        }

        public async Task<PurchaseOrder> Get(long id, bool includeLocation = false, bool includeTask = false, 
            bool includeWorkOrder = false, bool includeItems = false)
        {
            var query = _dbContext.PurchaseOrders.AsQueryable();

            if (includeLocation)
            {
                query = query.Include(x => x.PaintStoreLocation);
            }

            if (includeTask)
            {
                query = query.Include(x => x.Task);
            }

            if (includeWorkOrder)
            {
                query = query.Include(x => x.WorkOrder)
                    .Include(x => x.WorkOrder.Builder)
                    .Include(x => x.WorkOrder.Community)
                    .Include(x =>x.WorkOrder.JobInfo);
            }

            if (includeItems)
            {
                query = query.Include(x => x.PurchaseOrderItems);
            }

            return await query.FirstOrDefaultAsync(x => x.Id == id).ContinueWith(task =>
            {
                return Mapper.Map<PurchaseOrder>(task.Result);
            });
        }

        public Task<List<PurchaseOrder>> GetAll(long? taskId = null, long? workOrderId = null,
            bool includeStoreLocation = false, bool includeItems = false, bool includeTask = false, bool? emailed = null)
        {
            var query = _dbContext.PurchaseOrders.AsQueryable();

            if (taskId.HasValue)
            {
                query = query.Where(x => x.TaskId == taskId);
            }

            if (workOrderId.HasValue)
            {
                query = query.Where(x => x.WorkOrderId == workOrderId);
            }

            if (includeStoreLocation)
            {
                query = query.Include(x => x.PaintStoreLocation);
            }

            if (includeTask)
            {
                query = query.Include(x => x.Task);
            }

            if (includeItems)
            {
                query = query.Include(x => x.PurchaseOrderItems);
            }

            if (emailed.HasValue)
            {
                query = query.Where(x => x.EmailSent.HasValue == emailed);
            }

            return query.ToListAsync().ContinueWith(task => Mapper.Map<List<PurchaseOrder>>(task.Result));
        }

        public Task<List<PurchaseOrderSummary>> GetEmailedPurchaseOrders(params long[] taskIds)
        {
            return _dbContext.PurchaseOrders
                        .Where(x => taskIds.Contains(x.TaskId) && x.EmailSent != null)
                        .OrderByDescending(x => x.EmailSent)
                        .Select(x => new PurchaseOrderSummary { Id = x.Id, TaskId = x.TaskId, DeliveryInstructions = x.DeliveryInstructions })
                        .ToListAsync();
        }

        public async Task DeletePurchaseOrders(params long[] ids)
        {
            var po_items = await _dbContext.PurchaseOrderItems.Where(x => ids.Contains(x.PurchaseOrderId)).ToListAsync();
            var po = await _dbContext.PurchaseOrders.Where(x => ids.Contains(x.Id)).ToListAsync();

            _dbContext.PurchaseOrderItems.RemoveRange(po_items);

            _dbContext.PurchaseOrders.RemoveRange(po);

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeletePurchaseOrdersByTaskIds(params long[] taskIds)
        {
            var poIds = await _dbContext.PurchaseOrders
                            .Where(x => taskIds.Contains(x.TaskId))
                            .Select(x => x.Id)
                            .ToArrayAsync();

            await DeletePurchaseOrders(poIds);
        }

        public async Task DeleteUnsentPurchaseOrdersByStoreId(long paintStoreId)
        {
            var paintStoreLocationIds = _dbContext.StoreLocations
                .Where(x => x.StoreId == paintStoreId).Select(x=>x.Id).ToList();

            var pos = _dbContext.PurchaseOrders.Where(x => paintStoreLocationIds.Contains(x.PaintStoreLocationId)
                && !x.EmailSent.HasValue).ToList();

            var pos_ids = pos.Select(x => x.Id).ToList();
            var pos_items = _dbContext.PurchaseOrderItems.Where(x => pos_ids.Contains(x.PurchaseOrderId)).ToList();

            _dbContext.PurchaseOrderItems.RemoveRange(pos_items);

            _dbContext.PurchaseOrders.RemoveRange(pos);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> HasAnyPurchaseOrderEmailed(long workorderTaskId)
        {
            return await _dbContext.PurchaseOrders.AnyAsync(x => x.TaskId == workorderTaskId && x.EmailSent.HasValue);
        }
    }
}
