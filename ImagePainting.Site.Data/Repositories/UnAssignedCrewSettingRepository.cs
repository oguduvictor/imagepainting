﻿using System;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ImagePainting.Site.Common.Helpers;

namespace ImagePainting.Site.Data.Repositories
{
    internal class UnAssignedCrewSettingRepository : IUnAssignedCrewSettingRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public UnAssignedCrewSettingRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddUnAssignedCrewSettings(params UnAssignedCrewSetting[] unAssignedCrewSettings)
        {
            if (unAssignedCrewSettings.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(unAssignedCrewSettings));
            }

            _dbContext.UnAssignedCrewSettings.AddRange(unAssignedCrewSettings);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<IList<UnAssignedCrewSetting>> GetSettings(DateTimeRange dateTimeRange, IEnumerable<string> crewIds = null)
        {
            var query = _dbContext.UnAssignedCrewSettings.AsQueryable();

            if (dateTimeRange.BeginDateTime.Equals(dateTimeRange.EndDateTime))
            {
                query = query.Where(x => x.ScheduledDate == dateTimeRange.BeginDateTime);
            }

            if (!crewIds.IsNullOrEmpty())
            {
                query = query.Where(x => crewIds.Contains(x.UserId));
            }

            if (!dateTimeRange.IsNull())
            {
                query = query.Where(x => DbFunctions.TruncateTime(x.ScheduledDate) >= dateTimeRange.BeginDateTime.Date &&
                                            DbFunctions.TruncateTime(x.ScheduledDate) <= dateTimeRange.EndDateTime.Date);
            }

            return await query.ToListAsync();
        }

        public async Task UpdateSetting(params UnAssignedCrewSetting[] unAssignedCrewSettings)
        {
            if (unAssignedCrewSettings.Length < 1)
            {
                return;
            }

            foreach (var setting in unAssignedCrewSettings)
            {
                var dbSetting = _dbContext.UnAssignedCrewSettings.Local.FirstOrDefault(x => x.UserId == setting.UserId && x.ScheduledDate == setting.ScheduledDate);

                if (dbSetting.IsNull())
                {
                    dbSetting = setting;

                    _dbContext.UnAssignedCrewSettings.Attach(dbSetting);
                }

                _dbContext.Entry(dbSetting).Property(x => x.Busy).IsModified = true;
                _dbContext.Entry(dbSetting).Property(x => x.Emailed).IsModified = true;
            }
            
            await _dbContext.SaveChangesAsync();
        }

        public Task<UnAssignedCrewSetting> GetSetting(DateTime date, string crewId)
        {
            return _dbContext.UnAssignedCrewSettings.FirstOrDefaultAsync(x => x.UserId == crewId && x.ScheduledDate == date);
        }
    }
}
