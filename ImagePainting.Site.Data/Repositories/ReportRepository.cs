﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using ImagePainting.Site.Common.Models.Reports;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class ReportRepository : IReportRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public ReportRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<PurchaseOrderReportItem>> GetPurchaseReport(PurchaseReportQuery searchQuery)
        {
            var query = _dbContext.PurchaseOrders
                .Where(x => searchQuery.DateRange.BeginDateTime <= x.Date && searchQuery.DateRange.EndDateTime >= x.Date)
                .Include(x => x.WorkOrder.Builder)
                .Include(x => x.WorkOrder.Community)
                .Include(x => x.WorkOrder.JobInfo.Plan)
                .Include(x => x.Task.Crew)
                .Include(x => x.PaintStoreLocation.Store)
                .Include(x => x.PurchaseOrderItems)
                .Include(x => x.Task);

            if (searchQuery.BuilderId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.BuilderId == searchQuery.BuilderId);
            }

            if (searchQuery.CommunityId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.CommunityId == searchQuery.CommunityId);
            }

            if (!searchQuery.WorkOrder.IsNullOrEmpty())
            {
                query = query.Where(x => x.WorkOrder.Name.Contains(searchQuery.WorkOrder));
            }

            if (!searchQuery.PONumber.IsNullOrEmpty())
            {
                var poNumber = searchQuery.PONumber.ToSafeLong(0);// Convert.ToInt64(query.PONumber);
                query = query.Where(x => x.Id == poNumber);
            }

            if (searchQuery.StoreId.HasValue)
            {
                query = query.Where(x => x.PaintStoreLocation.StoreId == searchQuery.StoreId);
            }

            if (searchQuery.PlanId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.JobInfo.PlanId == searchQuery.PlanId);
            }

            if (!searchQuery.CrewIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchQuery.CrewIds.Contains(x.Task.CrewId));
            }

            if (searchQuery.MainTaskType != TaskType.Unknown)
            {
                query = query.Where(x => x.Task.TaskType == searchQuery.MainTaskType);
            }

            var purchaseOrders = await query.Where(x => !x.WorkOrder.Deleted).ToListAsync();

            return purchaseOrders.Select(x => new PurchaseOrderReportItem
            {
                Block = x.WorkOrder?.Block,
                Builder = new KeyValuePair<long, string>((int)x.WorkOrder?.BuilderId, x.WorkOrder?.Builder?.Name),
                Community = new KeyValuePair<long, string>((int)x.WorkOrder?.CommunityId, x.WorkOrder?.Community?.Name),
                Crew = x.Task?.Crew?.FullName,
                Lot = (int)x.WorkOrder?.Lot,
                PurchaseOrder = Mapper.Map<PurchaseOrder>(x),
                EmailSent = x.EmailSent,
                Store = new KeyValuePair<long, string>((long)x.PaintStoreLocation?.StoreId, x.PaintStoreLocation?.Store?.StoreName),
                Task = new KeyValuePair<long, string>(x.TaskId, x.Task?.Name),
                TaskType = x.Task.TaskType.ToString("F"),
                Total = x.Total,
                WorkOrder = new KeyValuePair<long, string>(x.WorkOrderId, x.WorkOrder.Name)
            });
        }

        public Task<IEnumerable<KeyValuePair<int, string>>> GetLotBlockOptions(int? communityId = null)
        {
            return _dbContext.WorkOrders
                 .Where(x => x.CommunityId == communityId && !x.Deleted)
                 .Select(x => new { x.Lot, x.Block })
                 .ToListAsync()
                 .ContinueWith(item => item.Result.Select(x => new KeyValuePair<int, string>(x.Lot.Value, x.Block)));
        }

        public async Task<TasksReport> GetTasksReport(TasksReportQuery searchQuery)
        {
            var query = _dbContext.WorkOrderTasks
                .Where(x => searchQuery.DateRange.BeginDateTime <= x.Completed && searchQuery.DateRange.EndDateTime >= x.Completed)
                .Include(x => x.WorkOrder)
                .Include(x => x.WorkOrder.JobInfo)
                .Include(x => x.WorkOrder.Builder)
                .Include(x => x.WorkOrder.Community)
                .Include(x => x.Crew);

            if (!searchQuery.TaskSchemaIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchQuery.TaskSchemaIds.Contains(x.SchemaId));
            }

            if (!searchQuery.WorkOrder.IsNullOrEmpty())
            {
                query = query.Where(x => x.WorkOrder.Name.Contains(searchQuery.WorkOrder));
            }

            if (searchQuery.BuilderId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.BuilderId == searchQuery.BuilderId);
            }
            
            if (searchQuery.CommunityId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.CommunityId == searchQuery.CommunityId);
            }

            if (searchQuery.PlanId.HasValue)
            {              
                query = query.Where(x => x.WorkOrder.JobInfo.PlanId == searchQuery.PlanId);
            }

            if (!searchQuery.CrewIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchQuery.CrewIds.Contains(x.CrewId));
            }

            if (!searchQuery.Superintendent.IsNullOrEmpty())
            {
                query = searchQuery.Superintendent == "No Superintendent" ? 
                    query.Where(x => x.WorkOrder.SuperIntendent == null) : query.Where(x => x.WorkOrder.SuperIntendent == searchQuery.Superintendent);

            }

            var result = await query.Where(x => !x.WorkOrder.Deleted).ToListAsync();

            var tasksReport = result.IsNullOrEmpty() 
                ? new List<TaskReportItem>()
                : result.Select(x => new TaskReportItem
                    {
                        Task = x.Name,
                        TaskId = x.Id,
                        WorkOrder = x.WorkOrder?.Name,
                        WorkOrderId = x.WorkOrderId,
                        Lot = x.WorkOrder.Lot ?? 0,
                        Block = x.WorkOrder?.Block,
                        Crew = x.Crew?.FullName,
                        Builder = x.WorkOrder?.Builder?.Name,
                        BuilderId = x.WorkOrder.BuilderId.Value,
                        Community = x.WorkOrder?.Community?.Name,
                        CommunityId = x.WorkOrder?.CommunityId ?? 0,
                        Completed = x.Completed
                    }).ToList();

            return new TasksReport
            {
                TaskReportItems = tasksReport
            };
        }

        public async Task<ProjectedRevenueReport> GetProjectedRevenueReport(ProjectedRevenueReportQuery searchQuery)
        {
            var query = _dbContext.WorkOrders
                .Where(x => (x.JobInfoId.HasValue || x.ExtraJobInfoId.HasValue) 
                            && 
                            (searchQuery.DateRange.BeginDateTime <= x.Modified && searchQuery.DateRange.EndDateTime >= x.Modified));

            if (searchQuery.BuilderId.HasValue)
            {
                query = query.Where(x => x.BuilderId == searchQuery.BuilderId);
            }

            if (searchQuery.CommunityId.HasValue)
            {
                query = query.Where(x => x.CommunityId == searchQuery.CommunityId);
            }
            
            var workOrder = await query.Where(x => !x.Deleted)
                                .Include(x => x.Builder)
                                .Include(x => x.Community)
                                .Include(x => x.JobInfo)
                                .Include(x => x.ExtraJobInfo)
                                .ToListAsync();

            var revenueReports = workOrder.IsNullOrEmpty() 
                ? new List<RevenueReportItem>()
                : workOrder.Select(x => new RevenueReportItem
                    {
                        Block = x.Block,
                        Builder = x.Builder.Name,
                        Community = x.Community.Name,
                        Completed = x.Completed,
                        Lot = x.Lot ?? 0,
                        TotalRevenue = ReportCalculator.CalculateRevenue(Mapper.Map<WorkOrder>(x)),
                        WorkOrder = x.Name,
                        WorkOrderId = x.Id
                    });

            return new ProjectedRevenueReport
            {
                RevenueReportItems = revenueReports
            };
        }

        public async Task<ProjectedLaborReport> GetProjectedLaborReport(ProjectedLaborReportQuery searchQuery)
        {
            var query = _dbContext.WorkOrderTasks
                            .Where(x => searchQuery.DateRange.BeginDateTime <= x.Completed 
                                && searchQuery.DateRange.EndDateTime >= x.Completed
                                && (x.PreWalkTaskId.HasValue 
                                || x.CompletionWalkTaskId.HasValue))
                            .Include(x => x.WorkOrder)
                            .Include(x => x.WorkOrder.JobInfo)
                            .Include(x => x.WorkOrder.Builder)
                            .Include(x => x.WorkOrder.Community)
                            .Include(x => x.WorkOrder.ExtraJobInfo)
                            .Include(x => x.Crew)
                            .Include(x => x.PreWalkTask)
                            .Include(x => x.CompletionWalkTask);

            if (searchQuery.BuilderId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.BuilderId == searchQuery.BuilderId);
            }

            if (searchQuery.CommunityId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.CommunityId == searchQuery.CommunityId);
            }

            if (searchQuery.PlanId.HasValue)
            {
                query = query.Where(x => x.WorkOrder.JobInfo.PlanId == searchQuery.PlanId);
            }

            if (!searchQuery.CrewIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchQuery.CrewIds.Contains(x.CrewId));
            }

            var workOrderTasks = await query.Where(x => !x.WorkOrder.Deleted).ToListAsync();
            var projection = new ProjectedLaborReport();

            if (!workOrderTasks.IsNullOrEmpty())
            {
                projection.LaborReportItems = workOrderTasks.Select(x => new LaborReportItem
                {
                    Task = x.Name,
                    TaskId = x.Id,
                    WorkOrder = x.WorkOrder?.Name,
                    WorkOrderId = x.WorkOrderId,
                    Lot = x.WorkOrder?.Lot ?? 0,
                    Block = x.WorkOrder?.Block,
                    Crew = x.Crew?.FullName,
                    Builder = x.WorkOrder?.Builder?.Name,
                    BuilderId = x.WorkOrder?.BuilderId ?? 0,
                    Community = x.WorkOrder?.Community?.Name,
                    CommunityId = x.WorkOrder?.CommunityId ?? 0,
                    Completed = x.Completed,
                    PreWalk = x.PreWalkTask?.Completed,
                    CompletionWalk = x.CompletionWalkTask?.Completed,
                    Amount = ReportCalculator.CalculateLaborAmount(Mapper.Map<WorkOrderTask>(x))
                });
            }

            return projection;
        }
    }
}
