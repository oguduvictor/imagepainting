﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class ColorSchemeRepository : IColorSchemeRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public ColorSchemeRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region InteriorColorScheme

        public async Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemes()
        {
            return await _dbContext.InteriorColorSchemes.ToListAsync();
        }

        public async Task<InteriorColorScheme> GetInteriorColorScheme(int id)
        {
            return await _dbContext.InteriorColorSchemes.FindAsync(id);
        }

        public async Task<bool> DeleteInteriorColorScheme(int id)
        {
            var colorSchemeWasDeleted = false;
            var colorScheme = await _dbContext.InteriorColorSchemes.FindAsync(id);

            if (!colorScheme.IsNull())
            {
                _dbContext.InteriorColorSchemes.Remove(colorScheme);

                await _dbContext.SaveChangesAsync();

                colorSchemeWasDeleted = true;
            }

            return colorSchemeWasDeleted;
        }

        public async Task<InteriorColorScheme> CreateInteriorColorScheme(InteriorColorScheme interiorColorScheme)
        {
            _dbContext.InteriorColorSchemes.Add(interiorColorScheme);

            await _dbContext.SaveChangesAsync();

            return interiorColorScheme;
        }

        public async Task<InteriorColorScheme> UpdateInteriorColorScheme(InteriorColorScheme interiorColorScheme)
        {
            _dbContext.Entry(interiorColorScheme).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return interiorColorScheme;
        }

        public Task<IEnumerable<InteriorColorScheme>> GetInteriorColorSchemeSummary(string colorCodePrefix = null)
        {
            var query = _dbContext.InteriorColorSchemes.AsQueryable();

            if(!colorCodePrefix.IsNullOrEmpty())
            {
                query = query.Where(x => x.ColorCode.Substring(0, colorCodePrefix.Length) == colorCodePrefix);
            }
            
            return query.Select(x => new { x.Id, x.ColorCode })
                .ToListAsync()
                .ContinueWith(color =>
                {
                    return color.Result.Select(x => new InteriorColorScheme { Id = x.Id, ColorCode = x.ColorCode });
                });
        }

        #endregion

        #region ExteriorColorScheme

        public async Task<IEnumerable<ExteriorColorScheme>> GetExteriorColorSchemes()
        {
            return await _dbContext.ExteriorColorSchemes.ToListAsync();
        }

        public async Task<ExteriorColorScheme> GetExteriorColorScheme(int id)
        {
            return await _dbContext.ExteriorColorSchemes.FindAsync(id);
        }

        public async Task<bool> DeleteExteriorColorScheme(int id)
        {
            var colorSchemeWasDeleted = false;
            var colorScheme = await _dbContext.ExteriorColorSchemes.FindAsync(id);

            if (!colorScheme.IsNull())
            {
                _dbContext.ExteriorColorSchemes.Remove(colorScheme);

                await _dbContext.SaveChangesAsync();

                colorSchemeWasDeleted = true;
            }

            return colorSchemeWasDeleted;
        }

        public async Task<ExteriorColorScheme> CreateExteriorColorScheme(ExteriorColorScheme exteriorColorScheme)
        {
            _dbContext.ExteriorColorSchemes.Add(exteriorColorScheme);

            await _dbContext.SaveChangesAsync();

            return exteriorColorScheme;
        }

        public async Task<ExteriorColorScheme> UpdateExteriorColorScheme(ExteriorColorScheme exteriorColorScheme)
        {
            _dbContext.Entry(exteriorColorScheme).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return exteriorColorScheme;
        }
        

        public Task<IEnumerable<ExteriorColorScheme>> GetExeteriorColorSchemeSummary(string colorCodePrefix = null)
        {
            var query = _dbContext.ExteriorColorSchemes.AsQueryable();

            if(!colorCodePrefix.IsNullOrEmpty())
            {
                query = query.Where(x => x.ColorCode.Substring(0, colorCodePrefix.Length) == colorCodePrefix);
            }

            return query.Select(x => new { x.Id, x.ColorCode })
                .ToListAsync()
                .ContinueWith(color =>
                {
                    return color.Result.Select(x => new ExteriorColorScheme { Id = x.Id, ColorCode = x.ColorCode });
                });
        }

        #endregion
    }
}
