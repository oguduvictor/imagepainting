﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class CostRepository : ICostRepository
    {
        private ImagePaintingContext _dbContext;

        public CostRepository(ImagePaintingContext context)
        {
            _dbContext = context;
        }

        public async Task<Cost> GetCost(int id, bool includeBuilder = false, bool includeCostItem = false)
        {
            var query = _dbContext.Costs.AsQueryable();

            if (includeBuilder)
            {
                query = query.Include(x => x.Builder);
            }

            if (includeCostItem)
            {
                query = query.Include(x => x.CostItem);
            }

            return await query.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<CostItem> GetCostItemByName(string costItemName)
        {
            return await _dbContext.CostItems
                        .Where(x => x.Name == costItemName)
                        .FirstOrDefaultAsync();
        }

        public Task<List<Cost>> GetCosts(int builderId, bool? isExtra = false)
        {
            var query = _dbContext.Costs.Where(x => x.BuilderId == builderId);

            if (isExtra.HasValue)
            {
                query = query.Where(x => x.CostItem.IsExtra == isExtra.Value);
            }

            return query.Include(x => x.CostItem).ToListAsync();
        }

        public async Task<Cost> CreateCost(Cost cost)
        {
            _dbContext.Costs.Add(cost);
            await _dbContext.SaveChangesAsync();

            return cost;
        }

        public async Task UpdateCost(Cost cost)
        {
            if (_dbContext.Costs.Local.FirstOrDefault(x => x.Id == cost.Id) == null)
            {
                _dbContext.Costs.Attach(cost);
            }

            _dbContext.Entry(cost).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteCost(int id)
        {
            var cost = await _dbContext.Costs.FindAsync(id);
            var costItemId = cost.CostItemId;

            _dbContext.Costs.Remove(cost);
            await _dbContext.SaveChangesAsync();

            var anotherCost = await _dbContext.Costs.FirstOrDefaultAsync(x => x.CostItemId == costItemId);

            // if no other cost depends on cost item, remove it.
            if (anotherCost.IsNull())
            {
                await DeleteCostItem(costItemId);
            }
        }

        public async Task DeleteCostItem(int id)
        {
            var costs = await _dbContext.Costs.Where(x => x.CostItemId == id).ToListAsync();

            foreach(var cost in costs)
            {
                _dbContext.Costs.Remove(cost);
            }

            if (costs.Any())
            {
                await _dbContext.SaveChangesAsync();
            }

            var costItem = _dbContext.CostItems.Local.FirstOrDefault(x => x.Id == id);

            if (costItem == null)
            {
                costItem = new CostItem { Id = id };
                _dbContext.CostItems.Attach(costItem);
            }

            _dbContext.CostItems.Remove(costItem);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<CostItem> CreateCostItem(CostItem costItem)
        {
            _dbContext.CostItems.Add(costItem);
            await _dbContext.SaveChangesAsync();

            return costItem;
        }

        public Task<List<CostItem>> GetCostItems(bool? isExtra = null)
        {
            var query = _dbContext.CostItems.AsQueryable();

            if (isExtra.HasValue)
            {
                query = query.Where(x => x.IsExtra == isExtra);
            }

            return query.ToListAsync();
        }

        public async Task UpdateCostItems(params CostItem[] costItems)
        {
            costItems.ForEach(costItem =>
            {
                _dbContext.Entry(costItem).State = EntityState.Modified;
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task<CostItem> GetCostItem(int id)
        {
            return await _dbContext.CostItems.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
