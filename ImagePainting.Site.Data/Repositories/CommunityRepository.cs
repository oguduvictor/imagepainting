﻿using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class CommunityRepository : ICommunityRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public CommunityRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Community> CreateCommunity(Community community)
        {
            _dbContext.Communities.Add(community);

            await _dbContext.SaveChangesAsync();

            return community;
        }

        public Task<List<Community>> GetCommunities(bool isSummary = true, bool includeBuilders = false, long? builderId = null)
        {
            var query = _dbContext.Communities.Where(x => x.Deleted == false);

            if (builderId.HasValue)
            {
                query = query.Where(x => x.Builders.Any(y => y.Id == builderId));
            }

            if (isSummary)
            {
                var task = query.Select(x => new { Id = x.Id, Name = x.Name, Abbreviation = x.Abbreviation }).ToListAsync();

                task.Wait();

                return Task.FromResult(task.Result.Select(x => new Community { Id = x.Id, Name = x.Name, Abbreviation = x.Abbreviation }).ToList());
            }

            if (includeBuilders)
            {
                query = query.Include(x => x.Builders);
            }

            return query.ToListAsync();
        }

        public async Task<Community> GetCommunity(int communityId, bool includeBuilders = false)
        {
            var query = _dbContext.Communities.AsQueryable();

            if (includeBuilders)
            {
                query = query.Include(x => x.Builders);
            }
            
            return await query.FirstOrDefaultAsync(x => x.Id == communityId);
        }

        public async Task<Community> UpdateCommunity(Community community)
        {
            _dbContext.Entry(community).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return community;
        }
    }
}
