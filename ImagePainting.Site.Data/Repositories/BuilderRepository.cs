﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class BuilderRepository : IBuilderRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public BuilderRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Builder> GetBuilder(int builderId, bool includeCommunities = false, bool includeCosts = false, bool includePlans = false)
        {
            var query = _dbContext.Builders.AsQueryable();

            if (includeCommunities)
            {
                query = query.Include(x => x.Communities);
            }

            if (includeCosts)
            {
                query = query.Include(x => x.Costs).Include(x => x.Costs.Select(t => t.CostItem));
            }

            var builder = await query.FirstOrDefaultAsync(x => x.Id == builderId);

            if (includePlans)
            {
                await _dbContext.Entry(builder)
                            .Collection(x => x.Plans)
                            .Query()
                            .Where(x => !x.Deleted)
                            .LoadAsync();
            }

            return builder;
        }

        public Task<List<Builder>> GetBuilders(bool isSummary = true, bool includeCommunities = false, bool includeCosts = false)
        {
            var query = _dbContext.Builders.Where(x => x.Deleted == false);

            if (isSummary)
            {
                var task = query.Select(x => new { Id = x.Id, Name = x.Name, Abbreviation = x.Abbreviation }).ToListAsync();

                task.Wait();

                return Task.FromResult(task.Result.Select(x => new Builder { Id = x.Id, Name = x.Name, Abbreviation = x.Abbreviation }).ToList());
            }

            if (includeCommunities)
            {
                query = query.Include(x => x.Communities);
            }

            if (includeCosts)
            {
                query = query.Include(x => x.Costs);
            }

            return query.ToListAsync();
        }

        public async Task<List<Builder>> GetBuilders(List<int> builderIds)
        {
            return await _dbContext.Builders.Where(x => builderIds.Contains(x.Id)).ToListAsync();
        }
        
        public async Task UpdateBuilderCosts(Builder builder)
        {
            builder.Costs.ForEach(cost =>
            {
                _dbContext.Entry(cost).State = EntityState.Modified;
            });

            await _dbContext.SaveChangesAsync();
        }

        public Task<List<State>> GetStates()
        {
            return _dbContext.States.ToListAsync();
        }

        public async Task<Builder> SaveBuilder(Builder builder)
        {
            _dbContext.Builders.Add(builder);
            await _dbContext.SaveChangesAsync();

            return builder;
        }

        public async Task<Builder> UpdateBuilder(Builder builder)
        {
            _dbContext.Entry(builder).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            return builder;
        }

        public void AddCosts(IEnumerable<Cost> costs, Builder builder)
        {
            costs.ForEach(cost =>
            {
                _dbContext.Entry(cost).State = EntityState.Added;
                builder.Costs.Add(cost);
            });
        }
    }
}
