﻿using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class ElevationRepository : IElevationRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public ElevationRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Elevation> GetElevation(int elevationId)
        {
            return await _dbContext.Elevations.FindAsync(elevationId);
        }

        public async Task<Elevation> CreateElevation(Elevation elevation)
        {
            _dbContext.Elevations.Add(elevation);
            await _dbContext.SaveChangesAsync();

            return elevation;
        }

        public async Task<Elevation> UpdateElevation(Elevation elevation)
        {
            _dbContext.Entry(elevation).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();

            return elevation;
        }

        public Task<List<Elevation>> GetElevationsSummary(int? planId = null, bool includeDeleted = false)
        {
            var query = _dbContext.Elevations.AsQueryable();

            if (planId.HasValue)
            {
                query = query.Where(x => x.PlanId == planId);
            }

            if (!includeDeleted)
            {
                query = query.Where(x => x.Deleted == includeDeleted);
            }

            var task = query.Select(x => new { Id = x.Id, Name = x.Name }).ToListAsync();

            task.Wait();

            return Task.FromResult(task.Result.Select(x => new Elevation { Id = x.Id, Name = x.Name }).ToList());
        }
    }
}
