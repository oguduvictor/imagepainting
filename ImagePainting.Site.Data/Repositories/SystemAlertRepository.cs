﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class SystemAlertRepository : ISystemAlertRepository
    {
        private ImagePaintingContext _dbContext;
        private DbSet<SystemAlert> _alerts;

        public SystemAlertRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
            _alerts = _dbContext.Set<SystemAlert>();
        }

        public async Task<IList<SystemAlert>> GetAlerts(IEnumerable<Guid> alertIds)
        {
            return await _alerts
                .Where(x => alertIds.Contains(x.Id))
                .Include(x => x.CreatedBy)
                .Include(x => x.ResolvedBy)
                .ToListAsync();
        }

        public Task Add(SystemAlert systemAlert)
        {
            _alerts.Add(systemAlert);

            return _dbContext.SaveChangesAsync();
        }

        public Task AddRange(IEnumerable<SystemAlert> systemAlerts)
        {
            _alerts.AddRange(systemAlerts);

            return _dbContext.SaveChangesAsync();
        }

        public Task Update(params SystemAlert[] systemAlerts)
        {
            systemAlerts.ForEach(systemAlert =>
            {
                var dbAlert = _alerts.Local.FirstOrDefault(x => x.Id == systemAlert.Id && x.ParentId == systemAlert.ParentId);

                if (dbAlert == null)
                {
                    dbAlert = systemAlert;
                    _alerts.Attach(dbAlert);
                }

                _dbContext.Entry(dbAlert).Property(x => x.Resolved).IsModified = true;
                _dbContext.Entry(dbAlert).Property(x => x.ResolvedById).IsModified = true;
            });

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteAlerts(params SystemAlert[] systemAlerts)
        {
            foreach (var alert in systemAlerts)
            {
                var dbAlert = _alerts.Local.FirstOrDefault(x => x.Id == alert.Id);

                if (dbAlert == null)
                {
                    dbAlert = alert;
                    _alerts.Attach(dbAlert);
                }

                _alerts.Remove(dbAlert);
            }

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<SystemAlert>> SearchAlerts(AlertSearchCriteria searchCriteria)
        {
            var query = _alerts.Include(x => x.CreatedBy).Include(x => x.ResolvedBy);

            if (searchCriteria.TargetObjectId.HasValue)
            {
                return await query.Where(x => x.TargetObjectId == searchCriteria.TargetObjectId).ToListAsync();
            }

            if (searchCriteria.ParentType.HasValue)
            {
                query = query.Where(x => x.ParentType == searchCriteria.ParentType);
            }

            if (!searchCriteria.ParentIds.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.ParentIds.Contains(x.ParentId));
            }

            if (searchCriteria.Resolved.HasValue)
            {
                if (searchCriteria.Resolved == true)
                {
                    query = query.Where(x => x.Resolved != null);
                }
                else
                {
                    query = query.Where(x => x.Resolved == null);
                }
            }

            if (!searchCriteria.AlertTypes.IsNullOrEmpty())
            {
                query = query.Where(x => searchCriteria.AlertTypes.Contains(x.AlertType));
            }

            return await query.ToListAsync();
        }
    }
}
