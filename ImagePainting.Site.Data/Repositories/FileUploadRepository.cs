﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Repositories
{
    internal class FileUploadRepository : IFileUploadRepository
    {
        private readonly ImagePaintingContext _dbContext;

        public FileUploadRepository(ImagePaintingContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<FileUpload> GetFileUpload(long id)
        {
            return _dbContext.Set<FileUpload>().FindAsync(id);
        }

        public Task<IEnumerable<FileUpload>> GetFileUploads(long ownerId, params FileOwnerType[] ownerType)
        {
            return _dbContext.Set<FileUpload>()
                            .Where(x => x.OwnerId == ownerId && ownerType.Contains(x.OwnerType))
                            .Select(x => new {x.Id, x.Name, x.Created, x.CreatedById, x.ContentType, x.OwnerType })
                            .ToListAsync()
                            .ContinueWith(task => {
                                return task.Result.Select(x => new FileUpload
                                    {
                                        Id = x.Id,
                                        Name = x.Name,
                                        OwnerId = ownerId,
                                        OwnerType = x.OwnerType,
                                        ContentType = x.ContentType,
                                        Created = x.Created,
                                        CreatedById = x.CreatedById
                                    });
                            });
        }

        public Task<IEnumerable<FileUpload>> GetFileUploads(IEnumerable<long> ownersId, params FileOwnerType[] ownerType)
        {
            return _dbContext.Set<FileUpload>()
                            .Where(x => ownersId.Contains(x.OwnerId) && ownerType.Contains(x.OwnerType))
                            .Select(x => new { x.Id, x.OwnerId, x.Name, x.Created, x.CreatedById, x.ContentType, x.OwnerType })
                            .ToListAsync()
                            .ContinueWith(task => {
                                return task.Result.Select(x => new FileUpload
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    OwnerId = x.OwnerId,
                                    OwnerType = x.OwnerType,
                                    ContentType = x.ContentType,
                                    Created = x.Created,
                                    CreatedById = x.CreatedById
                                });
                            });
        }

        public Task<int> CreateFileUpload(FileUpload fileUpload)
        {
            _dbContext.Set<FileUpload>().Add(fileUpload);

            return _dbContext.SaveChangesAsync();
        }

        public Task<int> CreateFileUploads(IEnumerable<FileUpload> fileUploads)
        {
            _dbContext.Set<FileUpload>().AddRange(fileUploads);

            return _dbContext.SaveChangesAsync();
        }

        public Task<int> DeleteFileUpload(long id)
        {
            var fileUpload = new FileUpload { Id = id };

            _dbContext.Set<FileUpload>().Attach(fileUpload);
            _dbContext.Entry(fileUpload).State = EntityState.Deleted;

            return _dbContext.SaveChangesAsync();
        }

        public async Task<int> DeleteFileUploads(IEnumerable<long> ownerIds, FileOwnerType ownerType)
        {
            var fileIds = await _dbContext.Set<FileUpload>()
                            .Where(y => ownerIds.Contains(y.OwnerId) && y.OwnerType == ownerType)
                            .Select(x => x.Id)
                            .ToListAsync();

            if (fileIds.IsNullOrEmpty())
            {
                return 0;
            }

            var files = new List<FileUpload>();

            foreach (var fileId in fileIds)
            {
                var file = _dbContext.Set<FileUpload>().Local.FirstOrDefault(f => f.Id == fileId);

                if (file.IsNull())
                {
                    file = new FileUpload { Id = fileId };
                    _dbContext.Set<FileUpload>().Attach(file);
                }

                files.Add(file);
            }

            _dbContext.Set<FileUpload>().RemoveRange(files);

            return await _dbContext.SaveChangesAsync();
        }

        public Task<bool> HasAnyFiles(long ownerId, FileOwnerType ownerType)
        {
            return _dbContext.Set<FileUpload>().AnyAsync(x => x.OwnerType == ownerType && x.OwnerId == ownerId);
        }

        public async Task<FileUpload> GetFirstOrDefault(long ownerId, FileOwnerType fileOwner)
        {
            return await _dbContext.Set<FileUpload>().FirstOrDefaultAsync(x => x.OwnerId == ownerId && x.OwnerType == fileOwner);
        }
    }
}
