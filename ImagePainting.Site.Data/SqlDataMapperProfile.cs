﻿using AutoMapper;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Models;
using System.Collections.Generic;

namespace ImagePainting.Site.Data
{
    public class SqlDataMapperProfile : Profile
    {
        public SqlDataMapperProfile()
        {
            CreateMap<Entities.WorkOrderTask, WorkOrderTask>()
                .ForMember(x => x.TaskItems, y => y.MapFrom(z => z.TaskItems.FromJson<IEnumerable<TaskItem>>()))
                .ForMember(x => x.ScheduleDateRange, y => y.MapFrom(z => new ScheduleDateRange(z.ScheduleDate, z.ScheduleDateEnd)))
                .ForMember(x => x.Permission, y => y.Ignore());

            CreateMap<WorkOrderTask, Entities.WorkOrderTask>()
                .ForMember(x => x.TaskItems, y => y.MapFrom(z => z.TaskItems.ToJson()))
                .ForMember(x => x.ScheduleDate, y => y.MapFrom(z => z.ScheduleDateRange.BeginDate))
                .ForMember(x => x.ScheduleDateEnd, y => y.MapFrom(z => z.ScheduleDateRange.EndDate));

            CreateMap<Entities.WorkOrder, WorkOrder>()
                .ForMember(x => x.Tasks, y => y.Ignore())
                .ReverseMap();

            CreateMap<ExtraJobInfo, Entities.ExtraJobInfo>()
                .ForMember(x => x.ExtraItems, y => y.MapFrom(z => z.ExtraItems.IsNullOrEmpty() ? null : z.ExtraItems.ToJson()));
              
            CreateMap<Entities.ExtraJobInfo, ExtraJobInfo>()
                .ForMember(x => x.ExtraItems, y => y.MapFrom(z => z.ExtraItems.FromJson<IEnumerable<ExtraItem>>()));

            CreateMap<Entities.JobInfo, JobInfo>()
                .ForMember(x => x.InteriorColors, y => y.MapFrom(z => z.InteriorColors.FromJson<InteriorColorScheme>()))
                .ForMember(x => x.ExteriorColors, y => y.MapFrom(z => z.ExteriorColors.FromJson<ExteriorColorScheme>()))
                .ForMember(x => x.JobCostItems, y => y.MapFrom(z => z.JobCostItems.FromJson<IEnumerable<JobCostItem>>()));

            CreateMap<JobInfo, Entities.JobInfo>()
                .ForMember(x => x.InteriorColors, y => y.MapFrom(z => z.InteriorColors.ToJson()))
                .ForMember(x => x.ExteriorColors, y => y.MapFrom(z => z.ExteriorColors.ToJson()))
                .ForMember(x => x.JobCostItems, y => y.MapFrom(z => z.JobCostItems.ToJson()));

            CreateMap<Entities.PurchaseOrder, PurchaseOrder>().ReverseMap();

            CreateMap<Entities.PurchaseOrderItem, PurchaseOrderItem>().ReverseMap();
        }
    }
}
