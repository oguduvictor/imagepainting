﻿using System;

namespace ImagePainting.Site.Data.Entities
{
    internal class ExtraJobInfo
    {
        public long Id { get; set; }

        public string ExtraItems { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public string Note { get; set; }
    }
}
