﻿namespace ImagePainting.Site.Data.Entities
{
    internal class PurchaseOrderItem
    {
        public long Id { get; set; }
        
        public string Name { get; set; }

        public string Color { get; set; }

        public decimal UnitPrice { get; set; }
        
        public decimal Quantity { get; set; }

        public long PurchaseOrderId { get; set; }

        public string SupplyType { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
