﻿using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Data.Entities
{
    internal class JobInfo
    {
        public DateTime Created { get; set; }

        public decimal FirstFloorSqft { get; set; }

        public decimal GarageSqft { get; set; }

        public long Id { get; set; }

        public decimal LanaiSqft { get; set; }

        public DateTime Modified { get; set; }

        public int? PlanId { get; set; }

        public int? ElevationId { get; set; }

        public long? PaintStoreId { get; set; }

        public virtual Store PaintStore { get; set; }

        public decimal SecondFloorSqft { get; set; }
        
        public string InteriorColors { get; set; }

        public string ExteriorColors { get; set; }
        
        public bool Confirmed { get; set; }

        public string Note { get; set; }

        public string JobCostItems { get; set; }

        public virtual Plan Plan { get; set; }

        public virtual Elevation Elevation { get; set; }
    }
}
