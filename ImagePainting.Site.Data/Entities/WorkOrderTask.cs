﻿using ImagePainting.Site.Common.Models.Enums;
using System;
using Model = ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Data.Entities
{
    internal class WorkOrderTask
    {
        public long Id { get; set; }

        public long WorkOrderId { get; set; }
        
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public bool IsLumpSum { get; set; }

        public string CrewId { get; set; }

        public int SchemaId { get; set; }

        public DateTime ScheduleDate { get; set; }

        public DateTime? ScheduleDateEnd { get; set; }

        public bool DateConfirmed { get; set; }

        public string TaskItems { get; set; }

        public long? PreWalkTaskId { get; set; }

        public long? CompletionWalkTaskId { get; set; }

        public WorkOrderTaskStatus Status { get; set; }

        public TaskType TaskType { get; set; }

        public DateTime? Completed { get; set; }

        public string CompletedById { get; set; }

        public DateTime? Unlocked { get; set; }

        public string UnlockedById { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string CreatedById { get; set; }

        public string ModifiedById { get; set; }

        public DateTime? EmailSent { get; set; }

        public bool IncludeBase { get; set; }
        
        public bool PreceedingTaskCompleted { get; set; }
        
        public bool SucceedingTaskCompleted { get; set; }

        public bool Deferred { get; set; }

        public virtual Model.UserInfo Crew { get; set; }

        public virtual WorkOrder WorkOrder { get; set; }

        public virtual WorkOrderTask PreWalkTask { get; set; }

        public virtual WorkOrderTask CompletionWalkTask { get; set; }

        public virtual Model.UserInfo CompletedBy { get; set; }

        public virtual Model.UserInfo UnlockedBy { get; set; }
    }
}
