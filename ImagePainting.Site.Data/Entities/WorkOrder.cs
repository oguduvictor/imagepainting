﻿using ImagePainting.Site.Common.Models;
using System;

namespace ImagePainting.Site.Data.Entities
{
    internal class WorkOrder
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public int? BuilderId { get; set; }

        public int? CommunityId { get; set; }

        public int? Lot { get; set; }

        public string Block { get; set; }

        public long? JobInfoId { get; set; }

        public long? ExtraJobInfoId { get; set; }

        public DateTime? Completed { get; set; }

        public DateTime Created { get; set; }

        public string CreatedById { get; set; }

        public string SuperIntendent { get; set; }

        public string CustomerRep { get; set; }

        public DateTime Modified { get; set; }

        public string ModifiedById { get; set; }

        public bool Deleted { get; set; }

        public bool JobInfoConfirmed { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhoneNumber { get; set; }
        
        public string CustomerEmail { get; set; }

        public virtual Builder Builder { get; set; }

        public virtual Community Community { get; set; }

        public virtual JobInfo JobInfo { get; set; }

        public virtual UserInfo CreatedBy { get; set; }

        public virtual ExtraJobInfo ExtraJobInfo { get; set; }
    }
}
