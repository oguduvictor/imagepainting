﻿using System;
using System.Collections.Generic;
using Model = ImagePainting.Site.Common.Models;

namespace ImagePainting.Site.Data.Entities
{
    internal class PurchaseOrder
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public string Email { get; set; }
        
        public string Address { get; set; }
        
        public string DeliveryInstructions { get; set; }

        public string Instructions { get; set; }

        public DateTime? EmailSent { get; set; }
        
        public decimal Total { get; set; }

        public long TaskId { get; set; }

        public long WorkOrderId { get; set; }

        public long PaintStoreLocationId { get; set; }

        public virtual Model.StoreLocation PaintStoreLocation { get; set; }
        
        public virtual List<PurchaseOrderItem> PurchaseOrderItems { get; set; }

        public virtual WorkOrderTask Task { get; set; }
        
        public virtual WorkOrder WorkOrder { get; set; }
    }
}
