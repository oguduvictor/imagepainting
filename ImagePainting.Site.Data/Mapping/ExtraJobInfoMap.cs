﻿using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    internal class ExtraJobInfoMap : EntityTypeConfiguration<Entities.ExtraJobInfo>
    {
        public ExtraJobInfoMap()
        {
            HasKey(t => t.Id);

            ToTable("ExtraJobInfos");
        }
    }
}
