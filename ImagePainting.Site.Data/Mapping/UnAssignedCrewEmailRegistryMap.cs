﻿using ImagePainting.Site.Common.Models.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class UnAssignedCrewEmailRegistryMap : EntityTypeConfiguration<UnAssignedCrewEmailRegistry>
    {
        public UnAssignedCrewEmailRegistryMap()
        {
            HasKey(x => new { x.UserId, x.ScheduledDate});

            ToTable("UnAssignedCrewEmailRegistries");
        }
    }
}
