using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class CommunityMap : EntityTypeConfiguration<Community>
    {
        public CommunityMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(64);

            Property(t => t.Abbreviation)
                .IsRequired()
                .HasMaxLength(2);

            // Table & Column Mappings
            ToTable("Communities");

            // Relationships
            HasMany(b => b.Builders)
                .WithMany(c => c.Communities)
                .Map(bc =>
                {
                    bc.MapLeftKey("CommunityId");
                    bc.MapRightKey("BuilderId");
                    bc.ToTable("BuilderCommunities");
                });
        }
    }
}
