﻿using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    internal class PurchaseOrderMap : EntityTypeConfiguration<Entities.PurchaseOrder>
    {
        public PurchaseOrderMap()
        {
            HasKey(x => x.Id);

            ToTable("PurchaseOrders");
            
            HasRequired(x => x.WorkOrder).WithMany().HasForeignKey(x => x.WorkOrderId);
            HasRequired(x => x.Task).WithMany().HasForeignKey(x => x.TaskId);
            HasRequired(x => x.PaintStoreLocation).WithMany().HasForeignKey(x => x.PaintStoreLocationId);
        }
    }
}
