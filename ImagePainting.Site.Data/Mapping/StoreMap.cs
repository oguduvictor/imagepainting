﻿using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class StoreMap : EntityTypeConfiguration<Store>
    {
        public StoreMap()
        {
            HasKey(x => x.Id);

            ToTable("Stores");
        }
    }
}
