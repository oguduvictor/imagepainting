﻿using ImagePainting.Site.Common.Models.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class UnAssignedCrewSettingMap : EntityTypeConfiguration<UnAssignedCrewSetting>
    {
        public UnAssignedCrewSettingMap()
        {
            HasKey(x => new { x.UserId, x.ScheduledDate});

            ToTable("UnAssignedCrewSettings");
        }
    }
}
