﻿using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class UserInfoMap : EntityTypeConfiguration<UserInfo>
    {
        public UserInfoMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Id)
            .IsRequired();

            Property(t => t.FirstName)
            .IsRequired()
            .HasMaxLength(32);

            Property(t => t.LastName)
            .IsRequired()
            .HasMaxLength(32);

            Property(t => t.City)
            .IsRequired()
            .HasMaxLength(32);

            Property(t => t.State)
            .IsRequired()
            .HasMaxLength(2);

            // Table
            ToTable("UserInfo");

            HasMany(t => t.Roles)
            .WithMany()
            .Map(t =>
            {
                t.ToTable("AspNetUserRoles");
                t.MapLeftKey("UserId");
                t.MapRightKey("RoleId");
            });
        }
    }
}