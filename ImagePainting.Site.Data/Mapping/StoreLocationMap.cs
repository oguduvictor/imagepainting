﻿using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class StoreLocationMap : EntityTypeConfiguration<StoreLocation>
    {
        public StoreLocationMap()
        {
            HasKey(x => x.Id);

            ToTable("StoreLocations");

            HasRequired(x => x.Store).WithMany(x => x.Locations).HasForeignKey(x => x.StoreId);
        }
    }
}
