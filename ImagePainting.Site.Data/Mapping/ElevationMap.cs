﻿using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class ElevationMap : EntityTypeConfiguration<Elevation>
    {
        public ElevationMap()
        {
            // primary key
            HasKey(x => x.Id);

            // table
            ToTable("Elevations");

            // Foreign key
            HasRequired(x => x.Plan).WithMany(y => y.Elevations).HasForeignKey(t => t.PlanId);
        }
    }
}
