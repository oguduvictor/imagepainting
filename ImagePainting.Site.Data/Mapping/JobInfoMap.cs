﻿using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    internal class JobInfoMap : EntityTypeConfiguration<Entities.JobInfo>
    {
        public JobInfoMap()
        {
            HasKey(t => t.Id);

            ToTable("JobInfos");
            
            HasOptional(t => t.PaintStore).WithMany().HasForeignKey(t => t.PaintStoreId);
        }
    }
}