﻿using ImagePainting.Site.Common.Models.Entities;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class SystemAlertMap : EntityTypeConfiguration<SystemAlert>
    {
        public SystemAlertMap()
        {
            ToTable("SystemAlerts")
               .HasKey(x => x.Id);

            HasRequired(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);

            HasOptional(x => x.ResolvedBy)
                .WithMany()
                .HasForeignKey(x => x.ResolvedById);
        }
    }
}
