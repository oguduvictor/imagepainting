﻿using ImagePainting.Site.Common.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class WorkOrderTaskSchemaMap : EntityTypeConfiguration<WorkOrderTaskSchema>
    {
        public WorkOrderTaskSchemaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.TaskItemsJson).HasColumnName("TaskItems");

            Ignore(x => x.TaskItems);
            Ignore(x => x.IsMainTask);

            ToTable("WorkOrderSchemas");
        }
    }
}
