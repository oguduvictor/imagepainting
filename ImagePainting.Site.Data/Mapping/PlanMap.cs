﻿using ImagePainting.Site.Common.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagePainting.Site.Data.Mapping
{
    class PlanMap : EntityTypeConfiguration<Plan>
    {
        public PlanMap ()
        {
            HasKey(x => x.Id);

            HasMany(b => b.Communities)
               .WithMany(c => c.Plans)
               .Map(bc =>
               {
                   bc.MapLeftKey("PlanId");
                   bc.MapRightKey("CommunityId");
                   bc.ToTable("CommunityPlans");
               });
        }
    }
}
