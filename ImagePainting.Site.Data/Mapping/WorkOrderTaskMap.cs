﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    internal class WorkOrderTaskMap : EntityTypeConfiguration<Entities.WorkOrderTask>
    {
        public WorkOrderTaskMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Deferred).HasColumnName("IsDeferred");
            
            HasRequired(x => x.WorkOrder)
                .WithMany()
                .HasForeignKey(x => x.WorkOrderId);

            HasOptional(x => x.PreWalkTask)
                .WithMany()
                .HasForeignKey(x => x.PreWalkTaskId)
                .WillCascadeOnDelete(true);

            HasOptional(x => x.CompletionWalkTask)
                .WithMany()
                .HasForeignKey(x => x.CompletionWalkTaskId);

            HasOptional(x => x.CompletedBy)
                .WithMany()
                .HasForeignKey(x => x.CompletedById);

            HasOptional(x => x.UnlockedBy)
                .WithMany()
                .HasForeignKey(x => x.UnlockedById);

            ToTable("WorkOrderTasks");
        }
    }
}
