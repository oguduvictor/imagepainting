﻿using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Models
{
    internal class PurchaseOrderItemMap: EntityTypeConfiguration<Entities.PurchaseOrderItem>
    {
        public PurchaseOrderItemMap()
        {
            HasKey(x => x.Id);

            ToTable("PurchaseOrderItems");
        }
    }
}
