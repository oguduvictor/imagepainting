using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class CostMap : EntityTypeConfiguration<Cost>
    {
        public CostMap()
        {
            HasKey(t => t.Id);

            HasRequired(x => x.CostItem).WithMany().HasForeignKey(t => t.CostItemId);
        }
    }
}