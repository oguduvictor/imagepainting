using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    internal class WorkOrderMap : EntityTypeConfiguration<Entities.WorkOrder>
    {
        public WorkOrderMap()
        {
            HasKey(t => t.Id);

            ToTable("WorkOrders");
            Property(t => t.Id).HasColumnName("Id");
            
            HasOptional(t => t.Builder).WithMany().HasForeignKey(t => t.BuilderId);
            HasOptional(t => t.Community).WithMany().HasForeignKey(t => t.CommunityId);
            HasOptional(t => t.JobInfo).WithMany().HasForeignKey(t => t.JobInfoId);
            HasOptional(t => t.ExtraJobInfo).WithMany().HasForeignKey(t => t.ExtraJobInfoId);
            HasOptional(t => t.CreatedBy).WithMany().HasForeignKey(t => t.CreatedById);
        }
    }
}