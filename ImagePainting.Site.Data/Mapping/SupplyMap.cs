﻿using ImagePainting.Site.Common.Models;
using System.Data.Entity.ModelConfiguration;

namespace ImagePainting.Site.Data.Mapping
{
    public class SupplyMap : EntityTypeConfiguration<Supply>
    {
        public SupplyMap()
        {
            HasRequired(x => x.Builder).WithMany().HasForeignKey(y => y.BuilderId);

            HasRequired(x => x.PaintStore).WithMany().HasForeignKey(s => s.PaintStoreId);
        }
    }
}
