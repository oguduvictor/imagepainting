﻿using System.Data.Entity;
using ImagePainting.Site.Data.Models;
using ImagePainting.Site.Data.Mapping;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Entities;

namespace ImagePainting.Site.Data
{
    internal class ImagePaintingContext : DbContext
    {
        static ImagePaintingContext()
        {
            Database.SetInitializer<ImagePaintingContext>(null);
        }

        public ImagePaintingContext()
            : base("Name=PaintDbConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Builder> Builders { get; set; }

        public DbSet<Community> Communities { get; set; }

        public DbSet<CostItem> CostItems { get; set; }

        public DbSet<Cost> Costs { get; set; }

        public DbSet<Elevation> Elevations { get; set; }

        public DbSet<Entities.ExtraJobInfo> ExtraJobInfos { get; set; }

        public DbSet<Entities.JobInfo> JobInfos { get; set; }

        public DbSet<Plan> Plans { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<State> States { get; set; }

        public DbSet<UserInfo> UserInfos { get; set; }

        public DbSet<Entities.WorkOrder> WorkOrders { get; set; }

        public DbSet<WorkOrderTaskSchema> WorkOrderTaskSchemas { get; set; }

        public DbSet<Entities.WorkOrderTask> WorkOrderTasks { get; set; }

        public DbSet<InteriorColorScheme> InteriorColorSchemes { get; set; }

        public DbSet<ExteriorColorScheme> ExteriorColorSchemes { get; set; }

        public DbSet<Store> Stores { get; set; }

        public DbSet<StoreLocation> StoreLocations { get; set; }

        public DbSet<Entities.PurchaseOrder> PurchaseOrders { get; set; }

        public DbSet<Entities.PurchaseOrderItem> PurchaseOrderItems { get; set; }

        public DbSet<Supply> Supplies { get; set; }

        public DbSet<SupplyType> SupplyType { get; set; }

        public DbSet<UnAssignedCrewSetting> UnAssignedCrewSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CommunityMap());
            modelBuilder.Configurations.Add(new CostMap());
            modelBuilder.Configurations.Add(new WorkOrderMap());
            modelBuilder.Configurations.Add(new JobInfoMap());
            modelBuilder.Configurations.Add(new UserInfoMap());
            modelBuilder.Configurations.Add(new WorkOrderTaskSchemaMap());
            modelBuilder.Configurations.Add(new WorkOrderTaskMap());
            modelBuilder.Configurations.Add(new ElevationMap());
            modelBuilder.Configurations.Add(new ExtraJobInfoMap());
            modelBuilder.Configurations.Add(new StoreLocationMap());
            modelBuilder.Configurations.Add(new StoreMap());
            modelBuilder.Configurations.Add(new PurchaseOrderMap());
            modelBuilder.Configurations.Add(new PurchaseOrderItemMap());
            modelBuilder.Configurations.Add(new PlanMap());
            modelBuilder.Configurations.Add(new UnAssignedCrewSettingMap());
            modelBuilder.Configurations.Add(new SystemAlertMap());

            modelBuilder.Entity<State>().HasKey(t => t.Code);
            modelBuilder.Entity<Role>().ToTable("AspNetRoles");
            modelBuilder.Entity<FileUpload>().ToTable("FileUploads");
            modelBuilder.Entity<SupplyType>().ToTable("SupplyType");
            modelBuilder.Entity<WorkOrderView>().ToTable("vw_WorkOrders");

            modelBuilder.Entity<TaskView>().ToTable("vw_Tasks").Ignore(t => t.Permission);
        }
    }
}