﻿using ImagePainting.Site.Common.Interfaces.Repository;
using ImagePainting.Site.Data.Repositories;
using LightInject;

namespace ImagePainting.Site.Data
{
    public class DataIoCModule : ICompositionRoot
    {
        public void Compose(IServiceRegistry container)
        {
            container.Register<ImagePaintingContext>(new PerScopeLifetime());

            container.Register<IBuilderRepository, BuilderRepository>(new PerScopeLifetime());
            container.Register<ICommunityRepository, CommunityRepository>(new PerScopeLifetime());
            container.Register<ICostRepository, CostRepository>(new PerScopeLifetime());
            container.Register<IFileUploadRepository, FileUploadRepository>(new PerScopeLifetime());
            container.Register<IPurchaseOrderRepository, PurchaseOrderRepository>(new PerScopeLifetime());
            container.Register<IReportRepository, ReportRepository>(new PerScopeLifetime());
            container.Register<IPaintStoreRepository, PaintStoreRepository>(new PerScopeLifetime());
            container.Register<IPlanRepository, PlanRepository>(new PerScopeLifetime());
            container.Register<IElevationRepository, ElevationRepository>(new PerScopeLifetime());
            container.Register<IUserInfoRepository, UserInfoRepository>(new PerScopeLifetime());
            container.Register<ITaskSchemaRepository, TaskSchemaRepository>(new PerScopeLifetime());
            container.Register<IColorSchemeRepository, ColorSchemeRepository>(new PerScopeLifetime());
            container.Register<IWorkOrderRepository, WorkOrderRepository>(new PerScopeLifetime());
            container.Register<ITaskRepository, TaskRepository>(new PerScopeLifetime());
            container.Register<IUnAssignedCrewSettingRepository, UnAssignedCrewSettingRepository>(new PerScopeLifetime());
            container.Register<IPurchaseOrderRepository, PurchaseOrderRepository>(new PerScopeLifetime());
            container.Register<ISupplyRepository, SupplyRepository>(new PerScopeLifetime());
            container.Register<ISystemAlertRepository, SystemAlertRepository>(new PerScopeLifetime());
        }
    }
}
