﻿using ImagePainting.Site.Common.ServiceBus;
using ImagePainting.Site.Retrofits;
using ImagePainting.Site.Retrofits.Commands;
using System.Threading.Tasks;

namespace ImagePaintingBackgroundProcessor.Handlers
{
    public class RetrofitChangesHandler :
        IHandler<RetrofitJobInfoNotesCommand>,
        IHandler<RetrofitJobExtraItemsCommand>,
        IHandler<RetrofitWorkOrderDescriptionCommand>
    {
        private readonly TaskWorkItemsUpdater _workitemsUpdater;
        private readonly JobInfoNoteRetrofix _jobInfoNoteRetrofix;
        private readonly WorkOrderDescriptionRefrofix _workOrderDescriptionRefrofix;

        public RetrofitChangesHandler(TaskWorkItemsUpdater workitemsUpdater, JobInfoNoteRetrofix jobInfoNoteRetrofix, WorkOrderDescriptionRefrofix workOrderDescriptionRefrofix)
        {
            _workitemsUpdater = workitemsUpdater;
            _jobInfoNoteRetrofix = jobInfoNoteRetrofix;
            _workOrderDescriptionRefrofix = workOrderDescriptionRefrofix;
        }

        public Task HandleAsync(RetrofitJobInfoNotesCommand command)
        {
            return _jobInfoNoteRetrofix.FixAllJobInfoNotes();
        }

        public Task HandleAsync(RetrofitJobExtraItemsCommand message)
        {
            return _workitemsUpdater.SetJobExtraTaskIds();
        }

        public Task HandleAsync(RetrofitWorkOrderDescriptionCommand message)
        {
            return _workOrderDescriptionRefrofix.FixDescriptionInWorkOrders();
        }
    }
}
