﻿using ImagePainting.Site.Common.ServiceBus;
using ImagePainting.Site.Domain.TaskManagement;
using System.Threading.Tasks;

namespace ImagePaintingBackgroundProcessor.Handlers
{
    public class UpdateWorkOrderTasksHandler : IHandler<TaskSchemaUpdatedEvent>
    {
        private readonly WorkOrderTaskUpdater _taskUpdater;

        public UpdateWorkOrderTasksHandler(WorkOrderTaskUpdater taskUpdater)
        {
            _taskUpdater = taskUpdater;
        }

        public Task HandleAsync(TaskSchemaUpdatedEvent @event)
        {
            return _taskUpdater.UpdateAllTasksFromSchema(@event.SchemaId, @event.AddedTaskItemNames, @event.DeletedTaskItemNames);
        }
    }
}
