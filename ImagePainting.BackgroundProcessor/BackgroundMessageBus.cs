﻿using ImagePainting.ServiceBus;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.ServiceBus;
using LightInject;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace ImagePaintingBackgroundProcessor
{
    public class BackgroundMessageBus : MessageBus
    {
        private readonly IServiceFactory _serviceFactory;

        public BackgroundMessageBus(IQueueManager queueManager, IServiceFactory serviceFactory) 
            : base(queueManager)
        {
            _serviceFactory = serviceFactory;
        }

        protected override Task ExecuteHandler(Type handlerType, IMessage message)
        {
            var handler = _serviceFactory.GetInstance(handlerType);
            var userContext = _serviceFactory.GetInstance<IUserContext>();

            (userContext as UserContext).SetUserId(message.UserId);

            var result = handlerType.InvokeMember(nameof(IHandler<IMessage>.HandleAsync), 
                BindingFlags.InvokeMethod, null, handler, new object[] { message });

            return result as Task;
        }
    }
}
