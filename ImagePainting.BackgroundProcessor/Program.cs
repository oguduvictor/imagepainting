﻿using ImagePaintingBackgroundProcessor.Handlers;
using ImagePainting.ServiceBus;
using ImagePainting.Site.Common.ServiceBus;
using LightInject;
using Topshelf;
using AutoMapper;
using ImagePainting.Site.Data;

namespace ImagePaintingBackgroundProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new ServiceContainer();

            IocConfig.Register(container);
            
            // Initializes auto mapper
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<SqlDataMapperProfile>();
            });

            var rootScope = container.BeginScope();
            var qManager = rootScope.GetInstance<IQueueManager>();
            var messageBus = new BackgroundMessageBus(qManager, rootScope);
            //var messageBus = rootScope.GetInstance<BackgroundMessageBus>();

            // register all events and handlers here
            messageBus.Register<TaskSchemaUpdatedEvent, UpdateWorkOrderTasksHandler>();
            messageBus.RegisterHandler<RetrofitChangesHandler>();

            HostFactory.Run(configure =>
            {
                configure.Service<BackgroundMessageBus>(service =>
                {
                    service.ConstructUsing(s => messageBus);
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                    service.WhenPaused(s => s.Stop());
                    service.WhenContinued(s => s.Start());
                });

                configure.RunAsLocalSystem();

                configure.SetDescription("Processor for Image Painting background jobs");
                configure.SetDisplayName("Image Painting Backgroud Processor");
                configure.SetServiceName("ImagePainting.BackgroundProcessor");
            });
        }
    }
}
