﻿using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Common.Interfaces.Service;
using ImagePainting.Site.Common.Models;
using ImagePainting.Site.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImagePaintingBackgroundProcessor
{
    public class UserContext : IUserContext
    {
        private readonly IUserCacheService _userCacheService;
        private UserInfoSummary _userInfo;
        private string _userId;

        public UserContext(IUserCacheService userCacheService)
        {
            _userCacheService = userCacheService;
        }

        public string Email
        {
            get
            {
                var user = GetUserInfo();
                return user.Email;
            }
        }

        public string FirstName
        {
            get
            {
                var user = GetUserInfo();
                return user.FirstName;
            }
        }

        public string LastName
        {
            get
            {
                var user = GetUserInfo();
                return user.LastName;
            }
        }

        public string UserId
        {
            get
            {
                return _userId;
            }
        }

        public IList<Guid> GetMemberIdsInRole(UserRole role)
        {
            throw new NotImplementedException();
        }

        public IList<UserRole> GetRoles(string userId)
        {
            throw new NotImplementedException();
        }

        public bool IsAdmin()
        {
            var user = GetUserInfo();

            return user.Roles.Any(role => role == UserRole.Administrator);
        }

        public bool IsInRole(string userId, UserRole role)
        {
            var user = GetUserInfo();

            return user.Roles.Any(x => x == role);
        }

        public void SetUserId(string userId)
        {
            _userId = userId;
            _userInfo = null;
        }

        private UserInfoSummary GetUserInfo()
        {
            if (_userId.IsNullOrEmpty())
            {
                throw new ArgumentNullException($"{nameof(_userId)} cannot be null");
            }

            if (_userInfo != null)
            {
                return _userInfo;
            }
            
            _userInfo = _userCacheService.GetUser(_userId);

            return _userInfo;
        }
    }
}
