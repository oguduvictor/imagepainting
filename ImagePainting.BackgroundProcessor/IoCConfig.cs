﻿using ImagePaintingBackgroundProcessor.Handlers;
using ImagePainting.ServiceBus;
using ImagePainting.Site.Common.Interfaces;
using ImagePainting.Site.Data;
using ImagePainting.Site.Domain;
using LightInject;
using ImagePainting.Site.Retrofits;

namespace ImagePaintingBackgroundProcessor
{
    public class IocConfig
    {
        public static void Register(IServiceContainer container)
        {
            container.RegisterFrom<DataIoCModule>();
            container.RegisterFrom<DomainIoCModule>();
            container.RegisterFrom<ServiceBusIoCModule>();
            container.RegisterFrom<RetrofitsIoCModule>();

            container.Register<IUserContext, UserContext>(new PerScopeLifetime());

            // TODO: register handlers as part of registering them with the bus
            container.Register<UpdateWorkOrderTasksHandler>(new PerScopeLifetime());
            container.Register<RetrofitChangesHandler>(new PerScopeLifetime());
        }
    }
}
