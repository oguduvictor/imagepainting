﻿namespace ImagePainting.ServiceBus.Queueing
{
    internal class QMessageQuery
    {
        public QMessage.MessageStatus? Status { get; set; }

        public int? AttemptsLessThan { get; set; }
    }
}