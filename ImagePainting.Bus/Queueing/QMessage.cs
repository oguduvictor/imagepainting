﻿using System;

namespace ImagePainting.ServiceBus.Queueing
{
    internal class QMessage
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string MessageTypeName { get; set; }
        public string MessageAssemblyName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string CreatedById { get; set; }
        public int Attempts { get; set; }
        public DateTime? LastAttemptAt { get; set; }
        public string ErrorMessage { get; set; }
       
        public MessageStatus Status { get; set; }

        internal enum MessageStatus
        {
            Unknown = 0,
            Queued = 1,
            Processing = 2,
            Completed = 3,
            Failed = 4
        }
    }
}
