﻿using ImagePainting.ServiceBus.Queueing;
using ImagePainting.Site.Common.Helpers;
using ImagePainting.Site.Common.ServiceBus;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ImagePainting.ServiceBus
{
    internal class QueueManager : IQueueManager
    {
        private readonly IQMessageRepository _qMessageRepository;
        private readonly ConcurrentBag<IHandler<IMessage>> _registeredHandlers;

        private bool _started = false;
        private const int ExecutionInterval = 2000; // milliseconds

        public QueueManager(IQMessageRepository qMessageRepository)
        {
            _qMessageRepository = qMessageRepository;
            _registeredHandlers = new ConcurrentBag<IHandler<IMessage>>();
        }

        public Task SendAsync(IMessage message)
        {
            var messageType = message.GetType();
            var qMessage = new QMessage
            {
                Id = Guid.NewGuid(),
                Message = message.ToJson(),
                Attempts = 0,
                Created = DateTime.UtcNow,
                Modified = DateTime.UtcNow,
                CreatedById = message.UserId,
                MessageTypeName = messageType.FullName,
                MessageAssemblyName = messageType.Assembly.FullName,
                Status = QMessage.MessageStatus.Queued
            };

           return _qMessageRepository.Add(qMessage);
        }

        public async Task StartAsync()
        {
            _started = true;

            while(_started)
            {
                var query = new QMessageQuery { AttemptsLessThan = 5 };
                var messages = await _qMessageRepository.Search(query);

                foreach (var qMessage in messages)
                {
                    await ProcessMessage(qMessage);
                }

                Thread.Sleep(ExecutionInterval);
            }
        }

        public void Stop()
        {
            _started = false;
        }

        public void RegisterHandler(IHandler<IMessage> handler)
        {
            if (!_registeredHandlers.Any(x => x == handler))
            {
                _registeredHandlers.Add(handler);
            }
        }

        private Task ProcessMessage(QMessage qMessage)
        {
            var type = Type.GetType($"{qMessage.MessageTypeName}, {qMessage.MessageAssemblyName}");
            var message = qMessage.Message.FromJson(type) as IMessage;

            foreach (var handler in _registeredHandlers)
            {
                try
                {
                    handler.HandleAsync(message).GetAwaiter().GetResult();
                    Dequeue(qMessage).GetAwaiter().GetResult();
                }
                catch (Exception ex)
                {
                    RequeueMessage(qMessage, ex.GetBaseException().Message).GetAwaiter().GetResult();
                }
            }

            return Task.CompletedTask;
        }

        private Task RequeueMessage(QMessage qMessage, string errorMessage = null)
        {
            qMessage.ErrorMessage = errorMessage;
            qMessage.LastAttemptAt = DateTime.UtcNow;
            qMessage.Modified = DateTime.UtcNow;
            qMessage.Status = QMessage.MessageStatus.Queued;
            qMessage.Attempts++;

            return _qMessageRepository.Update(qMessage);
        }

        private Task Dequeue(QMessage qMessage)
        {
            try
            {
                return _qMessageRepository.Delete(qMessage.Id);
            }
            catch
            {
                return Task.CompletedTask;
                // TODO: handle
            }
        }
    }
}
