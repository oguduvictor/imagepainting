﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImagePainting.ServiceBus.Queueing
{
    internal interface IQMessageRepository
    {
        Task Add(QMessage message);

        Task Update(QMessage message);

        Task Delete(Guid messageId);

        Task<IList<QMessage>> Search(QMessageQuery searchCriteria);
    }
}
