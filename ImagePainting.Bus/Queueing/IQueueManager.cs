﻿using System.Threading.Tasks;
using ImagePainting.Site.Common.ServiceBus;

namespace ImagePainting.ServiceBus
{
    public interface IQueueManager
    {
        void RegisterHandler(IHandler<IMessage> handler);
        Task SendAsync(IMessage message);
        Task StartAsync();
        void Stop();
    }
}