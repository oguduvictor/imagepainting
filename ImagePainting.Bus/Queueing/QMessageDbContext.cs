﻿using System.Data.Entity;

namespace ImagePainting.ServiceBus.Queueing
{
    internal class QMessageDbContext : DbContext
    {
        static QMessageDbContext()
        {
            Database.SetInitializer<QMessageDbContext>(null);
        }

        public QMessageDbContext()
            : base("Name=PaintDbConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<QMessage> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QMessage>().ToTable("QMessages");
        }
    }
}
