﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.ServiceBus.Queueing
{
    internal class QMessageRepository : IQMessageRepository
    {
        private readonly QMessageDbContext _dbContext;

        public QMessageRepository(QMessageDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task Add(QMessage message)
        {
            _dbContext.Messages.Add(message);

            return _dbContext.SaveChangesAsync();
        }

        public Task Delete(Guid messageId)
        {
            var message = _dbContext.Messages.Local.FirstOrDefault(x => x.Id == messageId);

            if (message == null)
            {
                message = new QMessage { Id = messageId };
                _dbContext.Messages.Attach(message);
            }

            _dbContext.Messages.Remove(message);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<QMessage>> Search(QMessageQuery searchCriteria)
        {
            var query = _dbContext.Messages.AsNoTracking().AsQueryable();

            if (searchCriteria.Status.HasValue)
            {
                query = query.Where(x => x.Status == searchCriteria.Status);
            }

            if (searchCriteria.AttemptsLessThan.HasValue)
            {
                query = query.Where(x => x.Attempts < searchCriteria.AttemptsLessThan.Value);
            }

            return await query.OrderBy(x => x.Created).ToListAsync();
        }

        public Task Update(QMessage message)
        {
            var dbMessage = _dbContext.Messages.Local.FirstOrDefault(x => x.Id == message.Id);

            if (dbMessage == null)
            {
                dbMessage = message;
                _dbContext.Messages.Attach(message);
            }
            else
            {
                dbMessage.Attempts = message.Attempts;
                dbMessage.ErrorMessage = message.ErrorMessage;
                dbMessage.LastAttemptAt = message.LastAttemptAt;
                dbMessage.Modified = message.Modified;
                dbMessage.Status = message.Status;
            }

            _dbContext.Entry(dbMessage).State = EntityState.Modified;

            return _dbContext.SaveChangesAsync();
        }
    }
}
