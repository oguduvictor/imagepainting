﻿using ImagePainting.Site.Common.ServiceBus;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace ImagePainting.ServiceBus
{
    public class MessageBus : IBus, IBusControl, IHandler<IMessage>, IDisposable
    {
        private ConcurrentDictionary<string, ConcurrentBag<Type>> _subscriptions;
        protected bool _disposed;
        private readonly IQueueManager _queueManager;

        public MessageBus(IQueueManager queueManager)
        {
            _queueManager = queueManager;
            _subscriptions = new ConcurrentDictionary<string, ConcurrentBag<Type>>();
        }

        public virtual Task PublishAsync(IMessage message) 
        {
            return _queueManager.SendAsync(message);
        }

        public virtual void Register<TMessage, THandler>()
            where TMessage : IMessage
            where THandler : IHandler<TMessage>
        {
            Register(typeof(TMessage), typeof(THandler));
        }

        public virtual void Register(Type messageType, Type handlerType)
        {
            var messageTypeName = messageType.FullName;
            var handlers = default(ConcurrentBag<Type>);

            _subscriptions.TryGetValue(messageTypeName, out handlers);

            if (handlers == null)
            {
                handlers = new ConcurrentBag<Type>();
                _subscriptions.TryAdd(messageTypeName, handlers);
            }

            var existingHandler = default(Type);

            if (!handlers.TryPeek(out existingHandler))
            {
                handlers.Add(handlerType);
            }
        }

        public virtual void RegisterHandler<THandler>()
        {
            var handlerType = typeof(THandler);
            var interfaces = handlerType.GetInterfaces();
            var handlerInterfaceName1 = "IHandler`1";

            foreach (var type in interfaces)
            {
                if (type.Name != handlerInterfaceName1)
                {
                    continue;
                }

                var messageType = type.GetGenericArguments().FirstOrDefault();
                var messageInterfaceType = messageType.GetInterface(nameof(IMessage));

                if (messageType != null && messageInterfaceType != null)
                {
                    Register(messageType, handlerType);
                }
            }
        }

        public virtual void UnRegister<TMessage, THandler>()
            where TMessage : IMessage
            where THandler : IHandler<TMessage>
        {
            var messageType = typeof(TMessage);
            var handlerType = typeof(THandler);
            var handlerTypes = GetMessageHandlerTypes(messageType);

            handlerTypes.TryTake(out handlerType);
        }

        public virtual Task SendAsync(IMessage message) 
        {
            return _queueManager.SendAsync(message);
        }

        /// <summary>
        /// Callback for a message broker
        /// </summary>
        /// <param name="message"></param>
        public async Task HandleAsync(IMessage message)
        {
            var handlerTypes = GetMessageHandlerTypes(message);

            foreach (var handlerType in handlerTypes)
            {
                await ExecuteHandler(handlerType, message);
            }
        }

        public void Start()
        {
            StartAsync().GetAwaiter().GetResult();
        }

        public Task StartAsync()
        {
            _queueManager.RegisterHandler(this);
            return _queueManager.StartAsync();
        }

        public void Stop()
        {
            _queueManager.Stop();
        }

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            Dispose(true);

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_disposed)
            {
                _subscriptions = null;
            }
        }

        protected virtual Task ExecuteHandler(Type handlerType, IMessage message)
        {
            throw new NotImplementedException();
        }

        private ConcurrentBag<Type> GetMessageHandlerTypes(IMessage message)
        {
            var messageType = message.GetType();

            return GetMessageHandlerTypes(messageType);
        }

        private ConcurrentBag<Type> GetMessageHandlerTypes(Type messageType)
        {
            var handlers = default(ConcurrentBag<Type>);

            _subscriptions.TryGetValue(messageType.FullName, out handlers);

            if (handlers == null)
            {
                return new ConcurrentBag<Type>();
            }

            return handlers;
        }
    }
}
