﻿using ImagePainting.ServiceBus.Queueing;
using ImagePainting.Site.Common.ServiceBus;
using LightInject;

namespace ImagePainting.ServiceBus
{
    public class ServiceBusIoCModule : ICompositionRoot
    {
        public void Compose(IServiceRegistry serviceRegistry)
        {
            serviceRegistry.Register<QMessageDbContext>(new PerScopeLifetime());
            serviceRegistry.Register<IQMessageRepository, QMessageRepository>(new PerScopeLifetime());
            serviceRegistry.Register<IQueueManager, QueueManager>(new PerScopeLifetime());
            serviceRegistry.Register<IBus, MessageBus>(new PerScopeLifetime());
            //serviceRegistry.Register<IBusControl, MessageBus>(new PerContainerLifetime());
        }
    }
}
